<?php
/**
 * 下载页面
 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class DownController extends HomebaseController {

	function index(){       
		return $this->fetch();
	}

	// 获取安卓和ios版本信息
	public function getversioninfo(){
        header('content-type:application:json;charset=utf8');
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:*');
        header('Access-Control-Allow-Headers:x-requested-with,content-type');
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $data['apk_ver'] = $this->configpub['apk_ver'];
        $data['apk_url'] = $this->configpub['apk_url'];
        $data['ipa_ver'] = isset($this->configpub['ipa_ver'])?$this->configpub['ipa_ver']:'';
        $data['ios_shelves'] = $this->configpub['ios_shelves'];
        $data['ipa_type'] = $this->configpub['ipa_type'];
        switch ($this->configpub['ipa_type']){
            case 1:
                $ipa_type = 'TF签';
                $ipa_url = $this->configpub['tf_ipa_url'];
                break;
            case 2:
                $ipa_type = '企业签';
                $ipa_url = $this->configpub['c_ipa_url'];
                break;
            case 3:
                $ipa_type = '超级签';
                $ipa_url = $this->configpub['super_ipa_url'];
                break;
            default:
                $ipa_type = 'TF签';
                $ipa_url = $this->configpub['tf_ipa_url'];
        }
        $data['ipa_type_text'] = $ipa_type;
        $data['ipa_url'] = $ipa_url;
        $rs['info'][0]=$data;

        return json_encode($rs);
    }

}