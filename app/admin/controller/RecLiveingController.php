<?php

/**
 * 直播列表
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class RecLiveingController extends AdminbaseController {
    protected function getLiveClass(){

        $liveclass=Db::name("live_class")->order('list_order asc, id desc')->column('id,name');

        return $liveclass;
    }
    
    protected function getTypes($k=''){
        $type=[
            '0'=>'普通房间',
            '1'=>'密码房间',
            '2'=>'门票房间',
            '3'=>'计时房间',
        ];
        
        if($k==''){
            return $type;
        }
        return $type[$k];
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        $map[]=['islive','=',1];
        $map[]=['isrecommend','=',1];
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';

        if($start_time!=""){
           $map[]=['starttime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['starttime','<=',strtotime($end_time) + 60*60*24];
        }
  
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $lianguid=getLianguser($uid);
            if($lianguid){
                $map[]=['uid',['=',$uid],['in',$lianguid],'or'];
            }else{
                $map[]=['uid','=',$uid];
            }
        }

        $this->configpri=getConfigPri();

        $lists = Db::name("live")
                ->where($map)
                ->order("sort ASC")
                ->paginate(30);
 
        $lists->each(function($v,$k){

             $v['userinfo']=getUserInfo($v['uid']);
             $where=[];
             $where['action']=1;
             $where['touid']=$v['uid'];
             $where['showid']=$v['showid'];
             /* 本场总收益 */
             $totalcoin=Db::name("user_coinrecord")->where($where)->sum('totalcoin');
             if(!$totalcoin){
                $totalcoin=0;
             }
             /* 送礼物总人数 */
             $total_nums=Db::name("user_coinrecord")->where($where)->group("uid")->count();
             if(!$total_nums){
                $total_nums=0;
             }
             /* 人均 */
             $total_average=0;
             if($totalcoin && $total_nums){
                $total_average=round($totalcoin/$total_nums,2);
             }
             
             /* 人数 */
            $nums=zSize('user_'.$v['stream']);
            
            $v['totalcoin']=$totalcoin;
            $v['total_nums']=$total_nums;
            $v['total_average']=$total_average;
            $v['nums']=$nums;
            
            if($v['isvideo']==0 && $this->configpri['cdn_switch']!=5){
                $v['pull']=PrivateKeyA('rtmp',$v['stream'],0);
            }
                
            return $v;           
        });
      
        $lists->appends($data);
        $arr[]= '';
        $page = $lists->render();
        $where[]=['islive','=',1];
        $where[]=['isrecommend','=',0];
        foreach ($lists as $key => $val){
            $arr[$key] =$val;
        }


        if(count($arr)<5){
            
             $list = Db::name("live")
                ->where($where)
                ->order("livenum desc")
                ->limit(5)
                ->select()
                ->toArray();
            foreach ($list as $k=> $v){
            
                $v['userinfo']=getUserInfo($v['uid']);
                $nums=zSize('user_'.$v['stream']);
                $v['totalcoin']=0;
                $v['total_nums']=0;
                $v['total_average']=0;
                $v['nums']=$nums;
                
                if($v['isvideo']==0 && $this->configpri['cdn_switch']!=5){
                    $v['pull']=PrivateKeyA('rtmp',$v['stream'],0);
                }
               $list[$k] = $v; 
            }

            $sort=array_column($list,"sort");
			array_multisort($sort, SORT_ASC, $list);
            $lists = array_filter(array_merge($arr,$list));
            $lists =array_slice($lists,0,5);
        }

        $liveclass=$this->getLiveClass();
        $liveclass[0]='默认分类';

  
        $this->assign('lists', $lists);

        $this->assign("page", $page);
        
        $this->assign("liveclass", $liveclass);
        
        $this->assign("type", $this->getTypes());
        
        return $this->fetch();

    }

    function del(){
        
        $uid = $this->request->param('uid', 0, 'intval');
     
        $rs = Db::name('live')->where(['uid' =>$uid])->update(array('isrecommend' =>0));
       
        if(!$rs){
            $this->error("删除失败！");
        }
		$this->resetCache();
		$action="直播管理-直播列表取消推荐直播UID：".$uid;
		setAdminLog($action);
        
        $this->success("删除成功！");
            
    }
    
    function add(){
        
        $this->assign("liveclass", $this->getLiveClass());
        
        $this->assign("type", $this->getTypes());
        
        return $this->fetch();
    }
    
    
    
    function edit(){
        $uid   = $this->request->param('uid', 0, 'intval');
        
        $data=Db::name('live')
            ->where("uid={$uid}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        
        $this->assign("liveclass", $this->getLiveClass());
        
        $this->assign("type", $this->getTypes());
        
        return $this->fetch();


    }
    public function editOrder()
    {
        $ids = $this->request->post("cert_scores/a"); 
        if (!empty($ids)) {
            foreach ($ids as $key => $r) {
                $data['sort'] = $r;
                Db::name('live')->where(['uid' =>intval($key)])->update($data);
            }
        }
      
        $this->resetCache();
        
		
		$action="直播列表排序 ";
		setAdminLog($action);
        $this->success("排序更新成功！");
    }
    
   function resetCache(){
        $key='comliveindex';
         delcache($key);
        return 1;
    }
        
}
