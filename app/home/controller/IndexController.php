<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace app\home\controller;

use cmf\controller\HomeBaseController;
use think\Db;
/**
 * 首页
 */
class IndexController extends HomebaseController {
	
    //首页
	public function index() {

		$this->assign("current",'index');
        //$data = [];
		$key = "comlive".__FUNCTION__ ;
		$data = getcaches($key);
		if(empty($data)){
		 /*获取推荐播放列表(正在直播，推荐，按粉丝数排序)*/
			 $where = "l.islive=1";
			 $list = Db::name("live")->alias('l')
				->leftJoin([['user u', 'l.uid=u.id']])
				->field('l.title,l.uid,l.thumb,l.stream,l.type,l.islive,l.isvideo,l.pull,l.liveclassid,u.avatar,u.user_nicename,avatar_thumb,u.cert_score,u.signature,u.ishot,u.isrecommend')
				->where($where)
				->order("u.cert_score desc")
				->limit(30)
				->select()
				->toArray();
				
			$config = cmf_get_option('site_info');
			//直播分类
			$classdata=Db::name("live_class")
					->field("id,pid,name")
					->order("list_order desc")
					->select()
					->toArray();
					
			foreach ($list as $k => $v){
                $userinfo=getUserInfo($v['uid']);
                $v['avatar']=$userinfo['avatar'];
                $v['avatar_thumb']=$userinfo['avatar_thumb'];
				$nums = zSize('user_'.$v['stream']);
				$v['nums'] = $nums;
				if($v['thumb']=="")
				{
					$v['thumb']=$v['avatar'];
				} 
				if($v['isvideo']==0){
					if($this->configpri['cdn_switch']!=5){
						$v['pull']=PrivateKeyA('http',$v['stream'].'.flv',0);
					}
				}
				$v['cert_tip'] = $v['hot_tip'] = '';
				$v['isrecommend'] = ($v['cert_score']>=$config['cert_score_tip']);
				$v['ishot'] = ($nums >= $config['hot_score_tip']);
				//$v['fans_nums']=Db::name("user_attention")->where("touid={$v['uid']}")->count();
				$list[$k]=$v;
			 }
			
				$sort=array_column($list,"cert_score");
				$sort1=array_column($list,"nums");
				array_multisort($sort, SORT_DESC,$sort1,SORT_DESC, $list);
			 //大屏推荐数据	
	 
            $where = "l.islive=1 and l.isrecommend=1";
 			$recscreen = Db::name("live")->alias('l')
				->leftJoin([['user u', 'l.uid=u.id']])
				->field('l.title,l.uid,l.thumb,l.stream,l.type,l.islive,l.isvideo,l.pull,l.liveclassid,u.avatar,u.user_nicename,avatar_thumb,u.cert_score,u.signature,u.ishot,u.isrecommend')
				->where($where)
				->order("l.sort ASC")
				->limit(5)
				->select()
				->toArray();
            $lists = $list;
            foreach ($recscreen as $k=> $v){
                
                $userinfo=getUserInfo($v['uid']);
                $v['avatar']=$userinfo['avatar'];
                $v['avatar_thumb']=$userinfo['avatar_thumb'];
                $nums=zSize('user_'.$v['stream']);
                $v['nums']=$nums;
                if($v['thumb']=="")
    			{
    				$v['thumb']=$v['avatar'];
    			} 
                if($v['isvideo']==0 && $this->configpri['cdn_switch']!=5){
                    $v['pull']=PrivateKeyA('rtmp',$v['stream'],0);
                }
                $v['cert_tip'] = $v['hot_tip'] = '';
				$v['isrecommend'] = ($v['cert_score']>=$config['cert_score_tip']);
				$v['ishot'] = ($nums >= $config['hot_score_tip']);
				
				foreach($lists as $key=> $val){
				   if($v['uid'] ==$val['uid']){
				       unset($lists[$key]);//过滤掉重复的数据
				   }
				}
               $recscreen[$k] = $v; 
            }
         
            if(count($recscreen)<5){
              
                $sort1=array_column($lists,"nums");
                array_multisort($sort1,SORT_DESC, $lists);
                $recscreen = array_filter(array_merge($recscreen,$lists));
            }					
				//特别推荐数据取七个
				$recommenddata = [];
				$recommenddata=array_slice($list,0,7);
				//重新组合数据
			   $indexdata = [];
			   foreach($classdata as $key => $val){
					$indexdata[$key]['id'] =  $val['id'];
					$indexdata[$key]['name'] =  $val['name'];
					$indexdata[$key]['pid'] =  $val['pid'];
				   foreach($list as $k =>$v){
					if($val['id']==$v['liveclassid']){
						$indexdata[$key]['list'][] =  $v;
					}
				   }
				}
		   
				/* 轮播 */
			$slide=Db::name("slide_item")->where("status='1' and slide_id='1'")->order("list_order asc")->select()->toArray();
			foreach($slide as $k=>$v){
				$v['image']=get_upload_path($v['image']);
				$slide[$k]=$v;
			}
			

			$data['slide'] = $slide;
			$data['indexdata'] = $indexdata;
			$data['recommend'] = $recommenddata;
			$data['recscreen'] = $recscreen;
			
			setcaches($key,$data,10);
	   }
		
		$this->assign("slide",$data['slide']);
		$this->assign("recscreen",$recscreen);
		$this->assign("indexdata",	$data['indexdata']);
		$this->assign("recommend",	$data['recommend']);

    	return $this->fetch();
    }	
	
	public function translate()
	{
        
        $this->assign("current",'');
        
        $keyword = $this->request->param('keyword');
        
		$keyword=checkNull($keyword);
        
		$pagesize = 18; 
		if($keyword=="")
		{
            $data = $this->request->param();
			$lists=Db::name("live")
					->field('uid,stream,title,city,islive')
					->where('islive=1')
					->order("starttime desc")
					->paginate($pagesize);
            $lists->each(function($v,$k){
                $userinfo=getUserInfo($v['uid']);
                
                $v['user_nicename']=$userinfo['user_nicename'];
                $v['avatar']=$userinfo['avatar'];
                return $v;
            });
            
            $lists->appends($data);
            $page = $lists->render();
            
			$msg["info"]='抱歉,没有找到关于"';
			$msg["name"]='';
			$msg["result"]='"的搜索结果';
			$msg["type"]='0';
		}else{
            $data = $this->request->param();
            $where1=[
                ['user_type','=',2],
                ['id','=',$keyword]
            ];
            $where2=[
                ['user_type','=',2],
                ['user_nicename','like','%'.$keyword.'%'],
            ];
            
			$count= Db::name('user')->whereor([$where1,$where2])->count();
            
            $lists = Db::name("user")
                ->whereor([$where1,$where2])
                ->order("consumption DESC")
                ->paginate($pagesize);
                
            $lists->each(function($v,$k){
                $v['avatar']=get_upload_path($v['avatar']);
                $v['islive']='0';
                $v['title']='';
                return $v;
            });
                
            $lists->appends($data);
            $page = $lists->render();

            
			$msg["info"]='共找到'.$count.'个关于"';
			$msg["name"]=$keyword;
			$msg["result"]='"的搜索结果';
			$msg["type"]='1';
		}
        
		$this->assign('lists',$lists);
		$this->assign('msg',$msg);
		$this->assign('page',$page);
		$this->assign('keyword',$keyword);
        
		return $this->fetch();
	}	
    
    /* 图片裁剪 */
    function cutImg(){
        
        $data = $this->request->param();
        $filepath=isset($data['filepath']) ? $data['filepath']: '';
        $filepath=checkNull($filepath);
        
        $width=isset($data['width']) ? $data['width']: '';
        $new_width=checkNull($width);
        
        $height=isset($data['height']) ? $data['height']: '';
        $new_height=checkNull($height);
        
        $source_info   = getimagesize($filepath);
        $source_width  = $source_info[0];
        $source_height = $source_info[1];
        $source_mime   = $source_info['mime'];
        $source_ratio  = $source_height / $source_width;
        $target_ratio  = $new_height / $new_width;
        // 源图过高
        if ($source_ratio > $target_ratio){

            $cropped_width  = $source_width;
            $cropped_height = $source_width * $target_ratio;
            $source_x = 0;
            $source_y = ($source_height - $cropped_height) / 2;
        }
        // 源图过宽
        elseif ($source_ratio < $target_ratio){
        	
            $cropped_width  = $source_height / $target_ratio;
            $cropped_height = $source_height;
            $source_x = ($source_width - $cropped_width) / 2;
            $source_y = 0;
        }
        // 源图适中
        else{

            $cropped_width  = $source_width;
            $cropped_height = $source_height;
            $source_x = 0;
            $source_y = 0;
        }

        switch ($source_mime){
            case 'image/gif':
                $source_image = imagecreatefromgif($filepath);
                break;
            case 'image/jpeg':
                $source_image = imagecreatefromjpeg($filepath);
                break;
            case 'image/png':
                $source_image = imagecreatefrompng($filepath);
                break;
            default:
                return false;
            break;
        }

        $target_image  = imagecreatetruecolor($new_width, $new_height);
        $cropped_image = imagecreatetruecolor($cropped_width, $cropped_height);
        // 裁剪
        imagecopy($cropped_image, $source_image, 0, 0, $source_x, $source_y, $cropped_width, $cropped_height);
        // 缩放
        imagecopyresampled($target_image, $cropped_image, 0, 0, 0, 0, $new_width, $new_height, $cropped_width, $cropped_height);
        header('Content-Type: image/jpeg');
        imagejpeg($target_image);
        imagedestroy($source_image);
        imagedestroy($target_image);
        imagedestroy($cropped_image);
    }

}


