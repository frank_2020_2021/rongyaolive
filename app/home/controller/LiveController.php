<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2014 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace app\home\controller;

use cmf\controller\HomeBaseController;
use EasyWeChat\ShakeAround\Page;
use PhpMyAdmin\Rte\General;
use think\Db;
/**
 * 直播
 */
class LiveController extends HomebaseController {
	
    //首页
	public function index() {
        $this->assign("current",'live');
        $data = $this->request->param();
        $pid=isset($data['pid']) ? $data['pid']: '';
        $page=isset($data['page']) ? $data['page']: 1;

        $key = "live_".__FUNCTION__."_pid".$pid."_page".$page;
        $historyData = getcaches($key);
        if(empty($historyData)){

            $map=[];
            $map[]=['l.islive','=',1];

            if($pid != ""){
                $map[]=['l.liveclassid','=',$pid];
            }
            // 获取直播分类
            $liveClass=Db::name("live_class")->where("pid=0")->order("list_order desc")->select()->toArray();

            // 获取总数
            $count=Db::name("live ")->alias('l')->leftJoin([['user u', 'u.id=l.uid']])->where($map)->count();
            /* 在线直播，根据推荐值》在线人数排序） */
            $live=Db::name("live ")->alias('l')
                ->leftJoin([['user u', 'u.id=l.uid']])
                ->field("l.title,l.uid,l.thumb,l.stream,l.type,l.islive,u.cert_score")
                ->where($map)->select()->toArray();

            $dataconfig = cmf_get_option('site_info');
            foreach ($live as $k=>$v){
                $userinfo=getUserInfo($v['uid']);

                $v['avatar']=$userinfo['avatar'];
                $v['avatar_thumb']=$userinfo['avatar_thumb'];
                $v['user_nicename']=$userinfo['user_nicename'];
                if($v['thumb']=="")
                {
                    $v['thumb']=$v['avatar'];
                }
                $nums=zSize('user_'.$v['stream']);
                $v['nums']=$nums;
                // 推荐标识和热门标识
                $cert_score_tip = $dataconfig['cert_score_tip'] ? $dataconfig['cert_score_tip'] : 0;
                $hot_score_tip = $dataconfig['hot_score_tip'] ? $dataconfig['hot_score_tip'] : 0;
                $v['is_recommend'] = 0;
                if($v['cert_score'] >= $cert_score_tip){
                    $v['is_recommend'] = 1;
                }
                $v['is_hot'] = 0;
                if($v['nums'] >= $hot_score_tip){
                    $v['is_hot'] = 1;
                }
                $live[$k]=$v;
            }

            if($live){
                $sort=array_column($live,"cert_score");
                $sort1=array_column($live,"nums");
                array_multisort($sort, SORT_DESC,$sort1,SORT_DESC,$live);
            }

            $historyData['liveClass'] = $liveClass;
            $historyData['live'] = $live;
            $historyData['count'] = $count;
            setcaches($key,$historyData,10);
        }
        $this->assign("liveClass",$historyData['liveClass']);
        $this->assign('live', $historyData['live']);
        $this->assign("count", $historyData['count']);
        $this->assign("page", $page);
        $this->assign("pagesize", 12);
        $this->assign("pid", $pid);
    	return $this->fetch();
    }	

}


