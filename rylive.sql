/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : rylive

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 07/02/2021 17:34:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cmf_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `cmf_admin_log`;
CREATE TABLE `cmf_admin_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminid` int(11) NOT NULL COMMENT '管理员ID',
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '管理员',
  `action` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '操作内容',
  `ip` bigint(20) NOT NULL COMMENT 'IP地址',
  `addtime` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 638 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_admin_log
-- ----------------------------
INSERT INTO `cmf_admin_log` VALUES (1, 1, 'admin', '管理组-管理员修改个人信息ID: 1', 2047485572, 1604059201);
INSERT INTO `cmf_admin_log` VALUES (2, 1, 'admin', '修改文件存储 存储类型 本地 ', 2047485572, 1604059522);
INSERT INTO `cmf_admin_log` VALUES (3, 1, 'admin', '修改文件存储 ', 2047485572, 1604059544);
INSERT INTO `cmf_admin_log` VALUES (4, 1, 'admin', '修私密配置 腾讯云直播推流域名 push.jiextx.com 腾讯云直播播流域名 play.jiextx.com ', 2047485572, 1604060237);
INSERT INTO `cmf_admin_log` VALUES (5, 1, 'admin', '设置推荐会员：42488', 1892640744, 1604060277);
INSERT INTO `cmf_admin_log` VALUES (6, 1, 'admin', '设置超管会员：42488', 1892640744, 1604060628);
INSERT INTO `cmf_admin_log` VALUES (7, 1, 'admin', '监控 关闭直播间：42488', 1892640744, 1604060769);
INSERT INTO `cmf_admin_log` VALUES (8, 1, 'admin', '修改文件存储 存储类型 七牛云存储 ', 2047485572, 1604061005);
INSERT INTO `cmf_admin_log` VALUES (9, 1, 'admin', '修改文件存储 存储类型 本地 ', 2047485572, 1604063491);
INSERT INTO `cmf_admin_log` VALUES (10, 1, 'admin', '修改文件存储 存储类型 七牛云存储 ', 2047485572, 1604063515);
INSERT INTO `cmf_admin_log` VALUES (11, 1, 'admin', '修改文件存储 存储类型 本地 ', 2047485572, 1604064787);
INSERT INTO `cmf_admin_log` VALUES (12, 1, 'admin', '修改文件存储 ', 2047485572, 1604065215);
INSERT INTO `cmf_admin_log` VALUES (13, 1, 'admin', '修改文件存储 ', 2047485572, 1604065455);
INSERT INTO `cmf_admin_log` VALUES (14, 1, 'admin', '修改文件存储 存储类型 七牛云存储 ', 2047485572, 1604119198);
INSERT INTO `cmf_admin_log` VALUES (15, 1, 'admin', '修改直播分类：8', 2047485572, 1604119211);
INSERT INTO `cmf_admin_log` VALUES (16, 1, 'admin', '修改直播分类：3', 2047485572, 1604119230);
INSERT INTO `cmf_admin_log` VALUES (17, 1, 'admin', '修改直播分类：4', 2047485572, 1604119519);
INSERT INTO `cmf_admin_log` VALUES (18, 1, 'admin', '修私密配置 聊天服务器带端口 http://www.jiextx.com:19967 ', 2047485572, 1604124194);
INSERT INTO `cmf_admin_log` VALUES (19, 1, 'admin', '修私密配置 聊天服务器带端口 http://jiextx.com:19967 ', 2047485572, 1604124602);
INSERT INTO `cmf_admin_log` VALUES (20, 1, 'admin', '修改公共配置 网站域名 http://jiextx.com ', 2047485572, 1604124615);
INSERT INTO `cmf_admin_log` VALUES (21, 1, 'admin', '修私密配置 短信接口平台 容联云 ', 2047485572, 1604126957);
INSERT INTO `cmf_admin_log` VALUES (22, 1, 'admin', '监控 关闭直播间：42488', 2047485572, 1604129161);
INSERT INTO `cmf_admin_log` VALUES (23, 1, 'admin', '直播管理-直播列表添加UID：1', 2047485572, 1604129295);
INSERT INTO `cmf_admin_log` VALUES (24, 1, 'admin', '直播管理-直播列表修改UID：1', 2047485572, 1604129593);
INSERT INTO `cmf_admin_log` VALUES (25, 1, 'admin', '直播管理-直播列表修改UID：1', 2047485572, 1604133035);
INSERT INTO `cmf_admin_log` VALUES (26, 1, 'admin', '编辑页面管理ID: 44', 2047485572, 1604133744);
INSERT INTO `cmf_admin_log` VALUES (27, 1, 'admin', '编辑登录奖励：1', 2047485572, 1604133749);
INSERT INTO `cmf_admin_log` VALUES (28, 1, 'admin', '修改会员等级：1', 2047485572, 1604133760);
INSERT INTO `cmf_admin_log` VALUES (29, 1, 'admin', '修改VIP：3', 2047485572, 1604133773);
INSERT INTO `cmf_admin_log` VALUES (30, 1, 'admin', '修私密配置 聊天服务器带端口 http://jiextx.com:80 ', 2047485572, 1604139764);
INSERT INTO `cmf_admin_log` VALUES (31, 1, 'admin', '更新直播分类排序', 3032456753, 1604164092);
INSERT INTO `cmf_admin_log` VALUES (32, 1, 'admin', '添加直播分类：13', 3032456753, 1604164155);
INSERT INTO `cmf_admin_log` VALUES (33, 1, 'admin', '更新直播分类排序', 3032456753, 1604164162);
INSERT INTO `cmf_admin_log` VALUES (34, 1, 'admin', '添加会员：42489', 3032456753, 1604164493);
INSERT INTO `cmf_admin_log` VALUES (35, 1, 'admin', '直播管理-直播列表添加UID：42489', 3032456753, 1604164526);
INSERT INTO `cmf_admin_log` VALUES (36, 1, 'admin', '添加会员：42490', 3032456753, 1604164950);
INSERT INTO `cmf_admin_log` VALUES (37, 1, 'admin', '添加视频：1', 3032456753, 1604166671);
INSERT INTO `cmf_admin_log` VALUES (38, 1, 'admin', '直播管理-直播列表修改UID：42489', 3032456753, 1604166828);
INSERT INTO `cmf_admin_log` VALUES (39, 1, 'admin', '直播管理-直播列表修改UID：42489', 3032456753, 1604166969);
INSERT INTO `cmf_admin_log` VALUES (40, 1, 'admin', '直播管理-直播列表添加UID：42490', 3032456753, 1604290600);
INSERT INTO `cmf_admin_log` VALUES (41, 1, 'admin', '管理组-添加角色ID: 7', 1892640744, 1604292095);
INSERT INTO `cmf_admin_log` VALUES (42, 1, 'admin', '管理组-授权角色权限ID: 7', 1892640744, 1604292133);
INSERT INTO `cmf_admin_log` VALUES (43, 1, 'admin', '管理组-添加管理员ID: 42491', 1892640744, 1604292256);
INSERT INTO `cmf_admin_log` VALUES (44, 42491, 'regame', '禁用会员：42492', 1892640744, 1604293176);
INSERT INTO `cmf_admin_log` VALUES (45, 42491, 'regame', '禁用会员：42492', 1892640744, 1604293639);
INSERT INTO `cmf_admin_log` VALUES (46, 42491, 'regame', '启用会员：42492', 1892640744, 1604293645);
INSERT INTO `cmf_admin_log` VALUES (47, 42491, 'regame', '禁用会员：42492', 1892640744, 1604293658);
INSERT INTO `cmf_admin_log` VALUES (48, 42491, 'regame', '设置超管会员：42492', 1892640744, 1604293666);
INSERT INTO `cmf_admin_log` VALUES (49, 1, 'admin', '禁用会员：42487', 2047485572, 1604293814);
INSERT INTO `cmf_admin_log` VALUES (50, 1, 'admin', '启用会员：42487', 2047485572, 1604293843);
INSERT INTO `cmf_admin_log` VALUES (51, 1, 'admin', '禁用会员：42492', 2047485572, 1604293858);
INSERT INTO `cmf_admin_log` VALUES (52, 42491, 'regame', '修改会员信息：42492', 1892640744, 1604293894);
INSERT INTO `cmf_admin_log` VALUES (53, 42491, 'regame', '修改会员信息：42492', 1892640744, 1604293899);
INSERT INTO `cmf_admin_log` VALUES (54, 42491, 'regame', '启用会员：42492', 1892640744, 1604293926);
INSERT INTO `cmf_admin_log` VALUES (55, 1, 'admin', '启用会员：42492', 2047485572, 1604293937);
INSERT INTO `cmf_admin_log` VALUES (56, 42491, 'regame', '禁用会员：42492', 1892640744, 1604293950);
INSERT INTO `cmf_admin_log` VALUES (57, 1, 'admin', '禁用会员：42493', 2047485572, 1604294047);
INSERT INTO `cmf_admin_log` VALUES (58, 1, 'admin', '禁用会员：42493', 2047485572, 1604294093);
INSERT INTO `cmf_admin_log` VALUES (59, 1, 'admin', '启用会员：42493', 2047485572, 1604294100);
INSERT INTO `cmf_admin_log` VALUES (60, 42491, 'regame', '设置推荐会员：42492', 1892640744, 1604294255);
INSERT INTO `cmf_admin_log` VALUES (61, 1, 'admin', '管理组-添加管理员ID: 42494', 1892640702, 1604294265);
INSERT INTO `cmf_admin_log` VALUES (62, 42491, 'regame', '开启僵尸粉会员：42495', 1892640744, 1604294531);
INSERT INTO `cmf_admin_log` VALUES (63, 42494, 'messi', '添加付费内容分类列表ID: 1', 1892640702, 1604294544);
INSERT INTO `cmf_admin_log` VALUES (64, 42491, 'regame', '开启僵尸粉会员：42493', 1892640744, 1604294545);
INSERT INTO `cmf_admin_log` VALUES (65, 42491, 'regame', '关闭僵尸粉会员：42495,42493,42492,42490,42489,42488,42487,42486', 1892640744, 1604294567);
INSERT INTO `cmf_admin_log` VALUES (66, 42494, 'messi', '修改付费内容分类列表ID: 1', 1892640702, 1604294577);
INSERT INTO `cmf_admin_log` VALUES (67, 42491, 'regame', '开启僵尸粉会员：42495,42493,42492,42490,42489,42488,42487,42486', 1892640744, 1604294581);
INSERT INTO `cmf_admin_log` VALUES (68, 42494, 'messi', '删除付费内容分类列表ID: 1', 1892640702, 1604294589);
INSERT INTO `cmf_admin_log` VALUES (69, 42491, 'regame', '关闭僵尸粉会员：42495,42493,42492,42490,42489,42488,42487,42486', 1892640744, 1604294596);
INSERT INTO `cmf_admin_log` VALUES (70, 42494, 'messi', '添加付费内容分类列表ID: 2', 1892640702, 1604294598);
INSERT INTO `cmf_admin_log` VALUES (71, 42491, 'regame', '删除会员：42495 - 13312345677', 1892640744, 1604294608);
INSERT INTO `cmf_admin_log` VALUES (72, 42494, 'messi', '更新付费内容分类列表排序 ', 1892640702, 1604294613);
INSERT INTO `cmf_admin_log` VALUES (73, 42494, 'messi', '添加付费内容分类列表ID: 3', 1892640702, 1604294620);
INSERT INTO `cmf_admin_log` VALUES (74, 42494, 'messi', '更新付费内容分类列表排序 ', 1892640702, 1604294633);
INSERT INTO `cmf_admin_log` VALUES (75, 42491, 'regame', '修改印象标签：2', 1892640744, 1604294654);
INSERT INTO `cmf_admin_log` VALUES (76, 42491, 'regame', '修改印象标签：2', 1892640744, 1604294658);
INSERT INTO `cmf_admin_log` VALUES (77, 42491, 'regame', '添加印象标签：17', 1892640744, 1604294695);
INSERT INTO `cmf_admin_log` VALUES (78, 42491, 'regame', '删除印象标签：17', 1892640744, 1604294751);
INSERT INTO `cmf_admin_log` VALUES (79, 42491, 'regame', '更新印象标签排序', 1892640744, 1604294762);
INSERT INTO `cmf_admin_log` VALUES (80, 42491, 'regame', '修改奖池管理 奖池开关 关 ', 1892640744, 1604294771);
INSERT INTO `cmf_admin_log` VALUES (81, 42491, 'regame', '修改奖池管理 奖池开关 开 幸运礼物-主播比例 60 幸运礼物-奖池比例 10 ', 1892640744, 1604294786);
INSERT INTO `cmf_admin_log` VALUES (82, 42491, 'regame', '修改奖池管理-列表等级：1', 1892640744, 1604294839);
INSERT INTO `cmf_admin_log` VALUES (83, 1, 'admin', '禁用会员：42492', 2047485572, 1604295428);
INSERT INTO `cmf_admin_log` VALUES (84, 1, 'admin', '启用会员：42492', 2047485572, 1604295443);
INSERT INTO `cmf_admin_log` VALUES (85, 42494, 'messi', '修改付费内容分类列表ID: 3', 1892640702, 1604295445);
INSERT INTO `cmf_admin_log` VALUES (86, 42494, 'messi', '修改付费内容分类列表ID: 3', 1892640702, 1604295460);
INSERT INTO `cmf_admin_log` VALUES (87, 42491, 'regame', '删除直播分类：2', 1892640744, 1604296859);
INSERT INTO `cmf_admin_log` VALUES (88, 42491, 'regame', '添加直播分类：14', 1892640744, 1604296895);
INSERT INTO `cmf_admin_log` VALUES (89, 42491, 'regame', '更新直播分类排序', 1892640744, 1604296907);
INSERT INTO `cmf_admin_log` VALUES (90, 1, 'admin', '禁用会员：42493', 2047485572, 1604297301);
INSERT INTO `cmf_admin_log` VALUES (91, 1, 'admin', '禁用会员：42493', 2047485572, 1604298392);
INSERT INTO `cmf_admin_log` VALUES (92, 1, 'admin', '启用会员：42493', 2047485572, 1604298396);
INSERT INTO `cmf_admin_log` VALUES (93, 1, 'admin', '禁用会员：42493', 2047485572, 1604298507);
INSERT INTO `cmf_admin_log` VALUES (94, 42491, 'regame', '禁用会员：42500', 1892640744, 1604300746);
INSERT INTO `cmf_admin_log` VALUES (95, 1, 'admin', '更新付费内容分类列表排序 ', 2047485572, 1604300908);
INSERT INTO `cmf_admin_log` VALUES (96, 1, 'admin', '更新付费内容分类列表排序 ', 2047485572, 1604300913);
INSERT INTO `cmf_admin_log` VALUES (97, 1, 'admin', '更新付费内容分类列表排序 ', 2047485572, 1604300987);
INSERT INTO `cmf_admin_log` VALUES (98, 42491, 'regame', '禁用会员：42505', 1892640744, 1604303188);
INSERT INTO `cmf_admin_log` VALUES (99, 42491, 'regame', '修改会员认证信息：42492 - 拒绝', 1892640744, 1604303266);
INSERT INTO `cmf_admin_log` VALUES (100, 42491, 'regame', '禁用会员：42505', 1892640744, 1604303359);
INSERT INTO `cmf_admin_log` VALUES (101, 42491, 'regame', '禁用会员：42505', 1892640744, 1604303370);
INSERT INTO `cmf_admin_log` VALUES (102, 42491, 'regame', '修改会员认证信息：42492 - 同意', 1892640744, 1604303439);
INSERT INTO `cmf_admin_log` VALUES (103, 1, 'admin', '禁用会员：42504', 2047485572, 1604303453);
INSERT INTO `cmf_admin_log` VALUES (104, 1, 'admin', '禁用会员：42504', 2047485572, 1604303464);
INSERT INTO `cmf_admin_log` VALUES (105, 42491, 'regame', '直播管理-直播列表添加UID：42492', 1892640744, 1604303937);
INSERT INTO `cmf_admin_log` VALUES (106, 42491, 'regame', '直播管理-直播列表删除UID：42492', 1892640744, 1604304130);
INSERT INTO `cmf_admin_log` VALUES (107, 42491, 'regame', '直播管理-直播列表添加UID：42492', 1892640744, 1604304144);
INSERT INTO `cmf_admin_log` VALUES (108, 42491, 'regame', '直播管理-直播列表修改UID：42492', 1892640744, 1604304166);
INSERT INTO `cmf_admin_log` VALUES (109, 42491, 'regame', '监控 关闭直播间：42490', 1892640744, 1604304202);
INSERT INTO `cmf_admin_log` VALUES (110, 42491, 'regame', '删除贴纸礼物：118', 1892640744, 1604304284);
INSERT INTO `cmf_admin_log` VALUES (111, 42491, 'regame', '更新贴纸礼物排序', 1892640744, 1604304287);
INSERT INTO `cmf_admin_log` VALUES (112, 42491, 'regame', '删除直播举报类型：9', 1892640744, 1604304474);
INSERT INTO `cmf_admin_log` VALUES (113, 42491, 'regame', '修改直播举报类型：8', 1892640744, 1604304481);
INSERT INTO `cmf_admin_log` VALUES (114, 42491, 'regame', '修改直播举报类型：8', 1892640744, 1604304492);
INSERT INTO `cmf_admin_log` VALUES (115, 42491, 'regame', '添加直播举报类型：10', 1892640744, 1604304505);
INSERT INTO `cmf_admin_log` VALUES (116, 42491, 'regame', '更新直播举报类型排序', 1892640744, 1604304515);
INSERT INTO `cmf_admin_log` VALUES (117, 42491, 'regame', '更新直播举报类型排序', 1892640744, 1604304518);
INSERT INTO `cmf_admin_log` VALUES (118, 1, 'admin', '修改贴纸礼物：117', 2047485572, 1604305345);
INSERT INTO `cmf_admin_log` VALUES (119, 42491, 'regame', '视频管理-添加音乐分类ID: 23', 1892640744, 1604307576);
INSERT INTO `cmf_admin_log` VALUES (120, 42491, 'regame', '视频管理-音乐分类更新排序', 1892640744, 1604307698);
INSERT INTO `cmf_admin_log` VALUES (121, 42491, 'regame', '视频管理-音乐分类更新排序', 1892640744, 1604307701);
INSERT INTO `cmf_admin_log` VALUES (122, 42491, 'regame', '视频管理-删除音乐分类ID: 19', 1892640744, 1604307734);
INSERT INTO `cmf_admin_log` VALUES (123, 42491, 'regame', '视频管理-添加音乐ID: 14', 1892640744, 1604308846);
INSERT INTO `cmf_admin_log` VALUES (124, 42491, 'regame', '视频管理-修改音乐ID: 14', 1892640744, 1604309026);
INSERT INTO `cmf_admin_log` VALUES (125, 42491, 'regame', '视频管理-删除音乐ID: 8', 1892640744, 1604309037);
INSERT INTO `cmf_admin_log` VALUES (126, 42491, 'regame', '视频管理-删除音乐ID: 7', 1892640744, 1604309191);
INSERT INTO `cmf_admin_log` VALUES (127, 42491, 'regame', '视频管理-修改音乐ID: 3', 1892640744, 1604309410);
INSERT INTO `cmf_admin_log` VALUES (128, 42491, 'regame', '管理组-管理员修改个人信息ID: 42491', 1892640744, 1604309527);
INSERT INTO `cmf_admin_log` VALUES (129, 42491, 'regame', '管理组-管理员修改个人信息ID: 42491', 1892640744, 1604309547);
INSERT INTO `cmf_admin_log` VALUES (130, 42491, 'regame', '添加视频分类：8', 1892640744, 1604309977);
INSERT INTO `cmf_admin_log` VALUES (131, 42491, 'regame', '更新视频分类排序', 1892640744, 1604309993);
INSERT INTO `cmf_admin_log` VALUES (132, 42491, 'regame', '添加视频分类：9', 1892640744, 1604310004);
INSERT INTO `cmf_admin_log` VALUES (133, 42491, 'regame', '删除视频分类：9', 1892640744, 1604310011);
INSERT INTO `cmf_admin_log` VALUES (134, 42491, 'regame', '更新视频分类排序', 1892640744, 1604310014);
INSERT INTO `cmf_admin_log` VALUES (135, 42491, 'regame', '添加视频：2', 1892640744, 1604310224);
INSERT INTO `cmf_admin_log` VALUES (136, 42491, 'regame', '上下架视频：视频ID(2),状态(1)', 1892640744, 1604310260);
INSERT INTO `cmf_admin_log` VALUES (137, 42491, 'regame', '上下架视频：视频ID(2),状态(0)', 1892640744, 1604310285);
INSERT INTO `cmf_admin_log` VALUES (138, 42491, 'regame', '添加视频：3', 1892640744, 1604310559);
INSERT INTO `cmf_admin_log` VALUES (139, 42491, 'regame', '上下架视频：视频ID(3),状态(1)', 1892640744, 1604310608);
INSERT INTO `cmf_admin_log` VALUES (140, 42491, 'regame', '视频管理-添加举报分类ID: 8', 1892640744, 1604310650);
INSERT INTO `cmf_admin_log` VALUES (141, 42491, 'regame', '视频管理-更新举报分类排序', 1892640744, 1604310655);
INSERT INTO `cmf_admin_log` VALUES (142, 42491, 'regame', '视频管理-更新举报分类排序', 1892640744, 1604310661);
INSERT INTO `cmf_admin_log` VALUES (143, 1, 'admin', '修私密配置 是否开启腾讯云播流鉴权 开 ', 2047485572, 1604311912);
INSERT INTO `cmf_admin_log` VALUES (144, 1, 'admin', '修私密配置 是否开启腾讯云播流鉴权 关 ', 2047485572, 1604314276);
INSERT INTO `cmf_admin_log` VALUES (145, 1, 'admin', '修私密配置 是否开启腾讯云播流鉴权 开 ', 2047485572, 1604314359);
INSERT INTO `cmf_admin_log` VALUES (146, 1, 'admin', '管理组-添加管理员ID: 42512', 1892640702, 1604315433);
INSERT INTO `cmf_admin_log` VALUES (147, 42512, 'kane', '添加会员：42513', 1892640702, 1604318667);
INSERT INTO `cmf_admin_log` VALUES (148, 42512, 'kane', '删除充值规则：1', 1892640702, 1604319349);
INSERT INTO `cmf_admin_log` VALUES (149, 42494, 'messi', '修改大转盘价格列表排序ID: 2', 1892640702, 1604319524);
INSERT INTO `cmf_admin_log` VALUES (150, 42512, 'kane', '导出充值记录：SELECT * FROM `cmf_liang` WHERE  ( status=1 and state=1 )  AND `uid` = 42513 LIMIT 1', 1892640702, 1604319540);
INSERT INTO `cmf_admin_log` VALUES (151, 42494, 'messi', '更新大转盘价格列表排序 ', 1892640702, 1604319541);
INSERT INTO `cmf_admin_log` VALUES (152, 42494, 'messi', '更新大转盘价格列表排序 ', 1892640702, 1604319550);
INSERT INTO `cmf_admin_log` VALUES (153, 42494, 'messi', '修改大转盘价格列表排序ID: 1', 1892640702, 1604319668);
INSERT INTO `cmf_admin_log` VALUES (154, 42494, 'messi', '修改大转盘价格列表排序ID: 1', 1892640702, 1604319674);
INSERT INTO `cmf_admin_log` VALUES (155, 42512, 'kane', '手动充值虚拟币ID：1', 1892640702, 1604319695);
INSERT INTO `cmf_admin_log` VALUES (156, 42494, 'messi', '修改大转盘价格列表排序ID: 2', 1892640702, 1604319695);
INSERT INTO `cmf_admin_log` VALUES (157, 42494, 'messi', '修改大转盘价格列表排序ID: 1', 1892640702, 1604319755);
INSERT INTO `cmf_admin_log` VALUES (158, 42494, 'messi', '修改大转盘价格列表排序ID: 2', 1892640702, 1604319773);
INSERT INTO `cmf_admin_log` VALUES (159, 1, 'admin', '视频管理-修改音乐ID: 5', 2047485572, 1604319961);
INSERT INTO `cmf_admin_log` VALUES (160, 1, 'admin', '视频管理-修改音乐ID: 1', 2047485572, 1604319971);
INSERT INTO `cmf_admin_log` VALUES (161, 1, 'admin', '视频管理-删除音乐ID: 14', 2047485572, 1604320041);
INSERT INTO `cmf_admin_log` VALUES (162, 42494, 'messi', '更新大转盘价格列表排序 ', 1892640702, 1604320143);
INSERT INTO `cmf_admin_log` VALUES (163, 42494, 'messi', '修改大转盘奖品列表ID: 1', 1892640702, 1604320166);
INSERT INTO `cmf_admin_log` VALUES (164, 42494, 'messi', '修改大转盘奖品列表ID: 1', 1892640702, 1604320180);
INSERT INTO `cmf_admin_log` VALUES (165, 42491, 'regame', '手动充值虚拟币ID：2', 1892640744, 1604320503);
INSERT INTO `cmf_admin_log` VALUES (166, 42494, 'messi', '编辑登录奖励：2', 1892640702, 1604320593);
INSERT INTO `cmf_admin_log` VALUES (167, 42494, 'messi', '编辑登录奖励：2', 1892640702, 1604320602);
INSERT INTO `cmf_admin_log` VALUES (168, 42494, 'messi', '发送系统消息：你好123456789', 1892640702, 1604320785);
INSERT INTO `cmf_admin_log` VALUES (169, 42512, 'kane', '修改VIP：1', 1892640702, 1604320866);
INSERT INTO `cmf_admin_log` VALUES (170, 42512, 'kane', '修改VIP：3', 1892640702, 1604320886);
INSERT INTO `cmf_admin_log` VALUES (171, 42494, 'messi', '发送系统消息：22222222222222222222222222', 1892640702, 1604320967);
INSERT INTO `cmf_admin_log` VALUES (172, 42512, 'kane', '修改用户VIP：42513', 1892640702, 1604320973);
INSERT INTO `cmf_admin_log` VALUES (173, 42512, 'kane', '修改用户VIP：42513', 1892640702, 1604320991);
INSERT INTO `cmf_admin_log` VALUES (174, 42512, 'kane', '删除用户VIP：1', 1892640702, 1604321039);
INSERT INTO `cmf_admin_log` VALUES (175, 1, 'admin', '视频管理-编辑音乐分类ID: 18', 2047485572, 1604321058);
INSERT INTO `cmf_admin_log` VALUES (176, 42494, 'messi', '更新页面管理排序 ', 1892640702, 1604321135);
INSERT INTO `cmf_admin_log` VALUES (177, 42494, 'messi', '更新页面管理排序 ', 1892640702, 1604321149);
INSERT INTO `cmf_admin_log` VALUES (178, 42512, 'kane', '修改坐骑：2', 1892640702, 1604321233);
INSERT INTO `cmf_admin_log` VALUES (179, 42494, 'messi', '编辑页面管理ID: 44', 1892640702, 1604321262);
INSERT INTO `cmf_admin_log` VALUES (180, 42494, 'messi', '添加页面管理ID: 45', 1892640702, 1604321290);
INSERT INTO `cmf_admin_log` VALUES (181, 42494, 'messi', '删除页面管理ID: 45', 1892640702, 1604321315);
INSERT INTO `cmf_admin_log` VALUES (182, 42512, 'kane', '修改坐骑：2', 1892640702, 1604321406);
INSERT INTO `cmf_admin_log` VALUES (183, 42512, 'kane', '修改坐骑：5', 1892640702, 1604321477);
INSERT INTO `cmf_admin_log` VALUES (184, 42494, 'messi', '修改付费内容分类列表ID: 2', 1892640702, 1604321712);
INSERT INTO `cmf_admin_log` VALUES (185, 1, 'admin', '视频管理-添加音乐ID: 15', 2047485572, 1604321803);
INSERT INTO `cmf_admin_log` VALUES (186, 42512, 'kane', '修改坐骑：2', 1892640702, 1604322339);
INSERT INTO `cmf_admin_log` VALUES (187, 42512, 'kane', '添加靓号：1', 1892640702, 1604322491);
INSERT INTO `cmf_admin_log` VALUES (188, 42512, 'kane', '添加靓号：2', 1892640702, 1604322511);
INSERT INTO `cmf_admin_log` VALUES (189, 1, 'admin', '修改大转盘价格列表排序ID: 1', 2047485572, 1604323463);
INSERT INTO `cmf_admin_log` VALUES (190, 1, 'admin', '编辑页面管理ID: 3', 2047485572, 1604324664);
INSERT INTO `cmf_admin_log` VALUES (191, 42512, 'kane', '修改家族信息：2', 1892640702, 1604373905);
INSERT INTO `cmf_admin_log` VALUES (192, 42512, 'kane', '添加会员：42516', 1892640702, 1604374115);
INSERT INTO `cmf_admin_log` VALUES (193, 42512, 'kane', '修私密配置 家族长修改成员分成比例是否管理员审核 开 客服链接 ', 1892640702, 1604374378);
INSERT INTO `cmf_admin_log` VALUES (194, 1, 'admin', '更新页面管理排序 ', 2047485572, 1604375878);
INSERT INTO `cmf_admin_log` VALUES (195, 42512, 'kane', '修改会员信息：42516', 1892640702, 1604380061);
INSERT INTO `cmf_admin_log` VALUES (196, 42512, 'kane', '修改家族成员信息：42516', 1892640702, 1604380082);
INSERT INTO `cmf_admin_log` VALUES (197, 42512, 'kane', '添加会员：42518', 1892640702, 1604380202);
INSERT INTO `cmf_admin_log` VALUES (198, 42512, 'kane', '修改家族成员信息：42516', 1892640702, 1604380261);
INSERT INTO `cmf_admin_log` VALUES (199, 1, 'admin', '修改会员等级：1', 2047485572, 1604380283);
INSERT INTO `cmf_admin_log` VALUES (200, 42512, 'kane', '修改家族成员信息：42518', 1892640702, 1604380314);
INSERT INTO `cmf_admin_log` VALUES (201, 42512, 'kane', '修改家族信息：2', 1892640702, 1604380798);
INSERT INTO `cmf_admin_log` VALUES (202, 42512, 'kane', '修改家族成员分成比例：42518,成功', 1892640702, 1604380855);
INSERT INTO `cmf_admin_log` VALUES (203, 42512, 'kane', '修改家族成员分成比例：42516,成功', 1892640702, 1604380868);
INSERT INTO `cmf_admin_log` VALUES (204, 42512, 'kane', '修私密配置 家族长修改成员分成比例是否管理员审核 关 ', 1892640702, 1604380908);
INSERT INTO `cmf_admin_log` VALUES (205, 42512, 'kane', '禁用家族：2', 1892640702, 1604381018);
INSERT INTO `cmf_admin_log` VALUES (206, 42512, 'kane', '启用家族：2', 1892640702, 1604381248);
INSERT INTO `cmf_admin_log` VALUES (207, 42494, 'messi', '直播管理-直播列表添加UID：42514', 1892640702, 1604381414);
INSERT INTO `cmf_admin_log` VALUES (208, 42494, 'messi', '直播管理-直播列表修改UID：42514', 1892640702, 1604381434);
INSERT INTO `cmf_admin_log` VALUES (209, 42512, 'kane', '添加靓号：3', 1892640702, 1604381743);
INSERT INTO `cmf_admin_log` VALUES (210, 42512, 'kane', '添加靓号：4', 1892640702, 1604381753);
INSERT INTO `cmf_admin_log` VALUES (211, 42512, 'kane', '修改靓号状态：3 - 停售', 1892640702, 1604381768);
INSERT INTO `cmf_admin_log` VALUES (212, 42494, 'messi', '编辑页面管理ID: 4', 1892640702, 1604381806);
INSERT INTO `cmf_admin_log` VALUES (213, 42512, 'kane', '添加靓号：5', 1892640702, 1604381825);
INSERT INTO `cmf_admin_log` VALUES (214, 42494, 'messi', '编辑页面管理ID: 2', 1892640702, 1604381846);
INSERT INTO `cmf_admin_log` VALUES (215, 42494, 'messi', '编辑页面管理ID: 3', 1892640702, 1604381886);
INSERT INTO `cmf_admin_log` VALUES (216, 42512, 'kane', '删除靓号：5', 1892640702, 1604382474);
INSERT INTO `cmf_admin_log` VALUES (217, 42512, 'kane', '添加用户VIP：42516', 1892640702, 1604382552);
INSERT INTO `cmf_admin_log` VALUES (218, 1, 'admin', '修改靓号排序', 2047485572, 1604382676);
INSERT INTO `cmf_admin_log` VALUES (219, 1, 'admin', '添加会员：42519', 2008517798, 1604382735);
INSERT INTO `cmf_admin_log` VALUES (220, 42512, 'kane', '修私密配置 一级分成 30 ', 1892640702, 1604383399);
INSERT INTO `cmf_admin_log` VALUES (221, 42512, 'kane', '添加会员：42520', 1892640702, 1604383450);
INSERT INTO `cmf_admin_log` VALUES (222, 1, 'admin', '发送系统消息：正常正常正常正常正常正常', 2047485572, 1604383861);
INSERT INTO `cmf_admin_log` VALUES (223, 1, 'admin', '发送系统消息：正常正常正常正常正常正常', 2047485572, 1604383879);
INSERT INTO `cmf_admin_log` VALUES (224, 42491, 'regame', '确认充值：8', 1892640744, 1604384421);
INSERT INTO `cmf_admin_log` VALUES (225, 42491, 'regame', '修改会员信息：42515', 1892640744, 1604384821);
INSERT INTO `cmf_admin_log` VALUES (226, 42512, 'kane', '直播管理-直播列表添加UID：42513', 1892640702, 1604385047);
INSERT INTO `cmf_admin_log` VALUES (227, 42512, 'kane', '直播管理-直播列表添加UID：42513', 1892640702, 1604385237);
INSERT INTO `cmf_admin_log` VALUES (228, 42494, 'messi', '更新物流公司列表顺序', 1892640702, 1604387219);
INSERT INTO `cmf_admin_log` VALUES (229, 42494, 'messi', '更新物流公司列表顺序', 1892640702, 1604387225);
INSERT INTO `cmf_admin_log` VALUES (230, 42494, 'messi', '编辑物流公司ID: 1', 1892640702, 1604387248);
INSERT INTO `cmf_admin_log` VALUES (231, 42494, 'messi', '编辑物流公司ID: 1', 1892640702, 1604387266);
INSERT INTO `cmf_admin_log` VALUES (232, 42494, 'messi', '更新物流公司列表顺序', 1892640702, 1604387276);
INSERT INTO `cmf_admin_log` VALUES (233, 42494, 'messi', '编辑商品分类：3', 1892640702, 1604388104);
INSERT INTO `cmf_admin_log` VALUES (234, 42494, 'messi', '编辑商品分类：3', 1892640702, 1604388119);
INSERT INTO `cmf_admin_log` VALUES (235, 42512, 'kane', '修改会员认证信息：42520 - 同意', 1892640702, 1604388230);
INSERT INTO `cmf_admin_log` VALUES (236, 42512, 'kane', '修改会员认证信息：42513 - 同意', 1892640702, 1604388736);
INSERT INTO `cmf_admin_log` VALUES (237, 42494, 'messi', '用户反馈标记处理：2', 1892640702, 1604388775);
INSERT INTO `cmf_admin_log` VALUES (238, 42491, 'regame', '管理组-管理员修改个人信息ID: 42491', 1892640744, 1604388914);
INSERT INTO `cmf_admin_log` VALUES (239, 42491, 'regame', '设置超管会员：42515', 1892640744, 1604388960);
INSERT INTO `cmf_admin_log` VALUES (240, 42491, 'regame', '取消超管会员：42515', 1892640744, 1604388998);
INSERT INTO `cmf_admin_log` VALUES (241, 42512, 'kane', '修改提现记录：1 - 同意', 1892640702, 1604389287);
INSERT INTO `cmf_admin_log` VALUES (242, 42491, 'regame', '修改会员信息：42515', 1892640744, 1604391261);
INSERT INTO `cmf_admin_log` VALUES (243, 42494, 'messi', '更新页面管理排序 ', 1892640702, 1604391484);
INSERT INTO `cmf_admin_log` VALUES (244, 42494, 'messi', '添加页面管理ID: 46', 1892640702, 1604392155);
INSERT INTO `cmf_admin_log` VALUES (245, 1, 'admin', '更新页面管理排序 ', 2047485572, 1604392445);
INSERT INTO `cmf_admin_log` VALUES (246, 42491, 'regame', '监控 关闭直播间：42492', 1892640744, 1604393695);
INSERT INTO `cmf_admin_log` VALUES (247, 42491, 'regame', '直播举报标记处理：2', 1892640744, 1604393815);
INSERT INTO `cmf_admin_log` VALUES (248, 1, 'admin', '设置超管会员：42522', 2047485572, 1604396885);
INSERT INTO `cmf_admin_log` VALUES (249, 1, 'admin', '取消超管会员：42522', 2047485572, 1604396892);
INSERT INTO `cmf_admin_log` VALUES (250, 1, 'admin', '修改公共配置 修改登录方式 关闭qq 关闭wx 关闭facebook 开启 ', 2047485572, 1604399130);
INSERT INTO `cmf_admin_log` VALUES (251, 42512, 'kane', '确认充值：2', 1892640702, 1604404026);
INSERT INTO `cmf_admin_log` VALUES (252, 42512, 'kane', '修改提现记录：2 - 拒绝', 1892640702, 1604404683);
INSERT INTO `cmf_admin_log` VALUES (253, 42512, 'kane', '导出提现记录：SELECT * FROM `cmf_liang` WHERE  ( status=1 and state=1 )  AND `uid` = 42520 LIMIT 1', 1892640702, 1604404717);
INSERT INTO `cmf_admin_log` VALUES (254, 42491, 'regame', '视频管理-添加音乐分类ID: 24', 1892640744, 1604406974);
INSERT INTO `cmf_admin_log` VALUES (255, 42494, 'messi', '更新页面管理排序 ', 1892640702, 1604407049);
INSERT INTO `cmf_admin_log` VALUES (256, 42491, 'regame', '视频管理-编辑音乐分类ID: 18', 1892640744, 1604407055);
INSERT INTO `cmf_admin_log` VALUES (257, 42494, 'messi', '发送系统消息：1111111111111111111111111', 1892640702, 1604407192);
INSERT INTO `cmf_admin_log` VALUES (258, 42494, 'messi', '发送系统消息：00000000000000', 1892640702, 1604407217);
INSERT INTO `cmf_admin_log` VALUES (259, 42494, 'messi', '发送系统消息：你好123456789777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777777', 1892640702, 1604407249);
INSERT INTO `cmf_admin_log` VALUES (260, 42491, 'regame', '视频管理-修改音乐ID: 15', 1892640744, 1604407363);
INSERT INTO `cmf_admin_log` VALUES (261, 42494, 'messi', '推送信息ID：1', 1892640702, 1604408405);
INSERT INTO `cmf_admin_log` VALUES (262, 42494, 'messi', '推送信息ID：2', 1892640702, 1604408444);
INSERT INTO `cmf_admin_log` VALUES (263, 42494, 'messi', '推送信息ID：3', 1892640702, 1604408492);
INSERT INTO `cmf_admin_log` VALUES (264, 42494, 'messi', '推送信息ID：4', 1892640702, 1604408558);
INSERT INTO `cmf_admin_log` VALUES (265, 42494, 'messi', '推送信息ID：5', 1892640702, 1604409182);
INSERT INTO `cmf_admin_log` VALUES (266, 42494, 'messi', '添加付费内容分类列表ID: 4', 1892640702, 1604409795);
INSERT INTO `cmf_admin_log` VALUES (267, 42494, 'messi', '添加付费内容分类列表ID: 5', 1892640702, 1604409858);
INSERT INTO `cmf_admin_log` VALUES (268, 42494, 'messi', '更新付费内容分类列表排序 ', 1892640702, 1604409870);
INSERT INTO `cmf_admin_log` VALUES (269, 42494, 'messi', '编辑页面管理ID: 2', 1892640702, 1604410137);
INSERT INTO `cmf_admin_log` VALUES (270, 42494, 'messi', '编辑页面管理ID: 2', 1892640702, 1604410147);
INSERT INTO `cmf_admin_log` VALUES (271, 1, 'admin', '修私密配置 极光推送模式 生产 ', 2047485572, 1604463229);
INSERT INTO `cmf_admin_log` VALUES (272, 42512, 'kane', '确认充值：9', 1892640702, 1604467977);
INSERT INTO `cmf_admin_log` VALUES (273, 42512, 'kane', '手动充值虚拟币ID：3', 1892640702, 1604468491);
INSERT INTO `cmf_admin_log` VALUES (274, 1, 'admin', '修改直播分类：4', 2047485572, 1604469722);
INSERT INTO `cmf_admin_log` VALUES (275, 1, 'admin', '修改公共配置 修改IPA版本号 20.10.20.1 修改IPA上架版本号 20.10.20.1 ', 2047485572, 1604485840);
INSERT INTO `cmf_admin_log` VALUES (276, 1, 'admin', '修改公共配置 修改服务协议跳转链接 http://www.baidu.com ', 2047485572, 1604487522);
INSERT INTO `cmf_admin_log` VALUES (277, 1, 'admin', '修改公共配置 修改服务协议跳转链接 /http://www.baidu.com ', 2047485572, 1604487532);
INSERT INTO `cmf_admin_log` VALUES (278, 1, 'admin', '设置推荐会员：42528', 2047485572, 1604487582);
INSERT INTO `cmf_admin_log` VALUES (279, 1, 'admin', '修改公共配置 修改隐私政策跳转链接 http://jiextx.com/portal/page/index?id=3 修改服务协议跳转链接 http://jiextx.com/portal/page/index?id=4 ', 2047485572, 1604488022);
INSERT INTO `cmf_admin_log` VALUES (280, 42491, 'regame', '修改公共配置 网站名称 xxoo直播 ', 1892640744, 1604488497);
INSERT INTO `cmf_admin_log` VALUES (281, 42494, 'messi', '发送系统消息：88888888888888alsgdasdgaskgkjcascsadckjascbjasdc23649283sdhcasdcv', 1892640702, 1604490430);
INSERT INTO `cmf_admin_log` VALUES (282, 42512, 'kane', '修改会员认证信息：42516 - 同意', 1892640702, 1604490768);
INSERT INTO `cmf_admin_log` VALUES (283, 42491, 'regame', '管理组-添加管理员ID: 42535', 1892640744, 1604491230);
INSERT INTO `cmf_admin_log` VALUES (284, 42491, 'regame', '手动充值虚拟币ID：4', 1892640744, 1604491851);
INSERT INTO `cmf_admin_log` VALUES (285, 42491, 'regame', '手动充值虚拟币ID：5', 1892640744, 1604492122);
INSERT INTO `cmf_admin_log` VALUES (286, 42494, 'messi', '更新付费内容分类列表排序 ', 1892640702, 1604492604);
INSERT INTO `cmf_admin_log` VALUES (287, 1, 'admin', '修改公共配置 修改IPA上架版本号  ', 2047485572, 1604492643);
INSERT INTO `cmf_admin_log` VALUES (288, 42491, 'regame', '推送信息ID：6', 1892640744, 1604494056);
INSERT INTO `cmf_admin_log` VALUES (289, 42535, 'irfan', '添加会员：42538', 2047485577, 1604494643);
INSERT INTO `cmf_admin_log` VALUES (290, 42535, 'irfan', '删除直播分类：14', 2047485628, 1604494993);
INSERT INTO `cmf_admin_log` VALUES (291, 42535, 'irfan', '删除直播分类：5', 2047485628, 1604495012);
INSERT INTO `cmf_admin_log` VALUES (292, 42535, 'irfan', '删除直播分类：4', 2047485628, 1604495015);
INSERT INTO `cmf_admin_log` VALUES (293, 42535, 'irfan', '删除直播分类：3', 2047485628, 1604495018);
INSERT INTO `cmf_admin_log` VALUES (294, 42535, 'irfan', '添加直播分类：15', 2047485628, 1604495102);
INSERT INTO `cmf_admin_log` VALUES (295, 42535, 'irfan', '删除直播分类：1', 2047485628, 1604495114);
INSERT INTO `cmf_admin_log` VALUES (296, 42535, 'irfan', '更新直播分类排序', 2047485628, 1604495126);
INSERT INTO `cmf_admin_log` VALUES (297, 42535, 'irfan', '删除直播分类：8', 2047485628, 1604495132);
INSERT INTO `cmf_admin_log` VALUES (298, 42535, 'irfan', '删除直播分类：6', 2047485628, 1604495139);
INSERT INTO `cmf_admin_log` VALUES (299, 42512, 'kane', '编辑页面管理ID: 3', 1892640702, 1604495654);
INSERT INTO `cmf_admin_log` VALUES (300, 42512, 'kane', '编辑页面管理ID: 4', 1892640702, 1604495665);
INSERT INTO `cmf_admin_log` VALUES (301, 1, 'admin', '添加会员：42539', 1892640593, 1604495844);
INSERT INTO `cmf_admin_log` VALUES (302, 42535, 'irfan', '删除直播分类：10', 2047485628, 1604495942);
INSERT INTO `cmf_admin_log` VALUES (303, 42535, 'irfan', '删除直播分类：7', 2047485628, 1604495954);
INSERT INTO `cmf_admin_log` VALUES (304, 42535, 'irfan', '幻灯片ID: 1 管理页面添加ID: 1', 2047485628, 1604496636);
INSERT INTO `cmf_admin_log` VALUES (305, 42535, 'irfan', '幻灯片ID: 2 管理页面添加ID: 2', 2047485628, 1604496727);
INSERT INTO `cmf_admin_log` VALUES (306, 42535, 'irfan', '幻灯片ID: 2 管理页面删除ID: 2', 2047485628, 1604496940);
INSERT INTO `cmf_admin_log` VALUES (307, 42535, 'irfan', '幻灯片ID: 1 管理页面删除ID: 1', 2047485628, 1604496955);
INSERT INTO `cmf_admin_log` VALUES (308, 42535, 'irfan', '设置超管会员：42538', 2047485628, 1604498327);
INSERT INTO `cmf_admin_log` VALUES (309, 42535, 'irfan', '设置推荐会员：42538', 2047485628, 1604498358);
INSERT INTO `cmf_admin_log` VALUES (310, 42535, 'irfan', '添加会员：42540', 2047485577, 1604502133);
INSERT INTO `cmf_admin_log` VALUES (311, 42535, 'irfan', '删除会员：42540 - 15188888888', 2047485577, 1604502557);
INSERT INTO `cmf_admin_log` VALUES (312, 42535, 'irfan', '更新直播分类排序', 1892640744, 1604504182);
INSERT INTO `cmf_admin_log` VALUES (313, 1, 'admin', '添加视频：8', 2008517798, 1604542677);
INSERT INTO `cmf_admin_log` VALUES (314, 1, 'admin', '修私密配置 极光推送模式 开发 ', 2047485572, 1604551489);
INSERT INTO `cmf_admin_log` VALUES (315, 1, 'admin', '添加会员：42543', 1892640593, 1604558880);
INSERT INTO `cmf_admin_log` VALUES (316, 1, 'admin', '修私密配置 客服链接 ', 2047485572, 1604561808);
INSERT INTO `cmf_admin_log` VALUES (317, 1, 'admin', '取消超管会员：42492', 2047485572, 1604567646);
INSERT INTO `cmf_admin_log` VALUES (318, 1, 'admin', '修改公共配置 修改微信推广域名 http://jiextx.com/wxshare/Share/show?roomnum= ', 2047485572, 1604570016);
INSERT INTO `cmf_admin_log` VALUES (319, 1, 'admin', '取消推荐会员：42492', 2047485572, 1604571558);
INSERT INTO `cmf_admin_log` VALUES (320, 1, 'admin', '设置推荐会员：42492', 2047485572, 1604571865);
INSERT INTO `cmf_admin_log` VALUES (321, 1, 'admin', '修私密配置 邀请开关 关 ', 2047485572, 1604574891);
INSERT INTO `cmf_admin_log` VALUES (322, 1, 'admin', '修改公共配置 修改分享方式 关闭qq 关闭qzone 关闭wx 关闭wchat 关闭facebook 关闭twitter 开启 ', 2047485572, 1604575036);
INSERT INTO `cmf_admin_log` VALUES (323, 1, 'admin', '修私密配置 家族控制开关 关 ', 2047485572, 1604575822);
INSERT INTO `cmf_admin_log` VALUES (324, 42512, 'kane', '直播管理-禁言管理删除用户ID：42516', 1892640702, 1604579954);
INSERT INTO `cmf_admin_log` VALUES (325, 42512, 'kane', '直播管理-禁言管理删除用户ID：42518', 1892640702, 1604580094);
INSERT INTO `cmf_admin_log` VALUES (326, 1, 'admin', '直播管理-踢人管理被踢用户ID：42545', 2047485572, 1604580814);
INSERT INTO `cmf_admin_log` VALUES (327, 1, 'admin', '直播管理-禁言管理删除用户ID：42545', 2047485572, 1604580846);
INSERT INTO `cmf_admin_log` VALUES (328, 1, 'admin', '直播管理-禁言管理删除用户ID：42545', 2047485572, 1604583079);
INSERT INTO `cmf_admin_log` VALUES (329, 1, 'admin', '设置超管会员：42492', 1892580493, 1604585421);
INSERT INTO `cmf_admin_log` VALUES (330, 42535, 'irfan', '修改公共配置 修改房间类型 关闭2;门票房间 关闭3;计时房间 ', 1892640744, 1604592129);
INSERT INTO `cmf_admin_log` VALUES (331, 42535, 'irfan', '添加会员：42546', 2047485577, 1604593004);
INSERT INTO `cmf_admin_log` VALUES (332, 42535, 'irfan', '删除会员：42546 - 15288888888', 2047485577, 1604593474);
INSERT INTO `cmf_admin_log` VALUES (333, 42535, 'irfan', '开启会员僵尸粉：42547', 2047485577, 1604594756);
INSERT INTO `cmf_admin_log` VALUES (334, 42535, 'irfan', '关闭会员僵尸粉：42547', 2047485577, 1604594791);
INSERT INTO `cmf_admin_log` VALUES (335, 1, 'admin', '设置超管会员：42542', 1892580493, 1604635384);
INSERT INTO `cmf_admin_log` VALUES (336, 1, 'admin', '确认充值：12', 1892580493, 1604635650);
INSERT INTO `cmf_admin_log` VALUES (337, 42491, 'regame', '修改会员信息：42492', 1892640744, 1604647070);
INSERT INTO `cmf_admin_log` VALUES (338, 42491, 'regame', '修改会员信息：42492', 1892640744, 1604647160);
INSERT INTO `cmf_admin_log` VALUES (339, 42491, 'regame', '修改会员信息：42492', 1892640744, 1604647662);
INSERT INTO `cmf_admin_log` VALUES (340, 42494, 'messi', '更新页面管理排序 ', 1892640702, 1604647983);
INSERT INTO `cmf_admin_log` VALUES (341, 42494, 'messi', '编辑页面管理ID: 3', 1892640702, 1604648024);
INSERT INTO `cmf_admin_log` VALUES (342, 42494, 'messi', '编辑页面管理ID: 4', 1892640702, 1604648044);
INSERT INTO `cmf_admin_log` VALUES (343, 42494, 'messi', '编辑页面管理ID: 2', 1892640702, 1604648704);
INSERT INTO `cmf_admin_log` VALUES (344, 1, 'admin', '修改会员信息：42548', 2047485572, 1604650907);
INSERT INTO `cmf_admin_log` VALUES (345, 1, 'admin', '修私密配置 客服链接 ', 2047485572, 1604656241);
INSERT INTO `cmf_admin_log` VALUES (346, 42491, 'regame', '取消热门会员：42515', 1892640744, 1604662693);
INSERT INTO `cmf_admin_log` VALUES (347, 42491, 'regame', '取消热门会员：42549', 1892640744, 1604662700);
INSERT INTO `cmf_admin_log` VALUES (348, 42512, 'kane', '添加会员：42550', 1892640702, 1604663653);
INSERT INTO `cmf_admin_log` VALUES (349, 42512, 'kane', '开启僵尸粉会员：42550', 1892640702, 1604664355);
INSERT INTO `cmf_admin_log` VALUES (350, 42512, 'kane', '开启会员僵尸粉：42550', 1892640702, 1604664375);
INSERT INTO `cmf_admin_log` VALUES (351, 1, 'admin', '修私密配置 客服链接 ', 2047485572, 1604665278);
INSERT INTO `cmf_admin_log` VALUES (352, 42494, 'messi', '发送系统消息：333333333333333333333333333', 1892640702, 1604666456);
INSERT INTO `cmf_admin_log` VALUES (353, 1, 'admin', '修私密配置 客服链接 ', 2047485572, 1604667365);
INSERT INTO `cmf_admin_log` VALUES (354, 42491, 'regame', '设置推荐会员：42515', 1892640744, 1604719373);
INSERT INTO `cmf_admin_log` VALUES (355, 42491, 'regame', '取消推荐会员：42515', 1892640744, 1604719785);
INSERT INTO `cmf_admin_log` VALUES (356, 42491, 'regame', '设置热门会员：42515', 1892640744, 1604719848);
INSERT INTO `cmf_admin_log` VALUES (357, 42491, 'regame', '取消热门会员：42515', 1892640744, 1604719877);
INSERT INTO `cmf_admin_log` VALUES (358, 42491, 'regame', '设置推荐会员：42551', 1892640744, 1604719886);
INSERT INTO `cmf_admin_log` VALUES (359, 42491, 'regame', '设置推荐会员：42515', 1892640744, 1604719941);
INSERT INTO `cmf_admin_log` VALUES (360, 42494, 'messi', '导出推送信息：SELECT * FROM `cmf_pushrecord` WHERE  `type` = 0 ORDER BY `id` DESC', 1892640702, 1604725719);
INSERT INTO `cmf_admin_log` VALUES (361, 1, 'admin', '修私密配置 极光推送模式 生产 ', 2047485572, 1604728899);
INSERT INTO `cmf_admin_log` VALUES (362, 1, 'admin', '设置推荐会员：42545', 2047485572, 1604729086);
INSERT INTO `cmf_admin_log` VALUES (363, 42491, 'regame', '设置推荐会员：42533', 1892640744, 1604729380);
INSERT INTO `cmf_admin_log` VALUES (364, 42494, 'messi', '禁用会员：42551', 1892640702, 1604731217);
INSERT INTO `cmf_admin_log` VALUES (365, 42494, 'messi', '禁用会员：42551', 1892640702, 1604731305);
INSERT INTO `cmf_admin_log` VALUES (366, 1, 'admin', '禁用会员：42551', 2047485572, 1604731524);
INSERT INTO `cmf_admin_log` VALUES (367, 1, 'admin', '启用会员：42551', 2047485572, 1604731536);
INSERT INTO `cmf_admin_log` VALUES (368, 1, 'admin', '禁用会员：42551', 2047485572, 1604731650);
INSERT INTO `cmf_admin_log` VALUES (369, 1, 'admin', '禁用会员：42551', 2047485572, 1604731809);
INSERT INTO `cmf_admin_log` VALUES (370, 1, 'admin', '禁用会员：42551', 2047485572, 1604732416);
INSERT INTO `cmf_admin_log` VALUES (371, 42494, 'messi', '禁用会员：42551', 1892640702, 1604737824);
INSERT INTO `cmf_admin_log` VALUES (372, 42535, 'irfan', '修改直播分类：9', 1849082855, 1604743379);
INSERT INTO `cmf_admin_log` VALUES (373, 42535, 'irfan', '添加直播分类：16', 1849082855, 1604743453);
INSERT INTO `cmf_admin_log` VALUES (374, 42535, 'irfan', '开启会员僵尸粉：42501', 1849082855, 1604743527);
INSERT INTO `cmf_admin_log` VALUES (375, 42535, 'irfan', '开启会员僵尸粉：42541', 1849082855, 1604743611);
INSERT INTO `cmf_admin_log` VALUES (376, 1, 'admin', '用户反馈标记处理：11', 2047485572, 1604750505);
INSERT INTO `cmf_admin_log` VALUES (377, 42494, 'messi', '动态下架ID: 12 原因: 99999999999999999999', 1892640702, 1604751360);
INSERT INTO `cmf_admin_log` VALUES (378, 42494, 'messi', '删除审核通过动态ID: 13', 1892640702, 1604751541);
INSERT INTO `cmf_admin_log` VALUES (379, 1, 'admin', '修改会员认证信息：42511 - 同意', 2047485572, 1604752112);
INSERT INTO `cmf_admin_log` VALUES (380, 1, 'admin', '修改会员认证信息：42496 - 同意', 2047485572, 1604752119);
INSERT INTO `cmf_admin_log` VALUES (381, 1, 'admin', '修改上传设置 最大同时上传文件数 9 ', 2047485572, 1604752701);
INSERT INTO `cmf_admin_log` VALUES (382, 1, 'admin', '修改上传设置 图片文件 大小: 1024 扩展名: jpg,jpeg,png,gif,bmp4,svga  ', 2047485572, 1604753915);
INSERT INTO `cmf_admin_log` VALUES (383, 1, 'admin', '直播管理-直播列表添加UID：1', 2047485572, 1604828362);
INSERT INTO `cmf_admin_log` VALUES (384, 1, 'admin', '修改公共配置 网站名称 荣耀直播 ', 2047485572, 1604836582);
INSERT INTO `cmf_admin_log` VALUES (385, 1, 'admin', '修私密配置 动态审核 开 ', 2047485572, 1604895135);
INSERT INTO `cmf_admin_log` VALUES (386, 42494, 'messi', '动态ID: 24 审核通过', 1892640702, 1604899311);
INSERT INTO `cmf_admin_log` VALUES (387, 42494, 'messi', '动态ID: 25 审核通过', 1892640702, 1604900226);
INSERT INTO `cmf_admin_log` VALUES (388, 42494, 'messi', '动态ID: 26 审核拒绝', 1892640702, 1604900344);
INSERT INTO `cmf_admin_log` VALUES (389, 42512, 'kane', '动态ID: 27 审核通过', 1892640702, 1604901263);
INSERT INTO `cmf_admin_log` VALUES (390, 42512, 'kane', '动态ID: 28 审核拒绝', 1892640702, 1604901310);
INSERT INTO `cmf_admin_log` VALUES (391, 42512, 'kane', '发送系统消息：ssss', 1892640702, 1604905166);
INSERT INTO `cmf_admin_log` VALUES (392, 42491, 'regame', '添加页面管理ID: 47', 1892640744, 1604907874);
INSERT INTO `cmf_admin_log` VALUES (393, 42491, 'regame', '删除页面管理ID: 47', 1892640744, 1604907902);
INSERT INTO `cmf_admin_log` VALUES (394, 42491, 'regame', '直播管理-禁言管理删除用户ID：42567', 1892640744, 1604910847);
INSERT INTO `cmf_admin_log` VALUES (395, 42491, 'regame', '直播管理-禁言管理删除用户ID：42515', 1892640744, 1604911468);
INSERT INTO `cmf_admin_log` VALUES (396, 42512, 'kane', '修改公共配置 修改IPA版本号 1.0.0 ', 1892640702, 1604920499);
INSERT INTO `cmf_admin_log` VALUES (397, 42512, 'kane', '修改公共配置 修改APK版本号 1.0.0 ', 1892640702, 1604920507);
INSERT INTO `cmf_admin_log` VALUES (398, 42512, 'kane', '修改公共配置 修改APK版本号 1.1.0 ', 1892640702, 1604920567);
INSERT INTO `cmf_admin_log` VALUES (399, 42494, 'messi', '推送信息ID：7', 1892640702, 1604921412);
INSERT INTO `cmf_admin_log` VALUES (400, 42494, 'messi', '推送信息ID：8', 1892640702, 1604921455);
INSERT INTO `cmf_admin_log` VALUES (401, 42494, 'messi', '推送信息ID：9', 1892640702, 1604921517);
INSERT INTO `cmf_admin_log` VALUES (402, 1, 'admin', '直播管理-直播列表修改UID：1', 2047485572, 1604923341);
INSERT INTO `cmf_admin_log` VALUES (403, 42512, 'kane', '添加直播分类：17', 1892640702, 1604923638);
INSERT INTO `cmf_admin_log` VALUES (404, 42491, 'regame', '直播管理-禁言管理删除用户ID：42570', 1892640744, 1604924330);
INSERT INTO `cmf_admin_log` VALUES (405, 42491, 'regame', '直播管理-踢人管理被踢用户ID：42570', 1892640744, 1604924389);
INSERT INTO `cmf_admin_log` VALUES (406, 42491, 'regame', '直播管理-禁言管理删除用户ID：42570', 1892640744, 1604924479);
INSERT INTO `cmf_admin_log` VALUES (407, 42491, 'regame', '直播管理-禁言管理删除用户ID：42570', 1892640744, 1604924549);
INSERT INTO `cmf_admin_log` VALUES (408, 42494, 'messi', '推送信息ID：10', 1892640702, 1604924628);
INSERT INTO `cmf_admin_log` VALUES (409, 42512, 'kane', '幻灯片ID: 1 管理页面添加ID: 3', 1892640702, 1604924709);
INSERT INTO `cmf_admin_log` VALUES (410, 42512, 'kane', '幻灯片ID: 1 管理页面编辑ID: 3', 1892640702, 1604924735);
INSERT INTO `cmf_admin_log` VALUES (411, 42512, 'kane', '幻灯片ID: 2 管理页面添加ID: 4', 1892640702, 1604924871);
INSERT INTO `cmf_admin_log` VALUES (412, 42512, 'kane', '幻灯片ID: 2 管理页面添加ID: 5', 1892640702, 1604924933);
INSERT INTO `cmf_admin_log` VALUES (413, 42512, 'kane', '添加引导页ID: 1', 1892640702, 1604925000);
INSERT INTO `cmf_admin_log` VALUES (414, 42512, 'kane', '修改引导页 ', 1892640702, 1604925012);
INSERT INTO `cmf_admin_log` VALUES (415, 42512, 'kane', '管理组-添加角色ID: 8', 1892640702, 1604925116);
INSERT INTO `cmf_admin_log` VALUES (416, 42491, 'regame', '直播管理-踢人管理被踢用户ID：42570', 1892640744, 1604925122);
INSERT INTO `cmf_admin_log` VALUES (417, 42512, 'kane', '管理组-添加管理员ID: 42573', 1892640702, 1604925154);
INSERT INTO `cmf_admin_log` VALUES (418, 42491, 'regame', '直播管理-禁播管理删除主播ID：42533', 1892640744, 1604925168);
INSERT INTO `cmf_admin_log` VALUES (419, 42512, 'kane', '管理组-授权角色权限ID: 8', 1892640702, 1604925177);
INSERT INTO `cmf_admin_log` VALUES (420, 42512, 'kane', '添加会员：42574', 1892640702, 1604925305);
INSERT INTO `cmf_admin_log` VALUES (421, 42535, 'irfan', '幻灯片ID: 2 管理页面删除ID: 4', 1892640702, 1604930321);
INSERT INTO `cmf_admin_log` VALUES (422, 42535, 'irfan', '幻灯片ID: 2 管理页面删除ID: 5', 1892640702, 1604930326);
INSERT INTO `cmf_admin_log` VALUES (423, 42535, 'irfan', '幻灯片ID: 2 管理页面添加ID: 6', 1892640702, 1604930436);
INSERT INTO `cmf_admin_log` VALUES (424, 42535, 'irfan', '幻灯片ID: 2 管理页面添加ID: 7', 1892640702, 1604930537);
INSERT INTO `cmf_admin_log` VALUES (425, 42535, 'irfan', '幻灯片ID: 2 管理页面添加ID: 8', 1892640702, 1604930576);
INSERT INTO `cmf_admin_log` VALUES (426, 42535, 'irfan', '幻灯片ID: 1 管理页面添加ID: 9', 1892640702, 1604930915);
INSERT INTO `cmf_admin_log` VALUES (427, 42535, 'irfan', '幻灯片ID: 1 管理页面删除ID: 3', 1892640702, 1604930964);
INSERT INTO `cmf_admin_log` VALUES (428, 42535, 'irfan', '幻灯片ID: 1 管理页面添加ID: 10', 1892640702, 1604931014);
INSERT INTO `cmf_admin_log` VALUES (429, 42535, 'irfan', '幻灯片ID: 1 管理页面添加ID: 11', 1892640702, 1604931062);
INSERT INTO `cmf_admin_log` VALUES (430, 42512, 'kane', '设置超管会员：42516', 1892640702, 1604936759);
INSERT INTO `cmf_admin_log` VALUES (431, 42494, 'messi', '更新印象标签排序', 1892640702, 1604938775);
INSERT INTO `cmf_admin_log` VALUES (432, 42494, 'messi', '删除印象标签：10', 1892640702, 1604938786);
INSERT INTO `cmf_admin_log` VALUES (433, 42494, 'messi', '删除印象标签：9', 1892640702, 1604938792);
INSERT INTO `cmf_admin_log` VALUES (434, 1, 'admin', '修私密配置 认证限制 开 ', 2047485572, 1604986280);
INSERT INTO `cmf_admin_log` VALUES (435, 1, 'admin', '修改直播分类：11', 2047485572, 1604987363);
INSERT INTO `cmf_admin_log` VALUES (436, 1, 'admin', '修改公共配置 修改APK版本号 1.0.0 ', 2047485572, 1604991320);
INSERT INTO `cmf_admin_log` VALUES (437, 42535, 'irfan', '修私密配置 认证限制 关 ', 2047485628, 1604994103);
INSERT INTO `cmf_admin_log` VALUES (438, 42535, 'irfan', '监控 关闭直播间：42521', 2047485628, 1604996285);
INSERT INTO `cmf_admin_log` VALUES (439, 1, 'admin', '直播管理-踢人管理被踢用户ID：42539', 2047485572, 1604999186);
INSERT INTO `cmf_admin_log` VALUES (440, 42535, 'irfan', '开启会员僵尸粉：42583', 2047485577, 1605013368);
INSERT INTO `cmf_admin_log` VALUES (441, 1, 'admin', '修私密配置 认证限制 开 ', 2047485572, 1605077685);
INSERT INTO `cmf_admin_log` VALUES (442, 1, 'admin', '直播管理-直播列表修改UID：1', 2047485572, 1605082801);
INSERT INTO `cmf_admin_log` VALUES (443, 1, 'admin', '直播管理-直播列表添加UID：42507', 2047485572, 1605082847);
INSERT INTO `cmf_admin_log` VALUES (444, 1, 'admin', '直播管理-直播列表删除UID：1', 2047485572, 1605082859);
INSERT INTO `cmf_admin_log` VALUES (445, 42535, 'irfan', '设置推荐会员：42583', 2047485577, 1605092782);
INSERT INTO `cmf_admin_log` VALUES (446, 42535, 'irfan', '取消推荐会员：42583', 2047485577, 1605092788);
INSERT INTO `cmf_admin_log` VALUES (447, 42535, 'irfan', '开启僵尸粉会员：42585', 1849071259, 1605092811);
INSERT INTO `cmf_admin_log` VALUES (448, 42494, 'messi', '修改会员认证信息：42521 - 同意', 1892640702, 1605095408);
INSERT INTO `cmf_admin_log` VALUES (449, 42491, 'regame', '修改会员认证信息：42515 - 同意', 1892640744, 1605095605);
INSERT INTO `cmf_admin_log` VALUES (450, 42491, 'regame', '设置热门会员：42515', 1892640744, 1605175735);
INSERT INTO `cmf_admin_log` VALUES (451, 42512, 'kane', '开启会员僵尸粉：42516', 1892640702, 1605178516);
INSERT INTO `cmf_admin_log` VALUES (452, 42512, 'kane', '开启僵尸粉会员：42516', 1892640702, 1605178562);
INSERT INTO `cmf_admin_log` VALUES (453, 42535, 'irfan', '直播管理-禁播管理删除主播ID：42578', 1892640744, 1605204904);
INSERT INTO `cmf_admin_log` VALUES (454, 1, 'admin', '修改会员认证信息：42524 - 同意', 2047485572, 1605241289);
INSERT INTO `cmf_admin_log` VALUES (455, 1, 'admin', '开启会员僵尸粉：42586', 2047485572, 1605242936);
INSERT INTO `cmf_admin_log` VALUES (456, 1, 'admin', '开启僵尸粉会员：42586', 2047485572, 1605242941);
INSERT INTO `cmf_admin_log` VALUES (457, 1, 'admin', '关闭僵尸粉会员：42586', 2047485572, 1605242947);
INSERT INTO `cmf_admin_log` VALUES (458, 1, 'admin', '关闭会员僵尸粉：42586', 2047485572, 1605242951);
INSERT INTO `cmf_admin_log` VALUES (459, 1, 'admin', '修改会员认证信息：42545 - 同意', 2047485572, 1605247889);
INSERT INTO `cmf_admin_log` VALUES (460, 1, 'admin', '修改会员认证信息：42542 - 同意', 1849082545, 1605249560);
INSERT INTO `cmf_admin_log` VALUES (461, 1, 'admin', '修私密配置 用户列表请求间隔(秒) 10 ', 2047485572, 1605251275);
INSERT INTO `cmf_admin_log` VALUES (462, 1, 'admin', '修改直播分类：12', 2047485572, 1605251473);
INSERT INTO `cmf_admin_log` VALUES (463, 42512, 'kane', '开启僵尸粉会员：42513', 1892641726, 1605272142);
INSERT INTO `cmf_admin_log` VALUES (464, 42512, 'kane', '开启会员僵尸粉：42513', 1892641726, 1605272150);
INSERT INTO `cmf_admin_log` VALUES (465, 42512, 'kane', '关闭僵尸粉会员：42513', 1892641726, 1605272346);
INSERT INTO `cmf_admin_log` VALUES (466, 42512, 'kane', '关闭会员僵尸粉：42513', 1892641726, 1605272358);
INSERT INTO `cmf_admin_log` VALUES (467, 42494, 'messi', '开启僵尸粉会员：42521', 1892641726, 1605272564);
INSERT INTO `cmf_admin_log` VALUES (468, 42494, 'messi', '关闭僵尸粉会员：42521', 1892641726, 1605272630);
INSERT INTO `cmf_admin_log` VALUES (469, 1, 'admin', '添加会员：42587', 2047485572, 1605272661);
INSERT INTO `cmf_admin_log` VALUES (470, 1, 'admin', '删除会员：42587 - 13645678998', 2047485572, 1605272673);
INSERT INTO `cmf_admin_log` VALUES (471, 42494, 'messi', '开启僵尸粉会员：42521', 1892641726, 1605272780);
INSERT INTO `cmf_admin_log` VALUES (472, 42535, 'irfan', '禁用会员：42488', 1892641768, 1605284559);
INSERT INTO `cmf_admin_log` VALUES (473, 42535, 'irfan', '禁用会员：42488', 1892641768, 1605284559);
INSERT INTO `cmf_admin_log` VALUES (474, 42535, 'irfan', '启用会员：42488', 1892641768, 1605287114);
INSERT INTO `cmf_admin_log` VALUES (475, 1, 'admin', '修改公共配置 修改房间类型 开启3;计时房间 ', 2047485572, 1605334307);
INSERT INTO `cmf_admin_log` VALUES (476, 1, 'admin', '修改公共配置 修改房间类型 关闭3;计时房间 ', 2047485572, 1605334376);
INSERT INTO `cmf_admin_log` VALUES (477, 42535, 'irfan', '修改会员认证信息：42544 - 同意', 2047485577, 1605337632);
INSERT INTO `cmf_admin_log` VALUES (478, 42535, 'irfan', '设置推荐会员：42586', 2047485628, 1605338929);
INSERT INTO `cmf_admin_log` VALUES (479, 42535, 'irfan', '取消热门会员：42585', 2047485628, 1605338945);
INSERT INTO `cmf_admin_log` VALUES (480, 42535, 'irfan', '添加会员：42590', 2047485597, 1605429686);
INSERT INTO `cmf_admin_log` VALUES (481, 42535, 'irfan', '添加会员：42591', 2047485597, 1605430523);
INSERT INTO `cmf_admin_log` VALUES (482, 42535, 'irfan', '添加会员：42592', 2047485597, 1605430553);
INSERT INTO `cmf_admin_log` VALUES (483, 42535, 'irfan', '添加会员：42593', 2047485597, 1605430574);
INSERT INTO `cmf_admin_log` VALUES (484, 42535, 'irfan', '添加会员：42594', 2047485597, 1605430619);
INSERT INTO `cmf_admin_log` VALUES (485, 42535, 'irfan', '添加会员：42595', 2047485597, 1605430646);
INSERT INTO `cmf_admin_log` VALUES (486, 42535, 'irfan', '添加会员：42596', 2047485597, 1605430699);
INSERT INTO `cmf_admin_log` VALUES (487, 42535, 'irfan', '添加会员：42597', 2047485597, 1605430721);
INSERT INTO `cmf_admin_log` VALUES (488, 42535, 'irfan', '添加会员：42598', 2047485597, 1605430745);
INSERT INTO `cmf_admin_log` VALUES (489, 42535, 'irfan', '添加会员：42599', 2047485597, 1605430783);
INSERT INTO `cmf_admin_log` VALUES (490, 42535, 'irfan', '添加会员：42600', 2047485597, 1605430812);
INSERT INTO `cmf_admin_log` VALUES (491, 42535, 'irfan', '添加会员：42601', 2047485597, 1605430837);
INSERT INTO `cmf_admin_log` VALUES (492, 42535, 'irfan', '添加会员：42602', 2047485597, 1605430866);
INSERT INTO `cmf_admin_log` VALUES (493, 42535, 'irfan', '添加会员：42603', 2047485597, 1605430887);
INSERT INTO `cmf_admin_log` VALUES (494, 42535, 'irfan', '添加会员：42604', 2047485597, 1605430935);
INSERT INTO `cmf_admin_log` VALUES (495, 42535, 'irfan', '添加会员：42605', 2047485597, 1605430954);
INSERT INTO `cmf_admin_log` VALUES (496, 42535, 'irfan', '添加会员：42606', 2047485597, 1605430972);
INSERT INTO `cmf_admin_log` VALUES (497, 42535, 'irfan', '添加会员：42607', 2047485597, 1605431046);
INSERT INTO `cmf_admin_log` VALUES (498, 42535, 'irfan', '添加会员：42608', 2047485597, 1605431063);
INSERT INTO `cmf_admin_log` VALUES (499, 42535, 'irfan', '添加会员：42609', 2047485597, 1605431093);
INSERT INTO `cmf_admin_log` VALUES (500, 42535, 'irfan', '修改会员信息：42596', 2047485597, 1605433201);
INSERT INTO `cmf_admin_log` VALUES (501, 42535, 'irfan', '修改会员认证信息：42590 - 同意', 2047485597, 1605434843);
INSERT INTO `cmf_admin_log` VALUES (502, 42535, 'irfan', '修改会员认证信息：42591 - 同意', 2047485597, 1605434851);
INSERT INTO `cmf_admin_log` VALUES (503, 42535, 'irfan', '修改会员认证信息：42592 - 同意', 2047485597, 1605434859);
INSERT INTO `cmf_admin_log` VALUES (504, 42535, 'irfan', '修改会员认证信息：42593 - 同意', 2047485597, 1605434866);
INSERT INTO `cmf_admin_log` VALUES (505, 42535, 'irfan', '修改会员认证信息：42594 - 同意', 2047485597, 1605434874);
INSERT INTO `cmf_admin_log` VALUES (506, 42535, 'irfan', '修改会员认证信息：42595 - 同意', 2047485597, 1605434880);
INSERT INTO `cmf_admin_log` VALUES (507, 42535, 'irfan', '修改会员认证信息：42596 - 同意', 2047485597, 1605434888);
INSERT INTO `cmf_admin_log` VALUES (508, 42535, 'irfan', '修改会员认证信息：42597 - 同意', 2047485597, 1605434894);
INSERT INTO `cmf_admin_log` VALUES (509, 42535, 'irfan', '修改会员认证信息：42598 - 同意', 2047485597, 1605434904);
INSERT INTO `cmf_admin_log` VALUES (510, 42535, 'irfan', '修改会员认证信息：42599 - 同意', 2047485597, 1605434912);
INSERT INTO `cmf_admin_log` VALUES (511, 42535, 'irfan', '修改会员认证信息：42600 - 同意', 2047485597, 1605434920);
INSERT INTO `cmf_admin_log` VALUES (512, 42535, 'irfan', '修改会员认证信息：42601 - 同意', 2047485597, 1605434928);
INSERT INTO `cmf_admin_log` VALUES (513, 42535, 'irfan', '修改会员认证信息：42602 - 同意', 2047485597, 1605434997);
INSERT INTO `cmf_admin_log` VALUES (514, 42535, 'irfan', '修改会员认证信息：42603 - 同意', 2047485597, 1605435005);
INSERT INTO `cmf_admin_log` VALUES (515, 42535, 'irfan', '修改会员认证信息：42604 - 同意', 2047485597, 1605435091);
INSERT INTO `cmf_admin_log` VALUES (516, 42535, 'irfan', '修改会员认证信息：42605 - 同意', 2047485597, 1605435098);
INSERT INTO `cmf_admin_log` VALUES (517, 42535, 'irfan', '修改会员认证信息：42606 - 同意', 2047485597, 1605435106);
INSERT INTO `cmf_admin_log` VALUES (518, 42535, 'irfan', '修改会员认证信息：42607 - 同意', 2047485597, 1605435113);
INSERT INTO `cmf_admin_log` VALUES (519, 42535, 'irfan', '修改会员认证信息：42608 - 同意', 2047485597, 1605435120);
INSERT INTO `cmf_admin_log` VALUES (520, 42535, 'irfan', '修改会员认证信息：42609 - 同意', 2047485597, 1605435126);
INSERT INTO `cmf_admin_log` VALUES (521, 42494, 'messi', '修改公共配置 修改房间类型 关闭1;密码房间 ', 1892641726, 1605511193);
INSERT INTO `cmf_admin_log` VALUES (522, 42494, 'messi', '修改公共配置 修改房间类型 开启1;密码房间 ', 1892641726, 1605511250);
INSERT INTO `cmf_admin_log` VALUES (523, 42494, 'messi', '修改公共配置 修改房间类型 开启2;门票房间 开启3;计时房间 ', 1892641726, 1605511856);
INSERT INTO `cmf_admin_log` VALUES (524, 1, 'admin', '监控 关闭直播间：42515', 2047485572, 1605524971);
INSERT INTO `cmf_admin_log` VALUES (525, 1, 'admin', '更新直播推荐值', 2047485572, 1605525077);
INSERT INTO `cmf_admin_log` VALUES (526, 42494, 'messi', '禁用会员：42504', 1892641726, 1605528952);
INSERT INTO `cmf_admin_log` VALUES (527, 42494, 'messi', '启用会员：42504', 1892641726, 1605528958);
INSERT INTO `cmf_admin_log` VALUES (528, 42494, 'messi', '禁用会员：42521', 1892641726, 1605528979);
INSERT INTO `cmf_admin_log` VALUES (529, 42494, 'messi', '启用会员：42521', 1892641726, 1605531425);
INSERT INTO `cmf_admin_log` VALUES (530, 1, 'admin', '修改直播分类：9', 2047485572, 1605531757);
INSERT INTO `cmf_admin_log` VALUES (531, 1, 'admin', '删除直播分类：11', 2047485572, 1605531763);
INSERT INTO `cmf_admin_log` VALUES (532, 1, 'admin', '删除直播分类：12', 2047485572, 1605531766);
INSERT INTO `cmf_admin_log` VALUES (533, 1, 'admin', '删除直播分类：17', 2047485572, 1605531769);
INSERT INTO `cmf_admin_log` VALUES (534, 1, 'admin', '删除直播分类：16', 2047485572, 1605531773);
INSERT INTO `cmf_admin_log` VALUES (535, 1, 'admin', '删除直播分类：13', 2047485572, 1605531777);
INSERT INTO `cmf_admin_log` VALUES (536, 1, 'admin', '修改会员认证信息：42609 - 拒绝', 2047485572, 1605532511);
INSERT INTO `cmf_admin_log` VALUES (537, 1, 'admin', '直播管理-直播列表添加UID：42506', 2047485572, 1605533927);
INSERT INTO `cmf_admin_log` VALUES (538, 1, 'admin', '直播管理-直播列表添加UID：42598', 2047485572, 1605534138);
INSERT INTO `cmf_admin_log` VALUES (539, 1, 'admin', '更新直播推荐值', 2047485572, 1605534168);
INSERT INTO `cmf_admin_log` VALUES (540, 1, 'admin', '设置推荐会员：42598', 2047485572, 1605534379);
INSERT INTO `cmf_admin_log` VALUES (541, 1, 'admin', '直播管理-直播列表添加UID：42595', 2047485572, 1605534478);
INSERT INTO `cmf_admin_log` VALUES (542, 1, 'admin', '直播管理-直播列表添加UID：42594', 2047485572, 1605534487);
INSERT INTO `cmf_admin_log` VALUES (543, 1, 'admin', '设置推荐会员：42594', 2047485572, 1605534530);
INSERT INTO `cmf_admin_log` VALUES (544, 1, 'admin', '设置推荐会员：42595', 2047485572, 1605534540);
INSERT INTO `cmf_admin_log` VALUES (545, 42494, 'messi', '更新直播推荐值', 1892641726, 1605595083);
INSERT INTO `cmf_admin_log` VALUES (546, 1, 'admin', '修改公共配置 网站域名 https://jiextx.com ', 2047485572, 1605674357);
INSERT INTO `cmf_admin_log` VALUES (547, 1, 'admin', '修改公共配置 网站名称 荣耀直播 ', 2047485572, 1605675162);
INSERT INTO `cmf_admin_log` VALUES (548, 1, 'admin', '修改公共配置 修改微信推广域名 https://jiextx.com/wxshare/Share/show?roomnum= ', 2047485572, 1605675171);
INSERT INTO `cmf_admin_log` VALUES (549, 1, 'admin', '修改公共配置 修改房间类型 关闭2;门票房间 关闭3;计时房间 ', 2047485572, 1605675177);
INSERT INTO `cmf_admin_log` VALUES (550, 1, 'admin', '修改公共配置 修改隐私政策跳转链接 https://jiextx.com/portal/page/index?id=3 修改服务协议跳转链接 https://jiextx.com/portal/page/index?id=4 ', 2047485572, 1605675185);
INSERT INTO `cmf_admin_log` VALUES (551, 1, 'admin', '修改公共配置 修改APK版本号 1.0.0 修改IPA版本号 1.0.0 ', 2047485572, 1605675247);
INSERT INTO `cmf_admin_log` VALUES (552, 1, 'admin', '修私密配置 聊天服务器带端口 https://jiextx.com:80 ', 2047485572, 1605675289);
INSERT INTO `cmf_admin_log` VALUES (553, 1, 'admin', '修改会员认证信息：42499 - 同意', 2047485572, 1605676306);
INSERT INTO `cmf_admin_log` VALUES (554, 1, 'admin', '修私密配置 聊天服务器带端口 https://jiextx.com:443 ', 2047485572, 1605678935);
INSERT INTO `cmf_admin_log` VALUES (555, 42494, 'messi', '修改会员认证信息：42563 - 同意', 1892641726, 1605679823);
INSERT INTO `cmf_admin_log` VALUES (556, 42494, 'messi', '修改会员认证信息：42501 - 同意', 1892641726, 1605698828);
INSERT INTO `cmf_admin_log` VALUES (557, 42512, 'kane', '开启僵尸粉会员：42513', 1892641726, 1605704001);
INSERT INTO `cmf_admin_log` VALUES (558, 42512, 'kane', '开启会员僵尸粉：42513', 1892641726, 1605704011);
INSERT INTO `cmf_admin_log` VALUES (559, 42512, 'kane', '关闭会员僵尸粉：42513', 1892641726, 1605704737);
INSERT INTO `cmf_admin_log` VALUES (560, 42512, 'kane', '关闭僵尸粉会员：42513', 1892641726, 1605704745);
INSERT INTO `cmf_admin_log` VALUES (561, 42512, 'kane', '开启僵尸粉会员：42513', 1892641726, 1605705122);
INSERT INTO `cmf_admin_log` VALUES (562, 42512, 'kane', '开启会员僵尸粉：42513', 1892641726, 1605705129);
INSERT INTO `cmf_admin_log` VALUES (563, 42512, 'kane', '更新直播推荐值', 1892641726, 1605757697);
INSERT INTO `cmf_admin_log` VALUES (564, 42494, 'messi', '更新直播推荐值', 1892641726, 1605757907);
INSERT INTO `cmf_admin_log` VALUES (565, 1, 'admin', '修私密配置 腾讯云直播播流域名 https://play.jiextx.com ', 2047485572, 1605765943);
INSERT INTO `cmf_admin_log` VALUES (566, 1, 'admin', '修私密配置 腾讯云直播播流域名 play.jiextx.com ', 2047485572, 1605766065);
INSERT INTO `cmf_admin_log` VALUES (567, 1, 'admin', '修改公共配置 修改APK版本号 1.1 修改IPA版本号 1.1 ', 2047485572, 1605768331);
INSERT INTO `cmf_admin_log` VALUES (568, 1, 'admin', '修改公共配置 修改APK版本号 1.0 修改IPA版本号 1.0 ', 2047485572, 1605769080);
INSERT INTO `cmf_admin_log` VALUES (569, 1, 'admin', '修私密配置 聊天服务器带端口 https://www.jiextx.com:443 ', 2047485572, 1605773595);
INSERT INTO `cmf_admin_log` VALUES (570, 42491, 'regame', '开启僵尸粉会员：42613,42612,42611,42610,42609,42608,42607,42606,42605,42604,42603,42602,42601,42600,42599,42598,42597,42596,42595,42594', 1892641768, 1605785538);
INSERT INTO `cmf_admin_log` VALUES (571, 42491, 'regame', '开启全部会员僵尸粉', 1892641768, 1605785544);
INSERT INTO `cmf_admin_log` VALUES (572, 42491, 'regame', '修改会员认证信息：42566 - 同意', 1892641768, 1605785596);
INSERT INTO `cmf_admin_log` VALUES (573, 42494, 'messi', '关闭僵尸粉会员：42521', 1892641726, 1605786497);
INSERT INTO `cmf_admin_log` VALUES (574, 42494, 'messi', '禁用会员：42499', 1892641726, 1605787047);
INSERT INTO `cmf_admin_log` VALUES (575, 42494, 'messi', '启用会员：42499', 1892641726, 1605787297);
INSERT INTO `cmf_admin_log` VALUES (576, 42512, 'kane', '关闭僵尸粉会员：42513', 1892641726, 1605790465);
INSERT INTO `cmf_admin_log` VALUES (577, 42512, 'kane', '关闭会员僵尸粉：42513', 1892641726, 1605790474);
INSERT INTO `cmf_admin_log` VALUES (578, 42512, 'kane', '开启僵尸粉会员：42513', 1892641726, 1605790483);
INSERT INTO `cmf_admin_log` VALUES (579, 42512, 'kane', '开启会员僵尸粉：42513', 1892641726, 1605790497);
INSERT INTO `cmf_admin_log` VALUES (580, 1, 'admin', '编辑主播等级：1', 3232258049, 1605795370);
INSERT INTO `cmf_admin_log` VALUES (581, 1, 'admin', '编辑主播等级：2', 3232258049, 1605795477);
INSERT INTO `cmf_admin_log` VALUES (582, 1, 'admin', '编辑主播等级：3', 3232258049, 1605795547);
INSERT INTO `cmf_admin_log` VALUES (583, 1, 'admin', '编辑主播等级：4', 3232258049, 1605795652);
INSERT INTO `cmf_admin_log` VALUES (584, 1, 'admin', '编辑主播等级：5', 1892780182, 1605801063);
INSERT INTO `cmf_admin_log` VALUES (585, 1, 'admin', '编辑主播等级：5', 1892780182, 1605801521);
INSERT INTO `cmf_admin_log` VALUES (586, 1, 'admin', '编辑主播等级：6', 1892780182, 1605801565);
INSERT INTO `cmf_admin_log` VALUES (587, 1, 'admin', '编辑主播等级：6', 1892780182, 1605801573);
INSERT INTO `cmf_admin_log` VALUES (588, 1, 'admin', '编辑主播等级：10', 1892780182, 1605801624);
INSERT INTO `cmf_admin_log` VALUES (589, 1, 'admin', '编辑主播等级：10', 1892780182, 1605801636);
INSERT INTO `cmf_admin_log` VALUES (590, 1, 'admin', '编辑主播等级：7', 1892780182, 1605801681);
INSERT INTO `cmf_admin_log` VALUES (591, 1, 'admin', '编辑主播等级：8', 1892780182, 1605801732);
INSERT INTO `cmf_admin_log` VALUES (592, 1, 'admin', '编辑主播等级：8', 1892780182, 1605801793);
INSERT INTO `cmf_admin_log` VALUES (593, 1, 'admin', '编辑主播等级：9', 1892780182, 1605801829);
INSERT INTO `cmf_admin_log` VALUES (594, 42494, 'messi', '更新直播推荐值', 1892641726, 1605842768);
INSERT INTO `cmf_admin_log` VALUES (595, 42494, 'messi', '推送信息ID：11', 1892641726, 1605850003);
INSERT INTO `cmf_admin_log` VALUES (596, 42491, 'regame', '关闭会员僵尸粉：42501', 1892639843, 1605850601);
INSERT INTO `cmf_admin_log` VALUES (597, 1, 'admin', '监控 关闭直播间：42566', 2130706433, 1605937558);
INSERT INTO `cmf_admin_log` VALUES (598, 1, 'admin', '监控 关闭直播间：42515', 2130706433, 1605937561);
INSERT INTO `cmf_admin_log` VALUES (599, 1, 'admin', '监控 关闭直播间：42501', 2130706433, 1605937563);
INSERT INTO `cmf_admin_log` VALUES (600, 1, 'admin', '修改上传设置 图片文件 大小: 1024 扩展名: jpg,jpeg,png,gif,bmp4,svga,apk  ', 2130706433, 1605961220);
INSERT INTO `cmf_admin_log` VALUES (601, 1, 'admin', '修改上传设置 图片文件 大小: 1024 扩展名: jpg,jpeg,png,gif,bmp4,svga  附件 大小: 102400 扩展名: txt,pdf,doc,docx,xls,xlsx,ppt,pptx,svga,mp4,apk  ', 2130706433, 1605961258);
INSERT INTO `cmf_admin_log` VALUES (602, 1, 'admin', '修改公共配置 修改APK下载链接 ', 2130706433, 1605962289);
INSERT INTO `cmf_admin_log` VALUES (603, 1, 'admin', '修改公共配置 修改APK下载链接 ', 2130706433, 1606027542);
INSERT INTO `cmf_admin_log` VALUES (604, 1, 'admin', '修改公共配置 修改APK下载链接 ', 2130706433, 1606031468);
INSERT INTO `cmf_admin_log` VALUES (605, 1, 'admin', '修改公共配置 修改APK下载链接 ', 2130706433, 1606033456);
INSERT INTO `cmf_admin_log` VALUES (606, 1, 'admin', '修改公共配置 修改APK下载链接 ', 2130706433, 1606033468);
INSERT INTO `cmf_admin_log` VALUES (607, 1, 'admin', '修改公共配置 修改APK下载链接 ', 2130706433, 1606043886);
INSERT INTO `cmf_admin_log` VALUES (608, 1, 'admin', '直播管理-直播列表添加UID：42507', 2130706433, 1606115183);
INSERT INTO `cmf_admin_log` VALUES (609, 1, 'admin', '直播管理-直播列表添加UID：42523', 2130706433, 1606115201);
INSERT INTO `cmf_admin_log` VALUES (610, 1, 'admin', '直播管理-直播列表添加UID：42506', 2130706433, 1606115214);
INSERT INTO `cmf_admin_log` VALUES (611, 1, 'admin', '直播管理-直播列表修改UID：42506', 2130706433, 1606279971);
INSERT INTO `cmf_admin_log` VALUES (612, 1, 'admin', '直播管理-直播列表修改UID：42506', 2130706433, 1606279998);
INSERT INTO `cmf_admin_log` VALUES (613, 1, 'admin', '修私密配置 敏感词 ', 2130706433, 1606297090);
INSERT INTO `cmf_admin_log` VALUES (614, 1, 'admin', '添加会员：42614', 2130706433, 1606374039);
INSERT INTO `cmf_admin_log` VALUES (615, 1, 'admin', '添加会员：42615', 2130706433, 1606374228);
INSERT INTO `cmf_admin_log` VALUES (616, 1, 'admin', '添加会员：42616', 2130706433, 1606374399);
INSERT INTO `cmf_admin_log` VALUES (617, 1, 'admin', '添加动态举报类型：10', 2130706433, 1606374541);
INSERT INTO `cmf_admin_log` VALUES (618, 1, 'admin', '删除动态举报类型：10', 2130706433, 1606374551);
INSERT INTO `cmf_admin_log` VALUES (619, 1, 'admin', '添加会员：42617', 2130706433, 1606374968);
INSERT INTO `cmf_admin_log` VALUES (620, 1, 'admin', '更新直播推荐值', 2130706433, 1606375281);
INSERT INTO `cmf_admin_log` VALUES (621, 1, 'admin', '添加会员：42618', 2130706433, 1606375494);
INSERT INTO `cmf_admin_log` VALUES (622, 1, 'admin', '添加会员：42619', 2130706433, 1606376756);
INSERT INTO `cmf_admin_log` VALUES (623, 1, 'admin', '修改公共配置 修改APK下载链接 修改IPA下载链接 https://www.yinongpin1211.com/EWgu.app ', 2130706433, 1606377801);
INSERT INTO `cmf_admin_log` VALUES (624, 1, 'admin', '监控 关闭直播间：42545', 2130706433, 1606971846);
INSERT INTO `cmf_admin_log` VALUES (625, 1, 'admin', '直播管理-直播列表修改UID：42595', 2130706433, 1606972216);
INSERT INTO `cmf_admin_log` VALUES (626, 1, 'admin', '直播管理-直播列表修改UID：42598', 2130706433, 1606972227);
INSERT INTO `cmf_admin_log` VALUES (627, 1, 'admin', '直播管理-直播列表添加UID：42492', 2130706433, 1606972245);
INSERT INTO `cmf_admin_log` VALUES (628, 1, 'admin', '直播管理-直播列表添加UID：1', 2130706433, 1606974166);
INSERT INTO `cmf_admin_log` VALUES (629, 1, 'admin', '直播管理-直播列表删除UID：1', 2130706433, 1606974177);
INSERT INTO `cmf_admin_log` VALUES (630, 1, 'admin', '直播管理-直播列表添加UID：1', 2130706433, 1606974337);
INSERT INTO `cmf_admin_log` VALUES (631, 1, 'admin', '修改公共配置 修改IPA类型 2 ', 2130706433, 1606993791);
INSERT INTO `cmf_admin_log` VALUES (632, 1, 'admin', '修改公共配置 修改APK版本号 1.2 修改APK下载链接 修改IPA上架版本号 1.0 修改IPA类型 1 修改IPA TF签下载链接 https://testflight.apple.com/join/fuvPkxAA 修改IPA 企业签下载链接 itms-services://?action=download-manifest&url=https://7niuapk.jiextx.com/PhoneLive1.2_20201203.plist 修改IPA 超级签下载链接 https://www.dongbang520.com/TeGT.app ', 2130706433, 1607057432);
INSERT INTO `cmf_admin_log` VALUES (633, 1, 'admin', '直播管理-直播列表修改UID：42492', 2130706433, 1607060917);
INSERT INTO `cmf_admin_log` VALUES (634, 1, 'admin', '直播管理-直播列表修改UID：42523', 2130706433, 1607060928);
INSERT INTO `cmf_admin_log` VALUES (635, 1, 'admin', '修私密配置 短信验证码IP限制次数 -1 ', 2130706433, 1610772167);
INSERT INTO `cmf_admin_log` VALUES (636, 1, 'admin', '修私密配置 短信验证码IP限制次数 10 ', 2130706433, 1610772174);
INSERT INTO `cmf_admin_log` VALUES (637, 1, 'admin', '添加会员：42620', 2130706433, 1611830031);

-- ----------------------------
-- Table structure for cmf_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `cmf_admin_menu`;
CREATE TABLE `cmf_admin_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父菜单id',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '菜单类型;1:有界面可访问菜单,2:无界面可访问菜单,0:只作为菜单',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态;1:显示,0:不显示',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `app` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '应用名',
  `controller` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '控制器名',
  `action` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作名称',
  `param` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '额外参数',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `controller`(`controller`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 507 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '后台菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_admin_menu
-- ----------------------------
INSERT INTO `cmf_admin_menu` VALUES (1, 0, 0, 1, 10000, 'admin', 'Plugin', 'default', '', '插件中心', 'cloud', '插件中心');
INSERT INTO `cmf_admin_menu` VALUES (6, 0, 0, 1, 3, 'admin', 'Setting', 'default', '', '设置', 'cogs', '系统设置入口');
INSERT INTO `cmf_admin_menu` VALUES (7, 6, 1, 0, 50, 'admin', 'Link', 'index', '', '友情链接', '', '友情链接管理');
INSERT INTO `cmf_admin_menu` VALUES (8, 7, 1, 0, 10000, 'admin', 'Link', 'add', '', '添加友情链接', '', '添加友情链接');
INSERT INTO `cmf_admin_menu` VALUES (9, 7, 2, 0, 10000, 'admin', 'Link', 'addPost', '', '添加友情链接提交保存', '', '添加友情链接提交保存');
INSERT INTO `cmf_admin_menu` VALUES (10, 7, 1, 0, 10000, 'admin', 'Link', 'edit', '', '编辑友情链接', '', '编辑友情链接');
INSERT INTO `cmf_admin_menu` VALUES (11, 7, 2, 0, 10000, 'admin', 'Link', 'editPost', '', '编辑友情链接提交保存', '', '编辑友情链接提交保存');
INSERT INTO `cmf_admin_menu` VALUES (12, 7, 2, 0, 10000, 'admin', 'Link', 'delete', '', '删除友情链接', '', '删除友情链接');
INSERT INTO `cmf_admin_menu` VALUES (13, 7, 2, 0, 10000, 'admin', 'Link', 'listOrder', '', '友情链接排序', '', '友情链接排序');
INSERT INTO `cmf_admin_menu` VALUES (14, 7, 2, 0, 10000, 'admin', 'Link', 'toggle', '', '友情链接显示隐藏', '', '友情链接显示隐藏');
INSERT INTO `cmf_admin_menu` VALUES (20, 0, 1, 0, 10000, 'admin', 'Menu', 'index', '', '后台菜单', '', '后台菜单管理');
INSERT INTO `cmf_admin_menu` VALUES (21, 20, 1, 0, 10000, 'admin', 'Menu', 'lists', '', '所有菜单', '', '后台所有菜单列表');
INSERT INTO `cmf_admin_menu` VALUES (22, 20, 1, 0, 10000, 'admin', 'Menu', 'add', '', '后台菜单添加', '', '后台菜单添加');
INSERT INTO `cmf_admin_menu` VALUES (23, 20, 2, 0, 10000, 'admin', 'Menu', 'addPost', '', '后台菜单添加提交保存', '', '后台菜单添加提交保存');
INSERT INTO `cmf_admin_menu` VALUES (24, 20, 1, 0, 10000, 'admin', 'Menu', 'edit', '', '后台菜单编辑', '', '后台菜单编辑');
INSERT INTO `cmf_admin_menu` VALUES (25, 20, 2, 0, 10000, 'admin', 'Menu', 'editPost', '', '后台菜单编辑提交保存', '', '后台菜单编辑提交保存');
INSERT INTO `cmf_admin_menu` VALUES (26, 20, 2, 0, 10000, 'admin', 'Menu', 'delete', '', '后台菜单删除', '', '后台菜单删除');
INSERT INTO `cmf_admin_menu` VALUES (27, 20, 2, 0, 10000, 'admin', 'Menu', 'listOrder', '', '后台菜单排序', '', '后台菜单排序');
INSERT INTO `cmf_admin_menu` VALUES (28, 20, 1, 0, 10000, 'admin', 'Menu', 'getActions', '', '导入新后台菜单', '', '导入新后台菜单');
INSERT INTO `cmf_admin_menu` VALUES (42, 1, 1, 1, 10000, 'admin', 'Plugin', 'index', '', '插件列表', '', '插件列表');
INSERT INTO `cmf_admin_menu` VALUES (43, 42, 2, 0, 10000, 'admin', 'Plugin', 'toggle', '', '插件启用禁用', '', '插件启用禁用');
INSERT INTO `cmf_admin_menu` VALUES (44, 42, 1, 0, 10000, 'admin', 'Plugin', 'setting', '', '插件设置', '', '插件设置');
INSERT INTO `cmf_admin_menu` VALUES (45, 42, 2, 0, 10000, 'admin', 'Plugin', 'settingPost', '', '插件设置提交', '', '插件设置提交');
INSERT INTO `cmf_admin_menu` VALUES (46, 42, 2, 0, 10000, 'admin', 'Plugin', 'install', '', '插件安装', '', '插件安装');
INSERT INTO `cmf_admin_menu` VALUES (47, 42, 2, 0, 10000, 'admin', 'Plugin', 'update', '', '插件更新', '', '插件更新');
INSERT INTO `cmf_admin_menu` VALUES (48, 42, 2, 0, 10000, 'admin', 'Plugin', 'uninstall', '', '卸载插件', '', '卸载插件');
INSERT INTO `cmf_admin_menu` VALUES (49, 110, 0, 1, 10000, 'admin', 'User', 'default', '', '管理组', '', '管理组');
INSERT INTO `cmf_admin_menu` VALUES (50, 49, 1, 1, 10000, 'admin', 'Rbac', 'index', '', '角色管理', '', '角色管理');
INSERT INTO `cmf_admin_menu` VALUES (51, 50, 1, 0, 10000, 'admin', 'Rbac', 'roleAdd', '', '添加角色', '', '添加角色');
INSERT INTO `cmf_admin_menu` VALUES (52, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleAddPost', '', '添加角色提交', '', '添加角色提交');
INSERT INTO `cmf_admin_menu` VALUES (53, 50, 1, 0, 10000, 'admin', 'Rbac', 'roleEdit', '', '编辑角色', '', '编辑角色');
INSERT INTO `cmf_admin_menu` VALUES (54, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleEditPost', '', '编辑角色提交', '', '编辑角色提交');
INSERT INTO `cmf_admin_menu` VALUES (55, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleDelete', '', '删除角色', '', '删除角色');
INSERT INTO `cmf_admin_menu` VALUES (56, 50, 1, 0, 10000, 'admin', 'Rbac', 'authorize', '', '设置角色权限', '', '设置角色权限');
INSERT INTO `cmf_admin_menu` VALUES (57, 50, 2, 0, 10000, 'admin', 'Rbac', 'authorizePost', '', '角色授权提交', '', '角色授权提交');
INSERT INTO `cmf_admin_menu` VALUES (58, 0, 1, 0, 10000, 'admin', 'RecycleBin', 'index', '', '回收站', '', '回收站');
INSERT INTO `cmf_admin_menu` VALUES (59, 58, 2, 0, 10000, 'admin', 'RecycleBin', 'restore', '', '回收站还原', '', '回收站还原');
INSERT INTO `cmf_admin_menu` VALUES (60, 58, 2, 0, 10000, 'admin', 'RecycleBin', 'delete', '', '回收站彻底删除', '', '回收站彻底删除');
INSERT INTO `cmf_admin_menu` VALUES (71, 6, 1, 1, 0, 'admin', 'Setting', 'site', '', '网站信息', '', '网站信息');
INSERT INTO `cmf_admin_menu` VALUES (72, 71, 2, 0, 10000, 'admin', 'Setting', 'sitePost', '', '网站信息设置提交', '', '网站信息设置提交');
INSERT INTO `cmf_admin_menu` VALUES (73, 0, 1, 0, 1, 'admin', 'Setting', 'password', '', '密码修改', '', '密码修改');
INSERT INTO `cmf_admin_menu` VALUES (74, 73, 2, 0, 10000, 'admin', 'Setting', 'passwordPost', '', '密码修改提交', '', '密码修改提交');
INSERT INTO `cmf_admin_menu` VALUES (75, 6, 1, 1, 10000, 'admin', 'Setting', 'upload', '', '上传设置', '', '上传设置');
INSERT INTO `cmf_admin_menu` VALUES (76, 75, 2, 0, 10000, 'admin', 'Setting', 'uploadPost', '', '上传设置提交', '', '上传设置提交');
INSERT INTO `cmf_admin_menu` VALUES (77, 6, 1, 0, 10000, 'admin', 'Setting', 'clearCache', '', '清除缓存', '', '清除缓存');
INSERT INTO `cmf_admin_menu` VALUES (78, 6, 1, 1, 40, 'admin', 'Slide', 'index', '', '幻灯片管理', '', '幻灯片管理');
INSERT INTO `cmf_admin_menu` VALUES (79, 78, 1, 0, 10000, 'admin', 'Slide', 'add', '', '添加幻灯片', '', '添加幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (80, 78, 2, 0, 10000, 'admin', 'Slide', 'addPost', '', '添加幻灯片提交', '', '添加幻灯片提交');
INSERT INTO `cmf_admin_menu` VALUES (81, 78, 1, 0, 10000, 'admin', 'Slide', 'edit', '', '编辑幻灯片', '', '编辑幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (82, 78, 2, 0, 10000, 'admin', 'Slide', 'editPost', '', '编辑幻灯片提交', '', '编辑幻灯片提交');
INSERT INTO `cmf_admin_menu` VALUES (83, 78, 2, 0, 10000, 'admin', 'Slide', 'delete', '', '删除幻灯片', '', '删除幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (84, 78, 1, 0, 10000, 'admin', 'SlideItem', 'index', '', '幻灯片页面列表', '', '幻灯片页面列表');
INSERT INTO `cmf_admin_menu` VALUES (85, 84, 1, 0, 10000, 'admin', 'SlideItem', 'add', '', '幻灯片页面添加', '', '幻灯片页面添加');
INSERT INTO `cmf_admin_menu` VALUES (86, 84, 2, 0, 10000, 'admin', 'SlideItem', 'addPost', '', '幻灯片页面添加提交', '', '幻灯片页面添加提交');
INSERT INTO `cmf_admin_menu` VALUES (87, 84, 1, 0, 10000, 'admin', 'SlideItem', 'edit', '', '幻灯片页面编辑', '', '幻灯片页面编辑');
INSERT INTO `cmf_admin_menu` VALUES (88, 84, 2, 0, 10000, 'admin', 'SlideItem', 'editPost', '', '幻灯片页面编辑提交', '', '幻灯片页面编辑提交');
INSERT INTO `cmf_admin_menu` VALUES (89, 84, 2, 0, 10000, 'admin', 'SlideItem', 'delete', '', '幻灯片页面删除', '', '幻灯片页面删除');
INSERT INTO `cmf_admin_menu` VALUES (90, 84, 2, 0, 10000, 'admin', 'SlideItem', 'ban', '', '幻灯片页面隐藏', '', '幻灯片页面隐藏');
INSERT INTO `cmf_admin_menu` VALUES (91, 84, 2, 0, 10000, 'admin', 'SlideItem', 'cancelBan', '', '幻灯片页面显示', '', '幻灯片页面显示');
INSERT INTO `cmf_admin_menu` VALUES (92, 84, 2, 0, 10000, 'admin', 'SlideItem', 'listOrder', '', '幻灯片页面排序', '', '幻灯片页面排序');
INSERT INTO `cmf_admin_menu` VALUES (93, 6, 1, 1, 10000, 'admin', 'Storage', 'index', '', '文件存储', '', '文件存储');
INSERT INTO `cmf_admin_menu` VALUES (94, 93, 2, 0, 10000, 'admin', 'Storage', 'settingPost', '', '文件存储设置提交', '', '文件存储设置提交');
INSERT INTO `cmf_admin_menu` VALUES (110, 0, 0, 1, 4, 'user', 'AdminIndex', 'default', '', '用户管理', 'group', '用户管理');
INSERT INTO `cmf_admin_menu` VALUES (111, 49, 1, 1, 10000, 'admin', 'User', 'index', '', '管理员', '', '管理员管理');
INSERT INTO `cmf_admin_menu` VALUES (112, 111, 1, 0, 10000, 'admin', 'User', 'add', '', '管理员添加', '', '管理员添加');
INSERT INTO `cmf_admin_menu` VALUES (113, 111, 2, 0, 10000, 'admin', 'User', 'addPost', '', '管理员添加提交', '', '管理员添加提交');
INSERT INTO `cmf_admin_menu` VALUES (114, 111, 1, 0, 10000, 'admin', 'User', 'edit', '', '管理员编辑', '', '管理员编辑');
INSERT INTO `cmf_admin_menu` VALUES (115, 111, 2, 0, 10000, 'admin', 'User', 'editPost', '', '管理员编辑提交', '', '管理员编辑提交');
INSERT INTO `cmf_admin_menu` VALUES (116, 111, 1, 0, 10000, 'admin', 'User', 'userInfo', '', '个人信息', '', '管理员个人信息修改');
INSERT INTO `cmf_admin_menu` VALUES (117, 111, 2, 0, 10000, 'admin', 'User', 'userInfoPost', '', '管理员个人信息修改提交', '', '管理员个人信息修改提交');
INSERT INTO `cmf_admin_menu` VALUES (118, 111, 2, 0, 10000, 'admin', 'User', 'delete', '', '管理员删除', '', '管理员删除');
INSERT INTO `cmf_admin_menu` VALUES (119, 111, 2, 0, 10000, 'admin', 'User', 'ban', '', '停用管理员', '', '停用管理员');
INSERT INTO `cmf_admin_menu` VALUES (120, 111, 2, 0, 10000, 'admin', 'User', 'cancelBan', '', '启用管理员', '', '启用管理员');
INSERT INTO `cmf_admin_menu` VALUES (121, 0, 1, 0, 10000, 'user', 'AdminAsset', 'index', '', '资源管理', 'file', '资源管理列表');
INSERT INTO `cmf_admin_menu` VALUES (122, 121, 2, 0, 10000, 'user', 'AdminAsset', 'delete', '', '删除文件', '', '删除文件');
INSERT INTO `cmf_admin_menu` VALUES (123, 110, 0, 1, 10000, 'user', 'AdminIndex', 'default1', '', '用户组', '', '用户组');
INSERT INTO `cmf_admin_menu` VALUES (124, 123, 1, 1, 1, 'user', 'AdminIndex', 'index', '', '本站用户', '', '本站用户');
INSERT INTO `cmf_admin_menu` VALUES (125, 124, 2, 0, 10000, 'user', 'AdminIndex', 'ban', '', '本站用户拉黑', '', '本站用户拉黑');
INSERT INTO `cmf_admin_menu` VALUES (126, 124, 2, 0, 10000, 'user', 'AdminIndex', 'cancelBan', '', '本站用户启用', '', '本站用户启用');
INSERT INTO `cmf_admin_menu` VALUES (127, 49, 1, 1, 10000, 'admin', 'adminlog', 'index', '', '操作日志', '', '');
INSERT INTO `cmf_admin_menu` VALUES (128, 127, 1, 0, 10000, 'admin', 'adminlog', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (129, 127, 1, 0, 10000, 'admin', 'adminlog', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (130, 0, 1, 0, 15, 'admin', 'Agent', 'default', '', '邀请奖励', 'sitemap', '');
INSERT INTO `cmf_admin_menu` VALUES (131, 130, 1, 1, 10000, 'admin', 'agent', 'index', '', '邀请关系', '', '');
INSERT INTO `cmf_admin_menu` VALUES (132, 130, 1, 1, 10000, 'admin', 'agent', 'index2', '', '邀请收益', '', '');
INSERT INTO `cmf_admin_menu` VALUES (133, 0, 1, 0, 11, 'admin', 'car', 'default', '', '商城管理', 'shopping-cart', '');
INSERT INTO `cmf_admin_menu` VALUES (134, 133, 1, 1, 10000, 'admin', 'car', 'index', '', '坐骑管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (135, 134, 1, 0, 10000, 'admin', 'car', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (136, 134, 1, 0, 10000, 'admin', 'car', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (137, 134, 1, 0, 10000, 'admin', 'car', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (138, 134, 1, 0, 10000, 'admin', 'car', 'editPost', '', '编辑添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (139, 134, 1, 0, 10000, 'admin', 'car', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (140, 134, 1, 0, 10000, 'admin', 'car', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (141, 133, 1, 1, 10000, 'admin', 'liang', 'index', '', '靓号管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (142, 141, 1, 0, 10000, 'admin', 'liang', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (143, 141, 1, 0, 10000, 'admin', 'liang', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (144, 6, 1, 1, 1, 'admin', 'setting', 'configpri', '', '私密设置', '', '');
INSERT INTO `cmf_admin_menu` VALUES (145, 144, 1, 0, 10000, 'admin', 'setting', 'configpriPost', '', '提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (146, 0, 1, 0, 5, 'admin', 'jackpot', 'set', '', '奖池管理', 'trophy', '');
INSERT INTO `cmf_admin_menu` VALUES (147, 146, 1, 0, 10000, 'admin', 'jackpot', 'setPost', '', '设置提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (148, 146, 1, 0, 10000, 'admin', 'jackpot', 'index', '', '奖池等级', '', '');
INSERT INTO `cmf_admin_menu` VALUES (149, 148, 1, 0, 10000, 'admin', 'jackpot', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (150, 148, 1, 0, 10000, 'admin', 'jackpot', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (151, 148, 1, 0, 10000, 'admin', 'jackpot', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (152, 148, 1, 0, 10000, 'admin', 'jackpot', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (153, 148, 1, 0, 10000, 'admin', 'jackpot', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (154, 0, 1, 1, 7, 'admin', 'live', 'default', '', '直播管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (155, 154, 1, 1, 1, 'admin', 'liveclass', 'index', '', '直播分类', '', '');
INSERT INTO `cmf_admin_menu` VALUES (156, 155, 1, 0, 10000, 'admin', 'liveclass', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (157, 155, 1, 0, 10000, 'admin', 'liveclass', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (158, 155, 1, 0, 10000, 'admin', 'liveclass', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (159, 155, 1, 0, 10000, 'admin', 'liveclass', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (160, 155, 1, 0, 10000, 'admin', 'liveclass', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (161, 155, 1, 0, 10000, 'admin', 'liveclass', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (162, 154, 1, 1, 2, 'admin', 'liveban', 'index', '', '禁播管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (163, 162, 1, 0, 10000, 'admin', 'liveban', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (164, 154, 1, 1, 3, 'admin', 'liveshut', 'index', '', '禁言管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (165, 164, 1, 0, 10000, 'admin', 'liveshut', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (166, 154, 1, 1, 4, 'admin', 'livekick', 'index', '', '踢人管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (167, 166, 1, 0, 10000, 'admin', 'livekick', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (168, 154, 1, 1, 5, 'admin', 'liveing', 'index', '', '直播列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (169, 168, 1, 0, 10000, 'admin', 'liveing', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (170, 168, 1, 0, 10000, 'admin', 'liveing', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (171, 168, 1, 0, 10000, 'admin', 'liveing', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (172, 168, 1, 0, 10000, 'admin', 'liveing', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (173, 168, 1, 0, 10000, 'admin', 'liveing', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (174, 154, 1, 1, 6, 'admin', 'monitor', 'index', '', '直播监控', '', '');
INSERT INTO `cmf_admin_menu` VALUES (175, 174, 1, 0, 10000, 'admin', 'monitor', 'full', '', '大屏', '', '');
INSERT INTO `cmf_admin_menu` VALUES (176, 174, 1, 0, 10000, 'admin', 'monitor', 'stop', '', '关播', '', '');
INSERT INTO `cmf_admin_menu` VALUES (177, 154, 1, 0, 7, 'admin', 'gift', 'index', '', '礼物管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (178, 177, 1, 0, 10000, 'admin', 'gift', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (179, 177, 1, 0, 10000, 'admin', 'gift', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (180, 177, 1, 0, 10000, 'admin', 'gift', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (181, 177, 1, 0, 10000, 'admin', 'gift', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (182, 177, 1, 0, 10000, 'admin', 'gift', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (183, 177, 1, 0, 10000, 'admin', 'gift', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (184, 154, 1, 1, 9, 'admin', 'report', 'defult', '', '举报管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (185, 184, 1, 1, 10000, 'admin', 'reportcat', 'index', '', '举报分类', '', '');
INSERT INTO `cmf_admin_menu` VALUES (186, 185, 1, 0, 10000, 'admin', 'reportcat', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (187, 185, 1, 0, 10000, 'admin', 'reportcat', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (188, 185, 1, 0, 10000, 'admin', 'reportcat', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (189, 185, 1, 0, 10000, 'admin', 'reportcat', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (190, 185, 1, 0, 10000, 'admin', 'reportcat', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (191, 185, 1, 0, 10000, 'admin', 'reportcat', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (192, 184, 1, 1, 10000, 'admin', 'report', 'index', '', '举报列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (193, 192, 1, 0, 10000, 'admin', 'report', 'setstatus', '', '处理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (194, 154, 1, 1, 10, 'admin', 'liverecord', 'index', '', '直播记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (195, 0, 1, 0, 8, 'admin', 'video', 'default', '', '视频管理', 'video-camera', '');
INSERT INTO `cmf_admin_menu` VALUES (196, 195, 1, 1, 2, 'admin', 'music', 'index', '', '音乐管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (197, 195, 1, 1, 1, 'admin', 'musiccat', 'index', '', '音乐分类', '', '');
INSERT INTO `cmf_admin_menu` VALUES (198, 196, 1, 0, 10000, 'admin', 'music', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (199, 196, 1, 0, 10000, 'admin', 'music', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (200, 196, 1, 0, 10000, 'admin', 'music', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (201, 196, 1, 0, 10000, 'admin', 'music', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (202, 196, 1, 0, 10000, 'admin', 'music', 'listen', '', '试听', '', '');
INSERT INTO `cmf_admin_menu` VALUES (203, 196, 1, 0, 10000, 'admin', 'music', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (204, 196, 1, 0, 10000, 'admin', 'music', 'canceldel', '', '取消删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (205, 197, 1, 0, 10000, 'admin', 'musiccat', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (206, 197, 1, 0, 10000, 'admin', 'musiccat', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (207, 197, 1, 0, 10000, 'admin', 'musiccat', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (208, 197, 1, 0, 10000, 'admin', 'musiccat', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (209, 197, 1, 0, 10000, 'admin', 'musiccat', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (210, 197, 1, 0, 10000, 'admin', 'musiccat', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (211, 0, 1, 0, 16, 'admin', 'shop', 'default', '', '店铺管理', 'shopping-cart', '');
INSERT INTO `cmf_admin_menu` VALUES (212, 211, 1, 1, 1, 'admin', 'shopapply', 'index', '', '店铺申请', '', '');
INSERT INTO `cmf_admin_menu` VALUES (213, 212, 1, 0, 1, 'admin', 'shopapply', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (214, 212, 1, 0, 2, 'admin', 'shopapply', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (215, 212, 1, 0, 3, 'admin', 'shopapply', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (216, 211, 1, 1, 2, 'admin', 'shopbond', 'index', '', '保证金', '', '');
INSERT INTO `cmf_admin_menu` VALUES (217, 216, 1, 0, 10000, 'admin', 'shopbond', 'setstatus', '', '处理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (218, 211, 1, 1, 3, 'admin', 'shopgoods', 'index', '', '商品管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (219, 218, 1, 0, 1, 'admin', 'shopgoods', 'setstatus', '', '上下架', '', '');
INSERT INTO `cmf_admin_menu` VALUES (220, 218, 1, 0, 2, 'admin', 'shopgoods', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (221, 0, 1, 0, 17, 'admin', 'turntable', 'default', '', '大转盘', 'gamepad', '');
INSERT INTO `cmf_admin_menu` VALUES (222, 221, 1, 1, 1, 'admin', 'turntablecon', 'index', '', '价格管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (223, 222, 1, 0, 10000, 'admin', 'turntablecon', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (224, 222, 1, 0, 10000, 'admin', 'turntablecon', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (225, 222, 1, 0, 10000, 'admin', 'turntablecon', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (226, 221, 1, 1, 2, 'admin', 'turntable', 'index', '', '奖品管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (227, 226, 1, 0, 10000, 'admin', 'turntable', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (228, 226, 1, 0, 10000, 'admin', 'turntable', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (229, 221, 1, 1, 3, 'admin', 'turntable', 'index2', '', '转盘记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (230, 221, 1, 1, 4, 'admin', 'turntable', 'index3', '', '线下奖品', '', '');
INSERT INTO `cmf_admin_menu` VALUES (231, 230, 1, 0, 10000, 'admin', 'turntable', 'setstatus', '', '处理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (232, 0, 1, 1, 14, 'admin', 'level', 'default', '', '等级管理', 'level-up', '');
INSERT INTO `cmf_admin_menu` VALUES (233, 232, 1, 1, 10000, 'admin', 'level', 'index', '', '用户等级', '', '');
INSERT INTO `cmf_admin_menu` VALUES (234, 233, 1, 0, 10000, 'admin', 'level', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (235, 233, 1, 0, 10000, 'admin', 'level', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (236, 233, 1, 0, 10000, 'admin', 'level', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (237, 233, 1, 0, 10000, 'admin', 'level', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (238, 233, 1, 0, 10000, 'admin', 'level', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (239, 232, 1, 1, 10000, 'admin', 'levelanchor', 'index', '', '主播等级', '', '');
INSERT INTO `cmf_admin_menu` VALUES (240, 239, 1, 0, 10000, 'admin', 'levelanchor', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (241, 239, 1, 0, 10000, 'admin', 'levelanchor', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (242, 239, 1, 0, 10000, 'admin', 'levelanchor', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (243, 239, 1, 0, 10000, 'admin', 'levelanchor', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (244, 239, 1, 0, 10000, 'admin', 'levelanchor', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (245, 0, 1, 0, 13, 'admin', 'guard', 'index', '', '守护管理', 'shield', '');
INSERT INTO `cmf_admin_menu` VALUES (246, 245, 1, 0, 10000, 'admin', 'guard', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (247, 245, 1, 0, 10000, 'admin', 'guard', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (248, 141, 1, 0, 10000, 'admin', 'liang', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (249, 141, 1, 0, 10000, 'admin', 'liang', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (250, 141, 1, 0, 10000, 'admin', 'liang', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (251, 141, 1, 0, 10000, 'admin', 'liang', 'setstatus', '', '设置状态', '', '');
INSERT INTO `cmf_admin_menu` VALUES (252, 141, 1, 0, 10000, 'admin', 'liang', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (253, 133, 1, 1, 10000, 'admin', 'vip', 'default', '', 'VIP管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (254, 253, 1, 1, 1, 'admin', 'vip', 'index', '', 'VIP列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (255, 254, 1, 0, 10000, 'admin', 'vip', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (256, 254, 1, 0, 10000, 'admin', 'vip', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (257, 254, 1, 0, 10000, 'admin', 'vip', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (258, 253, 1, 1, 2, 'admin', 'vipuser', 'index', '', 'VIP用户', '', '');
INSERT INTO `cmf_admin_menu` VALUES (259, 258, 1, 0, 10000, 'admin', 'vipuser', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (260, 258, 1, 0, 10000, 'admin', 'vipuser', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (261, 258, 1, 0, 10000, 'admin', 'vipuser', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (262, 258, 1, 0, 10000, 'admin', 'vipuser', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (263, 258, 1, 0, 10000, 'admin', 'vipuser', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (264, 0, 1, 0, 10, 'admin', 'family', 'default', '', '家族管理', 'university', '');
INSERT INTO `cmf_admin_menu` VALUES (265, 264, 1, 1, 10000, 'admin', 'family', 'index', '', '家族列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (266, 265, 1, 0, 10000, 'admin', 'family', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (267, 265, 1, 0, 10000, 'admin', 'family', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (268, 265, 1, 0, 10000, 'admin', 'family', 'disable', '', '禁用', '', '');
INSERT INTO `cmf_admin_menu` VALUES (269, 265, 1, 0, 10000, 'admin', 'family', 'enable', '', '启用', '', '');
INSERT INTO `cmf_admin_menu` VALUES (270, 265, 1, 0, 10000, 'admin', 'family', 'profit', '', '收益', '', '');
INSERT INTO `cmf_admin_menu` VALUES (271, 265, 1, 0, 10000, 'admin', 'family', 'cash', '', '提现', '', '');
INSERT INTO `cmf_admin_menu` VALUES (272, 265, 1, 0, 10000, 'admin', 'family', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (273, 264, 1, 1, 10000, 'admin', 'familyuser', 'index', '', '成员管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (274, 273, 1, 0, 10000, 'admin', 'familyuser', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (275, 273, 1, 0, 10000, 'admin', 'familyuser', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (276, 273, 1, 0, 10000, 'admin', 'familyuser', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (277, 273, 1, 0, 10000, 'admin', 'familyuser', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (278, 273, 1, 0, 10000, 'admin', 'familyuser', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (279, 0, 1, 0, 9, 'admin', 'finance', 'default', '', '财务管理', 'rmb', '');
INSERT INTO `cmf_admin_menu` VALUES (280, 279, 1, 1, 1, 'admin', 'chargerules', 'index', '', '充值规则', '', '');
INSERT INTO `cmf_admin_menu` VALUES (281, 280, 1, 0, 10000, 'admin', 'chargerules', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (282, 280, 1, 0, 10000, 'admin', 'chargerules', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (283, 280, 1, 0, 10000, 'admin', 'chargerules', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (284, 280, 1, 0, 10000, 'admin', 'chargerules', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (285, 280, 1, 0, 10000, 'admin', 'chargerules', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (286, 280, 1, 0, 10000, 'admin', 'chargerules', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (287, 279, 1, 1, 2, 'admin', 'charge', 'index', '', '钻石充值记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (288, 287, 1, 0, 10000, 'admin', 'charge', 'setpay', '', '确认支付', '', '');
INSERT INTO `cmf_admin_menu` VALUES (289, 287, 1, 0, 10000, 'admin', 'charge', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (290, 279, 1, 1, 3, 'admin', 'manual', 'index', '', '手动充值钻石', '', '');
INSERT INTO `cmf_admin_menu` VALUES (291, 290, 1, 0, 10000, 'admin', 'manual', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (292, 290, 1, 0, 10000, 'admin', 'manual', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (293, 290, 1, 0, 10000, 'admin', 'manual', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (294, 279, 1, 1, 4, 'admin', 'coinrecord', 'index', '', '钻石消费记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (295, 279, 1, 1, 5, 'admin', 'cash', 'index', '', '映票提现记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (296, 295, 1, 0, 10000, 'admin', 'cash', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (297, 295, 1, 0, 10000, 'admin', 'cash', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (298, 295, 1, 0, 10000, 'admin', 'cash', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (299, 6, 1, 1, 10000, 'admin', 'guide', 'set', '', '引导页', '', '');
INSERT INTO `cmf_admin_menu` VALUES (300, 299, 1, 0, 10000, 'admin', 'guide', 'setPost', '', '设置提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (301, 299, 1, 0, 10000, 'admin', 'guide', 'index', '', '管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (302, 301, 1, 0, 10000, 'admin', 'guide', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (303, 301, 1, 0, 10000, 'admin', 'guide', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (304, 301, 1, 0, 10000, 'admin', 'guide', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (305, 301, 1, 0, 10000, 'admin', 'guide', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (306, 301, 1, 0, 10000, 'admin', 'guide', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (307, 301, 1, 0, 10000, 'admin', 'guide', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (308, 0, 1, 1, 6, 'admin', 'auth', 'index', '', '身份认证', '', '');
INSERT INTO `cmf_admin_menu` VALUES (310, 308, 1, 0, 10000, 'admin', 'auth', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (311, 308, 1, 0, 10000, 'admin', 'auth', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (312, 195, 1, 1, 4, 'admin', 'video', 'index', 'isdel=0&status=1', '审核通过列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (313, 312, 1, 0, 10000, 'admin', 'video', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (314, 312, 1, 0, 10000, 'admin', 'video', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (315, 312, 1, 0, 10000, 'admin', 'video', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (316, 312, 1, 0, 10000, 'admin', 'video', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (317, 312, 1, 0, 10000, 'admin', 'video', 'setstatus', '', '上下架', '', '');
INSERT INTO `cmf_admin_menu` VALUES (318, 312, 1, 0, 10000, 'admin', 'video', 'see', '', '查看', '', '');
INSERT INTO `cmf_admin_menu` VALUES (319, 195, 1, 0, 8, 'admin', 'videocom', 'index', '', '视频评论', '', '');
INSERT INTO `cmf_admin_menu` VALUES (320, 319, 1, 0, 10000, 'admin', 'videocom', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (321, 195, 1, 1, 9, 'admin', 'videorepcat', 'index', '', '举报类型', '', '');
INSERT INTO `cmf_admin_menu` VALUES (322, 321, 1, 0, 10000, 'admin', 'videorepcat', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (323, 321, 1, 0, 10000, 'admin', 'videorepcat', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (324, 321, 1, 0, 10000, 'admin', 'videorepcat', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (325, 321, 1, 0, 10000, 'admin', 'videorepcat', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (326, 321, 1, 0, 10000, 'admin', 'videorepcat', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (327, 321, 1, 0, 10000, 'admin', 'videorepcat', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (328, 195, 1, 1, 10, 'admin', 'videorep', 'index', '', '举报列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (329, 328, 1, 0, 10000, 'admin', 'videorep', 'setstatus', '', '处理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (330, 328, 1, 0, 10000, 'admin', 'videorep', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (331, 0, 1, 0, 18, 'admin', 'Loginbonus', 'index', '', '登录奖励', '', '');
INSERT INTO `cmf_admin_menu` VALUES (332, 331, 1, 0, 10000, 'admin', 'Loginbonus', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (333, 331, 1, 0, 10000, 'admin', 'Loginbonus', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (334, 0, 1, 0, 12, 'admin', 'red', 'index', '', '红包管理', 'envelope', '');
INSERT INTO `cmf_admin_menu` VALUES (335, 334, 1, 0, 10000, 'admin', 'red', 'index2', '', '领取详情', '', '');
INSERT INTO `cmf_admin_menu` VALUES (336, 0, 1, 1, 19, 'admin', 'note', 'default', '', '消息管理', 'bell', '');
INSERT INTO `cmf_admin_menu` VALUES (337, 336, 1, 1, 10000, 'admin', 'sendcode', 'index', '', '验证码管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (338, 337, 1, 0, 10000, 'admin', 'sendcode', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (339, 337, 1, 0, 10000, 'admin', 'sendcode', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (340, 336, 1, 1, 10000, 'admin', 'push', 'index', '', '推送管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (341, 340, 1, 0, 10000, 'admin', 'push', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (342, 340, 1, 0, 10000, 'admin', 'push', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (343, 340, 1, 0, 10000, 'admin', 'push', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (344, 336, 1, 1, 10000, 'admin', 'system', 'index', '', '直播间消息', '', '');
INSERT INTO `cmf_admin_menu` VALUES (345, 344, 1, 0, 10000, 'admin', 'system', 'send', '', '发送', '', '');
INSERT INTO `cmf_admin_menu` VALUES (346, 123, 1, 0, 2, 'admin', 'Impression', 'index', '', '标签管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (347, 346, 1, 0, 10000, 'admin', 'Impression', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (348, 346, 1, 0, 10000, 'admin', 'Impression', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (349, 346, 1, 0, 10000, 'admin', 'Impression', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (350, 346, 1, 0, 10000, 'admin', 'Impression', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (351, 346, 1, 0, 10000, 'admin', 'Impression', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (352, 346, 1, 0, 10000, 'admin', 'Impression', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (353, 0, 1, 1, 20, 'admin', 'portal', 'default', '', '内容管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (354, 353, 1, 1, 10000, 'admin', 'feedback', 'index', '', '用户反馈', '', '');
INSERT INTO `cmf_admin_menu` VALUES (355, 354, 1, 0, 10000, 'admin', 'feedback', 'setstatus', '', '处理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (356, 354, 1, 0, 10000, 'admin', 'feedback', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (357, 353, 1, 1, 10000, 'portal', 'AdminPage', 'index', '', '页面管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (358, 357, 1, 0, 10000, 'portal', 'AdminPage', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (359, 357, 1, 0, 10000, 'portal', 'AdminPage', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (360, 357, 1, 0, 10000, 'portal', 'AdminPage', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (361, 357, 1, 0, 10000, 'portal', 'AdminPage', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (362, 357, 1, 0, 10000, 'portal', 'AdminPage', 'delete', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (363, 195, 1, 1, 3, 'admin', 'videoclass', 'index', '', '视频分类', '', '');
INSERT INTO `cmf_admin_menu` VALUES (364, 363, 1, 0, 10000, 'admin', 'videoclass', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (365, 363, 1, 0, 10000, 'admin', 'videoclass', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (366, 363, 1, 0, 10000, 'admin', 'videoclass', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (367, 363, 1, 0, 10000, 'admin', 'videoclass', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (368, 363, 1, 0, 10000, 'admin', 'videoclass', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (369, 363, 1, 0, 10000, 'admin', 'videoclass', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (370, 154, 1, 0, 8, 'admin', 'sticker', 'index', '', '贴纸列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (371, 370, 1, 0, 10000, 'admin', 'sticker', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (372, 370, 1, 0, 10000, 'admin', 'sticker', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (373, 370, 1, 0, 10000, 'admin', 'sticker', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (374, 370, 1, 0, 10000, 'admin', 'sticker', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (375, 370, 1, 0, 10000, 'admin', 'sticker', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (376, 370, 1, 0, 10000, 'admin', 'sticker', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (377, 0, 1, 1, 8, 'admin', 'Dynamic', 'default', '', '动态管理', 'star-o', '');
INSERT INTO `cmf_admin_menu` VALUES (378, 377, 1, 1, 10000, 'admin', 'Dynamic', 'index', 'isdel=0&status=1', '审核通过', '', '');
INSERT INTO `cmf_admin_menu` VALUES (379, 377, 1, 1, 10000, 'admin', 'Dynamic', 'wait', 'isdel=0&status=0', '等待审核', '', '');
INSERT INTO `cmf_admin_menu` VALUES (380, 377, 1, 1, 10000, 'admin', 'Dynamic', 'nopass', 'isdel=0&status=-1', '未通过', '', '');
INSERT INTO `cmf_admin_menu` VALUES (381, 377, 1, 1, 10000, 'admin', 'Dynamic', 'lower', 'isdel=1', '下架列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (382, 377, 1, 1, 10000, 'admin', 'Dynamicrepotcat', 'index', '', '举报类型', '', '');
INSERT INTO `cmf_admin_menu` VALUES (383, 382, 1, 0, 10000, 'admin', 'Dynamicrepotcat', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (384, 382, 1, 0, 10000, 'admin', 'Dynamicrepotcat', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (385, 382, 1, 0, 10000, 'admin', 'Dynamicrepotcat', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (386, 382, 1, 0, 10000, 'admin', 'Dynamicrepotcat', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (387, 382, 1, 0, 10000, 'admin', 'Dynamicrepotcat', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (388, 382, 1, 0, 10000, 'admin', 'Dynamicrepotcat', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (389, 377, 1, 1, 10000, 'admin', 'Dynamicrepot', 'index', '', '举报列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (390, 389, 1, 0, 10000, 'admin', 'Dynamicrepot', 'setstatus', '', '处理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (391, 389, 1, 0, 10000, 'admin', 'Dynamicrepot', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (392, 377, 1, 0, 10000, 'admin', 'Dynamiccom', 'index', '', '评论列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (393, 392, 1, 0, 10000, 'admin', 'Dynamiccom', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (394, 195, 1, 1, 5, 'admin', 'video', 'wait', 'isdel=0&status=0&is_draft=0', '待审核列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (395, 195, 1, 1, 6, 'admin', 'video', 'nopass', 'isdel=0&status=2', '未通过列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (396, 195, 1, 1, 7, 'admin', 'video', 'lower', 'isdel=1', '下架列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (397, 378, 1, 0, 10000, 'admin', 'dynamic', 'setstatus', '', '审核', '', '');
INSERT INTO `cmf_admin_menu` VALUES (398, 378, 1, 0, 10000, 'admin', 'dynamic', 'see', '', '查看', '', '');
INSERT INTO `cmf_admin_menu` VALUES (399, 378, 1, 0, 10000, 'admin', 'dynamic', 'setdel', '', '上下架', '', '');
INSERT INTO `cmf_admin_menu` VALUES (400, 378, 1, 0, 10000, 'admin', 'dynamic', 'setrecom', '', '设置推荐值', '', '');
INSERT INTO `cmf_admin_menu` VALUES (401, 378, 1, 0, 10000, 'admin', 'Dynamic', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (402, 154, 1, 0, 11, 'admin', 'game', 'index', '', '游戏记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (403, 402, 1, 0, 10000, 'admin', 'game', 'index2', '', 'x详情', '', '');
INSERT INTO `cmf_admin_menu` VALUES (404, 124, 1, 0, 10000, 'user', 'AdminIndex', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (405, 124, 1, 0, 10000, 'user', 'AdminIndex', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (406, 124, 1, 0, 10000, 'user', 'AdminIndex', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (407, 124, 1, 0, 10000, 'user', 'AdminIndex', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (408, 124, 1, 0, 10000, 'user', 'AdminIndex', 'setban', '', '禁用时间', '', '');
INSERT INTO `cmf_admin_menu` VALUES (409, 124, 1, 0, 10000, 'user', 'AdminIndex', 'setsuper', '', '设置、取消超管', '', '');
INSERT INTO `cmf_admin_menu` VALUES (410, 124, 1, 0, 10000, 'user', 'AdminIndex', 'sethot', '', '设置取消热门', '', '');
INSERT INTO `cmf_admin_menu` VALUES (411, 124, 1, 0, 10000, 'user', 'AdminIndex', 'setrecommend', '', '设置取消推荐', '', '');
INSERT INTO `cmf_admin_menu` VALUES (412, 124, 1, 0, 10000, 'user', 'AdminIndex', 'setzombie', '', '开启关闭僵尸粉', '', '');
INSERT INTO `cmf_admin_menu` VALUES (413, 124, 1, 0, 10000, 'user', 'AdminIndex', 'setzombiep', '', '设置取消为僵尸粉', '', '');
INSERT INTO `cmf_admin_menu` VALUES (414, 124, 1, 0, 10000, 'user', 'adminIndex', 'setzombiepall', '', '批量设置/取消为僵尸粉', '', '');
INSERT INTO `cmf_admin_menu` VALUES (415, 124, 1, 0, 10000, 'user', 'adminIndex', 'setzombieall', '', '一键开启/关闭僵尸粉', '', '');
INSERT INTO `cmf_admin_menu` VALUES (416, 211, 1, 1, 4, 'Admin', 'Goodsclass', 'index', '', '商品分类列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (417, 416, 1, 0, 1, 'Admin', 'Goodsclass', 'add', '', '商品分类添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (418, 211, 1, 1, 5, 'Admin', 'Buyeraddress', 'index', '', '收货地址管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (419, 211, 1, 1, 6, 'Admin', 'Express', 'index', '', '物流公司列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (420, 419, 1, 0, 1, 'Admin', 'Express', 'add', '', '物流公司添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (421, 420, 1, 0, 10000, 'Admin', 'Express', 'add_post', '', '物流公司添加保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (422, 419, 1, 0, 2, 'Admin', 'Express', 'edit', '', '物流公司编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (423, 422, 1, 0, 10000, 'Admin', 'Express', 'edit_post', '', '物流公司编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (424, 419, 1, 0, 3, 'Admin', 'Express', 'del', '', '物流公司删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (425, 419, 1, 0, 4, 'Admin', 'Express', 'listOrder', '', '物流公司排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (427, 211, 1, 1, 7, 'Admin', 'Refundreason', 'index', '', '买家申请退款原因', '', '');
INSERT INTO `cmf_admin_menu` VALUES (428, 427, 1, 0, 1, 'Admin', 'Refundreason', 'add', '', '添加退款原因', '', '');
INSERT INTO `cmf_admin_menu` VALUES (429, 428, 1, 0, 10000, 'Admin', 'Refundreason', 'add_post', '', '添加保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (430, 427, 1, 0, 2, 'Admin', 'Refundreason', 'edit', '', '编辑退款原因', '', '');
INSERT INTO `cmf_admin_menu` VALUES (431, 430, 1, 0, 10000, 'Admin', 'Refundreason', 'edit_post', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (432, 427, 1, 0, 3, 'Admin', 'Refundreason', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (433, 427, 1, 0, 4, 'Admin', 'Refundreason', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (434, 211, 1, 1, 8, 'Admin', 'Refusereason', 'index', '', '卖家拒绝退款原因', '', '');
INSERT INTO `cmf_admin_menu` VALUES (435, 211, 1, 1, 9, 'Admin', 'Platformreason', 'index', '', '退款平台介入原因', '', '');
INSERT INTO `cmf_admin_menu` VALUES (436, 211, 1, 1, 10, 'Admin', 'Goodsorder', 'index', '', '商品订单列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (437, 211, 1, 1, 11, 'Admin', 'Refundlist', 'index', '', '退款列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (438, 211, 1, 1, 12, 'Admin', 'Shopcash', 'index', '', '提现记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (439, 0, 1, 0, 21, 'admin', 'paidprogram', 'default', '', '付费内容', 'imdb', '');
INSERT INTO `cmf_admin_menu` VALUES (440, 434, 1, 0, 1, 'Admin', 'Refusereason', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (441, 440, 1, 0, 10000, 'Admin', 'Refusereason', 'add_post', '', '添加保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (442, 434, 1, 0, 2, 'Admin', 'Refusereason', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (443, 442, 1, 0, 10000, 'Admin', 'Refusereason', 'edit_post', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (444, 434, 1, 0, 3, 'Admin', 'Refusereason', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (445, 434, 1, 0, 4, 'Admin', 'Refusereason', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (446, 435, 1, 0, 1, 'Admin', 'Platformreason', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (447, 446, 1, 0, 10000, 'Admin', 'Platformreason', 'add_post', '', '添加保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (448, 435, 1, 0, 2, 'Admin', 'Platformreason', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (449, 448, 1, 0, 10000, 'Admin', 'Platformreason', 'edit_post', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (450, 435, 1, 0, 3, 'Admin', 'Platformreason', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (451, 435, 1, 0, 4, 'Admin', 'Platformreason', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (452, 436, 1, 0, 1, 'Admin', 'Goodsorder', 'info', '', '订单详情', '', '');
INSERT INTO `cmf_admin_menu` VALUES (453, 437, 1, 0, 1, 'Admin', 'Refundlist', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (454, 453, 1, 0, 1, 'Admin', 'Refundlist', 'edit_post', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (455, 438, 1, 0, 1, 'Admin', 'Shopcash', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (456, 455, 1, 0, 1, 'Admin', 'Shopcash', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (457, 438, 1, 0, 2, 'Admin', 'Shopcash', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (458, 439, 1, 1, 1, 'Admin', 'Paidprogramclass', 'classlist', '', '付费内容分类列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (459, 458, 1, 0, 1, 'Admin', 'Paidprogramclass', 'class_add', '', '添加付费内容分类', '', '');
INSERT INTO `cmf_admin_menu` VALUES (460, 459, 1, 0, 10000, 'Admin', 'Paidprogramclass', 'class_add_post', '', '添加保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (461, 458, 1, 0, 2, 'Admin', 'Paidprogramclass', 'class_edit', '', '编辑付费内容分类', '', '');
INSERT INTO `cmf_admin_menu` VALUES (462, 461, 1, 0, 10000, 'Admin', 'Paidprogramclass', 'class_edit_post', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (463, 458, 1, 0, 3, 'Admin', 'Paidprogramclass', 'class_del', '', '付费内容分类删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (464, 458, 1, 0, 4, 'Admin', 'Paidprogramclass', 'listOrder', '', '付费内容分类排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (465, 439, 1, 1, 2, 'Admin', 'Paidprogram', 'index', '', '付费内容列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (466, 439, 1, 1, 3, 'Admin', 'Paidprogram', 'applylist', '', '付费内容申请列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (467, 439, 1, 1, 4, 'Admin', 'Paidprogram', 'orderlist', '', '付费内容订单', '', '');
INSERT INTO `cmf_admin_menu` VALUES (468, 417, 1, 0, 10000, 'Admin', 'Goodsclass', 'addPost', '', '商品分类添加保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (469, 218, 1, 0, 3, 'Admin', 'Shopgoods', 'edit', '', '商品审核/详情', '', '');
INSERT INTO `cmf_admin_menu` VALUES (470, 218, 1, 0, 4, 'Admin', 'Shopgoods', 'editPost', '', '商品审核提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (471, 416, 1, 0, 2, 'Admin', 'Goodsclass', 'edit', '', '商品分类编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (472, 471, 1, 0, 10000, 'Admin', 'Goodsclass', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (473, 416, 1, 0, 3, 'Admin', 'Goodsclass', 'del', '', '商品分类删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (474, 418, 1, 0, 1, 'Admin', 'Buyeraddress', 'del', '', '收货地址删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (475, 218, 1, 0, 5, 'Admin', 'Shopgoods', 'commentlist', '', '评价列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (476, 475, 1, 0, 1, 'Admin', 'Shopgoods', 'delComment', '', '删除评价', '', '');
INSERT INTO `cmf_admin_menu` VALUES (477, 465, 1, 0, 1, 'Admin', 'Paidprogram', 'edit', '', '编辑付费内容', '', '');
INSERT INTO `cmf_admin_menu` VALUES (478, 477, 1, 0, 10000, 'Admin', 'Paidprogram', 'edit_post', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (479, 465, 1, 0, 10000, 'Admin', 'Paidprogram', 'del', '', '删除付费内容', '', '');
INSERT INTO `cmf_admin_menu` VALUES (480, 465, 1, 0, 10000, 'Admin', 'Paidprogram', 'videoplay', '', '付费内容观看', '', '');
INSERT INTO `cmf_admin_menu` VALUES (481, 466, 1, 0, 1, 'Admin', 'Paidprogram', 'apply_edit', '', '编辑付费内容申请', '', '');
INSERT INTO `cmf_admin_menu` VALUES (482, 481, 1, 0, 1, 'Admin', 'Paidprogram', 'apply_edit_post', '', '付费内容申请编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (483, 466, 1, 0, 2, 'Admin', 'Paidprogram', 'apply_del', '', '删除付费内容申请', '', '');
INSERT INTO `cmf_admin_menu` VALUES (484, 467, 1, 0, 10000, 'Admin', 'Paidprogram', 'setPay', '', '确认支付', '', '');
INSERT INTO `cmf_admin_menu` VALUES (485, 467, 1, 0, 10000, 'Admin', 'Paidprogram', 'export', '', '导出付费内容订单', '', '');
INSERT INTO `cmf_admin_menu` VALUES (487, 211, 1, 1, 13, 'Admin', 'Balance', 'index', '', '余额手动充值', '', '');
INSERT INTO `cmf_admin_menu` VALUES (488, 487, 1, 0, 10000, 'Admin', 'Balance', 'add', '', '充值添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (489, 488, 1, 0, 10000, 'Admin', 'Balance', 'addPost', '', '余额充值保存', '', '');
INSERT INTO `cmf_admin_menu` VALUES (490, 487, 1, 0, 10000, 'Admin', 'Balance', 'export', '', '余额充值记录导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (492, 264, 1, 1, 10000, 'admin', 'familyuser', 'divideapply', '', '分成申请列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (493, 492, 1, 0, 10000, 'admin', 'familyuser', 'applyedit', '', '审核', '', '');
INSERT INTO `cmf_admin_menu` VALUES (494, 493, 1, 0, 10000, 'admin', 'familyuser', 'applyeditPost', '', '审核提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (495, 492, 1, 0, 10000, 'admin', 'familyuser', 'delapply', '', '删除审核', '', '');
INSERT INTO `cmf_admin_menu` VALUES (496, 279, 1, 1, 10000, 'admin', 'Scorerecord', 'index', '', '积分记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (497, 377, 1, 1, 10000, 'admin', 'Dynamiclabel', 'index', '', '话题标签', '', '');
INSERT INTO `cmf_admin_menu` VALUES (498, 497, 1, 0, 10000, 'admin', 'Dynamiclabel', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (499, 498, 1, 0, 10000, 'admin', 'Dynamiclabel', 'add_post', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (500, 497, 1, 0, 10000, 'admin', 'Dynamiclabel', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (501, 500, 1, 0, 10000, 'admin', 'Dynamiclabel', 'edit_post', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (502, 497, 1, 0, 10000, 'admin', 'Dynamiclabel', 'listsorders', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (503, 497, 1, 0, 10000, 'admin', 'Dynamiclabel', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (504, 124, 1, 0, 10000, 'user', 'AdminIndex', 'del', '', '本站用户删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (505, 123, 1, 1, 10000, 'user', 'AdminIndex', 'authorindex', '', '主播管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (506, 6, 1, 1, 2, 'admin', 'domain', 'index', '', '域名管理', '', '');

-- ----------------------------
-- Table structure for cmf_agent
-- ----------------------------
DROP TABLE IF EXISTS `cmf_agent`;
CREATE TABLE `cmf_agent`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `one_uid` int(11) NOT NULL DEFAULT 0 COMMENT '上级用户id',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_agent
-- ----------------------------
INSERT INTO `cmf_agent` VALUES (1, 42520, 42513, 1604383481);

-- ----------------------------
-- Table structure for cmf_agent_code
-- ----------------------------
DROP TABLE IF EXISTS `cmf_agent_code`;
CREATE TABLE `cmf_agent_code`  (
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '邀请码',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_agent_code
-- ----------------------------
INSERT INTO `cmf_agent_code` VALUES (42486, 'TDC260');
INSERT INTO `cmf_agent_code` VALUES (42487, 'KZ7ZCQ');
INSERT INTO `cmf_agent_code` VALUES (42488, 'hwPMY8');
INSERT INTO `cmf_agent_code` VALUES (42492, 'xC4K05');
INSERT INTO `cmf_agent_code` VALUES (42493, '4A7Lr6');
INSERT INTO `cmf_agent_code` VALUES (42496, 'T3EWR8');
INSERT INTO `cmf_agent_code` VALUES (42497, '50Z4UL');
INSERT INTO `cmf_agent_code` VALUES (42498, 'CYK9EY');
INSERT INTO `cmf_agent_code` VALUES (42499, 'XR4880');
INSERT INTO `cmf_agent_code` VALUES (42500, '3KNQWG');
INSERT INTO `cmf_agent_code` VALUES (42501, '4P79KS');
INSERT INTO `cmf_agent_code` VALUES (42502, 'Th18Jb');
INSERT INTO `cmf_agent_code` VALUES (42503, 'WZUW1U');
INSERT INTO `cmf_agent_code` VALUES (42504, 'M52KE8');
INSERT INTO `cmf_agent_code` VALUES (42505, '168FMN');
INSERT INTO `cmf_agent_code` VALUES (42506, 'g8y8li');
INSERT INTO `cmf_agent_code` VALUES (42507, '7LsdEB');
INSERT INTO `cmf_agent_code` VALUES (42508, 'Q3uzuQ');
INSERT INTO `cmf_agent_code` VALUES (42509, '3qLSLE');
INSERT INTO `cmf_agent_code` VALUES (42510, 'n9mRby');
INSERT INTO `cmf_agent_code` VALUES (42511, 'UFmD9b');
INSERT INTO `cmf_agent_code` VALUES (42513, '0Yh9FU');
INSERT INTO `cmf_agent_code` VALUES (42514, 'E5JXFA');
INSERT INTO `cmf_agent_code` VALUES (42515, '2NFcMm');
INSERT INTO `cmf_agent_code` VALUES (42516, 'pOSug9');
INSERT INTO `cmf_agent_code` VALUES (42517, '39L84S');
INSERT INTO `cmf_agent_code` VALUES (42520, 'kCyrL7');
INSERT INTO `cmf_agent_code` VALUES (42521, 'PRY8GS');
INSERT INTO `cmf_agent_code` VALUES (42522, '0C9R2N');
INSERT INTO `cmf_agent_code` VALUES (42523, '6CWDZ6');
INSERT INTO `cmf_agent_code` VALUES (42524, 'XE659R');
INSERT INTO `cmf_agent_code` VALUES (42525, 'HT64LQ');
INSERT INTO `cmf_agent_code` VALUES (42526, '8RAgPQ');
INSERT INTO `cmf_agent_code` VALUES (42527, 'ZZT679');
INSERT INTO `cmf_agent_code` VALUES (42528, 'YJC9DB');
INSERT INTO `cmf_agent_code` VALUES (42529, 'NURG9N');
INSERT INTO `cmf_agent_code` VALUES (42530, 'Q56CJP');
INSERT INTO `cmf_agent_code` VALUES (42531, 'FXGLV8');
INSERT INTO `cmf_agent_code` VALUES (42532, 'CE29TE');
INSERT INTO `cmf_agent_code` VALUES (42533, 'r3lKvW');
INSERT INTO `cmf_agent_code` VALUES (42534, 'TUF4ZG');
INSERT INTO `cmf_agent_code` VALUES (42536, 'KHAJ4T');
INSERT INTO `cmf_agent_code` VALUES (42537, '9D5PVA');
INSERT INTO `cmf_agent_code` VALUES (42541, 'NPV8D4');
INSERT INTO `cmf_agent_code` VALUES (42542, '3hx66W');
INSERT INTO `cmf_agent_code` VALUES (42544, 'd56ISJ');
INSERT INTO `cmf_agent_code` VALUES (42545, 'n7F6JN');
INSERT INTO `cmf_agent_code` VALUES (42547, 'o8fVix');
INSERT INTO `cmf_agent_code` VALUES (42548, 'EB1ASK');
INSERT INTO `cmf_agent_code` VALUES (42549, 'Lq32S1');
INSERT INTO `cmf_agent_code` VALUES (42551, 'J4Z1A4');
INSERT INTO `cmf_agent_code` VALUES (42552, '3U2SUV');
INSERT INTO `cmf_agent_code` VALUES (42553, 'BPU88L');
INSERT INTO `cmf_agent_code` VALUES (42554, '2G0VZ3');
INSERT INTO `cmf_agent_code` VALUES (42555, '4N5G8X');
INSERT INTO `cmf_agent_code` VALUES (42556, '2J4PV7');
INSERT INTO `cmf_agent_code` VALUES (42557, 'BWCE8A');
INSERT INTO `cmf_agent_code` VALUES (42558, 'N4N3PF');
INSERT INTO `cmf_agent_code` VALUES (42559, 'TWV6NJ');
INSERT INTO `cmf_agent_code` VALUES (42560, 'C98XQ4');
INSERT INTO `cmf_agent_code` VALUES (42561, 'ZK8XU5');
INSERT INTO `cmf_agent_code` VALUES (42562, '7HLUZV');
INSERT INTO `cmf_agent_code` VALUES (42563, 'K42M7J');
INSERT INTO `cmf_agent_code` VALUES (42564, 'YYENX5');
INSERT INTO `cmf_agent_code` VALUES (42565, 'W9HLAS');
INSERT INTO `cmf_agent_code` VALUES (42566, '82xv7T');
INSERT INTO `cmf_agent_code` VALUES (42567, 'vo5zsK');
INSERT INTO `cmf_agent_code` VALUES (42568, '6TA6LW');
INSERT INTO `cmf_agent_code` VALUES (42569, '5U5HFW');
INSERT INTO `cmf_agent_code` VALUES (42570, 'nNh9AM');
INSERT INTO `cmf_agent_code` VALUES (42571, 'JRCN7P');
INSERT INTO `cmf_agent_code` VALUES (42572, 'iqSr6z');
INSERT INTO `cmf_agent_code` VALUES (42575, 'X9899P');
INSERT INTO `cmf_agent_code` VALUES (42576, '4RC8S1');
INSERT INTO `cmf_agent_code` VALUES (42577, 'D7L8SN');
INSERT INTO `cmf_agent_code` VALUES (42578, 'QH4N83');
INSERT INTO `cmf_agent_code` VALUES (42579, 'HN7L5B');
INSERT INTO `cmf_agent_code` VALUES (42580, '21ZXWZ');
INSERT INTO `cmf_agent_code` VALUES (42581, 'Cz8bpr');
INSERT INTO `cmf_agent_code` VALUES (42582, 'XD05UT');
INSERT INTO `cmf_agent_code` VALUES (42583, 'z5UlV5');
INSERT INTO `cmf_agent_code` VALUES (42584, 'RJYG7T');
INSERT INTO `cmf_agent_code` VALUES (42585, 'mJ48kH');
INSERT INTO `cmf_agent_code` VALUES (42586, '1KP2YJ');
INSERT INTO `cmf_agent_code` VALUES (42588, '4NC6BN');
INSERT INTO `cmf_agent_code` VALUES (42589, 'R4H3WK');
INSERT INTO `cmf_agent_code` VALUES (42610, 'vk19uH');
INSERT INTO `cmf_agent_code` VALUES (42611, 'QOFpw6');
INSERT INTO `cmf_agent_code` VALUES (42612, 'FDXGQ8');
INSERT INTO `cmf_agent_code` VALUES (42613, '7BSCCU');

-- ----------------------------
-- Table structure for cmf_agent_profit
-- ----------------------------
DROP TABLE IF EXISTS `cmf_agent_profit`;
CREATE TABLE `cmf_agent_profit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT 0 COMMENT '用户ID',
  `one_profit` decimal(65, 2) DEFAULT 0.00 COMMENT '一级收益',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_agent_profit_recode
-- ----------------------------
DROP TABLE IF EXISTS `cmf_agent_profit_recode`;
CREATE TABLE `cmf_agent_profit_recode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT 0 COMMENT '用户ID',
  `total` int(11) DEFAULT 0 COMMENT '消费总数',
  `one_uid` int(11) DEFAULT 0 COMMENT '一级ID',
  `one_profit` decimal(65, 2) DEFAULT 0.00 COMMENT '一级收益',
  `addtime` int(11) DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_asset
-- ----------------------------
DROP TABLE IF EXISTS `cmf_asset`;
CREATE TABLE `cmf_asset`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `file_size` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小,单位B',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传时间',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:可用,0:不可用',
  `download_times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '下载次数',
  `file_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件惟一码',
  `filename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `file_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件路径,相对于upload目录,可以为url',
  `file_md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件md5值',
  `file_sha1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `suffix` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件后缀名,不包括点',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '其它详细信息,JSON格式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 175 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '资源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_asset
-- ----------------------------
INSERT INTO `cmf_asset` VALUES (1, 1, 69386, 1604119156, 1, 0, '3799fad63b608df89d06f691e741549249ecd1f08fe66fb5e05c4607bf31337e', '121133.mp3', 'admin/20201031/8f317363f0c7a56473daffe77ea89ef5.jpg', '3799fad63b608df89d06f691e7415492', '43d6a6041cf4aa20885a4c800c2c557c3f92f444', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (2, 1, 84962, 1604119225, 1, 0, 'b7f0f6f88542f0536e7712a212e1b585ceb8c1ef214dcc56e096dd4f2a3f248f', '5c93539723ab8.jpg', 'admin/20201031/a51a3770eda8e59deba1ad0f7efc4509.jpg', 'b7f0f6f88542f0536e7712a212e1b585', 'b64bbf8d581cd1a049e992a3f213d05cd826c9aa', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (3, 1, 14999, 1604119516, 1, 0, '179adb33659403bc45c9cc7eb7549682e085db1e7717f6e30db48023af3532f9', 'img_234d3ada19bbd3b747bf43f71e7258ae.jpg', 'admin/20201031/1e47438a4ef50d3ef6d81719ab55d5d9.jpg', '179adb33659403bc45c9cc7eb7549682', 'e4bd81583946eb465973ddb2f74785ce9f02be8a', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (4, 1, 123832, 1604121325, 1, 0, 'd3515d805913b8ce2a518427feddaba09ba9d107d2a0b6260c8359b736f9bf16', '44f0bf80f75a3de3.jpg', 'admin/20201031/5d421b5ad827aedc88b4d51396cd841c.jpg', 'd3515d805913b8ce2a518427feddaba0', '104f7dc814172e0f8d09a9b81ba5c8113983d5df', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (5, 1, 13624, 1604164109, 1, 0, 'e60713c765d4cc410c8d913f9e0b389bd977d74fce69c41b97e866ed278a3f3a', '50_50.jpg', 'admin/20201101/3d0d65b131daab3be7b1848e55ea0228.jpg', 'e60713c765d4cc410c8d913f9e0b389b', '97adb048508c4637ea1bd2a8d329fc2066ace378', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (6, 1, 41053, 1604164468, 1, 0, '64303cc4eddd2c6533ea5d59924c3ae7f3f1f6631490f84e94053bcebd44af93', 'u=135040455,1092927311&fm=26&gp=0.jpg', 'user/20201101/2bedfcea6338a36bb436c0efdf8014c3.jpg', '64303cc4eddd2c6533ea5d59924c3ae7', 'd609304cf2748e38d185db8de4dafb79adb39577', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (7, 1, 29580, 1604164923, 1, 0, '722c93f6263750d8e8fab3ab3acb38a2678664e7b5c31748394ca60c793f43f1', 'u=1906469856,4113625838&fm=26&gp=0.jpg', 'user/20201101/c3cc807c20a28edbcfed1e731c2e2166.jpg', '722c93f6263750d8e8fab3ab3acb38a2', '44039e0de66b47289d04d416b2d17711fa8e8bfa', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (8, 1, 8924190, 1604166320, 1, 0, '8761df4c56b53d73fcc79e2b9822cd315140eb959f937e13ab669247721680bb', 'Adrift-HD.mp4.mp4', 'admin/20201101/2f7f893d464e4db47bd0a287ea555056.mp4', '8761df4c56b53d73fcc79e2b9822cd31', 'e8bbf23fb45433c81904b4784facd68f12528df2', 'mp4', NULL);
INSERT INTO `cmf_asset` VALUES (9, 42492, 160164, 1604292790, 1, 0, 'e6a3634b2a8326210a32247829f75e0f691fad7c0affd556f9dc9d002d1a8e87', '9769.jpg_wh860.jpg', 'home/20201102/44176b01829dcde4f926da016f43b083.jpg', 'e6a3634b2a8326210a32247829f75e0f', 'efe5b4d020a6b0a88c6a21f65f14a2818dc39e2a', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (10, 42492, 343196, 1604295209, 1, 0, 'e3a4254e0128b6e1d15b24b091f7d5d8327e610f11b940d70c76984feecaaefa', 'NO1.png', 'default/20201102/c39bb4203173ed0826431ac6c58d7049.png', 'e3a4254e0128b6e1d15b24b091f7d5d8', '0c03ec9c282c42afe3b410de9ebf27cfe21320b4', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (11, 42491, 343196, 1604296884, 1, 0, '6045b5196ef2132a67f9e59daded5fdf0eaab2fbc1c0c16e4912a0d164636393', 'NO1.png', 'admin/20201102/47d8c5a23aec13e8677695ce0793a7d8.png', '6045b5196ef2132a67f9e59daded5fdf', '4c1e1b44c0c70a0385c8633dbfbeed1554153ecd', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (12, 42502, 24920, 1604302855, 1, 0, '30ae0b898923d69cc188d1778bb839959f65ae6f6c85904ab7b35c8e4a10df0b', 'photo_2020-07-29_22-22-11.jpg', 'home/20201102/07c2efc2a9a470add114ae355c5aa7f4.jpg', '30ae0b898923d69cc188d1778bb83995', 'be44915bc5ac706f70bf25332a5d270a5438b42b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (13, 42487, 46583, 1604303735, 1, 0, '9c78b32ebbbdd8fc5108f07d1c9c20fd04be63b73bb5155e654310e6b81d5a21', 'ic_launcher.png', 'default/20201102/9680a70106e4eab92012cbc3f759ac17.png', '9c78b32ebbbdd8fc5108f07d1c9c20fd', 'e30fe2c10fa8cfbc326dc8faadb8d4af4738465b', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (14, 42487, 2627, 1604303749, 1, 0, 'e51a4425f1bbaee10a5f9f08b2b17fdc54d32c8c5125492f44301a26823f56a8', 'ic_launcher.png', 'default/20201102/8b444e818e86dd7ae3f980b98517b7b0.png', 'e51a4425f1bbaee10a5f9f08b2b17fdc', 'b855145664e13bb6aa6b3a4ab9812f8383d95ae4', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (15, 42487, 1956, 1604303762, 1, 0, '6504b8af3c4d687a9611dd8361e9f7563baf090a7415988089ae873d898697f9', 'ic_launcher.png', 'default/20201102/af6e2183823f7fa533b4434555c60dfa.png', '6504b8af3c4d687a9611dd8361e9f756', '2a162cdce82782bb10a476f25c6fe39b91fef305', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (16, 42491, 160164, 1604304256, 1, 0, 'e6a3634b2a8326210a32247829f75e0f691fad7c0affd556f9dc9d002d1a8e87', 'test.mp3', 'admin/20201102/0c3329f330e777cf2f9f24f097374adb.jpg', 'e6a3634b2a8326210a32247829f75e0f', 'efe5b4d020a6b0a88c6a21f65f14a2818dc39e2a', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (17, 1, 114490, 1604305342, 1, 0, '00794d34a1f5ee4ed861d9f21945aeb7dbd014061cfed6e2587ac8a8ccde8663', '441a7171ab9cb8b8f7e05b0c98c0b27c.jpg', 'admin/20201102/faada543a7f03c8ae52d91d76ede6fbe.jpg', '00794d34a1f5ee4ed861d9f21945aeb7', '04bf82f09c28a20adb9f088841beb5520a027c13', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (18, 42491, 343196, 1604307333, 1, 0, '1ca2f74f640e272e98c5c00ce6a561ec9a1a35ca160fc98e018f5e1701303997', 'NO1.png', 'admin/20201102/1886e5ec730b0197d3569f745e535dfc.png', '1ca2f74f640e272e98c5c00ce6a561ec', '6f5b0940cba3bdfd7a390a62402646015a09e0d5', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (19, 42491, 202165, 1604307572, 1, 0, '398aa055b75dca01382e52294c3ca75328cebd3acdaff4c3a67bc428d8dcdd67', 'NO2.png', 'admin/20201102/57bbd10c048d7393c7cd6d3453715dd4.png', '398aa055b75dca01382e52294c3ca753', '6f7c9f2d9f1f2057026be8d54ad782e9e5240805', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (20, 42491, 343196, 1604308567, 1, 0, '5295b49c821249b592d815b2859417806fb59259898ca10c17a1d54bc1f795ee', 'NO1.png', 'admin/20201102/b9c56b18b58adaace6d946371569aa89.png', '5295b49c821249b592d815b285941780', '7029e6294f9df18db99c1a403229feefb7847fde', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (21, 42491, 343196, 1604310044, 1, 0, 'eae4ee2cd024499ceda566693b9ef75dac86a023fecf43400bea7280927332d7', 'NO1.png', 'admin/20201102/5bd0334689372ebcd35b9cf49b38bd03.png', 'eae4ee2cd024499ceda566693b9ef75d', '211c93332e2e0425f799d7e5b7da2c4ce44a9bb7', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (22, 42512, 15586, 1604318625, 1, 0, 'bdb19a5bf4ee21a8797cb9bcbd7937e90f845799d0b70a99950f2584eb9d80e3', 'u=121526568,890188697&fm=26&gp=0.jpg', 'user/20201102/169a88fc1616b7b445729a971500224a.jpg', 'bdb19a5bf4ee21a8797cb9bcbd7937e9', 'fede96929257f0d8e06b7054a535a03912c1ce43', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (23, 42512, 22059, 1604318634, 1, 0, '7d93b11a294679fb42fe50b0b5c1b210236176fe25961c4c4a6e62606dea91c1', 'u=247423174,2820318442&fm=26&gp=0.jpg', 'user/20201102/7d171baffc84a935e126a63271c3f672.jpg', '7d93b11a294679fb42fe50b0b5c1b210', '2ae1b4238d93e0926020f912fa666285368223db', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (24, 42515, 160164, 1604320941, 1, 0, 'e6a3634b2a8326210a32247829f75e0f691fad7c0affd556f9dc9d002d1a8e87', '9769.jpg_wh860.jpg', 'home/20201102/354b5d55833922fceaff62d359404fd2.jpg', 'e6a3634b2a8326210a32247829f75e0f', 'efe5b4d020a6b0a88c6a21f65f14a2818dc39e2a', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (25, 42512, 286908, 1604321394, 1, 0, 'b4733aa375491985a12264a1652173150667cf55ce8be140c814c20d6a65ee4a', 'u=3278720085,289405952&fm=26&gp=0.jpg', 'admin/20201102/2fe950c6e6a0e336e8d3542130ee7281.jpg', 'b4733aa375491985a12264a165217315', '8d3c4d9efa9315c9191c1dcac7ded8d9c80d47b4', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (26, 42512, 345220, 1604321468, 1, 0, '61273fe1be50dd788d13b2f691945f1e642387f597a6bf91ac772d0c0034e102', 'timg.gif', 'admin/20201102/16c58132820440789170c109962c8cbc.gif', '61273fe1be50dd788d13b2f691945f1e', '39bc92a2589c3599784ef58cca2dc23d9038d952', 'gif', NULL);
INSERT INTO `cmf_asset` VALUES (27, 42513, 930794, 1604373052, 1, 0, '512eaedad8c464f17afc18fa4039618861f990421c81329c24b551c5bd96b891', '45C07A17-792F-411F-AAD8-396BEB1036F9.png', 'default/20201103/f6ee36fe15f957d8e6e38266078c264c.png', '512eaedad8c464f17afc18fa40396188', '03204da3e766306358481253e69c205c3d42e410', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (28, 42513, 198808, 1604373156, 1, 0, 'ad4c3872da52ed285d0e03db2488021deea92a1d30d26db2b4eaaff18a6c1310', '29E5B14E-593E-4508-8EA8-B8DEC463BA82.png', 'default/20201103/38463dd7d5d5bb6f47841a9e30a5d7cf.png', 'ad4c3872da52ed285d0e03db2488021d', 'e443927ddb29b1b6453c547098ed27d24d96fe60', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (29, 42513, 301311, 1604373170, 1, 0, '8686c8d11f6455e44aa74a46e2b089e5db79331ba2d286c94f913a209060198a', '737248B6-C892-4D81-AEAB-12B910790F81.png', 'default/20201103/e3ea7e0960a99d764e0d0d3d9404375d.png', '8686c8d11f6455e44aa74a46e2b089e5', '609ac25e2ef1e98b27828182481c37218ca313ad', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (30, 1, 34314, 1604380253, 1, 0, '9f42e3df279e9e2d8f67ec076d72d9fa9da0fc91435acce7338b5b4e688571d2', 'a0b30447a416457ca87765d9abcff050_th.jpg', 'admin/20201103/b50b89648ad2ee169974cad924168a92.jpg', '9f42e3df279e9e2d8f67ec076d72d9fa', '75a60949bc708ba1f8336995cbb9afe1c8d51bed', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (31, 1, 6034, 1604380267, 1, 0, 'ee18029122f54fa860ab0235d75d2f01cdfbaf54b1bd7d68d9d76c54ce137514', 'images.png', 'admin/20201103/823cd7d97e3123b5186b7d06b99c5c03.png', 'ee18029122f54fa860ab0235d75d2f01', '99b804a98716333e110d9b997ba11e0cc920b085', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (32, 1, 1091595, 1604380277, 1, 0, '0bdd2af34c587043bae89a718a7a2804fdac9c8756f251a32521e8b5f34c8646', 'DSC04548.jpg', 'admin/20201103/5d13be1a92838cb85365c32f7ca99ba3.jpg', '0bdd2af34c587043bae89a718a7a2804', 'e058ae8a140500e8d292da58460989c84d02a32f', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (33, 42514, 470024, 1604381949, 1, 0, '23290d4c55bfae88a3f70ec505ce29dcb6b757483bb3e3524e5706f343feaacf', 'Screenshot_20201103_120637_com.rongyao.phonelive.jpg', 'default/20201103/cf57bb8b184751338dae4f588c3963b2.jpg', '23290d4c55bfae88a3f70ec505ce29dc', 'ac622427ad0f52ecf8d25672a2814a8ce456bac6', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (34, 42515, 411252, 1604383278, 1, 0, 'fb49f6c4be04c45ad885ea87b1266d99ba80902b29c9c4f8b4bbcbcd7779c8ac', 'NO3.png', 'home/20201103/e45f78e66b9c8f5201815c5490bcbdc2.png', 'fb49f6c4be04c45ad885ea87b1266d99', '413a1788ebc27f84d10787f081eeae9ed17751cf', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (35, 42494, 19257, 1604384962, 1, 0, '3435ac125655d376517dd04a740c553158c9eb1adef882bb015ca1ed1b3b2546', 'Snipaste_2020-10-26_15-58-48.png', 'admin/20201103/1cb2c9c8e455cd427722698ef69e7a96.png', '3435ac125655d376517dd04a740c5531', '4c2bbf21f38aa01942b2b08946eee499601633d1', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (36, 1, 134111, 1604401669, 1, 0, '376557c929342f2cd4b064bfc7fcca1a19defba168424191bc505282a60bfdfe', '3092664f03a426337bf8813fe7a1d17c.jpg', 'default/20201103/344808095e389174be9fa7860eb1da7f.jpg', '376557c929342f2cd4b064bfc7fcca1a', '7fc83e8552a0c838730b927c2c844c59f420c6ff', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (37, 1, 28983, 1604401675, 1, 0, '8fa18a2e8c950ff32fcb43d1e92edc867859bd028622f6a3592f5de70cef60d1', '6b1d95f515a4424187aa4f4f8269cf1e.jpeg', 'default/20201103/4d0af380e8545b977cf51a4bcc430f60.jpeg', '8fa18a2e8c950ff32fcb43d1e92edc86', '5e9becc9161a0b1b4a37549a52321d83857b185c', 'jpeg', NULL);
INSERT INTO `cmf_asset` VALUES (38, 42491, 440901, 1604406564, 1, 0, 'a7e0a12ed35e97659e47c6b5eb905c9a08df10fc515b14940d05e56719e6403e', 'NO1.png', 'admin/20201103/bd9309882d378397b52c1fadcf6bde50.png', 'a7e0a12ed35e97659e47c6b5eb905c9a', '71e1bb01106d0deb9b1190e8a7879a76bf81828e', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (39, 42491, 305545, 1604406869, 1, 0, '18ffe96a1be494de8f788689653d10d009b0effb51ce8977bc31d476999d74fd', '屏幕截图 2020-11-03 143022.png', 'admin/20201103/045960a1cdd0d57c00cc5bec863367b7.png', '18ffe96a1be494de8f788689653d10d0', 'a7066052aebc94bcbc5d40030cc690d4ceed311c', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (40, 42491, 411252, 1604407044, 1, 0, 'fb49f6c4be04c45ad885ea87b1266d99ba80902b29c9c4f8b4bbcbcd7779c8ac', 'NO3.png', 'admin/20201103/0a8c3fd203b59f4a563d12ff76fb253d.png', 'fb49f6c4be04c45ad885ea87b1266d99', '413a1788ebc27f84d10787f081eeae9ed17751cf', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (41, 42496, 165096, 1604410747, 1, 0, 'a33df5b5bfd7f654857a79437894069d53dbf96e22d686770770e17183eea9b1', '0DC3AE1A-6F09-458F-9394-F0770AB44948.jpeg', 'default/20201103/e4f50befbaaab4892ea140ebfea08acc.jpeg', 'a33df5b5bfd7f654857a79437894069d', '1e838f223b3bbba10952ca0168db3be1d66c3ac5', 'jpeg', NULL);
INSERT INTO `cmf_asset` VALUES (42, 42526, 4870, 1604471852, 1, 0, 'fa2f55983283626271e8b396e92f77ec2b566fe14d1198f7ed384a10565e6c99', '-1_03121A45M603.jpg', 'home/20201104/0298b43b704ce2660e0d93e5af712597.jpg', 'fa2f55983283626271e8b396e92f77ec', 'a65fd84e479f8f391fc2df253af880025e31e7f8', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (43, 42536, 212496, 1604493027, 1, 0, '91da63f90c8ee419e167ff05ca5ba8dd2c2d5580b201269ad53c49368cc64d9f', '10465F02-C3F1-423C-8B54-F609D920A8FD.jpeg', 'default/20201104/6f1a83bad0225b147e10d78e731735bb.jpeg', '91da63f90c8ee419e167ff05ca5ba8dd', 'e721ba5724dd9c7a0bdcc6beff275b816225b253', 'jpeg', NULL);
INSERT INTO `cmf_asset` VALUES (44, 42536, 179034, 1604493544, 1, 0, '34066b1c8c361182178217fab83ff40a66c373f817a1837af40a82410b7bcf33', '2310A55D-3296-4818-A4D5-4B85324CA4DC.jpeg', 'default/20201104/4ee78ef0681cef83435cfca8b37c2e47.jpeg', '34066b1c8c361182178217fab83ff40a', '06c804ef9a313ecf409e276d6a8b65ed57528a71', 'jpeg', NULL);
INSERT INTO `cmf_asset` VALUES (45, 42536, 671059, 1604493567, 1, 0, 'cfb6fa54505994eec9b15565ad81ad27218529361d6ed8c2105a3ab9f52b9cd2', 'image.jpg', 'default/20201104/cba92f0edc2218dbed24fcf6c407717c.jpg', 'cfb6fa54505994eec9b15565ad81ad27', 'd024515f7ffeaabdb7c839fd592bebe7e2b50d6e', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (46, 42535, 226514, 1604495080, 1, 0, '738f605295e3ba8c75027a2d152126110c3e0f4ad77b0bd8cdb1e649b34f67d8', '7.jpg', 'admin/20201104/7a70aec0000584a78096feb4f90b0408.jpg', '738f605295e3ba8c75027a2d15212611', '4300d50ea128242db138676be3ebf1e4e185454f', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (47, 1, 5500, 1604495752, 1, 0, '5b1cbc096fb71ab30cca17155f7cbc72e334b19eb5917680b07cc4013fce638d', '80.png', 'user/20201104/fec4dbe89026c94a9c5cf586782079f3.png', '5b1cbc096fb71ab30cca17155f7cbc72', 'cb89a4d478ce10076de69848104a2fcd03cae7a7', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (48, 1, 9763, 1604495807, 1, 0, 'd793f8130d0398ab8d0f89b4fc6c12998874b506ce7ab46d8fc5b8402826ca85', '120.png', 'user/20201104/93ffc6f1920dadc7aea2e53309b581f9.png', 'd793f8130d0398ab8d0f89b4fc6c1299', '2f1bf69b088518b6653df9c93247cbd0fa472357', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (49, 1, 17723, 1604495830, 1, 0, 'd578009d89095beab9c3857f133236853e4c3dfc1137b27b15977fe28495d7c1', '180.png', 'user/20201104/ab704550e4199fb47dc2aacc11a96e81.png', 'd578009d89095beab9c3857f13323685', '045d66f9dd3e58bac261b116a623dd021f4472f3', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (50, 42535, 48449, 1604502093, 1, 0, '4d866a98a926d6341633b1344132cc3b0a2e35e0a2cdc499ced8b5fcb38893d9', 'image_2020-10-27_04-17-33.png', 'user/20201104/27989ec2911647ccf39153f3f7ace3f0.png', '4d866a98a926d6341633b1344132cc3b', '4e45a26b551d645acfe90fd2352d1ce59d0b16cd', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (51, 42535, 404905, 1604502116, 1, 0, 'b1d149de591beba01944c71789c2c7bfcf9604b2296bd922da8daf30b9e1c510', '641.png', 'user/20201104/8c7acaa34a0894e90d177244d0d3ff5c.png', 'b1d149de591beba01944c71789c2c7bf', '7117925607f07b4f5e9bb23f1a96926723d26162', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (52, 1, 95104, 1604542605, 1, 0, '9bf616ffda420f94bfed2f0a73d9a1d7a78543b791c2e1cb8b672f1b3f2bab85', 'label3.png', 'user/20201105/4ede3caeee6f961984a450944c51d3e1.png', '9bf616ffda420f94bfed2f0a73d9a1d7', '8351f3fa9f92cc01424baa39d29ea38c2f3f787d', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (53, 1, 1757614, 1604542664, 1, 0, '3bcfb1f400ccbe12af138d55644b9592cb06fe23fc9e69607eae45364d4f7c2d', 'I’ll be your monster.mp4', 'admin/20201105/cb669fea2243bfaab1864bd4ed6b091d.mp4', '3bcfb1f400ccbe12af138d55644b9592', '784ff0c0dbe2365bb3e630b975d34d8beb1ae583', 'mp4', NULL);
INSERT INTO `cmf_asset` VALUES (54, 1, 65863, 1604558849, 1, 0, '82e4209a2c19211f40fa6c465578aef24dcde21149883c41939d85bdb6dc22fa', 'HpdfS5Hq.jpeg', 'user/20201105/0c197cbde51b8c15858189c5d54d0ca7.jpeg', '82e4209a2c19211f40fa6c465578aef2', '3c07b15a15f2e3613e560dd9c3cbd596977207f0', 'jpeg', NULL);
INSERT INTO `cmf_asset` VALUES (55, 1, 412229, 1604558872, 1, 0, '98f2f6c8895a6a042c496f1b70e799f7c9790e4bb7ef3dc69c7ab5f01d6cce8f', 'musk.png', 'user/20201105/b757227557aea418cd359bf13afdf013.png', '98f2f6c8895a6a042c496f1b70e799f7', '1aa3cdb8a77206ac63176d4847cb6e9d657e6847', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (56, 42491, 60354, 1604647063, 1, 0, 'cd84feca611137a67bf8f72831a45845e1f441c22feb6169d94fe3d9b78e7ac3', 'timg.jpg', 'user/20201106/37cf7b231c9a69c2b7cd4e60406f37c8.jpg', 'cd84feca611137a67bf8f72831a45845', '0c5394ec19311bfc7fd1eacb89c621636d80e7da', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (57, 42524, 12172, 1604657381, 1, 0, 'e5caf1f6cbe8adad7b9945f7dbd8e8d9763ce062e1d82fffb16816f7372c29c4', '20201103205802040.png', 'default/20201106/1426315356a9e116650c42ec73e76a9d.png', 'e5caf1f6cbe8adad7b9945f7dbd8e8d9', 'b0f3516c0958ee00705e8c380aa5a73798580eb1', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (58, 42499, 343590, 1604660839, 1, 0, 'be30f9e4897307b0fe9904b06aa096f2211bb6ca089d3f75ed335603d6396a06', 'Screenshot_20201106_143323_com.rongyao.phonelive.jpg', 'default/20201106/69401f0559f8623b9313a48bb2a1cea1.jpg', 'be30f9e4897307b0fe9904b06aa096f2', '55014e6c35a2d799ecd142a4cf8214e9838dbf49', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (59, 42535, 121122, 1604743336, 1, 0, 'aad9c5050c955c1e4f5dd7fe31b19d01e643b28d8a00e42702e17560d63aead1', 'photo_2020-07-26_19-19-35.jpg', 'admin/20201107/0f19311c5df4a33a1c9932c2b03fe9c1.jpg', 'aad9c5050c955c1e4f5dd7fe31b19d01', '4b7c09ef5d377a5519f527a51e547e7bda1bde59', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (60, 42535, 70525, 1604743445, 1, 0, '489124d18879d9e3db97bc25b18edd62ff2993ec9fdb84ccde017e9158f69e73', 'photo_2020-05-20_22-33-48.jpg', 'admin/20201107/390c5fab37a1a7091f8734a3e2a5169d.jpg', '489124d18879d9e3db97bc25b18edd62', '1e7e216a9890e5c23d9d995f80a6edc0aba64591', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (61, 42504, 16023, 1604750353, 1, 0, 'f7d0cd5fd7763bdcfec33b53a75c33359c99e47e572a57b8afbdde7fce5c392f', '20201106194123158.png', 'default/20201107/12aeda210d1cc89105fc253e955b672d.png', 'f7d0cd5fd7763bdcfec33b53a75c3335', 'aae32c329924f375d695292063d98f689eca0b2d', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (62, 42545, 20749, 1604750812, 1, 0, 'cdc203aded40d47d4c3f58099106a05bf429dcf16cb20187af33ade501a71722', '17-48-28-028.jpg', 'default/20201107/36acfb60badecc19da5d054185bd7838.jpg', 'cdc203aded40d47d4c3f58099106a05b', '9126e422e21d26a059df63f46a2a1af481c8fa4d', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (63, 42545, 97580, 1604750934, 1, 0, 'f8e8fdb3901d52d250f194a7b1cd81b4c3bc022f04964e096b80a23a8c1273a5', 'IMG_20201107_200822_930.jpg', 'default/20201107/da9ce7332ed3a1302898296d22efd4f8.jpg', 'f8e8fdb3901d52d250f194a7b1cd81b4', '9535fb7199d6e367ab67cfafaf5fdd88cdc8a30b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (64, 42545, 58082, 1604751268, 1, 0, 'db7fef881a7ccd6f5350ce3fed73edd3f366e62ffa68dd939aebbdc8becfbe7c', 'IMG_20201107_201347_160.jpg', 'default/20201107/45e1638fe043ddb66db23bd88dcf902c.jpg', 'db7fef881a7ccd6f5350ce3fed73edd3', 'd7725f4b27f3023cdfa23cbfbbfcf648944271fa', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (65, 42501, 373074, 1604752871, 1, 0, '2908ecd77f94ad76ae33ca64c00533335d7e0cfdf13a690362c13f765a6b792c', '1492B8B7-62E3-4B4E-A37D-3B5A868D0E41.png', 'default/20201107/bbd26b3aa0135b6e99f372ced471ed98.png', '2908ecd77f94ad76ae33ca64c0053333', '517481ec1adfe61c52f5f169cee38c1a4f455c81', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (66, 42542, 143736, 1604772986, 1, 0, 'c3b52f29fdc4918270f5542e6559d03bf6a5e9ca2d15715d5cf843209c9a4752', 'Screenshot_2020-11-07-14-16-34-561_com.rongyao.phonelive.png', 'default/20201108/ef73fdf81e9ca1a5b8b748b71b140d41.png', 'c3b52f29fdc4918270f5542e6559d03b', '081eb55583ee79d336c81e68d1709691657fedf8', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (67, 42524, 72273, 1604808901, 1, 0, 'b9ed4705b134861a6294e29a922f073e3e73e7862ce32a6c763851c8367a9cb2', 'photo.jpg', 'default/20201108/ddfe29faab622a70a381dcf33e71527b.jpg', 'b9ed4705b134861a6294e29a922f073e', '4777d53d3e2edde1971f38b844f5152fc7643136', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (68, 42524, 114490, 1604810034, 1, 0, '00794d34a1f5ee4ed861d9f21945aeb7dbd014061cfed6e2587ac8a8ccde8663', '441a7171ab9cb8b8f7e05b0c98c0b27c.jpg', 'default/20201108/0683e56a08fe329dc8013903e82f3156.jpg', '00794d34a1f5ee4ed861d9f21945aeb7', '04bf82f09c28a20adb9f088841beb5520a027c13', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (69, 42524, 123832, 1604810119, 1, 0, 'd3515d805913b8ce2a518427feddaba09ba9d107d2a0b6260c8359b736f9bf16', '44f0bf80f75a3de3.jpg', 'default/20201108/0fa59f7319bcf90e38a094f901276e05.jpg', 'd3515d805913b8ce2a518427feddaba0', '104f7dc814172e0f8d09a9b81ba5c8113983d5df', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (70, 42545, 123832, 1604811458, 1, 0, 'd3515d805913b8ce2a518427feddaba09ba9d107d2a0b6260c8359b736f9bf16', '44f0bf80f75a3de3.jpg', 'default/20201108/b903c4bcb2eae261ba2ab74a239e85bc.jpg', 'd3515d805913b8ce2a518427feddaba0', '104f7dc814172e0f8d09a9b81ba5c8113983d5df', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (71, 42545, 411252, 1604811928, 1, 0, 'fb49f6c4be04c45ad885ea87b1266d99ba80902b29c9c4f8b4bbcbcd7779c8ac', '0a8c3fd203b59f4a563d12ff76fb253d.png', 'default/20201108/b06c33ebf119b3f3af305aa36a62dafe.png', 'fb49f6c4be04c45ad885ea87b1266d99', '413a1788ebc27f84d10787f081eeae9ed17751cf', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (72, 42545, 114490, 1604812475, 1, 0, '00794d34a1f5ee4ed861d9f21945aeb7dbd014061cfed6e2587ac8a8ccde8663', '441a7171ab9cb8b8f7e05b0c98c0b27c.jpg', 'default/20201108/2ae0c6b3693e0ed57f26fa96c25f8516.jpg', '00794d34a1f5ee4ed861d9f21945aeb7', '04bf82f09c28a20adb9f088841beb5520a027c13', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (73, 42545, 84962, 1604812745, 1, 0, 'b7f0f6f88542f0536e7712a212e1b585ceb8c1ef214dcc56e096dd4f2a3f248f', '5c93539723ab8.jpg', 'default/20201108/709c8aabe8fe016b833ec2d8af7e2d9b.jpg', 'b7f0f6f88542f0536e7712a212e1b585', 'b64bbf8d581cd1a049e992a3f213d05cd826c9aa', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (74, 42559, 405254, 1604814223, 1, 0, '76cb43afb01156e4e85c4f1a702fdcd5e131f52cece2eb63655170df11ad9b99', 'Screenshot_20201108_133124_com.rongyao.phonelive.jpg', 'default/20201108/7c954885b60c7ae67614f4da98eaf97c.jpg', '76cb43afb01156e4e85c4f1a702fdcd5', 'd07d651e675e3725a89f3db569c1f7a81b73bad4', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (75, 42545, 134111, 1604814612, 1, 0, '376557c929342f2cd4b064bfc7fcca1a19defba168424191bc505282a60bfdfe', '3092664f03a426337bf8813fe7a1d17c.jpg', 'default/20201108/db146da0596ea404a341c570805228a5.jpg', '376557c929342f2cd4b064bfc7fcca1a', '7fc83e8552a0c838730b927c2c844c59f420c6ff', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (76, 42548, 402343, 1604814826, 1, 0, 'd942a990ee1323e36fd6d0528709a79447cc76fc3ed4ad6bf17228553f8184e6', 'Screenshot_20201108_135059_com.rongyao.phonelive.jpg', 'default/20201108/c6bbda6e8aeab82bd67586a2eea55639.jpg', 'd942a990ee1323e36fd6d0528709a794', 'e457d40ad29add833072d5111db3f2b729249b90', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (77, 42545, 28983, 1604815987, 1, 0, '8fa18a2e8c950ff32fcb43d1e92edc867859bd028622f6a3592f5de70cef60d1', '6b1d95f515a4424187aa4f4f8269cf1e.jpeg', 'default/20201108/35e075866fa0a6d63d07c0d8c1e51bf8.jpeg', '8fa18a2e8c950ff32fcb43d1e92edc86', '5e9becc9161a0b1b4a37549a52321d83857b185c', 'jpeg', NULL);
INSERT INTO `cmf_asset` VALUES (78, 42545, 2025, 1604816148, 1, 0, '51020029f045fd783b7017f60077b492e9d34836b0b83ceed40020f45aeb4e1c', '巴拉圭.png', 'default/20201108/e4d558d491f306952201f958c6c46720.png', '51020029f045fd783b7017f60077b492', '2da51d297637842663835b75532a7be64c6213a9', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (79, 42545, 6474, 1604816186, 1, 0, '8f2d43a64345e03ce42219f1488d9f12fe77852c327b9715cf28cea7f330e5bc', '巴伦西亚.png', 'default/20201108/3adf8dea2f7de87bbd6e91f334186d4a.png', '8f2d43a64345e03ce42219f1488d9f12', 'e678d5eaca5c1282cc57ce336eefdd30dea89840', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (80, 42545, 71969, 1604816559, 1, 0, 'bd78ae0327543668baa438f241e588309c52ba4174aedb83f0832957f367610b', 'jietutu.jpg', 'default/20201108/d14d334eef8f12805bedd32409da5275.jpg', 'bd78ae0327543668baa438f241e58830', 'eaee826dc35f763a08e2a72634bb0855111c42bb', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (81, 42545, 362399, 1604834674, 1, 0, 'ec6ae6abad2d0fe7bcde8757e64af74a48dffe491969cf317e9d4680f319008f', 'IMG_20201107_215552_022.jpg', 'default/20201108/74b110c7f8b659512f98a64799c8c26d.jpg', 'ec6ae6abad2d0fe7bcde8757e64af74a', '9754b022424bf427825599eebc4e708da6cdec3d', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (82, 42545, 4870, 1604835740, 1, 0, 'fa2f55983283626271e8b396e92f77ec2b566fe14d1198f7ed384a10565e6c99', '-1_03121A45M603.jpg', 'default/20201108/7b1458a02fb9aaf504680381d2ba4bab.jpg', 'fa2f55983283626271e8b396e92f77ec', 'a65fd84e479f8f391fc2df253af880025e31e7f8', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (83, 42545, 8596, 1604900362, 1, 0, 'f2c2dfa1b1de660802b5ae99fa1de5c5bb63a9f5bc1cd2aaffb9935b071fe1e1', 'AC米兰.png', 'default/20201109/66b347e473bdb5cef822c904ce947530.png', 'f2c2dfa1b1de660802b5ae99fa1de5c5', '5cb8d34558e708d757f9c38f9dde175066030c3e', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (84, 42545, 12870, 1604900618, 1, 0, '3f26c0bd232b97b5fab4a488a860244919a99dbfd846e0cec508764ce7267c7a', '阿波罗.png', 'default/20201109/2ae6d9aadf20811069eb8b798a70bff0.png', '3f26c0bd232b97b5fab4a488a8602449', '11b37a0795977e7b7912236e493e2a2a57c31326', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (85, 1, 5431, 1604904217, 1, 0, 'a9f15609375c3d23eb521e4cf67b1eda82652b207f3ed3b040291fdc65878a5b', 'xiaoshiho.jpg', 'home/20201109/5b0f07cbe8f63b13b7efa0bacefcb5aa.jpg', 'a9f15609375c3d23eb521e4cf67b1eda', '54cb6f143e45f430515769609c5b99acbdc49acb', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (86, 42515, 6345, 1604906139, 1, 0, '2eed2995efc2246c6b61fa5e68d48f9c7a0e2bab4bcd28dca73e8820ea23a13a', '0001.jpg', 'home/20201109/7700befcc0cbd46e859ab0b68c2452bc.jpg', '2eed2995efc2246c6b61fa5e68d48f9c', '2fb7426a4baf97700b1125485f9d502e803384e1', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (87, 42491, 55535, 1604906573, 1, 0, 'c28542535315068d397219edba4097a13fac1753addf686cac210a8d9b67b35b', '0005.jpg', 'user/20201109/a73e6c6d823d95e30d703125d08df056.jpg', 'c28542535315068d397219edba4097a1', '5e943f11619f70f8d67470df9a82a410e87ec89b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (88, 42566, 6173, 1604906966, 1, 0, 'a1394084d12d59f4f80d451261d57494ac6334ceff8cb6b57239e5f3781351e2', '0003.jpg', 'home/20201109/edbbf8b07004c9499d51c681f9275e5a.jpg', 'a1394084d12d59f4f80d451261d57494', 'b180a12d5d640f172a639f07f4c8d720cfae97a2', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (89, 42539, 8596, 1604907474, 1, 0, 'f2c2dfa1b1de660802b5ae99fa1de5c5bb63a9f5bc1cd2aaffb9935b071fe1e1', 'AC米兰.png', 'default/20201109/12e19c9165ddf3dd597976c3cf9df88f.png', 'f2c2dfa1b1de660802b5ae99fa1de5c5', '5cb8d34558e708d757f9c38f9dde175066030c3e', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (90, 42566, 6035, 1604907688, 1, 0, 'e71f4ee011aad1cc116b5535533de9b8ebb4921dc947a8177344258471d8701e', '0004.jpg', 'default/20201109/e3aabda6461f1e87c13abe723b966f4f.jpg', 'e71f4ee011aad1cc116b5535533de9b8', 'bfdb264f09c430678b3c0fffd06c7941b03df637', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (91, 42566, 7703, 1604907807, 1, 0, 'be3372dc41a957e683b684af13a1a6ee155f2a2a6c96e4862c0016ae3763e5e8', '0002.jpg', 'default/20201109/6dce18ac8eb5016969933bd29398b0bd.jpg', 'be3372dc41a957e683b684af13a1a6ee', '330594a5086c54a6d6c2610cd636209ecea93df1', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (92, 42566, 55535, 1604908786, 1, 0, 'c28542535315068d397219edba4097a13fac1753addf686cac210a8d9b67b35b', '0005.jpg', 'default/20201109/8d6f33104166585309a061240da760ea.jpg', 'c28542535315068d397219edba4097a1', '5e943f11619f70f8d67470df9a82a410e87ec89b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (93, 42566, 77551, 1604908808, 1, 0, '7b13bfaf495bb3d9e04e2a5928da8c1e633025ce3205e8ca2114c4c7017c418f', '大图2.jpg', 'default/20201109/b720283ebab2548d4e62399e83e97f24.jpg', '7b13bfaf495bb3d9e04e2a5928da8c1e', 'e8fb56f95f4052d657ebf907600c9defdf7b0378', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (94, 1, 411252, 1604908895, 1, 0, 'fb49f6c4be04c45ad885ea87b1266d99ba80902b29c9c4f8b4bbcbcd7779c8ac', '0a8c3fd203b59f4a563d12ff76fb253d.png', 'default/20201109/51205167fc2c1a032be82f29899e8e83.png', 'fb49f6c4be04c45ad885ea87b1266d99', '413a1788ebc27f84d10787f081eeae9ed17751cf', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (95, 1, 55126, 1604909002, 1, 0, '83db9bdf3b694668547060802065e89d84038632c36b24754a1d70000cea76ea', 'photo_2020-11-09_16-03-05.jpg', 'default/20201109/3a8ef712dfd6216e96025a7c43842232.jpg', '83db9bdf3b694668547060802065e89d', '6659c4cce8831c4f6c4d68b6af8d67c11c8544b4', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (96, 42545, 379336, 1604910603, 1, 0, '2733d1dca928cd7374bb5ec099ce925fef327c1fae8b9412cfd64520720fccd1', 'IMG_20201107_215710_214.jpg', 'default/20201109/ea049717d744ac1a6302e0ad4aa7dc51.jpg', '2733d1dca928cd7374bb5ec099ce925f', 'ea690054013ffc6e05c4a26738aad893ac1952eb', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (97, 42504, 157117, 1604920214, 1, 0, '0fdfd8da19de5f61f6bbd30f1734f070b07829ae80da5daadff702be804bb53a', 'Screenshot_20201107_200208_com.rongyao.phonelive.jpg', 'default/20201109/17256655eb4a5e4a1417d2ac6901f2a1.jpg', '0fdfd8da19de5f61f6bbd30f1734f070', '7acea3fd9d84b72a0ac4a0da95532615613665cc', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (98, 42504, 142793, 1604920264, 1, 0, 'cbe30b6084adda3034d32923415784bb09e71e03b3800a93e418bbb51a396d4a', 'Screenshot_20201107_165716_com.rongyao.phonelive.jpg', 'default/20201109/c24f3c4bfa8cd51489fb9a3d15118b7a.jpg', 'cbe30b6084adda3034d32923415784bb', 'fedf2fba5a3a0b9035971c2ed475bc0c5fdcfa85', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (99, 42539, 4870, 1604921382, 1, 0, 'fa2f55983283626271e8b396e92f77ec2b566fe14d1198f7ed384a10565e6c99', '-1_03121A45M603.jpg', 'default/20201109/ea33cbe98ff16bb268569e07070c1763.jpg', 'fa2f55983283626271e8b396e92f77ec', 'a65fd84e479f8f391fc2df253af880025e31e7f8', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (100, 42539, 143736, 1604921569, 1, 0, 'c3b52f29fdc4918270f5542e6559d03bf6a5e9ca2d15715d5cf843209c9a4752', 'Screenshot_2020-11-07-14-16-34-561_com.rongyao.phonelive.png', 'default/20201109/83a9e104363dbc5659dcf52e7dbe09b9.png', 'c3b52f29fdc4918270f5542e6559d03b', '081eb55583ee79d336c81e68d1709691657fedf8', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (101, 42492, 20749, 1604922124, 1, 0, 'cdc203aded40d47d4c3f58099106a05bf429dcf16cb20187af33ade501a71722', '17-48-28-028.jpg', 'default/20201109/b1648abdb7a1abb9c9f3ccea700de517.jpg', 'cdc203aded40d47d4c3f58099106a05b', '9126e422e21d26a059df63f46a2a1af481c8fa4d', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (102, 42539, 16023, 1604922305, 1, 0, 'f7d0cd5fd7763bdcfec33b53a75c33359c99e47e572a57b8afbdde7fce5c392f', '20201106194123158.png', 'default/20201109/6342b67f893df725fcd49d70462cffe9.png', 'f7d0cd5fd7763bdcfec33b53a75c3335', 'aae32c329924f375d695292063d98f689eca0b2d', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (103, 42539, 810177, 1604922844, 1, 0, '1821770ac6f0832e03d3f5dc4ac5988a862f6d42125aba06c5a9e705c4695b8d', 'nana.jpg', 'default/20201109/6ac6d5bc3dfb9ed5f78c59acc3f0ea84.jpg', '1821770ac6f0832e03d3f5dc4ac5988a', '7aa29092279f129a95c64f8a791fb433158d2dc7', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (104, 42567, 60354, 1604923143, 1, 0, 'cd84feca611137a67bf8f72831a45845e1f441c22feb6169d94fe3d9b78e7ac3', 'timg.jpg', 'home/20201109/d14f2749c9bf42ed4d8fe666d483d483.jpg', 'cd84feca611137a67bf8f72831a45845', '0c5394ec19311bfc7fd1eacb89c621636d80e7da', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (105, 42521, 810177, 1604923301, 1, 0, '1821770ac6f0832e03d3f5dc4ac5988a862f6d42125aba06c5a9e705c4695b8d', 'IMG_20201109_195957_192.jpg', 'default/20201109/d371ffd694ad9f3eada14d6bf73085cc.jpg', '1821770ac6f0832e03d3f5dc4ac5988a', '7aa29092279f129a95c64f8a791fb433158d2dc7', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (106, 42539, 97580, 1604923424, 1, 0, 'f8e8fdb3901d52d250f194a7b1cd81b4c3bc022f04964e096b80a23a8c1273a5', 'IMG_20201107_200822_930.jpg', 'default/20201109/35f25e2f100981e4850a1a2deb981ac9.jpg', 'f8e8fdb3901d52d250f194a7b1cd81b4', '9535fb7199d6e367ab67cfafaf5fdd88cdc8a30b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (107, 42539, 362399, 1604923434, 1, 0, 'ec6ae6abad2d0fe7bcde8757e64af74a48dffe491969cf317e9d4680f319008f', 'IMG_20201107_215552_022.jpg', 'default/20201109/5771d3b4298115c12d95379ef50197ca.jpg', 'ec6ae6abad2d0fe7bcde8757e64af74a', '9754b022424bf427825599eebc4e708da6cdec3d', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (108, 42539, 20749, 1604923460, 1, 0, 'cdc203aded40d47d4c3f58099106a05bf429dcf16cb20187af33ade501a71722', '17-48-28-028.jpg', 'default/20201109/909b1d3c5a09c5f1b72b8b12f0bb1820.jpg', 'cdc203aded40d47d4c3f58099106a05b', '9126e422e21d26a059df63f46a2a1af481c8fa4d', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (109, 42539, 379336, 1604923533, 1, 0, '2733d1dca928cd7374bb5ec099ce925fef327c1fae8b9412cfd64520720fccd1', 'IMG_20201107_215710_214.jpg', 'default/20201109/65d5b2ec87961d126cf36ee5724b23f1.jpg', '2733d1dca928cd7374bb5ec099ce925f', 'ea690054013ffc6e05c4a26738aad893ac1952eb', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (110, 42569, 810177, 1604923584, 1, 0, '1821770ac6f0832e03d3f5dc4ac5988a862f6d42125aba06c5a9e705c4695b8d', 'IMG_20201109_200358_166.jpg', 'default/20201109/7d9dbf677398868debcff789037045b6.jpg', '1821770ac6f0832e03d3f5dc4ac5988a', '7aa29092279f129a95c64f8a791fb433158d2dc7', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (111, 42512, 60103, 1604923633, 1, 0, 'd7e6e43d07292b5cd4b1d54af0d765479bf81fb9cfc7ef7531e781f87af0bd8f', 'a70.jpg', 'admin/20201109/78d7cea1d5006490e4520de6da90277b.jpg', 'd7e6e43d07292b5cd4b1d54af0d76547', 'a0f2284528e081a9104e0f104277614718ad03be', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (112, 42512, 42891, 1604924725, 1, 0, '1372d07b1707d54e489dcb96b4d8662243796597e4a4fdc5339368c4da15fe27', 'a72.jpg', 'admin/20201109/368abce54a0390baa54df8990902bdfb.jpg', '1372d07b1707d54e489dcb96b4d86622', '9357cb7597740555d3a1744fee7faa760874aaf3', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (113, 42512, 141216, 1604924864, 1, 0, '691ef207abc0d87d748f76a523941da39884733989d0335bbf9ae1c7f23bc540', 'a44.png', 'admin/20201109/b5d3aee45cdb3f4c907dd919c1c1d137.png', '691ef207abc0d87d748f76a523941da3', 'b9ac2e7c6c07029bf9ebaf7f355a3204ada4bf28', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (114, 1, 7139, 1604929543, 1, 0, '0609d869679c34ce1d0c53b12fadcac677fcbd6feb945cab92b63771cc158b3b', '二维码.png', 'admin/20201109/93ca4d105edb01a0b44dde9260282fe5.png', '0609d869679c34ce1d0c53b12fadcac6', '3ff3d9b9987e9cf91e2ccd9ef5b8bc78cba7e44c', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (115, 42543, 287994, 1604929674, 1, 0, 'c551353377e0e96a059fd08c1cd7d82be4ed367918c3c1493f493494aeb93433', '更多资源上（youtaotu.com）选或联系q（3622002462）W25_0662.jpg', 'default/20201109/06f38746bfe392759db9ae3bfb3ee775.jpg', 'c551353377e0e96a059fd08c1cd7d82b', '72e878177e8ff731559c676f63615043ec856826', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (116, 42543, 810177, 1604930021, 1, 0, '1821770ac6f0832e03d3f5dc4ac5988a862f6d42125aba06c5a9e705c4695b8d', 'nana.jpg', 'default/20201109/0f77a03d49cd242bcbe3b46861ccd8cb.jpg', '1821770ac6f0832e03d3f5dc4ac5988a', '7aa29092279f129a95c64f8a791fb433158d2dc7', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (117, 42543, 12870, 1604930117, 1, 0, '3f26c0bd232b97b5fab4a488a860244919a99dbfd846e0cec508764ce7267c7a', '阿波罗.png', 'default/20201109/acdb07ccfcc17e80c91c7099cb4eb2e8.png', '3f26c0bd232b97b5fab4a488a8602449', '11b37a0795977e7b7912236e493e2a2a57c31326', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (118, 42535, 75151, 1604930398, 1, 0, '914888c9a43c56f96dfa8d92b3cf89d22a96c32c6b4f85f1d9d3965f1c745c45', '6.png', 'admin/20201109/5b85250262fbc4f8c9c0740606ac5db7.png', '914888c9a43c56f96dfa8d92b3cf89d2', '2a2404ae12d8e17093a8dfff60ef635937979908', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (119, 42543, 8596, 1604930495, 1, 0, 'f2c2dfa1b1de660802b5ae99fa1de5c5bb63a9f5bc1cd2aaffb9935b071fe1e1', 'AC米兰.png', 'default/20201109/0b362749167357baf5846dc213113db2.png', 'f2c2dfa1b1de660802b5ae99fa1de5c5', '5cb8d34558e708d757f9c38f9dde175066030c3e', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (120, 42535, 162801, 1604930514, 1, 0, 'f48dc1fb9c832d15c030e3132f4858378cfc7ebf16246b532b28d4ab4230ee35', '7.png', 'admin/20201109/73c1405675c5121168d0e20cc4e857fe.png', 'f48dc1fb9c832d15c030e3132f485837', 'cfbc57d160aeb8f168d1ffef46782aebcf18002e', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (121, 42535, 126175, 1604930570, 1, 0, 'd28a97d4eb883c85d3bfc5bab5dc0749352bb5cd9e0830da3dc945fc137a5a1f', '8.png', 'admin/20201109/825abc0efac507988c5709c5567727b8.png', 'd28a97d4eb883c85d3bfc5bab5dc0749', 'd3a989561e64a851b0367ab7a5c5f4b5245b8ecb', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (122, 42539, 520964, 1604930930, 1, 0, '29276350df940039779cbdb0cb6a6aebe1ae1b994ed5a2cb9029f118ea55e54d', '34B98309-550F-4FC0-AB15-3F0E65B54B5A.png', 'default/20201109/a7c7b221cbda81b98b1b903e74a5cce7.png', '29276350df940039779cbdb0cb6a6aeb', 'de406c062aaff9a0b93b0b6d631a599fe57675a0', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (123, 42543, 269768, 1604935765, 1, 0, 'b86af257bfebb656e0aaccbd5c2bb2b819d0a09f0063a28d55af8190f51cec44', '19011B93-B168-4F82-808C-F13C48B8CB36.png', 'default/20201109/097bd8f5830783a3ab66e63bb03275de.png', 'b86af257bfebb656e0aaccbd5c2bb2b8', '342bdadbfdf23ced88f9abc3a6f387a04e76e52f', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (124, 42543, 817274, 1604937137, 1, 0, 'dd75288fe4e297a710b8f52b5b0b858bc71e0011bdc44e4518dbdd0dd7ab5212', '03F1395B-F416-47A9-91DF-B85BC80EAB27.jpeg', 'default/20201109/a595e854424224d413152131a73a31af.jpeg', 'dd75288fe4e297a710b8f52b5b0b858b', 'e1f023d6a8b502dffe51abc66bf8bba5d9e4c4be', 'jpeg', NULL);
INSERT INTO `cmf_asset` VALUES (125, 42542, 4870, 1604987041, 1, 0, 'fa2f55983283626271e8b396e92f77ec2b566fe14d1198f7ed384a10565e6c99', '-1_03121A45M603.jpg', 'default/20201110/ecb99ff98119501c72908330e8251fa3.jpg', 'fa2f55983283626271e8b396e92f77ec', 'a65fd84e479f8f391fc2df253af880025e31e7f8', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (126, 1, 33131, 1604987360, 1, 0, '26812d01320f211e439feab8724d6d6f3b1297c26e10678add57d22f72a257fe', 'Img401154416.jpg', 'admin/20201110/18925f17770070b8dd19dcfda7a3d267.jpg', '26812d01320f211e439feab8724d6d6f', '7c9a0a26df07a76c044f67ea02200e6111572336', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (127, 42543, 16497, 1604988782, 1, 0, '61b56810a8aa023d3c1d8c9a4e03819175e1f084cec9a2d38e820e31dd9215c6', '奥林匹亚.png', 'default/20201110/76542058f88755a15ca120c0adc063b0.png', '61b56810a8aa023d3c1d8c9a4e038191', '88281641c19aeb20a16fdc61f0bc21cbdf1b7fcd', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (128, 42545, 341738, 1604990196, 1, 0, 'daa37678bf13f725cdb3a91f9b42c4497ca455960001d7bd39166cf3db17c7f3', 'IMG_20201110_115727_747.jpg', 'default/20201110/5db47fa260e6ea6e4ac4726056310425.jpg', 'daa37678bf13f725cdb3a91f9b42c449', 'e83877ef9b471a5567197bdd2517d8b4ce5bc9a8', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (129, 42543, 14524, 1604991577, 1, 0, '81907003103c7a6e28e3bcfc8d0028944e0af34b242bded222c384a42fd65691', '巴黎圣日耳曼.png', 'home/20201110/1ee51eb4d0357bbdd7dd753450466ac7.png', '81907003103c7a6e28e3bcfc8d002894', '32549ad0d62a39e603d1ab0b1d9ec9c7012ec0db', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (130, 42535, 3274, 1605008285, 1, 0, 'aefaeb7dc6795770a607335e5a272df45bcd0d54d953a672013a4755266c45e7', '微信公众号.jpg', 'admin/20201110/334246b771c0ab4a76c7913f4e852ead.jpg', 'aefaeb7dc6795770a607335e5a272df4', '8b058cbc9b0201db497aea26d4f5b5a328ecf846', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (131, 42539, 361712, 1605014672, 1, 0, 'e7493ea53cd2f357c71cc9ed59fff389e2fcbd6cdeab2f68274a80ea55136589', 'Screenshot_2020-11-10-15-01-54-896_com.rongyao.phonelive.jpg', 'default/20201110/232cc264cb3005dff0fe48cb82ad7d46.jpg', 'e7493ea53cd2f357c71cc9ed59fff389', 'c7d4c1bdc4232ceea3d64995e5b29d67010a951a', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (132, 42585, 88634, 1605017556, 1, 0, 'ed8de815fbd3fd41c46388f394c9f7a49a2398acfdb55a4ee1bd40684ec3df29', 'a7.png', 'default/20201110/37a7c1a4d2f5f4c1d29b87300deae7d0.png', 'ed8de815fbd3fd41c46388f394c9f7a4', 'f8d482419a5579c26cf5dcf916f81466fbb459cc', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (133, 42585, 60214, 1605017601, 1, 0, '5de7b40a0059afd2cf74bac7f452b55031334930b2ad9efc23864ec6fb28237d', 'a14.png', 'default/20201110/15d15377514bfe27907c1d16db4346a0.png', '5de7b40a0059afd2cf74bac7f452b550', '3ddabc29d11bcdf4b9c0e6e25af8530f983f99f7', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (134, 42585, 45499, 1605017633, 1, 0, 'c9b7b91edefea5266c77009d34415393d090a01e7fa5bc36dd65b1412500435d', 'a15.jpg', 'default/20201110/e7784c17805cc09799a532a2580616b0.jpg', 'c9b7b91edefea5266c77009d34415393', '0ba4fcc784b62e3928532253c8f4ada8787d5e59', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (135, 42521, 19257, 1605017733, 1, 0, '3435ac125655d376517dd04a740c553158c9eb1adef882bb015ca1ed1b3b2546', 'Snipaste_2020-10-26_15-58-48.png', 'default/20201110/ccf1ddce489dadad4cde22a2157aa0a1.png', '3435ac125655d376517dd04a740c5531', '4c2bbf21f38aa01942b2b08946eee499601633d1', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (136, 42521, 17674, 1605095301, 1, 0, '39a78fd1d2faf6ed408f87668333ba483b261816e1776190e7c7cc603b887772', 'Snipaste_2020-10-26_15-58-33.png', 'default/20201111/c055652ce832dca04635d69146c4d147.png', '39a78fd1d2faf6ed408f87668333ba48', '01d44d986fa89ab8253b478d7c9e992a21c159c8', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (137, 42515, 10910, 1605095398, 1, 0, '22803bd05bd3c926c14f801a3b5a87237ce95f5291cf45a5db6ad6dd3c746dba', 'photo_2020-11-11_19-47-56.jpg', 'default/20201111/296e166912ad91c1c659148fdbbe1db7.jpg', '22803bd05bd3c926c14f801a3b5a8723', 'dee326edceaba16b8f4bbdf02f5276a804c2b63e', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (138, 42515, 78593, 1605095402, 1, 0, 'a972c6d98d2848ef1434b6bd47281633d9389349662c3f62ae66d54d488f6366', '0006.jpg', 'default/20201111/7a55c7d7ecb5996057e1d16db86058e2.jpg', 'a972c6d98d2848ef1434b6bd47281633', '5a15ecb924399c62fd8185fb6cde355fe9af6865', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (139, 1, 4870, 1605249512, 1, 0, 'fa2f55983283626271e8b396e92f77ec2b566fe14d1198f7ed384a10565e6c99', '-1_03121A45M603.jpg', 'default/20201113/027060bd276e85c0cd29a168e8a1d34c.jpg', 'fa2f55983283626271e8b396e92f77ec', 'a65fd84e479f8f391fc2df253af880025e31e7f8', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (140, 42544, 18965, 1605337533, 1, 0, 'e79ad7a16fdb68582d6ada7537229745a9a294242321daf0b53640e87ee13ef6', 'new167026-20080629004049639649.jpg', 'default/20201114/efcaad4c27f31051b7427d5bfac35cd9.jpg', 'e79ad7a16fdb68582d6ada7537229745', '4053689c596b2d381b7060dfcb507b5cc1b29802', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (141, 42544, 41981, 1605337540, 1, 0, '2dd79ba844f79d23e60bacbba1482a5cf838b748e514d4bc5951be103568793c', '3.PNG', 'default/20201114/20c705502d389462330bb9cde1e532fb.png', '2dd79ba844f79d23e60bacbba1482a5c', '8ba5863ec2585a149f04010f906d4e52ea456861', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (142, 42590, 38284, 1605430272, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/03d2f0d997e29eac517ac1e7f6d89167.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (143, 42590, 68270, 1605430285, 1, 0, '5919fda41fa5f045355f0cb5468ff5730e90059970fd5591579f9e0f0e2ba212', 'photo_2020-11-11_17-06-24.jpg', 'default/20201115/9c0ca5e27a50062eab7c4f4b54c239bb.jpg', '5919fda41fa5f045355f0cb5468ff573', '093422bfffa3af6f27abbc2b795459574e4d8060', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (144, 42591, 38284, 1605432873, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/c47359f5b577e0dc442102f3febdefd3.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (145, 42592, 38284, 1605432936, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/5cbe5b09975e65e17f4de433a0cfd544.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (146, 42593, 38284, 1605432999, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/43223bb5fdbf59a441f79f0c91aeb68e.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (147, 42594, 38284, 1605433062, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/cc0178a64a199beeb6e9e575bf092e5e.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (148, 42595, 38284, 1605433121, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/a778a33945fed5b095a85efe9040a8cd.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (149, 42596, 38284, 1605433230, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/8b695224e599ec33d9f14f8289883464.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (150, 42597, 38284, 1605433287, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/84fa17b001ba5187fe8724b93eebbdff.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (151, 42598, 38284, 1605433366, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/76d4502008d189787c049e021be21191.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (152, 42599, 38284, 1605433918, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/900a31d57c13bc211340fecdcd34c85f.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (153, 42600, 38284, 1605434006, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/f5a548caae1ae599f707cb53550fb509.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (154, 42601, 38284, 1605434093, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/6355b9f366d818cd9f36ce467956dfa4.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (155, 42602, 38284, 1605434173, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/09e52d4e33e8d2871095d920c68746d6.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (156, 42603, 38284, 1605434228, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/77c88efc2a18b50bad5c393534f8ceb5.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (157, 42604, 38284, 1605434456, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/b26884b561d60001c768c862e1976f99.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (158, 42605, 38284, 1605434520, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/fd80d8f5eb3e81f161093c09ae812af4.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (159, 42606, 38284, 1605434577, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/5c4772e05eb370803a0db3b212f081b3.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (160, 42607, 38284, 1605434663, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/40e48e00f8c835fbd9dfbf1f8146c129.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (161, 42608, 38284, 1605434710, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/549abdcb99363bd9310bf20818129a45.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (162, 42609, 38284, 1605434802, 1, 0, '29b31f1cf10a321340d09b945a72aa4d93d6aac6117f3c6099cdc8b95ceba915', 'photo_2020-11-06_19-45-11.jpg', 'default/20201115/5c0133640ac985f8e8fe0d3a2a3c0306.jpg', '29b31f1cf10a321340d09b945a72aa4d', '6aa9cbd475df4427a2b146355c22cdca0b8df98b', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (163, 1, 3274, 1605675158, 1, 0, 'aefaeb7dc6795770a607335e5a272df45bcd0d54d953a672013a4755266c45e7', '公众号.jpg', 'admin/20201118/f18d7df0470f76a840c8689835375640.jpg', 'aefaeb7dc6795770a607335e5a272df4', '8b058cbc9b0201db497aea26d4f5b5a328ecf846', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (164, 42501, 17674, 1605698718, 1, 0, '39a78fd1d2faf6ed408f87668333ba483b261816e1776190e7c7cc603b887772', 'Snipaste_2020-10-26_15-58-33.png', 'default/20201118/6a4563f7b6eaf9ebb44879d23441fe9c.png', '39a78fd1d2faf6ed408f87668333ba48', '01d44d986fa89ab8253b478d7c9e992a21c159c8', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (165, 42501, 19257, 1605698748, 1, 0, '3435ac125655d376517dd04a740c553158c9eb1adef882bb015ca1ed1b3b2546', 'Snipaste_2020-10-26_15-58-48.png', 'default/20201118/aeba0bdbde73ac6a5c64efa7e1840f0a.png', '3435ac125655d376517dd04a740c5531', '4c2bbf21f38aa01942b2b08946eee499601633d1', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (166, 1, 1073, 1605795322, 1, 0, '8acc6c8b99dad3b009c63a33bcdcad0d445144f23c3d8c3107e0e35768b60fa6', 'photo_2020-11-19_22-14-41.jpg', 'admin/20201119/c3965b8c4730e03240a56a9ba27042ce.jpg', '8acc6c8b99dad3b009c63a33bcdcad0d', '47b6b3a85e71c759fac2c6a7f9d3a15133f2ec84', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (167, 1, 1007, 1605795342, 1, 0, '9cf3a628ca09e0c17a1d56099585bdc6840a1110fac6df44d527b70b5e79553d', 'photo_2020-11-19_22-14-48.jpg', 'admin/20201119/c3ffe3849ffc6b6c47b9acd3388ffa28.jpg', '9cf3a628ca09e0c17a1d56099585bdc6', 'ff1bc27417a8afa7c7c6ed91155864d650bbde99', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (168, 1, 5414736, 1605961316, 1, 0, '6d0f482fa3b01f80519e0bbf94ccdf91bccdfd9143d9e5a8e023b8b2198f3fbc', 'cangqiong-v1.2_12_jiagu_sign.apk', 'admin/20201121/a3e35156054be10f9fcd946edfc24669.apk', '6d0f482fa3b01f80519e0bbf94ccdf91', '50dede87dece38a80397a158beffd6928bdcd461', 'apk', NULL);
INSERT INTO `cmf_asset` VALUES (169, 1, 5398352, 1606021526, 1, 0, '153eef32e7e29b106966c30f34af839bece201a5ac1b603aedb84acb8664ed4a', 'xingkong-v1.2_12_jiagu_sign.apk', 'admin/20201122/7f05ad1677d753f374e7c3ad9ca7e62c.apk', '153eef32e7e29b106966c30f34af839b', '1713da41ae69daac022e49534e240529d423af17', 'apk', NULL);
INSERT INTO `cmf_asset` VALUES (170, 1, 5447504, 1606021953, 1, 0, '2a24fafa1a0900ab9fe452d0295e033b90802873b21fb7cb4c0e0435ada18b6a', 'struggle-v1.2_12_jiagu_sign.apk', 'admin/20201122/781ccfd204ce0a1cd46723f904eb84ac.apk', '2a24fafa1a0900ab9fe452d0295e033b', 'e709d8181d7ac0acb485542051a9d09afbd40952', 'apk', NULL);
INSERT INTO `cmf_asset` VALUES (171, 1, 5447504, 1606027533, 1, 0, 'f51a5f7ebc6650eb64df30a21254cd979bb1dd41ab7e177ce5427f5186b5d367', 'happy-v1.2_12_jiagu_sign.apk', 'admin/20201122/becb2653eee2cff8e82dc35cc34f5218.apk', 'f51a5f7ebc6650eb64df30a21254cd97', 'bfdfb3efa0a897dc507d71428b06d334dfeb719b', 'apk', NULL);
INSERT INTO `cmf_asset` VALUES (172, 1, 131072, 1606043872, 1, 0, '93d65022cec694e6236f90e80845e5b59b40947c753229f55684804b9014f88c', 'trailer_1.mp4', 'admin/20201122/c4225026e4a9095dbdf7a8bc8c606ba1.mp4', '93d65022cec694e6236f90e80845e5b5', '2bd77bfc0c05de105ce43e333f85cb5f2adbc6ae', 'mp4', NULL);
INSERT INTO `cmf_asset` VALUES (173, 1, 53948, 1611927326, 1, 0, 'a4e27627bb49552eb702c0a0183c35bd453ae2dd163709f00351cf147669e105', 'photo_2021-01-21_15-09-13.jpg', 'portal/20210129/6b2f4cf44adb2e6958485d141a70ca94.jpg', 'a4e27627bb49552eb702c0a0183c35bd', '3a4b7299d350449a69e21907c7812faaacf10975', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (174, 1, 144432, 1611927334, 1, 0, '3dfd3289cccc5c8457155080ad96e3dfe782c0274305bbb6c0f34b9a44c6b5ad', 'photo_2021-01-21_15-09-32.jpg', 'portal/20210129/25a1bb25d7a7fe99578669d4942222c5.jpg', '3dfd3289cccc5c8457155080ad96e3df', '88596097883ed1c74a28a7a866e1507901a1813a', 'jpg', NULL);

-- ----------------------------
-- Table structure for cmf_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_access`;
CREATE TABLE `cmf_auth_access`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色',
  `rule_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '权限规则分类,请加应用前缀,如admin_',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `rule_name`(`rule_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 551 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限授权表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_auth_access
-- ----------------------------
INSERT INTO `cmf_auth_access` VALUES (1, 7, 'admin/setting/password', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (2, 7, 'admin/setting/passwordpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (3, 7, 'admin/setting/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (4, 7, 'admin/setting/site', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (5, 7, 'admin/setting/sitepost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (6, 7, 'admin/setting/configpri', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (7, 7, 'admin/setting/configpripost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (8, 7, 'admin/slide/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (9, 7, 'admin/slide/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (10, 7, 'admin/slide/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (11, 7, 'admin/slide/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (12, 7, 'admin/slide/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (13, 7, 'admin/slide/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (14, 7, 'admin/slideitem/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (15, 7, 'admin/slideitem/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (16, 7, 'admin/slideitem/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (17, 7, 'admin/slideitem/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (18, 7, 'admin/slideitem/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (19, 7, 'admin/slideitem/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (20, 7, 'admin/slideitem/ban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (21, 7, 'admin/slideitem/cancelban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (22, 7, 'admin/slideitem/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (23, 7, 'admin/link/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (24, 7, 'admin/link/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (25, 7, 'admin/link/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (26, 7, 'admin/link/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (27, 7, 'admin/link/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (28, 7, 'admin/link/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (29, 7, 'admin/link/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (30, 7, 'admin/link/toggle', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (31, 7, 'admin/guide/set', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (32, 7, 'admin/guide/setpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (33, 7, 'admin/guide/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (34, 7, 'admin/guide/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (35, 7, 'admin/guide/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (36, 7, 'admin/guide/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (37, 7, 'admin/guide/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (38, 7, 'admin/guide/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (39, 7, 'admin/guide/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (40, 7, 'admin/setting/upload', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (41, 7, 'admin/setting/uploadpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (42, 7, 'admin/setting/clearcache', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (43, 7, 'admin/storage/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (44, 7, 'admin/storage/settingpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (45, 7, 'user/adminindex/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (46, 7, 'admin/user/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (47, 7, 'admin/rbac/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (48, 7, 'admin/rbac/roleadd', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (49, 7, 'admin/rbac/roleaddpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (50, 7, 'admin/rbac/roleedit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (51, 7, 'admin/rbac/roleeditpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (52, 7, 'admin/rbac/roledelete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (53, 7, 'admin/rbac/authorize', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (54, 7, 'admin/rbac/authorizepost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (55, 7, 'admin/user/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (56, 7, 'admin/user/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (57, 7, 'admin/user/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (58, 7, 'admin/user/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (59, 7, 'admin/user/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (60, 7, 'admin/user/userinfo', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (61, 7, 'admin/user/userinfopost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (62, 7, 'admin/user/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (63, 7, 'admin/user/ban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (64, 7, 'admin/user/cancelban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (65, 7, 'admin/adminlog/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (66, 7, 'admin/adminlog/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (67, 7, 'admin/adminlog/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (68, 7, 'user/adminindex/default1', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (69, 7, 'user/adminindex/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (70, 7, 'user/adminindex/ban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (71, 7, 'user/adminindex/cancelban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (72, 7, 'user/adminindex/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (73, 7, 'user/adminindex/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (74, 7, 'user/adminindex/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (75, 7, 'user/adminindex/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (76, 7, 'user/adminindex/setban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (77, 7, 'user/adminindex/setsuper', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (78, 7, 'user/adminindex/sethot', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (79, 7, 'user/adminindex/setrecommend', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (80, 7, 'user/adminindex/setzombie', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (81, 7, 'user/adminindex/setzombiep', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (82, 7, 'user/adminindex/setzombiepall', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (83, 7, 'user/adminindex/setzombieall', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (84, 7, 'user/adminindex/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (85, 7, 'admin/impression/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (86, 7, 'admin/impression/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (87, 7, 'admin/impression/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (88, 7, 'admin/impression/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (89, 7, 'admin/impression/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (90, 7, 'admin/impression/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (91, 7, 'admin/impression/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (92, 7, 'admin/jackpot/set', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (93, 7, 'admin/jackpot/setpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (94, 7, 'admin/jackpot/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (95, 7, 'admin/jackpot/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (96, 7, 'admin/jackpot/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (97, 7, 'admin/jackpot/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (98, 7, 'admin/jackpot/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (99, 7, 'admin/jackpot/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (100, 7, 'admin/auth/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (101, 7, 'admin/auth/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (102, 7, 'admin/auth/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (103, 7, 'admin/live/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (104, 7, 'admin/liveclass/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (105, 7, 'admin/liveclass/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (106, 7, 'admin/liveclass/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (107, 7, 'admin/liveclass/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (108, 7, 'admin/liveclass/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (109, 7, 'admin/liveclass/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (110, 7, 'admin/liveclass/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (111, 7, 'admin/liveban/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (112, 7, 'admin/liveban/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (113, 7, 'admin/liveshut/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (114, 7, 'admin/liveshut/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (115, 7, 'admin/livekick/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (116, 7, 'admin/livekick/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (117, 7, 'admin/liveing/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (118, 7, 'admin/liveing/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (119, 7, 'admin/liveing/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (120, 7, 'admin/liveing/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (121, 7, 'admin/liveing/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (122, 7, 'admin/liveing/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (123, 7, 'admin/monitor/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (124, 7, 'admin/monitor/full', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (125, 7, 'admin/monitor/stop', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (126, 7, 'admin/gift/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (127, 7, 'admin/gift/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (128, 7, 'admin/gift/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (129, 7, 'admin/gift/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (130, 7, 'admin/gift/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (131, 7, 'admin/gift/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (132, 7, 'admin/gift/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (133, 7, 'admin/sticker/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (134, 7, 'admin/sticker/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (135, 7, 'admin/sticker/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (136, 7, 'admin/sticker/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (137, 7, 'admin/sticker/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (138, 7, 'admin/sticker/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (139, 7, 'admin/sticker/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (140, 7, 'admin/report/defult', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (141, 7, 'admin/reportcat/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (142, 7, 'admin/reportcat/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (143, 7, 'admin/reportcat/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (144, 7, 'admin/reportcat/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (145, 7, 'admin/reportcat/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (146, 7, 'admin/reportcat/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (147, 7, 'admin/reportcat/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (148, 7, 'admin/report/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (149, 7, 'admin/report/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (150, 7, 'admin/liverecord/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (151, 7, 'admin/game/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (152, 7, 'admin/game/index2', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (153, 7, 'admin/dynamic/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (154, 7, 'admin/dynamic/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (155, 7, 'admin/dynamic/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (156, 7, 'admin/dynamic/see', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (157, 7, 'admin/dynamic/setdel', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (158, 7, 'admin/dynamic/setrecom', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (159, 7, 'admin/dynamic/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (160, 7, 'admin/dynamic/wait', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (161, 7, 'admin/dynamic/nopass', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (162, 7, 'admin/dynamic/lower', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (163, 7, 'admin/dynamicrepotcat/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (164, 7, 'admin/dynamicrepotcat/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (165, 7, 'admin/dynamicrepotcat/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (166, 7, 'admin/dynamicrepotcat/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (167, 7, 'admin/dynamicrepotcat/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (168, 7, 'admin/dynamicrepotcat/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (169, 7, 'admin/dynamicrepotcat/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (170, 7, 'admin/dynamicrepot/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (171, 7, 'admin/dynamicrepot/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (172, 7, 'admin/dynamicrepot/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (173, 7, 'admin/dynamiccom/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (174, 7, 'admin/dynamiccom/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (175, 7, 'admin/dynamiclabel/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (176, 7, 'admin/dynamiclabel/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (177, 7, 'admin/dynamiclabel/add_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (178, 7, 'admin/dynamiclabel/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (179, 7, 'admin/dynamiclabel/edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (180, 7, 'admin/dynamiclabel/listsorders', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (181, 7, 'admin/dynamiclabel/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (182, 7, 'admin/video/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (183, 7, 'admin/musiccat/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (184, 7, 'admin/musiccat/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (185, 7, 'admin/musiccat/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (186, 7, 'admin/musiccat/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (187, 7, 'admin/musiccat/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (188, 7, 'admin/musiccat/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (189, 7, 'admin/musiccat/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (190, 7, 'admin/music/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (191, 7, 'admin/music/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (192, 7, 'admin/music/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (193, 7, 'admin/music/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (194, 7, 'admin/music/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (195, 7, 'admin/music/listen', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (196, 7, 'admin/music/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (197, 7, 'admin/music/canceldel', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (198, 7, 'admin/videoclass/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (199, 7, 'admin/videoclass/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (200, 7, 'admin/videoclass/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (201, 7, 'admin/videoclass/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (202, 7, 'admin/videoclass/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (203, 7, 'admin/videoclass/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (204, 7, 'admin/videoclass/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (205, 7, 'admin/video/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (206, 7, 'admin/video/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (207, 7, 'admin/video/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (208, 7, 'admin/video/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (209, 7, 'admin/video/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (210, 7, 'admin/video/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (211, 7, 'admin/video/see', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (212, 7, 'admin/video/wait', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (213, 7, 'admin/video/nopass', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (214, 7, 'admin/video/lower', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (215, 7, 'admin/videocom/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (216, 7, 'admin/videocom/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (217, 7, 'admin/videorepcat/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (218, 7, 'admin/videorepcat/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (219, 7, 'admin/videorepcat/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (220, 7, 'admin/videorepcat/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (221, 7, 'admin/videorepcat/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (222, 7, 'admin/videorepcat/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (223, 7, 'admin/videorepcat/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (224, 7, 'admin/videorep/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (225, 7, 'admin/videorep/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (226, 7, 'admin/videorep/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (227, 7, 'admin/finance/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (228, 7, 'admin/chargerules/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (229, 7, 'admin/chargerules/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (230, 7, 'admin/chargerules/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (231, 7, 'admin/chargerules/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (232, 7, 'admin/chargerules/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (233, 7, 'admin/chargerules/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (234, 7, 'admin/chargerules/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (235, 7, 'admin/charge/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (236, 7, 'admin/charge/setpay', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (237, 7, 'admin/charge/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (238, 7, 'admin/manual/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (239, 7, 'admin/manual/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (240, 7, 'admin/manual/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (241, 7, 'admin/manual/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (242, 7, 'admin/coinrecord/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (243, 7, 'admin/cash/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (244, 7, 'admin/cash/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (245, 7, 'admin/cash/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (246, 7, 'admin/cash/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (247, 7, 'admin/scorerecord/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (248, 7, 'admin/family/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (249, 7, 'admin/family/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (250, 7, 'admin/family/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (251, 7, 'admin/family/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (252, 7, 'admin/family/disable', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (253, 7, 'admin/family/enable', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (254, 7, 'admin/family/profit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (255, 7, 'admin/family/cash', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (256, 7, 'admin/family/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (257, 7, 'admin/familyuser/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (258, 7, 'admin/familyuser/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (259, 7, 'admin/familyuser/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (260, 7, 'admin/familyuser/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (261, 7, 'admin/familyuser/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (262, 7, 'admin/familyuser/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (263, 7, 'admin/familyuser/divideapply', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (264, 7, 'admin/familyuser/applyedit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (265, 7, 'admin/familyuser/applyeditpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (266, 7, 'admin/familyuser/delapply', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (267, 7, 'admin/car/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (268, 7, 'admin/car/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (269, 7, 'admin/car/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (270, 7, 'admin/car/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (271, 7, 'admin/car/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (272, 7, 'admin/car/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (273, 7, 'admin/car/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (274, 7, 'admin/car/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (275, 7, 'admin/liang/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (276, 7, 'admin/liang/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (277, 7, 'admin/liang/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (278, 7, 'admin/liang/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (279, 7, 'admin/liang/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (280, 7, 'admin/liang/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (281, 7, 'admin/liang/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (282, 7, 'admin/liang/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (283, 7, 'admin/vip/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (284, 7, 'admin/vip/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (285, 7, 'admin/vip/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (286, 7, 'admin/vip/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (287, 7, 'admin/vip/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (288, 7, 'admin/vipuser/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (289, 7, 'admin/vipuser/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (290, 7, 'admin/vipuser/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (291, 7, 'admin/vipuser/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (292, 7, 'admin/vipuser/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (293, 7, 'admin/vipuser/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (294, 7, 'admin/red/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (295, 7, 'admin/red/index2', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (296, 7, 'admin/guard/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (297, 7, 'admin/guard/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (298, 7, 'admin/guard/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (299, 7, 'admin/level/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (300, 7, 'admin/level/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (301, 7, 'admin/level/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (302, 7, 'admin/level/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (303, 7, 'admin/level/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (304, 7, 'admin/level/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (305, 7, 'admin/level/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (306, 7, 'admin/levelanchor/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (307, 7, 'admin/levelanchor/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (308, 7, 'admin/levelanchor/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (309, 7, 'admin/levelanchor/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (310, 7, 'admin/levelanchor/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (311, 7, 'admin/levelanchor/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (312, 7, 'admin/agent/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (313, 7, 'admin/agent/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (314, 7, 'admin/agent/index2', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (315, 7, 'admin/shop/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (316, 7, 'admin/shopapply/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (317, 7, 'admin/shopapply/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (318, 7, 'admin/shopapply/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (319, 7, 'admin/shopapply/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (320, 7, 'admin/shopbond/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (321, 7, 'admin/shopbond/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (322, 7, 'admin/shopgoods/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (323, 7, 'admin/shopgoods/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (324, 7, 'admin/shopgoods/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (325, 7, 'admin/shopgoods/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (326, 7, 'admin/shopgoods/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (327, 7, 'admin/shopgoods/commentlist', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (328, 7, 'admin/shopgoods/delcomment', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (329, 7, 'admin/goodsclass/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (330, 7, 'admin/goodsclass/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (331, 7, 'admin/goodsclass/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (332, 7, 'admin/goodsclass/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (333, 7, 'admin/goodsclass/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (334, 7, 'admin/goodsclass/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (335, 7, 'admin/buyeraddress/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (336, 7, 'admin/buyeraddress/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (337, 7, 'admin/express/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (338, 7, 'admin/express/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (339, 7, 'admin/express/add_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (340, 7, 'admin/express/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (341, 7, 'admin/express/edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (342, 7, 'admin/express/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (343, 7, 'admin/express/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (344, 7, 'admin/refundreason/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (345, 7, 'admin/refundreason/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (346, 7, 'admin/refundreason/add_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (347, 7, 'admin/refundreason/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (348, 7, 'admin/refundreason/edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (349, 7, 'admin/refundreason/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (350, 7, 'admin/refundreason/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (351, 7, 'admin/refusereason/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (352, 7, 'admin/refusereason/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (353, 7, 'admin/refusereason/add_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (354, 7, 'admin/refusereason/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (355, 7, 'admin/refusereason/edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (356, 7, 'admin/refusereason/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (357, 7, 'admin/refusereason/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (358, 7, 'admin/platformreason/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (359, 7, 'admin/platformreason/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (360, 7, 'admin/platformreason/add_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (361, 7, 'admin/platformreason/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (362, 7, 'admin/platformreason/edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (363, 7, 'admin/platformreason/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (364, 7, 'admin/platformreason/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (365, 7, 'admin/goodsorder/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (366, 7, 'admin/goodsorder/info', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (367, 7, 'admin/refundlist/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (368, 7, 'admin/refundlist/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (369, 7, 'admin/refundlist/edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (370, 7, 'admin/shopcash/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (371, 7, 'admin/shopcash/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (372, 7, 'admin/shopcash/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (373, 7, 'admin/shopcash/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (374, 7, 'admin/balance/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (375, 7, 'admin/balance/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (376, 7, 'admin/balance/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (377, 7, 'admin/balance/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (378, 7, 'admin/turntable/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (379, 7, 'admin/turntablecon/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (380, 7, 'admin/turntablecon/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (381, 7, 'admin/turntablecon/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (382, 7, 'admin/turntablecon/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (383, 7, 'admin/turntable/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (384, 7, 'admin/turntable/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (385, 7, 'admin/turntable/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (386, 7, 'admin/turntable/index2', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (387, 7, 'admin/turntable/index3', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (388, 7, 'admin/turntable/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (389, 7, 'admin/loginbonus/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (390, 7, 'admin/loginbonus/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (391, 7, 'admin/loginbonus/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (392, 7, 'admin/note/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (393, 7, 'admin/sendcode/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (394, 7, 'admin/sendcode/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (395, 7, 'admin/sendcode/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (396, 7, 'admin/push/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (397, 7, 'admin/push/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (398, 7, 'admin/push/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (399, 7, 'admin/push/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (400, 7, 'admin/system/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (401, 7, 'admin/system/send', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (402, 7, 'admin/portal/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (403, 7, 'admin/feedback/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (404, 7, 'admin/feedback/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (405, 7, 'admin/feedback/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (406, 7, 'portal/adminpage/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (407, 7, 'portal/adminpage/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (408, 7, 'portal/adminpage/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (409, 7, 'portal/adminpage/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (410, 7, 'portal/adminpage/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (411, 7, 'portal/adminpage/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (412, 7, 'admin/paidprogram/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (413, 7, 'admin/paidprogramclass/classlist', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (414, 7, 'admin/paidprogramclass/class_add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (415, 7, 'admin/paidprogramclass/class_add_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (416, 7, 'admin/paidprogramclass/class_edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (417, 7, 'admin/paidprogramclass/class_edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (418, 7, 'admin/paidprogramclass/class_del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (419, 7, 'admin/paidprogramclass/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (420, 7, 'admin/paidprogram/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (421, 7, 'admin/paidprogram/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (422, 7, 'admin/paidprogram/edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (423, 7, 'admin/paidprogram/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (424, 7, 'admin/paidprogram/videoplay', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (425, 7, 'admin/paidprogram/applylist', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (426, 7, 'admin/paidprogram/apply_edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (427, 7, 'admin/paidprogram/apply_edit_post', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (428, 7, 'admin/paidprogram/apply_del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (429, 7, 'admin/paidprogram/orderlist', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (430, 7, 'admin/paidprogram/setpay', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (431, 7, 'admin/paidprogram/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (432, 7, 'admin/plugin/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (433, 7, 'admin/plugin/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (434, 7, 'admin/plugin/toggle', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (435, 7, 'admin/plugin/setting', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (436, 7, 'admin/plugin/settingpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (437, 7, 'admin/plugin/install', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (438, 7, 'admin/plugin/update', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (439, 7, 'admin/plugin/uninstall', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (440, 7, 'admin/menu/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (441, 7, 'admin/menu/lists', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (442, 7, 'admin/menu/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (443, 7, 'admin/menu/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (444, 7, 'admin/menu/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (445, 7, 'admin/menu/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (446, 7, 'admin/menu/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (447, 7, 'admin/menu/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (448, 7, 'admin/menu/getactions', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (449, 7, 'admin/recyclebin/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (450, 7, 'admin/recyclebin/restore', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (451, 7, 'admin/recyclebin/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (452, 7, 'user/adminasset/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (453, 7, 'user/adminasset/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (454, 8, 'user/adminindex/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (455, 8, 'admin/user/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (456, 8, 'admin/rbac/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (457, 8, 'admin/rbac/roleadd', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (458, 8, 'admin/rbac/roleaddpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (459, 8, 'admin/rbac/roleedit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (460, 8, 'admin/rbac/roleeditpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (461, 8, 'admin/rbac/roledelete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (462, 8, 'admin/rbac/authorize', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (463, 8, 'admin/rbac/authorizepost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (464, 8, 'admin/user/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (465, 8, 'admin/user/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (466, 8, 'admin/user/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (467, 8, 'admin/user/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (468, 8, 'admin/user/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (469, 8, 'admin/user/userinfo', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (470, 8, 'admin/user/userinfopost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (471, 8, 'admin/user/delete', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (472, 8, 'admin/user/ban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (473, 8, 'admin/user/cancelban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (474, 8, 'admin/adminlog/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (475, 8, 'admin/adminlog/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (476, 8, 'admin/adminlog/export', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (477, 8, 'user/adminindex/default1', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (478, 8, 'user/adminindex/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (479, 8, 'user/adminindex/ban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (480, 8, 'user/adminindex/cancelban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (481, 8, 'user/adminindex/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (482, 8, 'user/adminindex/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (483, 8, 'user/adminindex/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (484, 8, 'user/adminindex/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (485, 8, 'user/adminindex/setban', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (486, 8, 'user/adminindex/setsuper', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (487, 8, 'user/adminindex/sethot', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (488, 8, 'user/adminindex/setrecommend', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (489, 8, 'user/adminindex/setzombie', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (490, 8, 'user/adminindex/setzombiep', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (491, 8, 'user/adminindex/setzombiepall', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (492, 8, 'user/adminindex/setzombieall', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (493, 8, 'user/adminindex/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (494, 8, 'admin/impression/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (495, 8, 'admin/impression/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (496, 8, 'admin/impression/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (497, 8, 'admin/impression/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (498, 8, 'admin/impression/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (499, 8, 'admin/impression/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (500, 8, 'admin/impression/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (501, 8, 'admin/live/default', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (502, 8, 'admin/liveclass/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (503, 8, 'admin/liveclass/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (504, 8, 'admin/liveclass/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (505, 8, 'admin/liveclass/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (506, 8, 'admin/liveclass/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (507, 8, 'admin/liveclass/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (508, 8, 'admin/liveclass/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (509, 8, 'admin/liveban/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (510, 8, 'admin/liveban/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (511, 8, 'admin/liveshut/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (512, 8, 'admin/liveshut/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (513, 8, 'admin/livekick/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (514, 8, 'admin/livekick/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (515, 8, 'admin/liveing/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (516, 8, 'admin/liveing/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (517, 8, 'admin/liveing/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (518, 8, 'admin/liveing/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (519, 8, 'admin/liveing/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (520, 8, 'admin/liveing/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (521, 8, 'admin/monitor/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (522, 8, 'admin/monitor/full', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (523, 8, 'admin/monitor/stop', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (524, 8, 'admin/gift/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (525, 8, 'admin/gift/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (526, 8, 'admin/gift/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (527, 8, 'admin/gift/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (528, 8, 'admin/gift/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (529, 8, 'admin/gift/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (530, 8, 'admin/gift/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (531, 8, 'admin/sticker/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (532, 8, 'admin/sticker/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (533, 8, 'admin/sticker/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (534, 8, 'admin/sticker/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (535, 8, 'admin/sticker/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (536, 8, 'admin/sticker/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (537, 8, 'admin/sticker/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (538, 8, 'admin/report/defult', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (539, 8, 'admin/reportcat/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (540, 8, 'admin/reportcat/add', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (541, 8, 'admin/reportcat/addpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (542, 8, 'admin/reportcat/edit', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (543, 8, 'admin/reportcat/editpost', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (544, 8, 'admin/reportcat/listorder', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (545, 8, 'admin/reportcat/del', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (546, 8, 'admin/report/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (547, 8, 'admin/report/setstatus', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (548, 8, 'admin/liverecord/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (549, 8, 'admin/game/index', 'admin_url');
INSERT INTO `cmf_auth_access` VALUES (550, 8, 'admin/game/index2', 'admin_url');

-- ----------------------------
-- Table structure for cmf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_rule`;
CREATE TABLE `cmf_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否有效(0:无效,1:有效)',
  `app` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则所属app',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限规则分类，请加应用前缀,如admin_',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '额外url参数',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则描述',
  `condition` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `module`(`app`, `status`, `type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 513 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_auth_rule
-- ----------------------------
INSERT INTO `cmf_auth_rule` VALUES (1, 1, 'admin', 'admin_url', 'admin/Hook/index', '', '钩子管理', '');
INSERT INTO `cmf_auth_rule` VALUES (2, 1, 'admin', 'admin_url', 'admin/Hook/plugins', '', '钩子插件管理', '');
INSERT INTO `cmf_auth_rule` VALUES (3, 1, 'admin', 'admin_url', 'admin/Hook/pluginListOrder', '', '钩子插件排序', '');
INSERT INTO `cmf_auth_rule` VALUES (4, 1, 'admin', 'admin_url', 'admin/Hook/sync', '', '同步钩子', '');
INSERT INTO `cmf_auth_rule` VALUES (5, 1, 'admin', 'admin_url', 'admin/Link/index', '', '友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (6, 1, 'admin', 'admin_url', 'admin/Link/add', '', '添加友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (7, 1, 'admin', 'admin_url', 'admin/Link/addPost', '', '添加友情链接提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (8, 1, 'admin', 'admin_url', 'admin/Link/edit', '', '编辑友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (9, 1, 'admin', 'admin_url', 'admin/Link/editPost', '', '编辑友情链接提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (10, 1, 'admin', 'admin_url', 'admin/Link/delete', '', '删除友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (11, 1, 'admin', 'admin_url', 'admin/Link/listOrder', '', '友情链接排序', '');
INSERT INTO `cmf_auth_rule` VALUES (12, 1, 'admin', 'admin_url', 'admin/Link/toggle', '', '友情链接显示隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (13, 1, 'admin', 'admin_url', 'admin/Mailer/index', '', '邮箱配置', '');
INSERT INTO `cmf_auth_rule` VALUES (14, 1, 'admin', 'admin_url', 'admin/Mailer/indexPost', '', '邮箱配置提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (15, 1, 'admin', 'admin_url', 'admin/Mailer/template', '', '邮件模板', '');
INSERT INTO `cmf_auth_rule` VALUES (16, 1, 'admin', 'admin_url', 'admin/Mailer/templatePost', '', '邮件模板提交', '');
INSERT INTO `cmf_auth_rule` VALUES (17, 1, 'admin', 'admin_url', 'admin/Mailer/test', '', '邮件发送测试', '');
INSERT INTO `cmf_auth_rule` VALUES (18, 1, 'admin', 'admin_url', 'admin/Menu/index', '', '后台菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (19, 1, 'admin', 'admin_url', 'admin/Menu/lists', '', '所有菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (20, 1, 'admin', 'admin_url', 'admin/Menu/add', '', '后台菜单添加', '');
INSERT INTO `cmf_auth_rule` VALUES (21, 1, 'admin', 'admin_url', 'admin/Menu/addPost', '', '后台菜单添加提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (22, 1, 'admin', 'admin_url', 'admin/Menu/edit', '', '后台菜单编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (23, 1, 'admin', 'admin_url', 'admin/Menu/editPost', '', '后台菜单编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (24, 1, 'admin', 'admin_url', 'admin/Menu/delete', '', '后台菜单删除', '');
INSERT INTO `cmf_auth_rule` VALUES (25, 1, 'admin', 'admin_url', 'admin/Menu/listOrder', '', '后台菜单排序', '');
INSERT INTO `cmf_auth_rule` VALUES (26, 1, 'admin', 'admin_url', 'admin/Menu/getActions', '', '导入新后台菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (27, 1, 'admin', 'admin_url', 'admin/Nav/index', '', '导航管理', '');
INSERT INTO `cmf_auth_rule` VALUES (28, 1, 'admin', 'admin_url', 'admin/Nav/add', '', '添加导航', '');
INSERT INTO `cmf_auth_rule` VALUES (29, 1, 'admin', 'admin_url', 'admin/Nav/addPost', '', '添加导航提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (30, 1, 'admin', 'admin_url', 'admin/Nav/edit', '', '编辑导航', '');
INSERT INTO `cmf_auth_rule` VALUES (31, 1, 'admin', 'admin_url', 'admin/Nav/editPost', '', '编辑导航提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (32, 1, 'admin', 'admin_url', 'admin/Nav/delete', '', '删除导航', '');
INSERT INTO `cmf_auth_rule` VALUES (33, 1, 'admin', 'admin_url', 'admin/NavMenu/index', '', '导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (34, 1, 'admin', 'admin_url', 'admin/NavMenu/add', '', '添加导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (35, 1, 'admin', 'admin_url', 'admin/NavMenu/addPost', '', '添加导航菜单提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (36, 1, 'admin', 'admin_url', 'admin/NavMenu/edit', '', '编辑导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (37, 1, 'admin', 'admin_url', 'admin/NavMenu/editPost', '', '编辑导航菜单提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (38, 1, 'admin', 'admin_url', 'admin/NavMenu/delete', '', '删除导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (39, 1, 'admin', 'admin_url', 'admin/NavMenu/listOrder', '', '导航菜单排序', '');
INSERT INTO `cmf_auth_rule` VALUES (40, 1, 'admin', 'admin_url', 'admin/Plugin/default', '', '插件中心', '');
INSERT INTO `cmf_auth_rule` VALUES (41, 1, 'admin', 'admin_url', 'admin/Plugin/index', '', '插件列表', '');
INSERT INTO `cmf_auth_rule` VALUES (42, 1, 'admin', 'admin_url', 'admin/Plugin/toggle', '', '插件启用禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (43, 1, 'admin', 'admin_url', 'admin/Plugin/setting', '', '插件设置', '');
INSERT INTO `cmf_auth_rule` VALUES (44, 1, 'admin', 'admin_url', 'admin/Plugin/settingPost', '', '插件设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (45, 1, 'admin', 'admin_url', 'admin/Plugin/install', '', '插件安装', '');
INSERT INTO `cmf_auth_rule` VALUES (46, 1, 'admin', 'admin_url', 'admin/Plugin/update', '', '插件更新', '');
INSERT INTO `cmf_auth_rule` VALUES (47, 1, 'admin', 'admin_url', 'admin/Plugin/uninstall', '', '卸载插件', '');
INSERT INTO `cmf_auth_rule` VALUES (48, 1, 'admin', 'admin_url', 'admin/Rbac/index', '', '角色管理', '');
INSERT INTO `cmf_auth_rule` VALUES (49, 1, 'admin', 'admin_url', 'admin/Rbac/roleAdd', '', '添加角色', '');
INSERT INTO `cmf_auth_rule` VALUES (50, 1, 'admin', 'admin_url', 'admin/Rbac/roleAddPost', '', '添加角色提交', '');
INSERT INTO `cmf_auth_rule` VALUES (51, 1, 'admin', 'admin_url', 'admin/Rbac/roleEdit', '', '编辑角色', '');
INSERT INTO `cmf_auth_rule` VALUES (52, 1, 'admin', 'admin_url', 'admin/Rbac/roleEditPost', '', '编辑角色提交', '');
INSERT INTO `cmf_auth_rule` VALUES (53, 1, 'admin', 'admin_url', 'admin/Rbac/roleDelete', '', '删除角色', '');
INSERT INTO `cmf_auth_rule` VALUES (54, 1, 'admin', 'admin_url', 'admin/Rbac/authorize', '', '设置角色权限', '');
INSERT INTO `cmf_auth_rule` VALUES (55, 1, 'admin', 'admin_url', 'admin/Rbac/authorizePost', '', '角色授权提交', '');
INSERT INTO `cmf_auth_rule` VALUES (56, 1, 'admin', 'admin_url', 'admin/RecycleBin/index', '', '回收站', '');
INSERT INTO `cmf_auth_rule` VALUES (57, 1, 'admin', 'admin_url', 'admin/RecycleBin/restore', '', '回收站还原', '');
INSERT INTO `cmf_auth_rule` VALUES (58, 1, 'admin', 'admin_url', 'admin/RecycleBin/delete', '', '回收站彻底删除', '');
INSERT INTO `cmf_auth_rule` VALUES (59, 1, 'admin', 'admin_url', 'admin/Route/index', '', 'URL美化', '');
INSERT INTO `cmf_auth_rule` VALUES (60, 1, 'admin', 'admin_url', 'admin/Route/add', '', '添加路由规则', '');
INSERT INTO `cmf_auth_rule` VALUES (61, 1, 'admin', 'admin_url', 'admin/Route/addPost', '', '添加路由规则提交', '');
INSERT INTO `cmf_auth_rule` VALUES (62, 1, 'admin', 'admin_url', 'admin/Route/edit', '', '路由规则编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (63, 1, 'admin', 'admin_url', 'admin/Route/editPost', '', '路由规则编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (64, 1, 'admin', 'admin_url', 'admin/Route/delete', '', '路由规则删除', '');
INSERT INTO `cmf_auth_rule` VALUES (65, 1, 'admin', 'admin_url', 'admin/Route/ban', '', '路由规则禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (66, 1, 'admin', 'admin_url', 'admin/Route/open', '', '路由规则启用', '');
INSERT INTO `cmf_auth_rule` VALUES (67, 1, 'admin', 'admin_url', 'admin/Route/listOrder', '', '路由规则排序', '');
INSERT INTO `cmf_auth_rule` VALUES (68, 1, 'admin', 'admin_url', 'admin/Route/select', '', '选择URL', '');
INSERT INTO `cmf_auth_rule` VALUES (69, 1, 'admin', 'admin_url', 'admin/Setting/default', '', '设置', '');
INSERT INTO `cmf_auth_rule` VALUES (70, 1, 'admin', 'admin_url', 'admin/Setting/site', '', '网站信息', '');
INSERT INTO `cmf_auth_rule` VALUES (71, 1, 'admin', 'admin_url', 'admin/Setting/sitePost', '', '网站信息设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (72, 1, 'admin', 'admin_url', 'admin/Setting/password', '', '密码修改', '');
INSERT INTO `cmf_auth_rule` VALUES (73, 1, 'admin', 'admin_url', 'admin/Setting/passwordPost', '', '密码修改提交', '');
INSERT INTO `cmf_auth_rule` VALUES (74, 1, 'admin', 'admin_url', 'admin/Setting/upload', '', '上传设置', '');
INSERT INTO `cmf_auth_rule` VALUES (75, 1, 'admin', 'admin_url', 'admin/Setting/uploadPost', '', '上传设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (76, 1, 'admin', 'admin_url', 'admin/Setting/clearCache', '', '清除缓存', '');
INSERT INTO `cmf_auth_rule` VALUES (77, 1, 'admin', 'admin_url', 'admin/Slide/index', '', '幻灯片管理', '');
INSERT INTO `cmf_auth_rule` VALUES (78, 1, 'admin', 'admin_url', 'admin/Slide/add', '', '添加幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (79, 1, 'admin', 'admin_url', 'admin/Slide/addPost', '', '添加幻灯片提交', '');
INSERT INTO `cmf_auth_rule` VALUES (80, 1, 'admin', 'admin_url', 'admin/Slide/edit', '', '编辑幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (81, 1, 'admin', 'admin_url', 'admin/Slide/editPost', '', '编辑幻灯片提交', '');
INSERT INTO `cmf_auth_rule` VALUES (82, 1, 'admin', 'admin_url', 'admin/Slide/delete', '', '删除幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (83, 1, 'admin', 'admin_url', 'admin/SlideItem/index', '', '幻灯片页面列表', '');
INSERT INTO `cmf_auth_rule` VALUES (84, 1, 'admin', 'admin_url', 'admin/SlideItem/add', '', '幻灯片页面添加', '');
INSERT INTO `cmf_auth_rule` VALUES (85, 1, 'admin', 'admin_url', 'admin/SlideItem/addPost', '', '幻灯片页面添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (86, 1, 'admin', 'admin_url', 'admin/SlideItem/edit', '', '幻灯片页面编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (87, 1, 'admin', 'admin_url', 'admin/SlideItem/editPost', '', '幻灯片页面编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (88, 1, 'admin', 'admin_url', 'admin/SlideItem/delete', '', '幻灯片页面删除', '');
INSERT INTO `cmf_auth_rule` VALUES (89, 1, 'admin', 'admin_url', 'admin/SlideItem/ban', '', '幻灯片页面隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (90, 1, 'admin', 'admin_url', 'admin/SlideItem/cancelBan', '', '幻灯片页面显示', '');
INSERT INTO `cmf_auth_rule` VALUES (91, 1, 'admin', 'admin_url', 'admin/SlideItem/listOrder', '', '幻灯片页面排序', '');
INSERT INTO `cmf_auth_rule` VALUES (92, 1, 'admin', 'admin_url', 'admin/Storage/index', '', '文件存储', '');
INSERT INTO `cmf_auth_rule` VALUES (93, 1, 'admin', 'admin_url', 'admin/Storage/settingPost', '', '文件存储设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (94, 1, 'admin', 'admin_url', 'admin/Theme/index', '', '模板管理', '');
INSERT INTO `cmf_auth_rule` VALUES (95, 1, 'admin', 'admin_url', 'admin/Theme/install', '', '安装模板', '');
INSERT INTO `cmf_auth_rule` VALUES (96, 1, 'admin', 'admin_url', 'admin/Theme/uninstall', '', '卸载模板', '');
INSERT INTO `cmf_auth_rule` VALUES (97, 1, 'admin', 'admin_url', 'admin/Theme/installTheme', '', '模板安装', '');
INSERT INTO `cmf_auth_rule` VALUES (98, 1, 'admin', 'admin_url', 'admin/Theme/update', '', '模板更新', '');
INSERT INTO `cmf_auth_rule` VALUES (99, 1, 'admin', 'admin_url', 'admin/Theme/active', '', '启用模板', '');
INSERT INTO `cmf_auth_rule` VALUES (100, 1, 'admin', 'admin_url', 'admin/Theme/files', '', '模板文件列表', '');
INSERT INTO `cmf_auth_rule` VALUES (101, 1, 'admin', 'admin_url', 'admin/Theme/fileSetting', '', '模板文件设置', '');
INSERT INTO `cmf_auth_rule` VALUES (102, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayData', '', '模板文件数组数据列表', '');
INSERT INTO `cmf_auth_rule` VALUES (103, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataEdit', '', '模板文件数组数据添加编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (104, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataEditPost', '', '模板文件数组数据添加编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (105, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataDelete', '', '模板文件数组数据删除', '');
INSERT INTO `cmf_auth_rule` VALUES (106, 1, 'admin', 'admin_url', 'admin/Theme/settingPost', '', '模板文件编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (107, 1, 'admin', 'admin_url', 'admin/Theme/dataSource', '', '模板文件设置数据源', '');
INSERT INTO `cmf_auth_rule` VALUES (108, 1, 'admin', 'admin_url', 'admin/Theme/design', '', '模板设计', '');
INSERT INTO `cmf_auth_rule` VALUES (109, 1, 'admin', 'admin_url', 'admin/User/default', '', '管理组', '');
INSERT INTO `cmf_auth_rule` VALUES (110, 1, 'admin', 'admin_url', 'admin/User/index', '', '管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (111, 1, 'admin', 'admin_url', 'admin/User/add', '', '管理员添加', '');
INSERT INTO `cmf_auth_rule` VALUES (112, 1, 'admin', 'admin_url', 'admin/User/addPost', '', '管理员添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (113, 1, 'admin', 'admin_url', 'admin/User/edit', '', '管理员编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (114, 1, 'admin', 'admin_url', 'admin/User/editPost', '', '管理员编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (115, 1, 'admin', 'admin_url', 'admin/User/userInfo', '', '个人信息', '');
INSERT INTO `cmf_auth_rule` VALUES (116, 1, 'admin', 'admin_url', 'admin/User/userInfoPost', '', '管理员个人信息修改提交', '');
INSERT INTO `cmf_auth_rule` VALUES (117, 1, 'admin', 'admin_url', 'admin/User/delete', '', '管理员删除', '');
INSERT INTO `cmf_auth_rule` VALUES (118, 1, 'admin', 'admin_url', 'admin/User/ban', '', '停用管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (119, 1, 'admin', 'admin_url', 'admin/User/cancelBan', '', '启用管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (120, 1, 'user', 'admin_url', 'user/AdminAsset/index', '', '资源管理', '');
INSERT INTO `cmf_auth_rule` VALUES (121, 1, 'user', 'admin_url', 'user/AdminAsset/delete', '', '删除文件', '');
INSERT INTO `cmf_auth_rule` VALUES (122, 1, 'user', 'admin_url', 'user/AdminIndex/default', '', '用户管理', '');
INSERT INTO `cmf_auth_rule` VALUES (123, 1, 'user', 'admin_url', 'user/AdminIndex/default1', '', '用户组', '');
INSERT INTO `cmf_auth_rule` VALUES (124, 1, 'user', 'admin_url', 'user/AdminIndex/index', '', '本站用户', '');
INSERT INTO `cmf_auth_rule` VALUES (125, 1, 'user', 'admin_url', 'user/AdminIndex/ban', '', '本站用户拉黑', '');
INSERT INTO `cmf_auth_rule` VALUES (126, 1, 'user', 'admin_url', 'user/AdminIndex/cancelBan', '', '本站用户启用', '');
INSERT INTO `cmf_auth_rule` VALUES (127, 1, 'user', 'admin_url', 'user/AdminOauth/index', '', '第三方用户', '');
INSERT INTO `cmf_auth_rule` VALUES (128, 1, 'user', 'admin_url', 'user/AdminOauth/delete', '', '删除第三方用户绑定', '');
INSERT INTO `cmf_auth_rule` VALUES (129, 1, 'user', 'admin_url', 'user/AdminUserAction/index', '', '用户操作管理', '');
INSERT INTO `cmf_auth_rule` VALUES (130, 1, 'user', 'admin_url', 'user/AdminUserAction/edit', '', '编辑用户操作', '');
INSERT INTO `cmf_auth_rule` VALUES (131, 1, 'user', 'admin_url', 'user/AdminUserAction/editPost', '', '编辑用户操作提交', '');
INSERT INTO `cmf_auth_rule` VALUES (132, 1, 'user', 'admin_url', 'user/AdminUserAction/sync', '', '同步用户操作', '');
INSERT INTO `cmf_auth_rule` VALUES (133, 1, 'admin', 'admin_url', 'admin/adminlog/index', '', '操作日志', '');
INSERT INTO `cmf_auth_rule` VALUES (134, 1, 'admin', 'admin_url', 'admin/adminlog/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (135, 1, 'admin', 'admin_url', 'admin/adminlog/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (136, 1, 'admin', 'admin_url', 'admin/Agent/default', '', '邀请奖励', '');
INSERT INTO `cmf_auth_rule` VALUES (137, 1, 'admin', 'admin_url', 'admin/agent/index', '', '邀请关系', '');
INSERT INTO `cmf_auth_rule` VALUES (138, 1, 'admin', 'admin_url', 'admin/agent/index2', '', '邀请收益', '');
INSERT INTO `cmf_auth_rule` VALUES (139, 1, 'admin', 'admin_url', 'admin/car/default', '', '商城管理', '');
INSERT INTO `cmf_auth_rule` VALUES (140, 1, 'admin', 'admin_url', 'admin/car/index', '', '坐骑管理', '');
INSERT INTO `cmf_auth_rule` VALUES (141, 1, 'admin', 'admin_url', 'admin/car/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (142, 1, 'admin', 'admin_url', 'admin/car/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (143, 1, 'admin', 'admin_url', 'admin/car/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (144, 1, 'admin', 'admin_url', 'admin/car/editPost', '', '编辑添加', '');
INSERT INTO `cmf_auth_rule` VALUES (145, 1, 'admin', 'admin_url', 'admin/car/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (146, 1, 'admin', 'admin_url', 'admin/car/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (147, 1, 'admin', 'admin_url', 'admin/liang/index', '', '靓号管理', '');
INSERT INTO `cmf_auth_rule` VALUES (148, 1, 'admin', 'admin_url', 'admin/liang/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (149, 1, 'admin', 'admin_url', 'admin/liang/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (150, 1, 'admin', 'admin_url', 'admin/setting/configpri', '', '私密设置', '');
INSERT INTO `cmf_auth_rule` VALUES (151, 1, 'admin', 'admin_url', 'admin/setting/configpriPost', '', '提交', '');
INSERT INTO `cmf_auth_rule` VALUES (152, 1, 'admin', 'admin_url', 'admin/jackpot/set', '', '奖池管理', '');
INSERT INTO `cmf_auth_rule` VALUES (153, 1, 'admin', 'admin_url', 'admin/jackpot/setPost', '', '设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (154, 1, 'admin', 'admin_url', 'admin/jackpot/index', '', '奖池等级', '');
INSERT INTO `cmf_auth_rule` VALUES (155, 1, 'admin', 'admin_url', 'admin/jackpot/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (156, 1, 'admin', 'admin_url', 'admin/jackpot/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (157, 1, 'admin', 'admin_url', 'admin/jackpot/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (158, 1, 'admin', 'admin_url', 'admin/jackpot/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (159, 1, 'admin', 'admin_url', 'admin/jackpot/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (160, 1, 'admin', 'admin_url', 'admin/live/default', '', '直播管理', '');
INSERT INTO `cmf_auth_rule` VALUES (161, 1, 'admin', 'admin_url', 'admin/liveclass/index', '', '直播分类', '');
INSERT INTO `cmf_auth_rule` VALUES (162, 1, 'admin', 'admin_url', 'admin/liveclass/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (163, 1, 'admin', 'admin_url', 'admin/liveclass/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (164, 1, 'admin', 'admin_url', 'admin/liveclass/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (165, 1, 'admin', 'admin_url', 'admin/liveclass/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (166, 1, 'admin', 'admin_url', 'admin/liveclass/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (167, 1, 'admin', 'admin_url', 'admin/liveclass/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (168, 1, 'admin', 'admin_url', 'admin/liveban/index', '', '禁播管理', '');
INSERT INTO `cmf_auth_rule` VALUES (169, 1, 'admin', 'admin_url', 'admin/liveban/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (170, 1, 'admin', 'admin_url', 'admin/liveshut/index', '', '禁言管理', '');
INSERT INTO `cmf_auth_rule` VALUES (171, 1, 'admin', 'admin_url', 'admin/liveshut/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (172, 1, 'admin', 'admin_url', 'admin/livekick/index', '', '踢人管理', '');
INSERT INTO `cmf_auth_rule` VALUES (173, 1, 'admin', 'admin_url', 'admin/livekick/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (174, 1, 'admin', 'admin_url', 'admin/liveing/index', '', '直播列表', '');
INSERT INTO `cmf_auth_rule` VALUES (175, 1, 'admin', 'admin_url', 'admin/liveing/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (176, 1, 'admin', 'admin_url', 'admin/liveing/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (177, 1, 'admin', 'admin_url', 'admin/liveing/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (178, 1, 'admin', 'admin_url', 'admin/liveing/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (179, 1, 'admin', 'admin_url', 'admin/liveing/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (180, 1, 'admin', 'admin_url', 'admin/monitor/index', '', '直播监控', '');
INSERT INTO `cmf_auth_rule` VALUES (181, 1, 'admin', 'admin_url', 'admin/monitor/full', '', '大屏', '');
INSERT INTO `cmf_auth_rule` VALUES (182, 1, 'admin', 'admin_url', 'admin/monitor/stop', '', '关播', '');
INSERT INTO `cmf_auth_rule` VALUES (183, 1, 'admin', 'admin_url', 'admin/gift/index', '', '礼物管理', '');
INSERT INTO `cmf_auth_rule` VALUES (184, 1, 'admin', 'admin_url', 'admin/gift/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (185, 1, 'admin', 'admin_url', 'admin/gift/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (186, 1, 'admin', 'admin_url', 'admin/gift/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (187, 1, 'admin', 'admin_url', 'admin/gift/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (188, 1, 'admin', 'admin_url', 'admin/gift/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (189, 1, 'admin', 'admin_url', 'admin/gift/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (190, 1, 'admin', 'admin_url', 'admin/report/defult', '', '举报管理', '');
INSERT INTO `cmf_auth_rule` VALUES (191, 1, 'admin', 'admin_url', 'admin/reportcat/index', '', '举报分类', '');
INSERT INTO `cmf_auth_rule` VALUES (192, 1, 'admin', 'admin_url', 'admin/reportcat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (193, 1, 'admin', 'admin_url', 'admin/reportcat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (194, 1, 'admin', 'admin_url', 'admin/reportcat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (195, 1, 'admin', 'admin_url', 'admin/reportcat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (196, 1, 'admin', 'admin_url', 'admin/reportcat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (197, 1, 'admin', 'admin_url', 'admin/reportcat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (198, 1, 'admin', 'admin_url', 'admin/report/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (199, 1, 'admin', 'admin_url', 'admin/report/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (200, 1, 'admin', 'admin_url', 'admin/liverecord/index', '', '直播记录', '');
INSERT INTO `cmf_auth_rule` VALUES (201, 1, 'admin', 'admin_url', 'admin/video/default', '', '视频管理', '');
INSERT INTO `cmf_auth_rule` VALUES (202, 1, 'admin', 'admin_url', 'admin/music/index', '', '音乐管理', '');
INSERT INTO `cmf_auth_rule` VALUES (203, 1, 'admin', 'admin_url', 'admin/musiccat/index', '', '音乐分类', '');
INSERT INTO `cmf_auth_rule` VALUES (204, 1, 'admin', 'admin_url', 'admin/music/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (205, 1, 'admin', 'admin_url', 'admin/music/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (206, 1, 'admin', 'admin_url', 'admin/music/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (207, 1, 'admin', 'admin_url', 'admin/music/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (208, 1, 'admin', 'admin_url', 'admin/music/listen', '', '试听', '');
INSERT INTO `cmf_auth_rule` VALUES (209, 1, 'admin', 'admin_url', 'admin/music/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (210, 1, 'admin', 'admin_url', 'admin/music/canceldel', '', '取消删除', '');
INSERT INTO `cmf_auth_rule` VALUES (211, 1, 'admin', 'admin_url', 'admin/musiccat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (212, 1, 'admin', 'admin_url', 'admin/musiccat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (213, 1, 'admin', 'admin_url', 'admin/musiccat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (214, 1, 'admin', 'admin_url', 'admin/musiccat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (215, 1, 'admin', 'admin_url', 'admin/musiccat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (216, 1, 'admin', 'admin_url', 'admin/musiccat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (217, 1, 'admin', 'admin_url', 'admin/shop/default', '', '店铺管理', '');
INSERT INTO `cmf_auth_rule` VALUES (218, 1, 'admin', 'admin_url', 'admin/shopapply/index', '', '店铺申请', '');
INSERT INTO `cmf_auth_rule` VALUES (219, 1, 'admin', 'admin_url', 'admin/shopapply/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (220, 1, 'admin', 'admin_url', 'admin/shopapply/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (221, 1, 'admin', 'admin_url', 'admin/shopapply/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (222, 1, 'admin', 'admin_url', 'admin/shopbond/index', '', '保证金', '');
INSERT INTO `cmf_auth_rule` VALUES (223, 1, 'admin', 'admin_url', 'admin/shopbond/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (224, 1, 'admin', 'admin_url', 'admin/shopgoods/index', '', '商品管理', '');
INSERT INTO `cmf_auth_rule` VALUES (225, 1, 'admin', 'admin_url', 'admin/shopgoods/setstatus', '', '上下架', '');
INSERT INTO `cmf_auth_rule` VALUES (226, 1, 'admin', 'admin_url', 'admin/shopgoods/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (227, 1, 'admin', 'admin_url', 'admin/turntable/default', '', '大转盘', '');
INSERT INTO `cmf_auth_rule` VALUES (228, 1, 'admin', 'admin_url', 'admin/turntablecon/index', '', '价格管理', '');
INSERT INTO `cmf_auth_rule` VALUES (229, 1, 'admin', 'admin_url', 'admin/turntablecon/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (230, 1, 'admin', 'admin_url', 'admin/turntablecon/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (231, 1, 'admin', 'admin_url', 'admin/turntablecon/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (232, 1, 'admin', 'admin_url', 'admin/turntable/index', '', '奖品管理', '');
INSERT INTO `cmf_auth_rule` VALUES (233, 1, 'admin', 'admin_url', 'admin/turntable/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (234, 1, 'admin', 'admin_url', 'admin/turntable/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (235, 1, 'admin', 'admin_url', 'admin/turntable/index2', '', '转盘记录', '');
INSERT INTO `cmf_auth_rule` VALUES (236, 1, 'admin', 'admin_url', 'admin/turntable/index3', '', '线下奖品', '');
INSERT INTO `cmf_auth_rule` VALUES (237, 1, 'admin', 'admin_url', 'admin/turntable/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (238, 1, 'admin', 'admin_url', 'admin/level/default', '', '等级管理', '');
INSERT INTO `cmf_auth_rule` VALUES (239, 1, 'admin', 'admin_url', 'admin/level/index', '', '用户等级', '');
INSERT INTO `cmf_auth_rule` VALUES (240, 1, 'admin', 'admin_url', 'admin/level/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (241, 1, 'admin', 'admin_url', 'admin/level/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (242, 1, 'admin', 'admin_url', 'admin/level/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (243, 1, 'admin', 'admin_url', 'admin/level/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (244, 1, 'admin', 'admin_url', 'admin/level/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (245, 1, 'admin', 'admin_url', 'admin/levelanchor/index', '', '主播等级', '');
INSERT INTO `cmf_auth_rule` VALUES (246, 1, 'admin', 'admin_url', 'admin/levelanchor/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (247, 1, 'admin', 'admin_url', 'admin/levelanchor/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (248, 1, 'admin', 'admin_url', 'admin/levelanchor/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (249, 1, 'admin', 'admin_url', 'admin/levelanchor/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (250, 1, 'admin', 'admin_url', 'admin/levelanchor/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (251, 1, 'admin', 'admin_url', 'admin/guard/index', '', '守护管理', '');
INSERT INTO `cmf_auth_rule` VALUES (252, 1, 'admin', 'admin_url', 'admin/guard/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (253, 1, 'admin', 'admin_url', 'admin/guard/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (254, 1, 'admin', 'admin_url', 'admin/liang/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (255, 1, 'admin', 'admin_url', 'admin/liang/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (256, 1, 'admin', 'admin_url', 'admin/liang/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (257, 1, 'admin', 'admin_url', 'admin/liang/setstatus', '', '设置状态', '');
INSERT INTO `cmf_auth_rule` VALUES (258, 1, 'admin', 'admin_url', 'admin/liang/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (259, 1, 'admin', 'admin_url', 'admin/vip/default', '', 'VIP管理', '');
INSERT INTO `cmf_auth_rule` VALUES (260, 1, 'admin', 'admin_url', 'admin/vip/index', '', 'VIP列表', '');
INSERT INTO `cmf_auth_rule` VALUES (261, 1, 'admin', 'admin_url', 'admin/vip/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (262, 1, 'admin', 'admin_url', 'admin/vip/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (263, 1, 'admin', 'admin_url', 'admin/vip/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (264, 1, 'admin', 'admin_url', 'admin/vipuser/index', '', 'VIP用户', '');
INSERT INTO `cmf_auth_rule` VALUES (265, 1, 'admin', 'admin_url', 'admin/vipuser/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (266, 1, 'admin', 'admin_url', 'admin/vipuser/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (267, 1, 'admin', 'admin_url', 'admin/vipuser/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (268, 1, 'admin', 'admin_url', 'admin/vipuser/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (269, 1, 'admin', 'admin_url', 'admin/vipuser/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (270, 1, 'admin', 'admin_url', 'admin/family/default', '', '家族管理', '');
INSERT INTO `cmf_auth_rule` VALUES (271, 1, 'admin', 'admin_url', 'admin/family/index', '', '家族列表', '');
INSERT INTO `cmf_auth_rule` VALUES (272, 1, 'admin', 'admin_url', 'admin/family/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (273, 1, 'admin', 'admin_url', 'admin/family/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (274, 1, 'admin', 'admin_url', 'admin/family/disable', '', '禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (275, 1, 'admin', 'admin_url', 'admin/family/enable', '', '启用', '');
INSERT INTO `cmf_auth_rule` VALUES (276, 1, 'admin', 'admin_url', 'admin/family/profit', '', '收益', '');
INSERT INTO `cmf_auth_rule` VALUES (277, 1, 'admin', 'admin_url', 'admin/family/cash', '', '提现', '');
INSERT INTO `cmf_auth_rule` VALUES (278, 1, 'admin', 'admin_url', 'admin/family/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (279, 1, 'admin', 'admin_url', 'admin/familyuser/index', '', '成员管理', '');
INSERT INTO `cmf_auth_rule` VALUES (280, 1, 'admin', 'admin_url', 'admin/familyuser/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (281, 1, 'admin', 'admin_url', 'admin/familyuser/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (282, 1, 'admin', 'admin_url', 'admin/familyuser/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (283, 1, 'admin', 'admin_url', 'admin/familyuser/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (284, 1, 'admin', 'admin_url', 'admin/familyuser/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (285, 1, 'admin', 'admin_url', 'admin/finance/default', '', '财务管理', '');
INSERT INTO `cmf_auth_rule` VALUES (286, 1, 'admin', 'admin_url', 'admin/chargerules/index', '', '充值规则', '');
INSERT INTO `cmf_auth_rule` VALUES (287, 1, 'admin', 'admin_url', 'admin/chargerules/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (288, 1, 'admin', 'admin_url', 'admin/chargerules/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (289, 1, 'admin', 'admin_url', 'admin/chargerules/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (290, 1, 'admin', 'admin_url', 'admin/chargerules/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (291, 1, 'admin', 'admin_url', 'admin/chargerules/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (292, 1, 'admin', 'admin_url', 'admin/chargerules/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (293, 1, 'admin', 'admin_url', 'admin/charge/index', '', '钻石充值记录', '');
INSERT INTO `cmf_auth_rule` VALUES (294, 1, 'admin', 'admin_url', 'admin/charge/setpay', '', '确认支付', '');
INSERT INTO `cmf_auth_rule` VALUES (295, 1, 'admin', 'admin_url', 'admin/charge/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (296, 1, 'admin', 'admin_url', 'admin/manual/index', '', '手动充值钻石', '');
INSERT INTO `cmf_auth_rule` VALUES (297, 1, 'admin', 'admin_url', 'admin/manual/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (298, 1, 'admin', 'admin_url', 'admin/manual/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (299, 1, 'admin', 'admin_url', 'admin/manual/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (300, 1, 'admin', 'admin_url', 'admin/coinrecord/index', '', '钻石消费记录', '');
INSERT INTO `cmf_auth_rule` VALUES (301, 1, 'admin', 'admin_url', 'admin/cash/index', '', '映票提现记录', '');
INSERT INTO `cmf_auth_rule` VALUES (302, 1, 'admin', 'admin_url', 'admin/cash/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (303, 1, 'admin', 'admin_url', 'admin/cash/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (304, 1, 'admin', 'admin_url', 'admin/cash/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (305, 1, 'admin', 'admin_url', 'admin/guide/set', '', '引导页', '');
INSERT INTO `cmf_auth_rule` VALUES (306, 1, 'admin', 'admin_url', 'admin/guide/setPost', '', '设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (307, 1, 'admin', 'admin_url', 'admin/guide/index', '', '管理', '');
INSERT INTO `cmf_auth_rule` VALUES (308, 1, 'admin', 'admin_url', 'admin/guide/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (309, 1, 'admin', 'admin_url', 'admin/guide/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (310, 1, 'admin', 'admin_url', 'admin/guide/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (311, 1, 'admin', 'admin_url', 'admin/guide/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (312, 1, 'admin', 'admin_url', 'admin/guide/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (313, 1, 'admin', 'admin_url', 'admin/guide/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (314, 1, 'admin', 'admin_url', 'admin/auth/index', '', '身份认证', '');
INSERT INTO `cmf_auth_rule` VALUES (315, 1, 'admin', 'admin_url', 'admin/auth/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (316, 1, 'admin', 'admin_url', 'admin/auth/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (317, 1, 'admin', 'admin_url', 'admin/auth/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (318, 1, 'admin', 'admin_url', 'admin/video/index', 'isdel=0&status=1', '审核通过列表', '');
INSERT INTO `cmf_auth_rule` VALUES (319, 1, 'admin', 'admin_url', 'admin/video/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (320, 1, 'admin', 'admin_url', 'admin/video/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (321, 1, 'admin', 'admin_url', 'admin/video/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (322, 1, 'admin', 'admin_url', 'admin/video/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (323, 1, 'admin', 'admin_url', 'admin/video/setstatus', '', '上下架', '');
INSERT INTO `cmf_auth_rule` VALUES (324, 1, 'admin', 'admin_url', 'admin/video/see', '', '查看', '');
INSERT INTO `cmf_auth_rule` VALUES (325, 1, 'admin', 'admin_url', 'admin/videocom/index', '', '视频评论', '');
INSERT INTO `cmf_auth_rule` VALUES (326, 1, 'admin', 'admin_url', 'admin/videocom/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (327, 1, 'admin', 'admin_url', 'admin/videorepcat/index', '', '举报类型', '');
INSERT INTO `cmf_auth_rule` VALUES (328, 1, 'admin', 'admin_url', 'admin/videorepcat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (329, 1, 'admin', 'admin_url', 'admin/videorepcat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (330, 1, 'admin', 'admin_url', 'admin/videorepcat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (331, 1, 'admin', 'admin_url', 'admin/videorepcat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (332, 1, 'admin', 'admin_url', 'admin/videorepcat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (333, 1, 'admin', 'admin_url', 'admin/videorepcat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (334, 1, 'admin', 'admin_url', 'admin/videorep/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (335, 1, 'admin', 'admin_url', 'admin/videorep/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (336, 1, 'admin', 'admin_url', 'admin/videorep/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (337, 1, 'admin', 'admin_url', 'admin/Loginbonus/index', '', '登录奖励', '');
INSERT INTO `cmf_auth_rule` VALUES (338, 1, 'admin', 'admin_url', 'admin/Loginbonus/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (339, 1, 'admin', 'admin_url', 'admin/Loginbonus/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (340, 1, 'admin', 'admin_url', 'admin/red/index', '', '红包管理', '');
INSERT INTO `cmf_auth_rule` VALUES (341, 1, 'admin', 'admin_url', 'admin/red/index2', '', '领取详情', '');
INSERT INTO `cmf_auth_rule` VALUES (342, 1, 'admin', 'admin_url', 'admin/note/default', '', '消息管理', '');
INSERT INTO `cmf_auth_rule` VALUES (343, 1, 'admin', 'admin_url', 'admin/sendcode/index', '', '验证码管理', '');
INSERT INTO `cmf_auth_rule` VALUES (344, 1, 'admin', 'admin_url', 'admin/sendcode/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (345, 1, 'admin', 'admin_url', 'admin/sendcode/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (346, 1, 'admin', 'admin_url', 'admin/push/index', '', '推送管理', '');
INSERT INTO `cmf_auth_rule` VALUES (347, 1, 'admin', 'admin_url', 'admin/push/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (348, 1, 'admin', 'admin_url', 'admin/push/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (349, 1, 'admin', 'admin_url', 'admin/push/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (350, 1, 'admin', 'admin_url', 'admin/system/index', '', '直播间消息', '');
INSERT INTO `cmf_auth_rule` VALUES (351, 1, 'admin', 'admin_url', 'admin/system/send', '', '发送', '');
INSERT INTO `cmf_auth_rule` VALUES (352, 1, 'admin', 'admin_url', 'admin/Impression/index', '', '标签管理', '');
INSERT INTO `cmf_auth_rule` VALUES (353, 1, 'admin', 'admin_url', 'admin/Impression/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (354, 1, 'admin', 'admin_url', 'admin/Impression/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (355, 1, 'admin', 'admin_url', 'admin/Impression/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (356, 1, 'admin', 'admin_url', 'admin/Impression/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (357, 1, 'admin', 'admin_url', 'admin/Impression/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (358, 1, 'admin', 'admin_url', 'admin/Impression/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (359, 1, 'admin', 'admin_url', 'admin/portal/default', '', '内容管理', '');
INSERT INTO `cmf_auth_rule` VALUES (360, 1, 'admin', 'admin_url', 'admin/feedback/index', '', '用户反馈', '');
INSERT INTO `cmf_auth_rule` VALUES (361, 1, 'admin', 'admin_url', 'admin/feedback/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (362, 1, 'admin', 'admin_url', 'admin/feedback/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (363, 1, 'portal', 'admin_url', 'portal/AdminPage/index', '', '页面管理', '');
INSERT INTO `cmf_auth_rule` VALUES (364, 1, 'portal', 'admin_url', 'portal/AdminPage/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (365, 1, 'portal', 'admin_url', 'portal/AdminPage/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (366, 1, 'portal', 'admin_url', 'portal/AdminPage/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (367, 1, 'portal', 'admin_url', 'portal/AdminPage/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (368, 1, 'portal', 'admin_url', 'portal/AdminPage/delete', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (369, 1, 'admin', 'admin_url', 'admin/videoclass/index', '', '视频分类', '');
INSERT INTO `cmf_auth_rule` VALUES (370, 1, 'admin', 'admin_url', 'admin/videoclass/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (371, 1, 'admin', 'admin_url', 'admin/videoclass/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (372, 1, 'admin', 'admin_url', 'admin/videoclass/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (373, 1, 'admin', 'admin_url', 'admin/videoclass/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (374, 1, 'admin', 'admin_url', 'admin/videoclass/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (375, 1, 'admin', 'admin_url', 'admin/videoclass/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (376, 1, 'admin', 'admin_url', 'admin/sticker/index', '', '贴纸列表', '');
INSERT INTO `cmf_auth_rule` VALUES (377, 1, 'admin', 'admin_url', 'admin/sticker/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (378, 1, 'admin', 'admin_url', 'admin/sticker/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (379, 1, 'admin', 'admin_url', 'admin/sticker/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (380, 1, 'admin', 'admin_url', 'admin/sticker/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (381, 1, 'admin', 'admin_url', 'admin/sticker/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (382, 1, 'admin', 'admin_url', 'admin/sticker/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (383, 1, 'admin', 'admin_url', 'admin/Dynamic/default', '', '动态管理', '');
INSERT INTO `cmf_auth_rule` VALUES (384, 1, 'admin', 'admin_url', 'admin/Dynamic/index', 'isdel=0&status=1', '审核通过', '');
INSERT INTO `cmf_auth_rule` VALUES (385, 1, 'admin', 'admin_url', 'admin/Dynamic/wait', 'isdel=0&status=0', '等待审核', '');
INSERT INTO `cmf_auth_rule` VALUES (386, 1, 'admin', 'admin_url', 'admin/Dynamic/nopass', 'isdel=0&status=-1', '未通过', '');
INSERT INTO `cmf_auth_rule` VALUES (387, 1, 'admin', 'admin_url', 'admin/Dynamic/lower', 'isdel=1', '下架列表', '');
INSERT INTO `cmf_auth_rule` VALUES (388, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/index', '', '举报类型', '');
INSERT INTO `cmf_auth_rule` VALUES (389, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (390, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (391, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (392, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (393, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (394, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (395, 1, 'admin', 'admin_url', 'admin/Dynamicrepot/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (396, 1, 'admin', 'admin_url', 'admin/Dynamicrepot/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (397, 1, 'admin', 'admin_url', 'admin/Dynamicrepot/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (398, 1, 'admin', 'admin_url', 'admin/Dynamiccom/index', '', '评论列表', '');
INSERT INTO `cmf_auth_rule` VALUES (399, 1, 'admin', 'admin_url', 'admin/Dynamiccom/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (400, 1, 'admin', 'admin_url', 'admin/video/wait', 'isdel=0&status=0&is_draft=0', '待审核列表', '');
INSERT INTO `cmf_auth_rule` VALUES (401, 1, 'admin', 'admin_url', 'admin/video/nopass', 'isdel=0&status=2', '未通过列表', '');
INSERT INTO `cmf_auth_rule` VALUES (402, 1, 'admin', 'admin_url', 'admin/video/lower', 'isdel=1', '下架列表', '');
INSERT INTO `cmf_auth_rule` VALUES (403, 1, 'admin', 'admin_url', 'admin/dynamic/setstatus', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (404, 1, 'admin', 'admin_url', 'admin/dynamic/see', '', '查看', '');
INSERT INTO `cmf_auth_rule` VALUES (405, 1, 'admin', 'admin_url', 'admin/dynamic/setdel', '', '上下架', '');
INSERT INTO `cmf_auth_rule` VALUES (406, 1, 'admin', 'admin_url', 'admin/dynamic/setrecom', '', '设置推荐值', '');
INSERT INTO `cmf_auth_rule` VALUES (407, 1, 'admin', 'admin_url', 'admin/Dynamic/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (408, 1, 'admin', 'admin_url', 'admin/game/index', '', '游戏记录', '');
INSERT INTO `cmf_auth_rule` VALUES (409, 1, 'admin', 'admin_url', 'admin/game/index2', '', 'x详情', '');
INSERT INTO `cmf_auth_rule` VALUES (410, 1, 'user', 'admin_url', 'user/AdminIndex/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (411, 1, 'user', 'admin_url', 'user/AdminIndex/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (412, 1, 'user', 'admin_url', 'user/AdminIndex/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (413, 1, 'user', 'admin_url', 'user/AdminIndex/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (414, 1, 'user', 'admin_url', 'user/AdminIndex/setban', '', '禁用时间', '');
INSERT INTO `cmf_auth_rule` VALUES (415, 1, 'user', 'admin_url', 'user/AdminIndex/setsuper', '', '设置、取消超管', '');
INSERT INTO `cmf_auth_rule` VALUES (416, 1, 'user', 'admin_url', 'user/AdminIndex/sethot', '', '设置取消热门', '');
INSERT INTO `cmf_auth_rule` VALUES (417, 1, 'user', 'admin_url', 'user/AdminIndex/setrecommend', '', '设置取消推荐', '');
INSERT INTO `cmf_auth_rule` VALUES (418, 1, 'user', 'admin_url', 'user/AdminIndex/setzombie', '', '开启关闭僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (419, 1, 'user', 'admin_url', 'user/AdminIndex/setzombiep', '', '设置取消为僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (420, 1, 'user', 'admin_url', 'user/adminIndex/setzombiepall', '', '批量设置/取消为僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (421, 1, 'user', 'admin_url', 'user/adminIndex/setzombieall', '', '一键开启/关闭僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (422, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/index', '', '商品分类列表', '');
INSERT INTO `cmf_auth_rule` VALUES (423, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/add', '', '商品分类添加', '');
INSERT INTO `cmf_auth_rule` VALUES (424, 1, 'Admin', 'admin_url', 'Admin/Buyeraddress/index', '', '收货地址管理', '');
INSERT INTO `cmf_auth_rule` VALUES (425, 1, 'Admin', 'admin_url', 'Admin/Express/index', '', '物流公司列表', '');
INSERT INTO `cmf_auth_rule` VALUES (426, 1, 'Admin', 'admin_url', 'Admin/Express/add', '', '物流公司添加', '');
INSERT INTO `cmf_auth_rule` VALUES (427, 1, 'Admin', 'admin_url', 'Admin/Express/add_post', '', '物流公司添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (428, 1, 'Admin', 'admin_url', 'Admin/Express/edit', '', '物流公司编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (429, 1, 'Admin', 'admin_url', 'Admin/Express/edit_post', '', '物流公司编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (430, 1, 'Admin', 'admin_url', 'Admin/Express/del', '', '物流公司删除', '');
INSERT INTO `cmf_auth_rule` VALUES (431, 1, 'Admin', 'admin_url', 'Admin/Express/listOrder', '', '物流公司排序', '');
INSERT INTO `cmf_auth_rule` VALUES (432, 1, 'Admin', 'admin_url', 'Admin/Express/changeStatus', '', '物流公司显示/隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (433, 1, 'Admin', 'admin_url', 'Admin/Refundreason/index', '', '买家申请退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (434, 1, 'Admin', 'admin_url', 'Admin/Refundreason/add', '', '添加退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (435, 1, 'Admin', 'admin_url', 'Admin/Refundreason/add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (436, 1, 'Admin', 'admin_url', 'Admin/Refundreason/edit', '', '编辑退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (437, 1, 'Admin', 'admin_url', 'Admin/Refundreason/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (438, 1, 'Admin', 'admin_url', 'Admin/Refundreason/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (439, 1, 'Admin', 'admin_url', 'Admin/Refundreason/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (440, 1, 'Admin', 'admin_url', 'Admin/Refusereason/index', '', '卖家拒绝退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (441, 1, 'Admin', 'admin_url', 'Admin/Platformreason/index', '', '退款平台介入原因', '');
INSERT INTO `cmf_auth_rule` VALUES (442, 1, 'Admin', 'admin_url', 'Admin/Goodsorder/index', '', '商品订单列表', '');
INSERT INTO `cmf_auth_rule` VALUES (443, 1, 'Admin', 'admin_url', 'Admin/Refundlist/index', '', '退款列表', '');
INSERT INTO `cmf_auth_rule` VALUES (444, 1, 'Admin', 'admin_url', 'Admin/Shopcash/index', '', '提现记录', '');
INSERT INTO `cmf_auth_rule` VALUES (445, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/default', '', '付费内容', '');
INSERT INTO `cmf_auth_rule` VALUES (446, 1, 'Admin', 'admin_url', 'Admin/Refusereason/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (447, 1, 'Admin', 'admin_url', 'Admin/Refusereason/add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (448, 1, 'Admin', 'admin_url', 'Admin/Refusereason/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (449, 1, 'Admin', 'admin_url', 'Admin/Refusereason/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (450, 1, 'Admin', 'admin_url', 'Admin/Refusereason/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (451, 1, 'Admin', 'admin_url', 'Admin/Refusereason/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (452, 1, 'Admin', 'admin_url', 'Admin/Platformreason/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (453, 1, 'Admin', 'admin_url', 'Admin/Platformreason/add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (454, 1, 'Admin', 'admin_url', 'Admin/Platformreason/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (455, 1, 'Admin', 'admin_url', 'Admin/Platformreason/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (456, 1, 'Admin', 'admin_url', 'Admin/Platformreason/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (457, 1, 'Admin', 'admin_url', 'Admin/Platformreason/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (458, 1, 'Admin', 'admin_url', 'Admin/Goodsorder/info', '', '订单详情', '');
INSERT INTO `cmf_auth_rule` VALUES (459, 1, 'Admin', 'admin_url', 'Admin/Refundlist/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (460, 1, 'Admin', 'admin_url', 'Admin/Refundlist/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (461, 1, 'Admin', 'admin_url', 'Admin/Shopcash/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (462, 1, 'Admin', 'admin_url', 'Admin/Shopcash/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (463, 1, 'Admin', 'admin_url', 'Admin/Shopcash/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (464, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/classlist', '', '付费内容分类列表', '');
INSERT INTO `cmf_auth_rule` VALUES (465, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_add', '', '添加付费内容分类', '');
INSERT INTO `cmf_auth_rule` VALUES (466, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (467, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_edit', '', '编辑付费内容分类', '');
INSERT INTO `cmf_auth_rule` VALUES (468, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (469, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_del', '', '付费内容分类删除', '');
INSERT INTO `cmf_auth_rule` VALUES (470, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/listOrder', '', '付费内容分类排序', '');
INSERT INTO `cmf_auth_rule` VALUES (471, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/index', '', '付费内容列表', '');
INSERT INTO `cmf_auth_rule` VALUES (472, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/applylist', '', '付费内容申请列表', '');
INSERT INTO `cmf_auth_rule` VALUES (473, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/orderlist', '', '付费内容订单', '');
INSERT INTO `cmf_auth_rule` VALUES (474, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/addPost', '', '商品分类添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (475, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/edit', '', '商品审核/详情', '');
INSERT INTO `cmf_auth_rule` VALUES (476, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/editPost', '', '商品审核提交', '');
INSERT INTO `cmf_auth_rule` VALUES (477, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/edit', '', '商品分类编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (478, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (479, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/del', '', '商品分类删除', '');
INSERT INTO `cmf_auth_rule` VALUES (480, 1, 'Admin', 'admin_url', 'Admin/Buyeraddress/del', '', '收货地址删除', '');
INSERT INTO `cmf_auth_rule` VALUES (481, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/commentlist', '', '评价列表', '');
INSERT INTO `cmf_auth_rule` VALUES (482, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/delComment', '', '删除评价', '');
INSERT INTO `cmf_auth_rule` VALUES (483, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/edit', '', '编辑付费内容', '');
INSERT INTO `cmf_auth_rule` VALUES (484, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (485, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/del', '', '删除付费内容', '');
INSERT INTO `cmf_auth_rule` VALUES (486, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/videoplay', '', '付费内容观看', '');
INSERT INTO `cmf_auth_rule` VALUES (487, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/apply_edit', '', '编辑付费内容申请', '');
INSERT INTO `cmf_auth_rule` VALUES (488, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/apply_edit_post', '', '付费内容申请编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (489, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/apply_del', '', '删除付费内容申请', '');
INSERT INTO `cmf_auth_rule` VALUES (490, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/setPay', '', '确认支付', '');
INSERT INTO `cmf_auth_rule` VALUES (491, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/export', '', '导出付费内容订单', '');
INSERT INTO `cmf_auth_rule` VALUES (492, 1, 'Admin', 'admin_url', 'Admin/Video/draft', 'is_draft=1', '草稿视频列表', '');
INSERT INTO `cmf_auth_rule` VALUES (493, 1, 'Admin', 'admin_url', 'Admin/Balance/index', '', '余额手动充值', '');
INSERT INTO `cmf_auth_rule` VALUES (494, 1, 'Admin', 'admin_url', 'Admin/Balance/add', '', '充值添加', '');
INSERT INTO `cmf_auth_rule` VALUES (495, 1, 'Admin', 'admin_url', 'Admin/Balance/addPost', '', '余额充值保存', '');
INSERT INTO `cmf_auth_rule` VALUES (496, 1, 'Admin', 'admin_url', 'Admin/Balance/export', '', '余额充值记录导出', '');
INSERT INTO `cmf_auth_rule` VALUES (497, 1, 'admin', 'admin_url', 'admin/goodsgroup/index', '', '拼团列表', '');
INSERT INTO `cmf_auth_rule` VALUES (498, 1, 'admin', 'admin_url', 'admin/familyuser/divideapply', '', '分成申请列表', '');
INSERT INTO `cmf_auth_rule` VALUES (499, 1, 'admin', 'admin_url', 'admin/familyuser/applyedit', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (500, 1, 'admin', 'admin_url', 'admin/familyuser/applyeditPost', '', '审核提交', '');
INSERT INTO `cmf_auth_rule` VALUES (501, 1, 'admin', 'admin_url', 'admin/familyuser/delapply', '', '删除审核', '');
INSERT INTO `cmf_auth_rule` VALUES (502, 1, 'admin', 'admin_url', 'admin/Scorerecord/index', '', '积分记录', '');
INSERT INTO `cmf_auth_rule` VALUES (503, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/index', '', '话题标签', '');
INSERT INTO `cmf_auth_rule` VALUES (504, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (505, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/add_post', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (506, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (507, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (508, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/listsorders', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (509, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (510, 1, 'user', 'admin_url', 'user/AdminIndex/del', '', '本站用户删除', '');
INSERT INTO `cmf_auth_rule` VALUES (511, 1, 'user', 'admin_url', 'user/AdminIndex/authorindex', '', '主播管理', '');
INSERT INTO `cmf_auth_rule` VALUES (512, 1, 'admin', 'admin_url', 'admin/domain/index', '', '域名管理', '');

-- ----------------------------
-- Table structure for cmf_backpack
-- ----------------------------
DROP TABLE IF EXISTS `cmf_backpack`;
CREATE TABLE `cmf_backpack`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `giftid` int(11) NOT NULL DEFAULT 0 COMMENT '礼物ID',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_backpack
-- ----------------------------
INSERT INTO `cmf_backpack` VALUES (1, 42516, 74, 2);
INSERT INTO `cmf_backpack` VALUES (2, 42516, 19, 5);
INSERT INTO `cmf_backpack` VALUES (3, 42516, 5, 1);
INSERT INTO `cmf_backpack` VALUES (4, 42516, 28, 2);
INSERT INTO `cmf_backpack` VALUES (5, 42488, 19, 1);
INSERT INTO `cmf_backpack` VALUES (6, 42488, 74, 1);

-- ----------------------------
-- Table structure for cmf_balance_charge_admin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_balance_charge_admin`;
CREATE TABLE `cmf_balance_charge_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '充值对象ID',
  `balance` int(20) NOT NULL DEFAULT 0 COMMENT '金额',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_car
-- ----------------------------
DROP TABLE IF EXISTS `cmf_car`;
CREATE TABLE `cmf_car`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片链接',
  `swf` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '动画链接',
  `swftime` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '动画时长',
  `needcoin` int(20) NOT NULL DEFAULT 0 COMMENT '价格',
  `score` int(11) NOT NULL DEFAULT 0 COMMENT '积分价格',
  `list_order` int(10) NOT NULL DEFAULT 9999 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `words` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '进场话术',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_car
-- ----------------------------
INSERT INTO `cmf_car` VALUES (1, '小乌龟', 'car_1.png', 'car_gif_1.gif', 4.00, 100, 100, 3, 1500455416, '骑着小乌龟进场了', 0);
INSERT INTO `cmf_car` VALUES (2, '小毛驴', 'admin/20201102/2466349ac49d7735c4b0e5e4a60779eb.jpg', 'admin/20201102/2fe950c6e6a0e336e8d3542130ee7281.jpg', 5.00, 200, 200, 1, 1500455559, '骑着小毛驴进场了', 0);
INSERT INTO `cmf_car` VALUES (5, '魔法扫把', 'admin/20201102/8ea002656b1f036880222c6c07ca4e35.jpg', 'admin/20201102/16c58132820440789170c109962c8cbc.gif', 4.00, 300, 300, 2, 1501585432, '骑着魔法扫把进场了', 0);

-- ----------------------------
-- Table structure for cmf_car_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_car_user`;
CREATE TABLE `cmf_car_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `carid` int(11) NOT NULL DEFAULT 0 COMMENT '坐骑ID',
  `endtime` int(11) NOT NULL DEFAULT 0 COMMENT '到期时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否启用',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_car_user
-- ----------------------------
INSERT INTO `cmf_car_user` VALUES (1, 42513, 2, 1606913502, 0, 1604321502);
INSERT INTO `cmf_car_user` VALUES (2, 42520, 5, 1607060141, 0, 1604468141);

-- ----------------------------
-- Table structure for cmf_cash_account
-- ----------------------------
DROP TABLE IF EXISTS `cmf_cash_account`;
CREATE TABLE `cmf_cash_account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型，1表示支付宝，2表示微信，3表示银行卡',
  `account_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '银行名称',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '账号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_uid`(`id`, `uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_cash_account
-- ----------------------------
INSERT INTO `cmf_cash_account` VALUES (1, 42514, 3, '招商银行', '李白', '6543548754648754648', 1604387481);
INSERT INTO `cmf_cash_account` VALUES (2, 42520, 3, '邮政储蓄银行', '拉拉队', '6413131464591916165', 1604387569);
INSERT INTO `cmf_cash_account` VALUES (3, 42513, 1, '', '淡定淡', '13222222222', 1604404610);
INSERT INTO `cmf_cash_account` VALUES (4, 42509, 2, '', '', 'sdfsd', 1604474605);
INSERT INTO `cmf_cash_account` VALUES (6, 42511, 2, '', '', 'sdfafsadf', 1604490006);

-- ----------------------------
-- Table structure for cmf_cash_record
-- ----------------------------
DROP TABLE IF EXISTS `cmf_cash_record`;
CREATE TABLE `cmf_cash_record`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `money` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '提现金额',
  `votes` int(20) NOT NULL DEFAULT 0 COMMENT '提现映票数',
  `orderno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方订单号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态，0审核中，1审核通过，2审核拒绝',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '申请时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '账号类型',
  `account_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '银行名称',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '帐号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_cash_record
-- ----------------------------
INSERT INTO `cmf_cash_record` VALUES (1, 42520, 3.20, 400, '42520_1604389194447', '', 1, 1604389194, 1604389287, 3, '邮政储蓄银行', '6413131464591916165', '拉拉队');
INSERT INTO `cmf_cash_record` VALUES (2, 42513, 22.40, 2800, '42513_1604404615306', '', 2, 1604404615, 1604404683, 1, '', '13222222222', '淡定淡');

-- ----------------------------
-- Table structure for cmf_charge_admin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_admin`;
CREATE TABLE `cmf_charge_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '充值对象ID',
  `coin` int(20) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_charge_admin
-- ----------------------------
INSERT INTO `cmf_charge_admin` VALUES (1, 42513, 10000, 1604319695, 'kane', '1892640702');
INSERT INTO `cmf_charge_admin` VALUES (2, 42515, 100000, 1604320503, 'regame', '1892640744');
INSERT INTO `cmf_charge_admin` VALUES (3, 42516, 1000, 1604468491, 'kane', '1892640702');
INSERT INTO `cmf_charge_admin` VALUES (4, 42533, 2147483647, 1604491851, 'regame', '1892640744');
INSERT INTO `cmf_charge_admin` VALUES (5, 42488, 2147483647, 1604492122, 'regame', '1892640744');

-- ----------------------------
-- Table structure for cmf_charge_rules
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_rules`;
CREATE TABLE `cmf_charge_rules`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `coin_ios` int(11) NOT NULL DEFAULT 0 COMMENT '苹果钻石数',
  `money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '安卓金额',
  `product_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '苹果项目ID',
  `give` int(11) NOT NULL DEFAULT 0 COMMENT '赠送钻石数',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_charge_rules
-- ----------------------------
INSERT INTO `cmf_charge_rules` VALUES (2, '3000钻石', 3000, 2100, 30.00, 'coin_3000', 0, 1, 1484985389);
INSERT INTO `cmf_charge_rules` VALUES (3, '9800钻石', 9800, 9800, 98.00, 'coin_9800', 200, 2, 1484985412);
INSERT INTO `cmf_charge_rules` VALUES (4, '38800钻石', 38800, 38800, 388.00, 'coin_38800', 500, 3, 1484985445);
INSERT INTO `cmf_charge_rules` VALUES (5, '58800钻石', 58800, 58800, 588.00, 'coin_58800', 1200, 4, 1484985458);

-- ----------------------------
-- Table structure for cmf_charge_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_user`;
CREATE TABLE `cmf_charge_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '充值对象ID',
  `money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '金额',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `coin_give` int(11) NOT NULL DEFAULT 0 COMMENT '赠送钻石数',
  `orderno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商家订单号',
  `trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方平台订单号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '支付类型',
  `ambient` tinyint(1) NOT NULL DEFAULT 0 COMMENT '支付环境',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_charge_user
-- ----------------------------
INSERT INTO `cmf_charge_user` VALUES (1, 42513, 42513, 30.00, 3000, 0, '42513_42513_1102201211_7872', '', 0, 1604319131, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (2, 42513, 42513, 30.00, 3000, 0, '42513_42513_1102201803_4558', '', 1, 1604319483, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (3, 42515, 42515, 30.00, 3000, 0, '42515_42515_1102201945_3151', '', 0, 1604319585, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (4, 42502, 42502, 30.00, 3000, 0, '42502_42502_1102221447_4062', '', 0, 1604326487, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (5, 42502, 42502, 30.00, 3000, 0, '42502_42502_1102221514_5550', '', 0, 1604326514, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (6, 42502, 42502, 30.00, 3000, 0, '42502_42502_1102235148_7791', '', 0, 1604332308, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (7, 42502, 42502, 30.00, 3000, 0, '42502_42502_1103000435_1219', '', 0, 1604333075, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (8, 42492, 42492, 30.00, 3000, 0, '42492_42492_1103141631_2360', '', 1, 1604384191, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (9, 42520, 42520, 30.00, 3000, 0, '42520_42520_1104133224_5323', '', 1, 1604467944, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (10, 42509, 42509, 30.00, 3000, 0, '42509_42509_1104141700_4437', '', 0, 1604470620, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (11, 42529, 42529, 30.00, 3000, 0, '42529_42529_1105183327_5054', '', 0, 1604572407, 1, 1);
INSERT INTO `cmf_charge_user` VALUES (12, 42542, 42542, 588.00, 58800, 1200, '42542_42542_1106120658_9481', '', 1, 1604635618, 1, 1);

-- ----------------------------
-- Table structure for cmf_domain
-- ----------------------------
DROP TABLE IF EXISTS `cmf_domain`;
CREATE TABLE `cmf_domain`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `app_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'app接口地址',
  `socket_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'socket地址',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '状态：0 不显示  1 显示',
  `add_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '域名表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `cmf_dynamic`;
CREATE TABLE `cmf_dynamic`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '标题',
  `thumb` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '图片地址：多张图片用分号隔开',
  `video_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '视频封面',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '视频地址',
  `voice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '语音链接',
  `length` int(11) DEFAULT 0 COMMENT '语音时长',
  `likes` int(11) NOT NULL DEFAULT 0 COMMENT '点赞数',
  `comments` int(11) NOT NULL DEFAULT 0 COMMENT '评论数',
  `type` int(10) NOT NULL DEFAULT 0 COMMENT '动态类型：0：纯文字；1：文字+图片；2：文字+视频；3：文字+语音',
  `isdel` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1删除（下架）0不下架',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '视频状态 0未审核 1通过 2拒绝',
  `uptime` int(12) DEFAULT 0 COMMENT '审核不通过时间（第一次审核不通过时更改此值，用于判断是否发送极光IM）',
  `xiajia_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '下架原因',
  `lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '维度',
  `lng` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '经度',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '城市',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '详细地理位置',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '发布时间',
  `fail_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '审核原因',
  `show_val` int(12) NOT NULL DEFAULT 0 COMMENT '曝光值',
  `recommend_val` int(20) DEFAULT 0 COMMENT '推荐值',
  `labelid` int(11) NOT NULL DEFAULT 0 COMMENT '标签ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_dynamic
-- ----------------------------
INSERT INTO `cmf_dynamic` VALUES (1, 42514, '哈哈哈哈哈', '', '', '', '', 0, 0, 2, 0, 0, 1, 1604375649, '', '0.0', '0.0', '', '', 1604375649, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (2, 42514, '哈哈哈8888', '', '', '', '', 0, 0, 1, 0, 1, 1, 1604383151, '', '0.0', '0.0', '', '', 1604383151, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (3, 42524, 'fasfasefa befw', 'android_42524_20201105_140318_3317080.jpg', '', '', '', 0, 0, 0, 1, 0, 1, 1604556198, '', '0.0', '0.0', '', '', 1604556198, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (4, 42492, '测试九张图', 'android_42492_20201105_183603_2418047.jpg;android_42492_20201105_183603_9927012.jpg;android_42492_20201105_183604_4373683.jpg;android_42492_20201105_183604_8773651.jpg;android_42492_20201105_183605_3227692.jpg;android_42492_20201105_183605_7658591.jpg;android_42492_20201105_183606_3825921.jpg;android_42492_20201105_183606_822139.jpg;android_42492_20201105_183607_2623297.jpg', '', '', '', 0, 1, 1, 1, 0, 1, 1604572568, '', '0.0', '0.0', '', '', 1604572568, '', 0, 0, 2);
INSERT INTO `cmf_dynamic` VALUES (5, 42492, '十七岁侬头模同特㧹铽偷偷脱俗后手失而复得1和你而过收书顺顺our牛瑞OST后送过去狗肉汤明天，你投入哦去MP天气太热泥塑木雕木瓜女孩子晚上。ejxvekxbeibzoebzivwjzveizvezivwizvwaibwO-689wap..com.cn..com.cn.comwap...com.cnwap..com.com.cn.www..com.cn-/.c/.com.cn-,?_\\\",?_!!_\\\"', '', '', '', '', 0, 4, 1, 0, 0, 1, 1604572731, '', '0.0', '0.0', '', '', 1604572731, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (6, 42514, '54664676764664646799794664646464664643313134327257646466464667643436464646646464664616466444445444444444444444444455555555664646949499494994645546463131324264646646464994646649464661616649494664646646', 'android_42514_20201105_191746_1581841.jpg;android_42514_20201105_191748_8061795.jpg;android_42514_20201105_191749_8397262.jpg;android_42514_20201105_191752_096440.jpg;android_42514_20201105_191754_227365.jpg;android_42514_20201105_191755_2687107.jpg;android_42514_20201105_191757_0689182.jpg;android_42514_20201105_191758_1544132.jpg;android_42514_20201105_191759_2378386.jpg', '', '', '', 0, 0, 2, 1, 0, 1, 1604575079, '', '0.0', '0.0', '', '', 1604575079, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (7, 42531, '666', '', '', '', '', 0, 0, 0, 0, 0, 1, 1604579027, '', '0.0', '0.0', '', '', 1604579027, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (8, 42531, '8888', '', '', '', '', 0, 0, 0, 0, 0, 1, 1604579038, '', '0.0', '0.0', '', '', 1604579038, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (9, 42541, '测试', '', '', '', '', 0, 0, 0, 0, 0, 1, 1604588966, '', '0.0', '0.0', '', '', 1604588966, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (10, 42499, '', '', 'android_42499_20201106_192036_1524326.jpg', 'android_42499_20201106_191840_9941342.mp4', '', 0, 1, 1, 2, 0, 1, 1604661640, '', '0.0', '0.0', '', '', 1604661640, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (11, 42516, '发多多少少山山水水', '42516_IOS_20201106194236_action_image0_cover.png', '', '', '', 0, 0, 0, 1, 0, 1, 1604662962, '', '14.468843', '121.005246', '帕拉纳克市', '', 1604662962, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (12, 42504, '1234567896466448', 'android_42504_20201107_200640_145987.jpg', '', '', '', 0, 0, 0, 1, 1, 1, 1604750801, '99999999999999999999', '0.0', '0.0', '', '', 1604750801, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (14, 42524, 'afasefafe', 'android_42524_20201108_121032_9126961.jpg', '', '', '', 0, 0, 0, 1, 0, 1, 1604808633, '', '0.0', '0.0', '', '', 1604808633, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (15, 42524, 'adfasdf', '', '', '', '', 0, 1, 1, 0, 0, 1, 1604808851, '', '0.0', '0.0', '', '', 1604808851, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (16, 42516, '点点滴滴的广告费分#广告费', '42516_IOS_20201109113553_action_image1_cover.png;42516_IOS_20201109113552_action_image0_cover.png', '', '', '', 0, 0, 1, 1, 0, 1, 1604892958, '', '14.468924', '121.005341', '帕拉纳克市', '', 1604892958, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (17, 42552, '视频', '', 'android_42552_20201109_114041_4428242.jpg', 'android_42552_20201109_113821_1151085.mp4', '', 0, 0, 0, 2, 0, 1, 1604893242, '', '0.0', '0.0', '', '', 1604893242, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (18, 42516, '', '42516_IOS_20201109115903_action_image0_cover.png', '', '', '', 0, 0, 0, 1, 0, 1, 1604894345, '', '14.469012', '121.005393', '帕拉纳克市', '', 1604894345, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (19, 42503, '', 'android_42503_20201109_115929_3059686.jpg', '', '', '', 0, 0, 0, 1, 0, 1, 1604894370, '', '0.0', '0.0', '', '', 1604894370, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (20, 42539, '分享动态标题', 'android_42539_20201109_120553_9009228.jpg', '', '', '', 0, 0, 0, 1, 0, 1, 1604894755, '', '0.0', '0.0', '', '', 1604894755, '', 0, 0, 7);
INSERT INTO `cmf_dynamic` VALUES (21, 42539, '测试', '', '', '', '', 0, 0, 0, 0, 0, 1, 1604895058, '', '0.0', '0.0', '', '', 1604895058, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (22, 42539, '审核村长', '', '', '', '', 0, 0, 0, 0, 0, 0, 1604895152, '', '0.0', '0.0', '', '', 1604895152, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (23, 42521, '测试', 'android_42521_20201109_132028_8139757.jpg', '', '', '', 0, 0, 0, 1, 0, 0, 1604899230, '', '0.0', '0.0', '', '', 1604899230, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (24, 42521, '测试123', 'android_42521_20201109_132115_1642042.jpg;android_42521_20201109_132116_7054319.jpg;android_42521_20201109_132117_6536587.jpg;android_42521_20201109_132118_9317956.jpg', '', '', '', 0, 0, 0, 1, 0, 1, 1604899280, '', '0.0', '0.0', '', '', 1604899280, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (25, 42565, '测试', 'android_42565_20201109_133640_5949074.jpg;android_42565_20201109_133645_4046714.jpg;android_42565_20201109_133648_4057153.jpg', '', '', '', 0, 0, 0, 1, 0, 1, 1604900213, '', '0.0', '0.0', '', '', 1604900213, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (26, 42565, '', 'android_42565_20201109_133743_0266439.jpg;android_42565_20201109_133746_9475904.jpg;android_42565_20201109_133749_9171055.jpg;android_42565_20201109_133752_3074717.jpg;android_42565_20201109_133801_8326410.jpg;android_42565_20201109_133804_1443148.jpg;android_42565_20201109_133825_2454317.jpg;android_42565_20201109_133832_5162964.jpg;android_42565_20201109_133846_6479295.jpg', '', '', '', 0, 0, 0, 1, 0, -1, 1604900331, '', '0.0', '0.0', '', '', 1604900331, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (27, 42513, 'Gfffdddr', '', '', '', '', 0, 1, 1, 0, 0, 1, 1604901226, '', '14.468817', '121.005121', '帕拉纳克市', '', 1604901226, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (28, 42513, 'Hhjjjh', '', '', '', '', 0, 0, 0, 0, 0, -1, 1604901287, '', '14.468846', '121.005212', '帕拉纳克市', '', 1604901287, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (29, 42516, 'Dddedwew', '', '', '', '', 0, 0, 0, 0, 0, 0, 1604920855, '', '14.468943', '121.005289', '帕拉纳克市', '', 1604920855, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (30, 42518, '啊啊啊啊啊', '', '', '', '', 0, 0, 0, 0, 0, 0, 1604920870, '', '0.0', '0.0', '', '', 1604920870, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (31, 42577, '啦啦啦啦啦啦', '', '', '', '', 0, 0, 0, 0, 0, 0, 1604931827, '', '0.0', '0.0', '', '', 1604931827, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (32, 42577, '啦啦啦啦啦啦', '', '', '', '', 0, 0, 0, 0, 0, 0, 1604931980, '', '0.0', '0.0', '', '', 1604931980, '', 0, 0, 0);
INSERT INTO `cmf_dynamic` VALUES (33, 42496, '测试语音动态，台风的声音', '', '', '', '42496_IOS_20201111230458_action_audio.m4a', 9, 0, 0, 3, 0, 0, 1605107103, '', '', '', '好像在火星', '', 1605107103, '', 0, 0, 1);

-- ----------------------------
-- Table structure for cmf_dynamic_comments
-- ----------------------------
DROP TABLE IF EXISTS `cmf_dynamic_comments`;
CREATE TABLE `cmf_dynamic_comments`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '评论用户ID',
  `touid` int(10) NOT NULL DEFAULT 0 COMMENT '被评论用户ID',
  `dynamicid` int(10) NOT NULL DEFAULT 0 COMMENT '动态ID',
  `commentid` int(10) NOT NULL DEFAULT 0 COMMENT '评论iD',
  `parentid` int(10) NOT NULL DEFAULT 0 COMMENT '上级评论ID',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论内容',
  `likes` int(11) NOT NULL DEFAULT 0 COMMENT '点赞数',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '提交时间',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型，0文字1语音',
  `voice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '语音链接',
  `length` int(11) NOT NULL DEFAULT 0 COMMENT '时长',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_dynamic_comments
-- ----------------------------
INSERT INTO `cmf_dynamic_comments` VALUES (1, 42496, 0, 1, 0, 0, '测试一下', 1, 1604382182, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (2, 42514, 42514, 2, 0, 0, '红红火火恍恍惚惚', 0, 1604383172, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (3, 42514, 42514, 1, 0, 0, '6666', 0, 1604383792, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (4, 42531, 42514, 6, 0, 0, '6666666666666666666', 1, 1604580727, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (5, 42531, 42514, 6, 0, 0, '就是就是就是就是觉得就', 1, 1604580740, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (6, 42531, 42492, 5, 0, 0, 'djjdjjsjdjjss', 0, 1604580755, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (7, 42514, 42499, 10, 0, 0, '你好帅', 0, 1604661798, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (8, 42504, 42492, 4, 0, 0, '美女一枚', 0, 1604750721, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (9, 42516, 0, 16, 0, 0, '得到的数据', 0, 1604892979, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (10, 42516, 0, 15, 0, 0, '当年的少男少女时间', 0, 1604893294, 0, '', 0);
INSERT INTO `cmf_dynamic_comments` VALUES (11, 42496, 0, 27, 0, 0, '测试', 0, 1605107529, 0, '', 0);

-- ----------------------------
-- Table structure for cmf_dynamic_comments_like
-- ----------------------------
DROP TABLE IF EXISTS `cmf_dynamic_comments_like`;
CREATE TABLE `cmf_dynamic_comments_like`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '点赞用户ID',
  `commentid` int(10) NOT NULL DEFAULT 0 COMMENT '评论ID',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '提交时间',
  `touid` int(12) NOT NULL DEFAULT 0 COMMENT '被喜欢的评论者id',
  `dynamicid` int(12) NOT NULL DEFAULT 0 COMMENT '评论所属动态id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_dynamic_comments_like
-- ----------------------------
INSERT INTO `cmf_dynamic_comments_like` VALUES (1, 42514, 1, 1604383805, 42496, 1);
INSERT INTO `cmf_dynamic_comments_like` VALUES (3, 42496, 5, 1604839474, 42531, 6);
INSERT INTO `cmf_dynamic_comments_like` VALUES (5, 42496, 4, 1604839478, 42531, 6);

-- ----------------------------
-- Table structure for cmf_dynamic_label
-- ----------------------------
DROP TABLE IF EXISTS `cmf_dynamic_label`;
CREATE TABLE `cmf_dynamic_label`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面',
  `orderno` int(11) NOT NULL DEFAULT 10000 COMMENT '序号',
  `isrecommend` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否推荐 0否  1是',
  `use_nums` int(11) NOT NULL DEFAULT 0 COMMENT '使用次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_dynamic_label
-- ----------------------------
INSERT INTO `cmf_dynamic_label` VALUES (1, '如何快速搭建直播平台', 'dynamic_label_1.png', 10, 1, 11);
INSERT INTO `cmf_dynamic_label` VALUES (2, '如何上热门话题榜单', 'dynamic_label_2.png', 1, 1, 8);
INSERT INTO `cmf_dynamic_label` VALUES (3, '中国的领土一寸也不能丢', 'dynamic_label_3.png', 3, 1, 2);
INSERT INTO `cmf_dynamic_label` VALUES (4, '蜘蛛倒挂睡觉不会掉下来吗', 'dynamic_label_4.png', 4, 1, 2);
INSERT INTO `cmf_dynamic_label` VALUES (5, '三星堆遗址再次启动发掘', 'dynamic_label_5.png', 2, 0, 0);
INSERT INTO `cmf_dynamic_label` VALUES (6, '好人好事图鉴', 'dynamic_label_6.png', 5, 1, 2);
INSERT INTO `cmf_dynamic_label` VALUES (7, '致敬教育的燃灯者', 'dynamic_label_7.png', 6, 1, 2);
INSERT INTO `cmf_dynamic_label` VALUES (8, '身边的美食鉴赏', 'dynamic_label_8.png', 7, 1, 0);
INSERT INTO `cmf_dynamic_label` VALUES (9, '人人公益节', 'dynamic_label_9.png', 8, 1, 0);
INSERT INTO `cmf_dynamic_label` VALUES (10, '如何看待综艺恶意剪辑', 'dynamic_label_10.png', 9, 0, 1);

-- ----------------------------
-- Table structure for cmf_dynamic_like
-- ----------------------------
DROP TABLE IF EXISTS `cmf_dynamic_like`;
CREATE TABLE `cmf_dynamic_like`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '点赞用户',
  `dynamicid` int(10) NOT NULL DEFAULT 0 COMMENT '动态id',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '点赞时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '动态是否被删除或被拒绝 0被删除或被拒绝 1 正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_dynamic_like
-- ----------------------------
INSERT INTO `cmf_dynamic_like` VALUES (6, 42531, 5, 1604580664, 1);
INSERT INTO `cmf_dynamic_like` VALUES (7, 42531, 4, 1604580685, 1);
INSERT INTO `cmf_dynamic_like` VALUES (9, 42514, 10, 1604661781, 1);
INSERT INTO `cmf_dynamic_like` VALUES (25, 42496, 5, 1604839531, 1);
INSERT INTO `cmf_dynamic_like` VALUES (26, 42516, 15, 1604893287, 1);
INSERT INTO `cmf_dynamic_like` VALUES (27, 42516, 5, 1604894383, 1);
INSERT INTO `cmf_dynamic_like` VALUES (28, 42578, 5, 1604941509, 1);
INSERT INTO `cmf_dynamic_like` VALUES (30, 42496, 27, 1605107514, 1);

-- ----------------------------
-- Table structure for cmf_dynamic_report
-- ----------------------------
DROP TABLE IF EXISTS `cmf_dynamic_report`;
CREATE TABLE `cmf_dynamic_report`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '举报用户ID',
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '被举报用户ID',
  `dynamicid` int(11) NOT NULL DEFAULT 0 COMMENT '动态ID',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '举报内容',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0处理中 1已处理  2审核失败',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '提交时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `dynamic_type` int(10) NOT NULL DEFAULT 0 COMMENT '动态类型：0：纯文字；1：文字+图片‘；2：视频+图片；3：语音+图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_dynamic_report_classify
-- ----------------------------
DROP TABLE IF EXISTS `cmf_dynamic_report_classify`;
CREATE TABLE `cmf_dynamic_report_classify`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `list_order` int(10) NOT NULL DEFAULT 10000 COMMENT '排序',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '举报类型名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_dynamic_report_classify
-- ----------------------------
INSERT INTO `cmf_dynamic_report_classify` VALUES (1, 1, '低俗色情');
INSERT INTO `cmf_dynamic_report_classify` VALUES (2, 2, '侮辱谩骂');
INSERT INTO `cmf_dynamic_report_classify` VALUES (3, 3, '盗用他人作品');
INSERT INTO `cmf_dynamic_report_classify` VALUES (4, 4, '骗取点击');
INSERT INTO `cmf_dynamic_report_classify` VALUES (5, 5, '其他');
INSERT INTO `cmf_dynamic_report_classify` VALUES (6, 10000, '垃圾广告');
INSERT INTO `cmf_dynamic_report_classify` VALUES (7, 10000, '用户为未成年');
INSERT INTO `cmf_dynamic_report_classify` VALUES (8, 10000, '任性打抱不平，就爱举报	');
INSERT INTO `cmf_dynamic_report_classify` VALUES (9, 10000, '引人不适	');

-- ----------------------------
-- Table structure for cmf_family
-- ----------------------------
DROP TABLE IF EXISTS `cmf_family`;
CREATE TABLE `cmf_family`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '家族名称',
  `badge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '家族图标',
  `apply_pos` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '身份证正面',
  `apply_side` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '身份证背面',
  `briefing` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `carded` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '证件号',
  `fullname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '申请时间',
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '申请状态 0未审核 1 审核失败 2 审核通过 3',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '失败原因',
  `disable` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否禁用',
  `divide_family` int(11) NOT NULL DEFAULT 0 COMMENT '分成比例',
  `istip` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否需要通知',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_family
-- ----------------------------
INSERT INTO `cmf_family` VALUES (1, 42487, 'android大家族', 'http://7niuobs.jiextx.com/default/20201102/af6e2183823f7fa533b4434555c60dfa.png', 'http://7niuobs.jiextx.com/default/20201102/9680a70106e4eab92012cbc3f759ac17.png', 'http://7niuobs.jiextx.com/default/20201102/8b444e818e86dd7ae3f980b98517b7b0.png', '开心浪起来', '160156190301064560', 'lorta', 1604303783, 0, '', 0, 50, 0);
INSERT INTO `cmf_family` VALUES (2, 42513, '家族一', 'http://7niuobs.jiextx.com/default/20201103/e3ea7e0960a99d764e0d0d3d9404375d.png', 'http://7niuobs.jiextx.com/default/20201103/f6ee36fe15f957d8e6e38266078c264c.png', 'http://7niuobs.jiextx.com/default/20201103/38463dd7d5d5bb6f47841a9e30a5d7cf.png', 'Qqq', '511602199012122345', 'Kane', 1604373216, 2, '', 0, 30, 0);

-- ----------------------------
-- Table structure for cmf_family_profit
-- ----------------------------
DROP TABLE IF EXISTS `cmf_family_profit`;
CREATE TABLE `cmf_family_profit`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '主播ID',
  `familyid` int(11) NOT NULL DEFAULT 0 COMMENT '家族ID',
  `time` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '格式化日期',
  `profit_anthor` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '主播收益',
  `total` int(11) NOT NULL DEFAULT 0 COMMENT '总收益',
  `profit` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '家族收益',
  `addtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_family_profit
-- ----------------------------
INSERT INTO `cmf_family_profit` VALUES (1, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383948);
INSERT INTO `cmf_family_profit` VALUES (2, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383949);
INSERT INTO `cmf_family_profit` VALUES (3, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383950);
INSERT INTO `cmf_family_profit` VALUES (4, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383951);
INSERT INTO `cmf_family_profit` VALUES (5, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383952);
INSERT INTO `cmf_family_profit` VALUES (6, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383952);
INSERT INTO `cmf_family_profit` VALUES (7, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383952);
INSERT INTO `cmf_family_profit` VALUES (8, 42514, 2, '2020-11-03', 700.00, 1000, 300.00, 1604383952);
INSERT INTO `cmf_family_profit` VALUES (9, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393708);
INSERT INTO `cmf_family_profit` VALUES (10, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393712);
INSERT INTO `cmf_family_profit` VALUES (11, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393714);
INSERT INTO `cmf_family_profit` VALUES (12, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393715);
INSERT INTO `cmf_family_profit` VALUES (13, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393715);
INSERT INTO `cmf_family_profit` VALUES (14, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393715);
INSERT INTO `cmf_family_profit` VALUES (15, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393716);
INSERT INTO `cmf_family_profit` VALUES (16, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393716);
INSERT INTO `cmf_family_profit` VALUES (17, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393716);
INSERT INTO `cmf_family_profit` VALUES (18, 42514, 2, '2020-11-03', 0.00, 0, 0.00, 1604393716);
INSERT INTO `cmf_family_profit` VALUES (19, 42516, 2, '2020-11-03', 850.00, 1000, 150.00, 1604404057);
INSERT INTO `cmf_family_profit` VALUES (20, 42516, 2, '2020-11-03', 850.00, 1000, 150.00, 1604404059);
INSERT INTO `cmf_family_profit` VALUES (21, 42516, 2, '2020-11-03', 850.00, 1000, 150.00, 1604404060);
INSERT INTO `cmf_family_profit` VALUES (22, 42520, 2, '2020-11-04', 350.00, 500, 150.00, 1604468529);
INSERT INTO `cmf_family_profit` VALUES (23, 42514, 2, '2020-11-04', 0.00, 0, 0.00, 1604491590);
INSERT INTO `cmf_family_profit` VALUES (24, 42514, 2, '2020-11-04', 350.00, 500, 150.00, 1604491758);
INSERT INTO `cmf_family_profit` VALUES (25, 42514, 2, '2020-11-04', 350.00, 500, 150.00, 1604491760);
INSERT INTO `cmf_family_profit` VALUES (26, 42514, 2, '2020-11-04', 2100.00, 3000, 900.00, 1604491787);
INSERT INTO `cmf_family_profit` VALUES (27, 42514, 2, '2020-11-04', 2100.00, 3000, 900.00, 1604491793);
INSERT INTO `cmf_family_profit` VALUES (28, 42514, 2, '2020-11-04', 117.60, 168, 50.40, 1604491810);
INSERT INTO `cmf_family_profit` VALUES (29, 42514, 2, '2020-11-04', 117.60, 168, 50.40, 1604491817);
INSERT INTO `cmf_family_profit` VALUES (30, 42514, 2, '2020-11-04', 0.00, 0, 0.00, 1604491837);
INSERT INTO `cmf_family_profit` VALUES (31, 42514, 2, '2020-11-04', 700.00, 1000, 300.00, 1604491878);
INSERT INTO `cmf_family_profit` VALUES (32, 42514, 2, '2020-11-04', 700.00, 1000, 300.00, 1604492310);

-- ----------------------------
-- Table structure for cmf_family_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_family_user`;
CREATE TABLE `cmf_family_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `familyid` int(11) NOT NULL DEFAULT 0 COMMENT '家族ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原因',
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
  `signout` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否退出',
  `istip` tinyint(1) NOT NULL DEFAULT 0 COMMENT '审核后是否需要通知',
  `signout_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '踢出或拒绝理由',
  `signout_istip` tinyint(4) NOT NULL DEFAULT 0 COMMENT '踢出或拒绝是否需要通知',
  `divide_family` int(11) NOT NULL DEFAULT -1 COMMENT '家族分成',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_family_user
-- ----------------------------
INSERT INTO `cmf_family_user` VALUES (2, 42516, 2, 1604380136, 1604380868, '', 2, 0, 0, '', 0, 15);
INSERT INTO `cmf_family_user` VALUES (3, 42518, 2, 1604380241, 1604380855, '', 3, 2, 0, '', 1, 5);
INSERT INTO `cmf_family_user` VALUES (4, 42514, 2, 1604380817, 1604381069, '', 2, 0, 0, '', 0, -1);
INSERT INTO `cmf_family_user` VALUES (5, 42520, 2, 1604387609, 1604389656, '', 2, 0, 1, '', 0, -1);

-- ----------------------------
-- Table structure for cmf_family_user_divide_apply
-- ----------------------------
DROP TABLE IF EXISTS `cmf_family_user_divide_apply`;
CREATE TABLE `cmf_family_user_divide_apply`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户id',
  `familyid` int(11) NOT NULL DEFAULT 0 COMMENT '家族id',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '处理状态 0 等待审核 1 同意 -1 拒绝',
  `divide` int(11) NOT NULL DEFAULT 0 COMMENT '家族分成',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_family_user_divide_apply
-- ----------------------------
INSERT INTO `cmf_family_user_divide_apply` VALUES (1, 42516, 2, 1604380805, 1604380868, 1, 10);
INSERT INTO `cmf_family_user_divide_apply` VALUES (2, 42518, 2, 1604380813, 1604380855, 1, 5);

-- ----------------------------
-- Table structure for cmf_feedback
-- ----------------------------
DROP TABLE IF EXISTS `cmf_feedback`;
CREATE TABLE `cmf_feedback`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统版本号',
  `model` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `addtime` int(11) NOT NULL COMMENT '提交时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_feedback
-- ----------------------------
INSERT INTO `cmf_feedback` VALUES (1, 42514, '', '10', 'ANA-AN00', '哈哈哈', 1604381934, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (2, 42514, '', '10', 'ANA-AN00', '1111', 1604381950, 1, 1604388775, '');
INSERT INTO `cmf_feedback` VALUES (3, 42496, '', '14.1', 'iPhone', '的黄金时间嘻嘻嘻', 1604399788, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (4, 42521, '', '10', 'HRY-LX1', '76665gg悔恨交加', 1604410091, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (5, 42514, '', '10', 'ANA-AN00', '测试手机', 1604490521, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (6, 42524, '', '5.1.1', 'LIO-AN00', '打发打发的分散', 1604657372, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (7, 42524, '', '5.1.1', 'LIO-AN00', '打发打发', 1604657382, 0, 0, 'http://7niuobs.jiextx.com/default/20201106/1426315356a9e116650c42ec73e76a9d.png');
INSERT INTO `cmf_feedback` VALUES (8, 42524, '', '5.1.1', 'LIO-AN00', '打发打发', 1604657383, 0, 0, 'http://7niuobs.jiextx.com/default/20201106/1426315356a9e116650c42ec73e76a9d.png');
INSERT INTO `cmf_feedback` VALUES (9, 42499, '', '10', 'HRY-LX1', '5554％％＝＋555444554＋＋455544＋＝/8556545666666666655566666665785657888888887675/5！6％454％45；4；2％4 464 6 46 64％4343；64％6*7*68！5*6*54％6％4；＋5；＋5 97*988*94％6％4646/864/64％4％689/*89*/5！5！/56％46％94。4959*8', 1604660866, 0, 0, 'http://7niuobs.jiextx.com/default/20201106/69401f0559f8623b9313a48bb2a1cea1.jpg');
INSERT INTO `cmf_feedback` VALUES (10, 42504, '', '10', 'ANA-AN00', '57676644664949424213346499787542316494954576797675488494542213136497643342121579763864245464664646446646646433535994646469494634312135698978709464646946465683342454846343124597988764316125484646435484', 1604750289, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (11, 42504, '', '10', 'ANA-AN00', '噢噢噢', 1604750358, 1, 1604750505, 'http://7niuobs.jiextx.com/default/20201107/12aeda210d1cc89105fc253e955b672d.png');
INSERT INTO `cmf_feedback` VALUES (12, 42501, '', '13.7', 'iPhone', '广告费', 1604752907, 0, 0, 'http://7niuobs.jiextx.com/default/20201107/bbd26b3aa0135b6e99f372ced471ed98.png');
INSERT INTO `cmf_feedback` VALUES (13, 42542, '', '9', 'Redmi Note 8', '测试', 1604772993, 0, 0, 'http://7niuobs.jiextx.com/default/20201108/c6161abfdb754079c557a57855f82c86.png');
INSERT INTO `cmf_feedback` VALUES (14, 42524, '', '5.1.1', 'LIO-AN00', 'fadfadf', 1604808909, 0, 0, 'http://7niuobs.jiextx.com/default/20201108/ddfe29faab622a70a381dcf33e71527b.jpg');
INSERT INTO `cmf_feedback` VALUES (15, 42504, '', '10', 'ANA-AN00', '3335568555', 1604920277, 0, 0, 'http://7niuobs.jiextx.com/default/20201109/c24f3c4bfa8cd51489fb9a3d15118b7a.jpg');
INSERT INTO `cmf_feedback` VALUES (16, 42539, '', '10', 'PCHM30', '111', 1604922289, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (17, 42545, '', '10', 'PCHM30', 'af', 1604924357, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (18, 42488, '', '12.4.1', 'iPhone', '2225555555', 1604926709, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (19, 42543, '', '9', 'Redmi Note 8', 'sdd', 1604930821, 0, 0, 'http://7niuobs.jiextx.com/default/20201109/266b339849ae1ad1e79b589792061686.jpg');
INSERT INTO `cmf_feedback` VALUES (20, 42539, '', '14.0', 'iPhone', 'q we', 1604930941, 0, 0, 'http://7niuobs.jiextx.com/default/20201109/a7c7b221cbda81b98b1b903e74a5cce7.png');
INSERT INTO `cmf_feedback` VALUES (21, 42543, '', '10', 'PCHM30', '他天天', 1604934779, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (22, 42543, '', '14.0', 'iPhone', '时尚大方是的', 1604935768, 0, 0, 'http://7niuobs.jiextx.com/default/20201109/097bd8f5830783a3ab66e63bb03275de.png');
INSERT INTO `cmf_feedback` VALUES (23, 42543, '', '14.0', 'iPhone', '测试111', 1604937156, 0, 0, 'http://7niuobs.jiextx.com/default/20201109/a595e854424224d413152131a73a31af.jpeg');
INSERT INTO `cmf_feedback` VALUES (24, 42516, '', '13.7', 'iPhone 11', '结构特点', 1604990113, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (25, 42516, '', '13.7', 'iPhone 11', '突然的发点点滴滴', 1604990187, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (26, 42499, '', '10', 'ANA-AN00', '5448855', 1605850202, 0, 0, '');
INSERT INTO `cmf_feedback` VALUES (27, 42516, '', '10', 'HRY-AL00a', '啦啦啦啦', 1605850209, 0, 0, '');

-- ----------------------------
-- Table structure for cmf_game
-- ----------------------------
DROP TABLE IF EXISTS `cmf_game`;
CREATE TABLE `cmf_game`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` tinyint(1) DEFAULT 0 COMMENT '游戏名称',
  `liveuid` int(11) DEFAULT 0 COMMENT '主播ID',
  `bankerid` int(11) DEFAULT 0 COMMENT '庄家ID，0表示平台',
  `stream` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '直播流名',
  `starttime` int(11) DEFAULT 0 COMMENT '本次游戏开始时间',
  `endtime` int(11) DEFAULT 0 COMMENT '游戏结束时间',
  `result` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '本轮游戏结果',
  `state` tinyint(1) DEFAULT 0 COMMENT '当前游戏状态（0是进行中，1是正常结束，2是 主播关闭 3意外结束）',
  `banker_profit` int(11) DEFAULT 0 COMMENT '庄家收益',
  `banker_card` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '庄家牌型',
  `isintervene` tinyint(1) DEFAULT 0 COMMENT '是否进行系统干预',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_game
-- ----------------------------
INSERT INTO `cmf_game` VALUES (1, 3, 42513, 0, '42513_1604384196', 1604384226, 1604384256, '4', 1, 0, '', 0);
INSERT INTO `cmf_game` VALUES (2, 3, 42514, 0, '42514_1604491293', 1604492043, 1604492073, '1', 1, 40, '', 0);

-- ----------------------------
-- Table structure for cmf_gamerecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_gamerecord`;
CREATE TABLE `cmf_gamerecord`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` tinyint(1) DEFAULT 0 COMMENT '游戏类型',
  `uid` int(11) DEFAULT 0 COMMENT '用户ID',
  `liveuid` int(11) DEFAULT 0 COMMENT '主播ID',
  `gameid` int(11) DEFAULT 0 COMMENT '游戏ID',
  `coin_1` int(11) DEFAULT 0 COMMENT '1位置下注金额',
  `coin_2` int(11) DEFAULT 0 COMMENT '2位置下注金额',
  `coin_3` int(11) DEFAULT 0 COMMENT '3位置下注金额',
  `coin_4` int(11) DEFAULT 0 COMMENT '4位置下注金额',
  `coin_5` int(11) DEFAULT 0 COMMENT '5位置下注金额',
  `coin_6` int(11) DEFAULT 0 COMMENT '6位置下注金额',
  `status` tinyint(1) DEFAULT 0 COMMENT '处理状态 0 未处理 1 已处理',
  `addtime` int(11) DEFAULT 0 COMMENT '下注时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_gamerecord
-- ----------------------------
INSERT INTO `cmf_gamerecord` VALUES (1, 3, 42516, 42514, 2, 0, 20, 10, 0, 0, 0, 1, 1604492050);
INSERT INTO `cmf_gamerecord` VALUES (2, 3, 42534, 42514, 2, 0, 10, 0, 0, 0, 0, 1, 1604492052);

-- ----------------------------
-- Table structure for cmf_getcode_limit_ip
-- ----------------------------
DROP TABLE IF EXISTS `cmf_getcode_limit_ip`;
CREATE TABLE `cmf_getcode_limit_ip`  (
  `ip` bigint(20) NOT NULL COMMENT 'ip地址',
  `date` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '时间',
  `times` tinyint(4) NOT NULL DEFAULT 0 COMMENT '次数',
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_gift
-- ----------------------------
DROP TABLE IF EXISTS `cmf_gift`;
CREATE TABLE `cmf_gift`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mark` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识，0普通，1热门，2守护',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '类型,0普通礼物，1豪华礼物，2贴纸礼物，3手绘礼物',
  `sid` int(11) NOT NULL DEFAULT 0 COMMENT '分类ID',
  `giftname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `needcoin` int(11) NOT NULL DEFAULT 0 COMMENT '价格',
  `gifticon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `list_order` int(3) NOT NULL DEFAULT 9999 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `swftype` tinyint(1) NOT NULL DEFAULT 0 COMMENT '动画类型，0gif,1svga',
  `swf` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '动画链接',
  `swftime` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '动画时长',
  `isplatgift` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否全站礼物：0：否；1：是',
  `sticker_id` int(11) NOT NULL DEFAULT 0 COMMENT '贴纸ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 118 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_gift
-- ----------------------------
INSERT INTO `cmf_gift` VALUES (1, 2, 1, 0, '爱神丘比特', 5000, 'gift_1.png', 9999, 1578386860, 1, 'gift_gif_1.svga', 6.30, 1, 0);
INSERT INTO `cmf_gift` VALUES (2, 0, 1, 0, '爱心飞机', 2000, 'gift_2.png', 9999, 1578386860, 1, 'gift_gif_2.svga', 7.50, 1, 0);
INSERT INTO `cmf_gift` VALUES (3, 0, 1, 0, '告白气球', 1000, 'gift_3.png', 9999, 1578386860, 1, 'gift_gif_3.svga', 4.10, 1, 0);
INSERT INTO `cmf_gift` VALUES (4, 0, 1, 0, '流星雨', 10000, 'gift_4.png', 9999, 1578386860, 1, 'gift_gif_4.svga', 6.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (5, 0, 1, 0, '玫瑰花束', 500, 'gift_5.png', 9999, 1578386860, 1, 'gift_gif_5.svga', 4.10, 1, 0);
INSERT INTO `cmf_gift` VALUES (6, 2, 1, 0, '梦幻城堡', 20000, 'gift_6.png', 9999, 1578386860, 1, 'gift_gif_6.svga', 7.60, 0, 0);
INSERT INTO `cmf_gift` VALUES (7, 0, 1, 0, '跑车', 300, 'gift_7.png', 9999, 1578386860, 1, 'gift_gif_7.svga', 3.50, 1, 0);
INSERT INTO `cmf_gift` VALUES (8, 1, 1, 0, '鹊桥相会', 100, 'gift_8.png', 9999, 1578386860, 1, 'gift_gif_8.svga', 7.50, 0, 0);
INSERT INTO `cmf_gift` VALUES (9, 0, 1, 0, '旋转木马', 500, 'gift_9.png', 9999, 1578386860, 1, 'gift_gif_9.svga', 5.00, 1, 0);
INSERT INTO `cmf_gift` VALUES (10, 0, 1, 0, '游轮', 1000, 'gift_10.png', 9999, 1578386860, 1, 'gift_gif_10.svga', 4.90, 1, 0);
INSERT INTO `cmf_gift` VALUES (11, 0, 0, 0, '百合', 50, 'gift_11.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (12, 0, 0, 0, '棒棒糖', 5, 'gift_12.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (13, 0, 0, 0, '藏宝箱', 300, 'gift_13.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (14, 0, 0, 0, '蛋糕', 50, 'gift_14.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (15, 0, 0, 0, '粉丝牌', 100, 'gift_15.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (16, 0, 0, 0, '干杯', 10, 'gift_16.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (17, 2, 0, 0, '皇冠', 150, 'gift_17.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (18, 0, 0, 0, '金话筒', 130, 'gift_18.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (19, 0, 0, 0, '玫瑰', 10, 'gift_19.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (20, 3, 0, 0, '魔法棒', 20, 'gift_20.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (21, 1, 0, 0, '情书', 1, 'gift_21.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (22, 0, 0, 0, '圣诞袜子', 2, 'gift_22.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (23, 2, 0, 0, '水晶球', 23, 'gift_23.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (24, 0, 3, 0, '甜心巧克力', 24, 'gift_24.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (25, 3, 0, 0, '幸运福袋', 25, 'gift_25.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (26, 0, 0, 0, '樱花奶茶', 26, 'gift_26.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (27, 1, 0, 0, '招财猫', 27, 'gift_27.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (28, 3, 0, 0, '钻戒', 28, 'gift_28.png', 9999, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (29, 0, 2, 0, '狗狗眼镜', 1000, 'face_036.png', 4, 1578386860, 0, '', 4.00, 0, 1);
INSERT INTO `cmf_gift` VALUES (30, 0, 2, 0, '雪茄墨镜', 2000, 'face_037.png', 3, 1578386860, 0, '', 3.00, 0, 2);
INSERT INTO `cmf_gift` VALUES (62, 3, 0, 0, '四叶之草', 1, 'gift_62.png', 1, 1516002459, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (74, 0, 3, 0, '星星', 1, 'gift_74.png', 2, 1578386860, 0, '', 0.00, 0, 0);
INSERT INTO `cmf_gift` VALUES (75, 0, 1, 0, '百变小丑', 1000, 'gift_29.png', 9999, 1578386860, 1, 'gift_gif_29.svga', 5.00, 1, 0);
INSERT INTO `cmf_gift` VALUES (76, 0, 1, 0, '钞票枪', 1000, 'gift_30.png', 9999, 1578386860, 1, 'gift_gif_30.svga', 2.90, 1, 0);
INSERT INTO `cmf_gift` VALUES (77, 0, 1, 0, '星际火箭', 1000, 'gift_31.png', 9999, 1578386860, 1, 'gift_gif_31.svga', 3.70, 1, 0);
INSERT INTO `cmf_gift` VALUES (116, 0, 2, 0, '尖叫鸡', 3000, 'face_039.png', 2, 1584174130, 0, '', 2.90, 0, 3);
INSERT INTO `cmf_gift` VALUES (117, 2, 2, 0, '蒙面美人', 3500, 'admin/20201102/faada543a7f03c8ae52d91d76ede6fbe.jpg', 1, 1584174130, 0, '', 4.10, 0, 4);

-- ----------------------------
-- Table structure for cmf_gift_luck_rate
-- ----------------------------
DROP TABLE IF EXISTS `cmf_gift_luck_rate`;
CREATE TABLE `cmf_gift_luck_rate`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `giftid` int(11) NOT NULL DEFAULT 0 COMMENT '礼物ID',
  `nums` int(10) UNSIGNED NOT NULL COMMENT '数量',
  `times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '倍数',
  `rate` decimal(20, 3) NOT NULL DEFAULT 0.000 COMMENT '中奖概率',
  `isall` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否全站，0否1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_gift_sort
-- ----------------------------
DROP TABLE IF EXISTS `cmf_gift_sort`;
CREATE TABLE `cmf_gift_sort`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `orderno` int(3) NOT NULL DEFAULT 0 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_guard
-- ----------------------------
DROP TABLE IF EXISTS `cmf_guard`;
CREATE TABLE `cmf_guard`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '守护名称',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '守护类型，1普通2尊贵',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '价格',
  `length_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '时长类型，0天，1月，2年',
  `length` int(11) NOT NULL DEFAULT 0 COMMENT '时长',
  `length_time` int(11) NOT NULL DEFAULT 0 COMMENT '时长秒数',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_guard
-- ----------------------------
INSERT INTO `cmf_guard` VALUES (1, '7天体验', 1, 300, 0, 7, 604800, 1540862056, 1601005243, 0);
INSERT INTO `cmf_guard` VALUES (2, '1个月', 1, 1000, 1, 1, 2592000, 1540862139, 1601005269, 1);
INSERT INTO `cmf_guard` VALUES (3, '尊贵守护全年', 2, 12000, 2, 1, 31536000, 1540862377, 1546479176, 2);

-- ----------------------------
-- Table structure for cmf_guard_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_guard_user`;
CREATE TABLE `cmf_guard_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `liveuid` int(11) NOT NULL DEFAULT 0 COMMENT '主播ID',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '守护类型,1普通守护，2尊贵守护',
  `endtime` int(11) NOT NULL DEFAULT 0 COMMENT '到期时间',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `liveuid_index`(`liveuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_guard_user
-- ----------------------------
INSERT INTO `cmf_guard_user` VALUES (1, 42516, 42520, 1, 1607665798, 1604468998);
INSERT INTO `cmf_guard_user` VALUES (2, 42516, 42514, 1, 1605096755, 1604491955);

-- ----------------------------
-- Table structure for cmf_guide
-- ----------------------------
DROP TABLE IF EXISTS `cmf_guide`;
CREATE TABLE `cmf_guide`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片/视频链接',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '跳转链接',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型',
  `list_order` int(11) NOT NULL DEFAULT 10000 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_guide
-- ----------------------------
INSERT INTO `cmf_guide` VALUES (1, 'admin/20201109/807d74de972cbc36eaa8db75c2bee874.jpg', 'https://www.baidu.com', 0, 10000, 1604925000, 1604925000);

-- ----------------------------
-- Table structure for cmf_hook
-- ----------------------------
DROP TABLE IF EXISTS `cmf_hook`;
CREATE TABLE `cmf_hook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '钩子类型(1:系统钩子;2:应用钩子;3:模板钩子;4:后台模板钩子)',
  `once` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否只允许一个插件运行(0:多个;1:一个)',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '钩子名称',
  `hook` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '钩子',
  `app` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '应用名(只有应用钩子才用)',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统钩子表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_hook
-- ----------------------------
INSERT INTO `cmf_hook` VALUES (2, 1, 0, '应用开始', 'app_begin', 'cmf', '应用开始');
INSERT INTO `cmf_hook` VALUES (3, 1, 0, '模块初始化', 'module_init', 'cmf', '模块初始化');
INSERT INTO `cmf_hook` VALUES (4, 1, 0, '控制器开始', 'action_begin', 'cmf', '控制器开始');
INSERT INTO `cmf_hook` VALUES (5, 1, 0, '视图输出过滤', 'view_filter', 'cmf', '视图输出过滤');
INSERT INTO `cmf_hook` VALUES (6, 1, 0, '应用结束', 'app_end', 'cmf', '应用结束');
INSERT INTO `cmf_hook` VALUES (7, 1, 0, '日志write方法', 'log_write', 'cmf', '日志write方法');
INSERT INTO `cmf_hook` VALUES (8, 1, 0, '输出结束', 'response_end', 'cmf', '输出结束');
INSERT INTO `cmf_hook` VALUES (9, 1, 0, '后台控制器初始化', 'admin_init', 'cmf', '后台控制器初始化');
INSERT INTO `cmf_hook` VALUES (10, 1, 0, '前台控制器初始化', 'home_init', 'cmf', '前台控制器初始化');
INSERT INTO `cmf_hook` VALUES (11, 1, 1, '发送手机验证码', 'send_mobile_verification_code', 'cmf', '发送手机验证码');
INSERT INTO `cmf_hook` VALUES (12, 3, 0, '模板 body标签开始', 'body_start', '', '模板 body标签开始');
INSERT INTO `cmf_hook` VALUES (13, 3, 0, '模板 head标签结束前', 'before_head_end', '', '模板 head标签结束前');
INSERT INTO `cmf_hook` VALUES (14, 3, 0, '模板底部开始', 'footer_start', '', '模板底部开始');
INSERT INTO `cmf_hook` VALUES (15, 3, 0, '模板底部开始之前', 'before_footer', '', '模板底部开始之前');
INSERT INTO `cmf_hook` VALUES (16, 3, 0, '模板底部结束之前', 'before_footer_end', '', '模板底部结束之前');
INSERT INTO `cmf_hook` VALUES (17, 3, 0, '模板 body 标签结束之前', 'before_body_end', '', '模板 body 标签结束之前');
INSERT INTO `cmf_hook` VALUES (18, 3, 0, '模板左边栏开始', 'left_sidebar_start', '', '模板左边栏开始');
INSERT INTO `cmf_hook` VALUES (19, 3, 0, '模板左边栏结束之前', 'before_left_sidebar_end', '', '模板左边栏结束之前');
INSERT INTO `cmf_hook` VALUES (20, 3, 0, '模板右边栏开始', 'right_sidebar_start', '', '模板右边栏开始');
INSERT INTO `cmf_hook` VALUES (21, 3, 0, '模板右边栏结束之前', 'before_right_sidebar_end', '', '模板右边栏结束之前');
INSERT INTO `cmf_hook` VALUES (22, 3, 1, '评论区', 'comment', '', '评论区');
INSERT INTO `cmf_hook` VALUES (23, 3, 1, '留言区', 'guestbook', '', '留言区');
INSERT INTO `cmf_hook` VALUES (24, 2, 0, '后台首页仪表盘', 'admin_dashboard', 'admin', '后台首页仪表盘');
INSERT INTO `cmf_hook` VALUES (25, 4, 0, '后台模板 head标签结束前', 'admin_before_head_end', '', '后台模板 head标签结束前');
INSERT INTO `cmf_hook` VALUES (26, 4, 0, '后台模板 body 标签结束之前', 'admin_before_body_end', '', '后台模板 body 标签结束之前');
INSERT INTO `cmf_hook` VALUES (27, 2, 0, '后台登录页面', 'admin_login', 'admin', '后台登录页面');
INSERT INTO `cmf_hook` VALUES (28, 1, 1, '前台模板切换', 'switch_theme', 'cmf', '前台模板切换');
INSERT INTO `cmf_hook` VALUES (29, 3, 0, '主要内容之后', 'after_content', '', '主要内容之后');
INSERT INTO `cmf_hook` VALUES (32, 2, 1, '获取上传界面', 'fetch_upload_view', 'user', '获取上传界面');
INSERT INTO `cmf_hook` VALUES (33, 3, 0, '主要内容之前', 'before_content', 'cmf', '主要内容之前');
INSERT INTO `cmf_hook` VALUES (34, 1, 0, '日志写入完成', 'log_write_done', 'cmf', '日志写入完成');
INSERT INTO `cmf_hook` VALUES (35, 1, 1, '后台模板切换', 'switch_admin_theme', 'cmf', '后台模板切换');
INSERT INTO `cmf_hook` VALUES (36, 1, 1, '验证码图片', 'captcha_image', 'cmf', '验证码图片');
INSERT INTO `cmf_hook` VALUES (37, 2, 1, '后台模板设计界面', 'admin_theme_design_view', 'admin', '后台模板设计界面');
INSERT INTO `cmf_hook` VALUES (38, 2, 1, '后台设置网站信息界面', 'admin_setting_site_view', 'admin', '后台设置网站信息界面');
INSERT INTO `cmf_hook` VALUES (39, 2, 1, '后台清除缓存界面', 'admin_setting_clear_cache_view', 'admin', '后台清除缓存界面');
INSERT INTO `cmf_hook` VALUES (40, 2, 1, '后台导航管理界面', 'admin_nav_index_view', 'admin', '后台导航管理界面');
INSERT INTO `cmf_hook` VALUES (41, 2, 1, '后台友情链接管理界面', 'admin_link_index_view', 'admin', '后台友情链接管理界面');
INSERT INTO `cmf_hook` VALUES (42, 2, 1, '后台幻灯片管理界面', 'admin_slide_index_view', 'admin', '后台幻灯片管理界面');
INSERT INTO `cmf_hook` VALUES (43, 2, 1, '后台管理员列表界面', 'admin_user_index_view', 'admin', '后台管理员列表界面');
INSERT INTO `cmf_hook` VALUES (44, 2, 1, '后台角色管理界面', 'admin_rbac_index_view', 'admin', '后台角色管理界面');
INSERT INTO `cmf_hook` VALUES (49, 2, 1, '用户管理本站用户列表界面', 'user_admin_index_view', 'user', '用户管理本站用户列表界面');
INSERT INTO `cmf_hook` VALUES (50, 2, 1, '资源管理列表界面', 'user_admin_asset_index_view', 'user', '资源管理列表界面');
INSERT INTO `cmf_hook` VALUES (51, 2, 1, '用户管理第三方用户列表界面', 'user_admin_oauth_index_view', 'user', '用户管理第三方用户列表界面');
INSERT INTO `cmf_hook` VALUES (52, 2, 1, '后台首页界面', 'admin_index_index_view', 'admin', '后台首页界面');
INSERT INTO `cmf_hook` VALUES (53, 2, 1, '后台回收站界面', 'admin_recycle_bin_index_view', 'admin', '后台回收站界面');
INSERT INTO `cmf_hook` VALUES (54, 2, 1, '后台菜单管理界面', 'admin_menu_index_view', 'admin', '后台菜单管理界面');
INSERT INTO `cmf_hook` VALUES (55, 2, 1, '后台自定义登录是否开启钩子', 'admin_custom_login_open', 'admin', '后台自定义登录是否开启钩子');
INSERT INTO `cmf_hook` VALUES (64, 2, 1, '后台幻灯片页面列表界面', 'admin_slide_item_index_view', 'admin', '后台幻灯片页面列表界面');
INSERT INTO `cmf_hook` VALUES (65, 2, 1, '后台幻灯片页面添加界面', 'admin_slide_item_add_view', 'admin', '后台幻灯片页面添加界面');
INSERT INTO `cmf_hook` VALUES (66, 2, 1, '后台幻灯片页面编辑界面', 'admin_slide_item_edit_view', 'admin', '后台幻灯片页面编辑界面');
INSERT INTO `cmf_hook` VALUES (67, 2, 1, '后台管理员添加界面', 'admin_user_add_view', 'admin', '后台管理员添加界面');
INSERT INTO `cmf_hook` VALUES (68, 2, 1, '后台管理员编辑界面', 'admin_user_edit_view', 'admin', '后台管理员编辑界面');
INSERT INTO `cmf_hook` VALUES (69, 2, 1, '后台角色添加界面', 'admin_rbac_role_add_view', 'admin', '后台角色添加界面');
INSERT INTO `cmf_hook` VALUES (70, 2, 1, '后台角色编辑界面', 'admin_rbac_role_edit_view', 'admin', '后台角色编辑界面');
INSERT INTO `cmf_hook` VALUES (71, 2, 1, '后台角色授权界面', 'admin_rbac_authorize_view', 'admin', '后台角色授权界面');

-- ----------------------------
-- Table structure for cmf_hook_plugin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_hook_plugin`;
CREATE TABLE `cmf_hook_plugin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `hook` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '钩子名',
  `plugin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统钩子插件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_hook_plugin
-- ----------------------------
INSERT INTO `cmf_hook_plugin` VALUES (1, 10000, 1, 'fetch_upload_view', 'Qiniu');

-- ----------------------------
-- Table structure for cmf_jackpot
-- ----------------------------
DROP TABLE IF EXISTS `cmf_jackpot`;
CREATE TABLE `cmf_jackpot`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `total` bigint(20) NOT NULL DEFAULT 0 COMMENT '奖池金额',
  `level` tinyint(4) NOT NULL DEFAULT 0 COMMENT '奖池等级',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_jackpot
-- ----------------------------
INSERT INTO `cmf_jackpot` VALUES (1, 56, 0);

-- ----------------------------
-- Table structure for cmf_jackpot_level
-- ----------------------------
DROP TABLE IF EXISTS `cmf_jackpot_level`;
CREATE TABLE `cmf_jackpot_level`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `levelid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `level_up` int(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经验下限',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_jackpot_level
-- ----------------------------
INSERT INTO `cmf_jackpot_level` VALUES (1, 1, 1000, 1457923458);
INSERT INTO `cmf_jackpot_level` VALUES (2, 2, 10000, 1459240543);
INSERT INTO `cmf_jackpot_level` VALUES (3, 3, 15000, 1459240560);

-- ----------------------------
-- Table structure for cmf_jackpot_rate
-- ----------------------------
DROP TABLE IF EXISTS `cmf_jackpot_rate`;
CREATE TABLE `cmf_jackpot_rate`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `giftid` int(11) NOT NULL DEFAULT 0 COMMENT '礼物ID',
  `nums` int(10) UNSIGNED NOT NULL COMMENT '数量',
  `rate_jackpot` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '奖池概率',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_label
-- ----------------------------
DROP TABLE IF EXISTS `cmf_label`;
CREATE TABLE `cmf_label`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标签名称',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  `colour` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '颜色',
  `colour2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '尾色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_label
-- ----------------------------
INSERT INTO `cmf_label` VALUES (1, '小清新', 2, '#4edc90', '#22804e');
INSERT INTO `cmf_label` VALUES (2, '阳光女神369', 1, '#2595b8', '#33caf9');
INSERT INTO `cmf_label` VALUES (3, '性感', 3, '#e5007f', '#f05bad');
INSERT INTO `cmf_label` VALUES (4, '二次元', 4, '#d48d24', '#f9b552');
INSERT INTO `cmf_label` VALUES (5, '好声音', 5, '#b3aa04', '#fff649');
INSERT INTO `cmf_label` VALUES (6, '喊麦达人', 6, '#0a8c88', '#06d9d1');
INSERT INTO `cmf_label` VALUES (7, '才艺', 7, '#648717', '#8fc41e');
INSERT INTO `cmf_label` VALUES (8, '潮范儿', 8, '#990e5a', '#ea2893');
INSERT INTO `cmf_label` VALUES (11, '高颜值', 7, '#1a9995', '#01d8d0');
INSERT INTO `cmf_label` VALUES (12, '游戏大神', 7, '#ed6942', '#e69881');
INSERT INTO `cmf_label` VALUES (13, '帅气男神', 7, '#01d9ce', '#6be3dd');
INSERT INTO `cmf_label` VALUES (15, '小哥哥', 3, '#ff0000', '#ff0000');
INSERT INTO `cmf_label` VALUES (16, '清纯可人', 7, '#318742', '#f08bcd');

-- ----------------------------
-- Table structure for cmf_label_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_label_user`;
CREATE TABLE `cmf_label_user`  (
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '对方ID',
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '选择的标签',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  INDEX `uid_touid_index`(`uid`, `touid`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `touid`(`touid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_label_user
-- ----------------------------
INSERT INTO `cmf_label_user` VALUES (42516, 42520, '15,2,6', 1604469196, 1604469196);
INSERT INTO `cmf_label_user` VALUES (42584, 42598, '2,3,6,', 1605758872, 1605758872);

-- ----------------------------
-- Table structure for cmf_level
-- ----------------------------
DROP TABLE IF EXISTS `cmf_level`;
CREATE TABLE `cmf_level`  (
  `levelid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `levelname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '等级名称',
  `level_up` int(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经验上限',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标识',
  `colour` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称颜色',
  `thumb_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像角标',
  `bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '背景',
  PRIMARY KEY (`id`, `levelid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_level
-- ----------------------------
INSERT INTO `cmf_level` VALUES (1, '一级', 49, 1457923458, 1, 'admin/20201103/b50b89648ad2ee169974cad924168a92.jpg', 'b2d65e', 'admin/20201103/823cd7d97e3123b5186b7d06b99c5c03.png', 'admin/20201103/5d13be1a92838cb85365c32f7ca99ba3.jpg');
INSERT INTO `cmf_level` VALUES (2, '二级', 149, 1459240543, 2, 'level_2.png', 'b2d65e', 'level_mark_2.png', 'level_bg_2.png');
INSERT INTO `cmf_level` VALUES (3, '三级', 299, 1459240560, 3, 'level_3.png', 'b2d65e', 'level_mark_3.png', 'level_bg_3.png');
INSERT INTO `cmf_level` VALUES (4, '四级', 499, 1459240618, 4, 'level_4.png', 'b2d65e', 'level_mark_4.png', 'level_bg_4.png');
INSERT INTO `cmf_level` VALUES (5, '五级', 899, 1459240659, 5, 'level_5.png', 'b2d65e', 'level_mark_5.png', 'level_bg_5.png');
INSERT INTO `cmf_level` VALUES (6, '六级', 20000, 1459240684, 6, 'level_6.png', 'ffb500', 'level_mark_6.png', 'level_bg_6.png');
INSERT INTO `cmf_level` VALUES (7, '七级', 29999, 1463802287, 7, 'level_7.png', 'ffb500', 'level_mark_7.png', 'level_bg_7.png');
INSERT INTO `cmf_level` VALUES (8, '八级', 39999, 1463980529, 8, 'level_8.png', 'ffb500', 'level_mark_8.png', 'level_bg_8.png');
INSERT INTO `cmf_level` VALUES (9, '九级', 49999, 1463980683, 9, 'level_9.png', 'ffb500', 'level_mark_9.png', 'level_bg_9.png');
INSERT INTO `cmf_level` VALUES (10, '十级', 59999, 1465371136, 10, 'level_10.png', 'ffb500', 'level_mark_10.png', 'level_bg_10.png');
INSERT INTO `cmf_level` VALUES (11, '十一级', 69999, 1506127062, 16, 'level_11.png', '00b6f1', 'level_mark_11.png', 'level_bg_11.png');
INSERT INTO `cmf_level` VALUES (12, '十二级', 79999, 1514969928, 18, 'level_12.png', '00b6f1', 'level_mark_12.png', 'level_bg_12.png');
INSERT INTO `cmf_level` VALUES (13, '十三级', 89999, 1514969950, 19, 'level_13.png', '00b6f1', 'level_mark_13.png', 'level_bg_13.png');
INSERT INTO `cmf_level` VALUES (14, '十四级', 99999, 1514969962, 20, 'level_14.png', '00b6f1', 'level_mark_14.png', 'level_bg_14.png');
INSERT INTO `cmf_level` VALUES (15, '十五级', 199999, 1514970130, 21, 'level_15.png', '00b6f1', 'level_mark_15.png', 'level_bg_15.png');
INSERT INTO `cmf_level` VALUES (16, '十六级', 299999, 1514970245, 22, 'level_16.png', 'f36836', 'level_mark_16.png', 'level_bg_16.png');
INSERT INTO `cmf_level` VALUES (17, '十七级', 399999, 1514970263, 23, 'level_17.png', 'f36836', 'level_mark_17.png', 'level_bg_17.png');
INSERT INTO `cmf_level` VALUES (18, '十八级', 499999, 1514970277, 24, 'level_18.png', 'f36836', 'level_mark_18.png', 'level_bg_18.png');
INSERT INTO `cmf_level` VALUES (19, '十九级', 599999, 1514970300, 25, 'level_19.png', 'f36836', 'level_mark_19.png', 'level_bg_19.png');
INSERT INTO `cmf_level` VALUES (20, '神王', 999999999, 1514970325, 26, 'level_20.png', 'f36836', 'level_mark_20.png', 'level_bg_20.png');

-- ----------------------------
-- Table structure for cmf_level_anchor
-- ----------------------------
DROP TABLE IF EXISTS `cmf_level_anchor`;
CREATE TABLE `cmf_level_anchor`  (
  `levelid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `levelname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '等级名称',
  `level_up` int(20) UNSIGNED DEFAULT 0 COMMENT '等级上限',
  `addtime` int(11) DEFAULT 0 COMMENT '添加时间',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标识',
  `thumb_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像角标',
  `bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '背景',
  PRIMARY KEY (`id`, `levelid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_level_anchor
-- ----------------------------
INSERT INTO `cmf_level_anchor` VALUES (1, '一级', 49, 1457923458, 1, 'admin/20201119/c3965b8c4730e03240a56a9ba27042ce.jpg', 'admin/20201119/c3ffe3849ffc6b6c47b9acd3388ffa28.jpg', 'admin/20201119/7f5cbac6facef3a95aac7bf2744d1a6b.jpg');
INSERT INTO `cmf_level_anchor` VALUES (2, '二级', 149, 1459240543, 2, 'admin/20201119/31b3ecc1cd1d3bbadf158a2e1c0b50eb.jpg', 'admin/20201119/9959b89a6e2f7843c6bc6b73bf16f014.jpg', 'admin/20201119/8bb05850b3e5e50790e9def8c43a4a3a.jpg');
INSERT INTO `cmf_level_anchor` VALUES (3, '三级', 299, 1459240560, 3, 'admin/20201119/2a400c24d63228b413a0e4714cd9b1b0.jpg', 'admin/20201119/718f675d0184a1a447b3c61127fc4a43.jpg', 'admin/20201119/2c7397d4d2621486b8a7e00e55dc1cc7.jpg');
INSERT INTO `cmf_level_anchor` VALUES (4, '四级', 499, 1459240618, 4, 'admin/20201119/6f561d77cdb88fd46ac43c315e99f846.jpg', 'admin/20201119/30c89596e1b486366e2e3edf97b37611.jpg', 'admin/20201119/67bd94aa97df3147c72629f8ad293c11.jpg');
INSERT INTO `cmf_level_anchor` VALUES (5, '五级', 899, 1459240659, 5, 'admin/20201119/71a9869781926040ac39915fb71cd3b8.jpg', 'admin/20201119/0af4d7d187881f2a323dede60a42a03c.jpg', 'admin/20201119/55bdc800f98ac334ebed36805f22c3eb.jpg');
INSERT INTO `cmf_level_anchor` VALUES (6, '六级', 20000, 1459240684, 6, 'admin/20201119/b55a9ab828a31bd8d90ff1d28f222ca4.jpg', 'admin/20201119/a84ade0e699c8a553bc678e349e58f95.jpg', 'admin/20201119/280f0d933aa09494435070e1cfa7e2c3.jpg');
INSERT INTO `cmf_level_anchor` VALUES (7, '七级', 29999, 1463802287, 7, 'admin/20201120/801e4a2eee151c46151ca4f6cb8941ad.jpg', 'admin/20201120/3c2274bcc3c0caca50a3556837d0f9de.jpg', 'admin/20201120/9fe7dced25971a044a293b69f4753776.jpg');
INSERT INTO `cmf_level_anchor` VALUES (8, '八级', 39999, 1463980529, 8, 'admin/20201120/eca57622027c982d5d6fb38e3501d1d1.jpg', 'admin/20201120/c7acf3ca39f406dcc18559cb8522d08a.jpg', 'admin/20201120/37163bdd36e34d72c2761079f63cf698.jpg');
INSERT INTO `cmf_level_anchor` VALUES (9, '九级', 1000000, 1463980683, 9, 'admin/20201120/c9f4be303253191b1973260f0609197e.jpg', 'admin/20201120/c1a5446adac387a3682160e8c4efc70f.jpg', 'admin/20201120/e7fe3516593ee871a3e31307af83e422.jpg');
INSERT INTO `cmf_level_anchor` VALUES (10, '十级', 9999999, 1465371136, 10, 'admin/20201119/879ac1943638c2fe30591e6d6e792150.jpg', 'admin/20201120/ff78f74fbefaa08ad46f6618c1621dd5.jpg', 'admin/20201120/2f2c2d9a66ab8f0dfc25e7da0ab17de9.jpg');

-- ----------------------------
-- Table structure for cmf_liang
-- ----------------------------
DROP TABLE IF EXISTS `cmf_liang`;
CREATE TABLE `cmf_liang`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '靓号',
  `length` int(11) NOT NULL DEFAULT 0 COMMENT '长度',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '价格',
  `score` int(11) NOT NULL DEFAULT 0 COMMENT '积分价格',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '购买用户ID',
  `buytime` int(11) NOT NULL DEFAULT 0 COMMENT '购买时间',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '靓号销售状态',
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '启用状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_liang
-- ----------------------------
INSERT INTO `cmf_liang` VALUES (1, '66666', 5, 500, 500, 1604322491, 0, 0, 9999, 0, 0);
INSERT INTO `cmf_liang` VALUES (2, '88888', 5, 800, 800, 1604322511, 42513, 1604322538, 100, 1, 1);
INSERT INTO `cmf_liang` VALUES (3, '33333', 5, 200, 200, 1604381743, 0, 0, 9999, 2, 0);
INSERT INTO `cmf_liang` VALUES (4, '55555', 5, 500, 500, 1604381753, 0, 0, 99, 0, 0);

-- ----------------------------
-- Table structure for cmf_link
-- ----------------------------
DROP TABLE IF EXISTS `cmf_link`;
CREATE TABLE `cmf_link`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:显示;0:不显示',
  `rating` int(11) NOT NULL DEFAULT 0 COMMENT '友情链接评级',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接描述',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接地址',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '友情链接名称',
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接图标',
  `target` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接打开方式',
  `rel` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '链接与网站的关系',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '友情链接表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_live
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live`;
CREATE TABLE `cmf_live`  (
  `uid` int(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `showid` int(12) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `islive` int(1) NOT NULL DEFAULT 0 COMMENT '直播状态',
  `starttime` int(12) NOT NULL DEFAULT 0 COMMENT '开播时间',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '城市',
  `stream` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '流名',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面图',
  `pull` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '播流地址',
  `lng` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '维度',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '直播类型',
  `type_val` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '类型值',
  `isvideo` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否假视频',
  `wy_cid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '网易房间ID(当不使用网易是默认为空)',
  `goodnum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '靓号',
  `anyway` tinyint(1) NOT NULL DEFAULT 1 COMMENT '横竖屏，0表示竖屏，1表示横屏',
  `liveclassid` int(11) NOT NULL DEFAULT 0 COMMENT '直播分类ID',
  `hotvotes` int(11) NOT NULL DEFAULT 0 COMMENT '热门礼物总额',
  `pkuid` int(11) NOT NULL DEFAULT 0 COMMENT 'PK对方ID',
  `pkstream` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'pk对方的流名',
  `ismic` tinyint(1) NOT NULL DEFAULT 0 COMMENT '连麦开关，0是关，1是开',
  `ishot` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否热门',
  `isrecommend` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否推荐',
  `deviceinfo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备信息',
  `isshop` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启店铺',
  `game_action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '游戏类型',
  `banker_coin` bigint(20) DEFAULT 0 COMMENT '庄家余额',
  `isoff` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否断流，0否1是',
  `offtime` bigint(20) NOT NULL DEFAULT 0 COMMENT '断流时间',
  `recommend_time` int(1) NOT NULL DEFAULT 0 COMMENT '推荐时间',
  `livenum` int(10) DEFAULT NULL COMMENT '房间在线人数',
  `sort` mediumint(8) DEFAULT NULL COMMENT '推荐排序',
  PRIMARY KEY (`uid`) USING BTREE,
  INDEX `index_islive_starttime`(`islive`, `starttime`) USING BTREE,
  INDEX `index_uid_stream`(`uid`, `stream`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_live
-- ----------------------------
INSERT INTO `cmf_live` VALUES (1, 1606974337, 1, 1606974337, '', '', '好像在火星', '1_1606974337', '', '', '', '', 0, '', 1, '', '0', 0, 0, 0, 0, '', 0, 1, 0, '', 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `cmf_live` VALUES (42492, 1606972245, 1, 1606972245, '22222222222222', '', '好像在火星', '42492_1606972245', '', 'd\'sa\'adsaa', '', '', 0, '', 1, '', '0', 0, 9, 0, 0, '', 0, 1, 1, '', 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `cmf_live` VALUES (42506, 1606115214, 1, 1606115214, '', '', '好像在火星', '42506_1606115214', '', 'http://yblive.yunbaozb.com//video_37616_IOS_20200423161107.mp4', '', '', 0, '', 1, '', '0', 1, 9, 0, 0, '', 0, 1, 0, '', 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `cmf_live` VALUES (42507, 1606115183, 1, 1606115183, '', '', '好像在火星', '42507_1606115183', '', 'http://yblive.yunbaozb.com/android_37737_20200423_160928_4678730.mp4', '', '', 0, '', 1, '', '0', 0, 9, 0, 0, '', 0, 1, 0, '', 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `cmf_live` VALUES (42523, 1606115200, 1, 1606115200, '111', '', '好像在火星', '42523_1606115200', '', 'http://yblive.yunbaozb.com//video_37616_IOS_20200423161107.mp4', '', '', 0, '', 1, '', '0', 1, 9, 0, 0, '', 0, 1, 0, '', 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `cmf_live` VALUES (42594, 1605534487, 1, 1605534487, '', '', '好像在火星', '42594_1605534487', '', 'https://media.w3.org/2010/05/sintel/trailer.mp4', '', '', 0, '', 1, '', '0', 0, 9, 0, 0, '', 0, 1, 1, '', 0, 0, 0, 0, 0, 1605534530, NULL, NULL);
INSERT INTO `cmf_live` VALUES (42595, 1605534478, 1, 1605534478, '大师傅大师傅士大夫', '', '好像在火星', '42595_1605534478', '', 'https://media.w3.org/2010/05/sintel/trailer.mp4', '', '', 0, '', 1, '', '0', 0, 9, 0, 0, '', 0, 1, 1, '', 0, 0, 0, 0, 0, 1605534540, NULL, NULL);
INSERT INTO `cmf_live` VALUES (42598, 1605534138, 1, 1605534138, '1111111111111111', '', '好像在火星', '42598_1605534138', '', 'https://media.w3.org/2010/05/sintel/trailer.mp4', '', '', 0, '', 1, '', '0', 0, 9, 0, 0, '', 0, 1, 1, '', 0, 0, 0, 0, 0, 1605534379, NULL, NULL);

-- ----------------------------
-- Table structure for cmf_live_ban
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_ban`;
CREATE TABLE `cmf_live_ban`  (
  `liveuid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '主播ID',
  `superid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '超管ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`liveuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_live_class
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_class`;
CREATE TABLE `cmf_live_class`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL DEFAULT 0 COMMENT '父级ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `des` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_live_class
-- ----------------------------
INSERT INTO `cmf_live_class` VALUES (9, 0, '篮球', 'admin/20201107/0f19311c5df4a33a1c9932c2b03fe9c1.jpg', '吃货？主厨？唯美食不可辜负', 1);
INSERT INTO `cmf_live_class` VALUES (15, 0, '足球', 'admin/20201104/7a70aec0000584a78096feb4f90b0408.jpg', '足球直播推荐', 11);

-- ----------------------------
-- Table structure for cmf_live_kick
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_kick`;
CREATE TABLE `cmf_live_kick`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `liveuid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '主播ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `actionid` int(11) NOT NULL DEFAULT 0 COMMENT '操作用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_live_kick
-- ----------------------------
INSERT INTO `cmf_live_kick` VALUES (1, 42541, 42488, 1604576663, 42538);
INSERT INTO `cmf_live_kick` VALUES (2, 42541, 42538, 1604577077, 42538);
INSERT INTO `cmf_live_kick` VALUES (4, 42516, 42492, 1604580271, 42492);
INSERT INTO `cmf_live_kick` VALUES (5, 42518, 42492, 1604580276, 42492);

-- ----------------------------
-- Table structure for cmf_live_manager
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_manager`;
CREATE TABLE `cmf_live_manager`  (
  `uid` int(12) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `liveuid` int(12) NOT NULL DEFAULT 0 COMMENT '主播ID',
  INDEX `uid_touid_index`(`liveuid`, `uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_live_manager
-- ----------------------------
INSERT INTO `cmf_live_manager` VALUES (42566, 42515);
INSERT INTO `cmf_live_manager` VALUES (42504, 42521);
INSERT INTO `cmf_live_manager` VALUES (42516, 42545);
INSERT INTO `cmf_live_manager` VALUES (42521, 42545);
INSERT INTO `cmf_live_manager` VALUES (42567, 42545);
INSERT INTO `cmf_live_manager` VALUES (42570, 42567);

-- ----------------------------
-- Table structure for cmf_live_record
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_record`;
CREATE TABLE `cmf_live_record`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `showid` int(11) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '关播时人数',
  `starttime` int(11) NOT NULL DEFAULT 0 COMMENT '开始时间',
  `endtime` int(11) NOT NULL DEFAULT 0 COMMENT '结束时间',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '城市',
  `stream` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '流名',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面图',
  `lng` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '纬度',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '直播类型',
  `type_val` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '类型值',
  `votes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '本次直播收益',
  `time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '格式日期',
  `liveclassid` int(11) NOT NULL DEFAULT 0 COMMENT '直播分类ID',
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '回放地址',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_uid_start`(`uid`, `starttime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 562 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_live_record
-- ----------------------------
INSERT INTO `cmf_live_record` VALUES (1, 42488, 1604059771, 0, 1604059801, 1604059829, 'irfan本色出演', '', '好像在火星', '42488_1604059771', '', '', '', 0, '', '0', '2020-10-30', 0, '');
INSERT INTO `cmf_live_record` VALUES (2, 42488, 1604059844, 0, 1604059856, 1604060769, 'irfan', '', '好像在火星', '42488_1604059844', '', '', '', 0, '', '0', '2020-10-30', 0, '');
INSERT INTO `cmf_live_record` VALUES (3, 42488, 1604060784, 0, 1604060823, 1604061348, 'irfan', '', '好像在火星', '42488_1604060784', '', '', '', 0, '', '0', '2020-10-30', 0, '');
INSERT INTO `cmf_live_record` VALUES (4, 42488, 1604125788, 0, 1604125810, 1604129161, 'irfan', '', '好像在火星', '42488_1604125788', '', '', '', 0, '', '0', '2020-10-31', 0, '');
INSERT INTO `cmf_live_record` VALUES (5, 42489, 1604167038, 0, 1604167059, 1604167078, '旅游', '', '好像在火星', '42489_1604167038', '', '', '', 0, '', '0', '2020-11-01', 13, '');
INSERT INTO `cmf_live_record` VALUES (6, 42490, 1604292485, 0, 1604292489, 1604292497, '', '', '好像在火星', '42490_1604292485', '', '', '', 0, '', '0', '2020-11-02', 13, '');
INSERT INTO `cmf_live_record` VALUES (7, 42490, 1604302203, 0, 1604302225, 1604304202, '', '', '好像在火星', '42490_1604302203', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (8, 42509, 1604305232, 0, 1604305235, 1604305241, '', '', '好像在火星', '42509_1604305232', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (9, 42511, 1604305837, 0, 1604305843, 1604306400, '', '', '好像在火星', '42511_1604305837', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (10, 42511, 1604306745, 0, 1604306748, 1604306757, '', '', '好像在火星', '42511_1604306745', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (11, 42511, 1604310510, 0, 1604310517, 1604310526, 'ceshi1111', '', '好像在火星', '42511_1604310510', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (12, 42511, 1604310550, 0, 1604310557, 1604310922, 'ceshisss', '', '好像在火星', '42511_1604310550', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (13, 42511, 1604311148, 0, 1604311154, 1604311176, '', '', '好像在火星', '42511_1604311148', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (14, 42511, 1604311176, 0, 1604311184, 1604311773, '', '', '好像在火星', '42511_1604311176', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (15, 42511, 1604313122, 0, 1604313130, 1604313136, '', '', '好像在火星', '42511_1604313122', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (16, 42511, 1604313145, 0, 1604313149, 1604313644, '', '', '好像在火星', '42511_1604313145', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (17, 42511, 1604313644, 0, 1604313651, 1604313706, '', '', '好像在火星', '42511_1604313644', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (18, 42511, 1604313706, 0, 1604313709, 1604313718, '', '', '好像在火星', '42511_1604313706', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (19, 42511, 1604313728, 0, 1604313732, 1604314155, '', '', '好像在火星', '42511_1604313728', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (20, 42511, 1604314155, 0, 1604314162, 1604314282, 'sdfafaf', '', '好像在火星', '42511_1604314155', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (21, 42511, 1604314306, 0, 1604314314, 1604314365, '', '', '好像在火星', '42511_1604314306', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (22, 42511, 1604314365, 0, 1604314376, 1604314547, '', '', '好像在火星', '42511_1604314365', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (23, 42511, 1604314547, 0, 1604314617, 1604314907, '', '', '好像在火星', '42511_1604314547', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (24, 42511, 1604314907, 0, 1604314911, 1604315433, '', '', '好像在火星', '42511_1604314907', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (25, 42511, 1604315433, 0, 1604315443, 1604315901, 'sdsasadasdasdas', '', '好像在火星', '42511_1604315433', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (26, 42511, 1604315901, 0, 1604316014, 1604316441, 'j', '', '好像在火星', '42511_1604315901', '', '', '', 1, '123', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (27, 42511, 1604316441, 0, 1604316450, 1604316707, '12321312', '', '好像在火星', '42511_1604316441', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (28, 42511, 1604316707, 0, 1604316710, 1604316919, '', '', '好像在火星', '42511_1604316707', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (29, 42511, 1604316919, 0, 1604316922, 1604318155, '', '', '好像在火星', '42511_1604316919', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (30, 42511, 1604318155, 0, 1604318158, 1604318252, '', '', '好像在火星', '42511_1604318155', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (31, 42511, 1604318252, 0, 1604318256, 1604318279, '', '', '好像在火星', '42511_1604318252', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (32, 42514, 1604375466, 0, 1604375466, 1604375502, '', '', '好像在火星', '42514_1604375466', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (33, 42511, 1604318279, 0, 1604318319, 1604376934, '', '', '好像在火星', '42511_1604318279', '', '', '', 0, '', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (34, 42511, 1604376989, 0, 1604377100, 1604377453, '', '', '好像在火星', '42511_1604376989', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (35, 42511, 1604377459, 0, 1604377462, 1604378775, '', '', '好像在火星', '42511_1604377459', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (36, 42511, 1604378775, 0, 1604378777, 1604380292, '', '', '好像在火星', '42511_1604378775', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (37, 42511, 1604380292, 0, 1604380295, 1604383199, '', '', '好像在火星', '42511_1604380292', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (38, 42511, 1604383207, 0, 1604383210, 1604383279, '', '', '好像在火星', '42511_1604383207', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (39, 42511, 1604383300, 0, 1604383304, 1604383313, '', '', '好像在火星', '42511_1604383300', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (40, 42519, 1604382747, 0, 1604382794, 1604383405, '测试开播', '', '好像在火星', '42519_1604382747', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (41, 42514, 1604383859, 1, 1604383859, 1604383968, '', '', '好像在火星', '42514_1604383859', '', '0.0', '0.0', 0, '0', '8000.00', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (42, 42515, 1604382660, 1, 1604382715, 1604385078, '20201103rgm002', '', '好像在火星', '42515_1604382660', '', '', '', 3, '20', '20.00', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (43, 42514, 1604385255, 0, 1604385255, 1604385279, '', '', '好像在火星', '42514_1604385255', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 5, '');
INSERT INTO `cmf_live_record` VALUES (44, 42514, 1604385737, 0, 1604385737, 1604385854, '', '', '好像在火星', '42514_1604385737', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (45, 42520, 1604385868, 0, 1604385868, 1604385904, '啊啊啊', '', '好像在火星', '42520_1604385868', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (46, 42492, 1604304144, 0, 1604304144, 1604385924, '', '', '好像在火星', '42492_1604304144', '', '', '', 1, '1111', '0', '2020-11-02', 0, '');
INSERT INTO `cmf_live_record` VALUES (47, 42520, 1604385973, 0, 1604385973, 1604386011, '啊啊啊', '', '好像在火星', '42520_1604385973', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (48, 42520, 1604386058, 0, 1604386058, 1604386136, '啦啦啦', '', '好像在火星', '42520_1604386058', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 14, '');
INSERT INTO `cmf_live_record` VALUES (49, 42511, 1604385834, 0, 1604386180, 1604386198, '', '', '好像在火星', '42511_1604385834', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (50, 42520, 1604386198, 0, 1604386198, 1604386227, '', '', '好像在火星', '42520_1604386198', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (51, 42511, 1604386204, 0, 1604386210, 1604386713, '', '', '好像在火星', '42511_1604386204', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (52, 42511, 1604386719, 0, 1604386759, 1604386777, '', '', '好像在火星', '42511_1604386719', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (53, 42511, 1604386804, 0, 1604386860, 1604387030, '', '', '好像在火星', '42511_1604386804', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (54, 42520, 1604386353, 0, 1604386353, 1604387051, '啦啦啦', '', '好像在火星', '42520_1604386353', '', '0.0', '0.0', 0, '0', '500.00', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (55, 42511, 1604387050, 2, 1604387091, 1604387261, '', '', '好像在火星', '42511_1604387050', '', '', '', 0, '', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (56, 42492, 1604390390, 2, 1604390782, 1604393695, 'regame', '', '好像在火星', '42492_1604390390', '', '', '', 1, '1111', '10', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (57, 42514, 1604392678, 0, 1604392678, 1604393776, '', '', '好像在火星', '42514_1604392678', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 7, '');
INSERT INTO `cmf_live_record` VALUES (58, 42517, 1604394672, 1, 1604394672, 1604394863, '', '', '好像在火星', '42517_1604394672', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (59, 42514, 1604395679, 1, 1604395679, 1604395818, '', '', '好像在火星', '42514_1604395679', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (60, 42514, 1604396023, 1, 1604396023, 1604396565, '', '', '好像在火星', '42514_1604396023', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (61, 42517, 1604398008, 1, 1604398008, 1604399179, '', '', '好像在火星', '42517_1604398008', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 14, '');
INSERT INTO `cmf_live_record` VALUES (62, 42496, 1604400986, 1, 1604400986, 1604401021, '测试小姐姐', '', '好像在火星', '42496_1604400986', '', '', '', 0, '', '0', '2020-11-03', 14, '');
INSERT INTO `cmf_live_record` VALUES (63, 42517, 1604399549, 0, 1604399549, 1604404030, '', '', '好像在火星', '42517_1604399549', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 14, '');
INSERT INTO `cmf_live_record` VALUES (64, 42516, 1604403971, 1, 1604403971, 1604404098, '哈哈哈', '', '好像在火星', '42516_1604403971', '', '0.0', '0.0', 1, '111111', '3000.00', '2020-11-03', 14, '');
INSERT INTO `cmf_live_record` VALUES (65, 42524, 1604406166, 0, 1604406166, 1604406222, '', '', '好像在火星', '42524_1604406166', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 14, '');
INSERT INTO `cmf_live_record` VALUES (66, 42514, 1604406617, 0, 1604406617, 1604406845, '', '', '好像在火星', '42514_1604406617', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 3, '');
INSERT INTO `cmf_live_record` VALUES (67, 42517, 1604406895, 1, 1604406895, 1604407041, '', '', '好像在火星', '42517_1604406895', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 8, '');
INSERT INTO `cmf_live_record` VALUES (68, 42525, 1604407161, 0, 1604407161, 1604407198, '6666', '', '马卡蒂', '42525_1604407161', '', '121.028248', '14.567412', 0, '', '0', '2020-11-03', 5, '');
INSERT INTO `cmf_live_record` VALUES (69, 42525, 1604407850, 0, 1604407850, 1604407877, '', '', '马卡蒂', '42525_1604407850', '', '121.028243', '14.567409', 0, '', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (70, 42514, 1604407203, 0, 1604407203, 1604408421, '', '', '好像在火星', '42514_1604407203', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 4, '');
INSERT INTO `cmf_live_record` VALUES (71, 42514, 1604409422, 0, 1604409422, 1604410366, '', '', '好像在火星', '42514_1604409422', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 5, '');
INSERT INTO `cmf_live_record` VALUES (72, 42514, 1604410386, 0, 1604410386, 1604410406, '', '', '好像在火星', '42514_1604410386', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 5, '');
INSERT INTO `cmf_live_record` VALUES (73, 42514, 1604410418, 1, 1604410418, 1604410434, '', '', '好像在火星', '42514_1604410418', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 5, '');
INSERT INTO `cmf_live_record` VALUES (74, 42514, 1604410458, 0, 1604410458, 1604410468, '', '', '好像在火星', '42514_1604410458', '', '0.0', '0.0', 0, '0', '0', '2020-11-03', 5, '');
INSERT INTO `cmf_live_record` VALUES (75, 42502, 1604392216, 0, 1604392243, 1604414512, '星秀', '', '好像在火星', '42502_1604392216', '', '', '', 1, '123', '0', '2020-11-03', 0, '');
INSERT INTO `cmf_live_record` VALUES (76, 42517, 1604465175, 1, 1604465175, 1604465385, '', '', '好像在火星', '42517_1604465175', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 5, '');
INSERT INTO `cmf_live_record` VALUES (77, 42517, 1604465455, 0, 1604465455, 1604465520, '', '', '好像在火星', '42517_1604465455', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 3, '');
INSERT INTO `cmf_live_record` VALUES (78, 42517, 1604465629, 1, 1604465629, 1604465660, '', '', '好像在火星', '42517_1604465629', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 7, '');
INSERT INTO `cmf_live_record` VALUES (79, 42517, 1604465740, 0, 1604465740, 1604465807, '', '', '好像在火星', '42517_1604465740', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 14, '');
INSERT INTO `cmf_live_record` VALUES (80, 42517, 1604466870, 1, 1604466870, 1604467363, '', '', '好像在火星', '42517_1604466870', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 5, '');
INSERT INTO `cmf_live_record` VALUES (81, 42520, 1604468385, 0, 1604468385, 1604469475, '哈哈哈', '', '好像在火星', '42520_1604468385', '', '0.0', '0.0', 1, '000000', '1820.00', '2020-11-04', 14, '');
INSERT INTO `cmf_live_record` VALUES (82, 42517, 1604467392, 1, 1604467392, 1604470038, '', '', '好像在火星', '42517_1604467392', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 6, '');
INSERT INTO `cmf_live_record` VALUES (83, 42526, 1604471631, 0, 1604471647, 1604471683, '3213', '', '好像在火星', '42526_1604471631', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (84, 42526, 1604471733, 0, 1604471759, 1604471875, '2312', '', '好像在火星', '42526_1604471733', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (85, 42526, 1604471875, 0, 1604471914, 1604472161, '2132', '', '好像在火星', '42526_1604471875', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (86, 42526, 1604472171, 0, 1604472209, 1604472215, '123', '', '好像在火星', '42526_1604472171', '', '', '', 2, '60', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (87, 42526, 1604472231, 0, 1604472250, 1604472286, '2231', '', '好像在火星', '42526_1604472231', '', '', '', 1, '123', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (88, 42517, 1604473658, 0, 1604473658, 1604474722, '', '', '好像在火星', '42517_1604473658', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 9, '');
INSERT INTO `cmf_live_record` VALUES (89, 42492, 1604474352, 0, 1604474372, 1604476462, '132', '', '好像在火星', '42492_1604474352', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (90, 42492, 1604476539, 0, 1604476553, 1604476893, '1111', '', '好像在火星', '42492_1604476539', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (91, 42526, 1604472295, 0, 1604472306, 1604477302, '2313', '', '好像在火星', '42526_1604472295', '', '', '', 3, '60', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (92, 42492, 1604477181, 0, 1604477207, 1604477305, '111', '', '好像在火星', '42492_1604477181', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (93, 42529, 1604478681, 0, 1604478681, 1604478977, '', '', '好像在火星', '42529_1604478681', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 3, '');
INSERT INTO `cmf_live_record` VALUES (94, 42517, 1604478168, 0, 1604478168, 1604479604, '', '', '好像在火星', '42517_1604478168', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 6, '');
INSERT INTO `cmf_live_record` VALUES (95, 42517, 1604480600, 2, 1604480600, 1604481207, '', '', '好像在火星', '42517_1604480600', '', '0.0', '0.0', 0, '0', '5.00', '2020-11-04', 4, '');
INSERT INTO `cmf_live_record` VALUES (96, 42517, 1604482681, 0, 1604482681, 1604484552, '', '', '好像在火星', '42517_1604482681', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 8, '');
INSERT INTO `cmf_live_record` VALUES (97, 42511, 1604485995, 0, 1604486042, 1604486066, '', '', '好像在火星', '42511_1604485995', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (98, 42511, 1604486072, 1, 1604486095, 1604487744, '', '', '好像在火星', '42511_1604486072', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (99, 42515, 1604488251, 0, 1604488267, 1604488658, '', '', '好像在火星', '42515_1604488251', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (100, 42515, 1604488829, 0, 1604488852, 1604489012, '1104', '', '好像在火星', '42515_1604488829', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (101, 42511, 1604489206, 0, 1604489227, 1604489981, '', '', '好像在火星', '42511_1604489206', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (102, 42514, 1604490406, 0, 1604490406, 1604490489, '', '', '好像在火星', '42514_1604490406', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 7, '');
INSERT INTO `cmf_live_record` VALUES (103, 42511, 1604489982, 0, 1604491136, 1604491141, '', '', '好像在火星', '42511_1604489982', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (104, 42521, 1604491161, 0, 1604491161, 1604491299, '', '', '好像在火星', '42521_1604491161', '', '0.0', '0.0', 1, '0000', '0', '2020-11-04', 5, '');
INSERT INTO `cmf_live_record` VALUES (105, 42514, 1604491293, 3, 1604491293, 1604492228, 'test', '', '好像在火星', '42514_1604491293', '', '0.0', '0.0', 0, '0', '8880.00', '2020-11-04', 3, '');
INSERT INTO `cmf_live_record` VALUES (106, 42511, 1604491812, 0, 1604491825, 1604493570, '', '', '好像在火星', '42511_1604491812', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (107, 42514, 1604493947, 1, 1604493953, 1604493962, '1', '', '好像在火星', '42514_1604493947', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (108, 42514, 1604494065, 0, 1604494102, 1604494935, '', '', '好像在火星', '42514_1604494065', '', '', '', 0, '', '0', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (109, 42515, 1604489018, 1, 1604489098, 1604495181, '1104', '', '好像在火星', '42515_1604489018', '', '', '', 0, '', '10.00', '2020-11-04', 0, '');
INSERT INTO `cmf_live_record` VALUES (110, 42538, 1604497143, 0, 1604497143, 1604497155, '111', '', '好像在火星', '42538_1604497143', '', '0.0', '0.0', 0, '0', '0', '2020-11-04', 8, '');
INSERT INTO `cmf_live_record` VALUES (111, 42517, 1604552536, 0, 1604552536, 1604553745, '', '', '好像在火星', '42517_1604552536', '', '0.0', '0.0', 1, '123456', '0', '2020-11-05', 9, '');
INSERT INTO `cmf_live_record` VALUES (112, 42492, 1604551622, 0, 1604551661, 1604555831, '主播发言显示的发言人不正确', '', '好像在火星', '42492_1604551622', '', '', '', 0, '', '30.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (113, 42539, 1604557375, 0, 1604557647, 1604558186, '', '', '好像在火星', '42539_1604557375', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (114, 42539, 1604558339, 0, 1604558398, 1604558407, '', '', '好像在火星', '42539_1604558339', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (115, 42539, 1604558406, 0, 1604558484, 1604558533, '', '', '好像在火星', '42539_1604558406', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (116, 42523, 1604554981, 0, 1604554981, 1604558679, '', '', '好像在火星', '42523_1604554981', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 12, '');
INSERT INTO `cmf_live_record` VALUES (117, 42523, 1604558698, 0, 1604558698, 1604558705, '', '', '好像在火星', '42523_1604558698', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 15, '');
INSERT INTO `cmf_live_record` VALUES (118, 42523, 1604558718, 0, 1604558718, 1604558900, '', '', '好像在火星', '42523_1604558718', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 12, '');
INSERT INTO `cmf_live_record` VALUES (119, 42492, 1604559584, 0, 1604559599, 1604559649, '23123', '', '好像在火星', '42492_1604559584', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (120, 42523, 1604559861, 0, 1604559861, 1604559883, '', '', '好像在火星', '42523_1604559861', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 9, '');
INSERT INTO `cmf_live_record` VALUES (121, 42492, 1604561271, 0, 1604561298, 1604561852, '发言人', '', '好像在火星', '42492_1604561271', '', '', '', 0, '', '30.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (122, 42492, 1604562092, 0, 1604562112, 1604563157, '', '', '好像在火星', '42492_1604562092', '', '', '', 0, '', '50.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (123, 42492, 1604563157, 0, 1604563160, 1604563165, '', '', '好像在火星', '42492_1604563157', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (124, 42492, 1604563191, 0, 1604563216, 1604563485, '', '', '好像在火星', '42492_1604563191', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (125, 42492, 1604566111, 2, 1604566135, 1604567562, '', '', '好像在火星', '42492_1604566111', '', '', '', 0, '', '20.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (126, 42492, 1604567562, 0, 1604567568, 1604567571, '', '', '好像在火星', '42492_1604567562', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (127, 42492, 1604567578, 0, 1604567595, 1604569766, '', '', '好像在火星', '42492_1604567578', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (128, 42496, 1604571228, 0, 1604571228, 1604571264, '小哥哥', '', '好像在火星', '42496_1604571228', '', '', '', 0, '', '0', '2020-11-05', 15, '');
INSERT INTO `cmf_live_record` VALUES (129, 42523, 1604569594, 0, 1604569594, 1604571714, '', '', '好像在火星', '42523_1604569594', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 12, '');
INSERT INTO `cmf_live_record` VALUES (130, 42492, 1604571757, 0, 1604571757, 1604571863, '', '', '好像在火星', '42492_1604571757', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 12, '');
INSERT INTO `cmf_live_record` VALUES (131, 42492, 1604571917, 1, 1604571917, 1604572353, '快来看@@@@@美女', '', '好像在火星', '42492_1604571917', '', '0.0', '0.0', 0, '0', '60.00', '2020-11-05', 12, '');
INSERT INTO `cmf_live_record` VALUES (132, 42523, 1604572165, 0, 1604572165, 1604572649, '', '', '好像在火星', '42523_1604572165', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 15, '');
INSERT INTO `cmf_live_record` VALUES (133, 42542, 1604571105, 0, 1604571120, 1604572859, '232', '', '好像在火星', '42542_1604571105', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (134, 42515, 1604562328, 0, 1604562334, 1604573158, '', '', '好像在火星', '42515_1604562328', '', '', '', 0, '', '20.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (135, 42542, 1604573031, 0, 1604573040, 1604574103, '233', '', '好像在火星', '42542_1604573031', '', '', '', 0, '', '10.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (136, 42492, 1604574022, 0, 1604574065, 1604574115, '', '', '好像在火星', '42492_1604574022', '', '', '', 0, '', '10.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (137, 42492, 1604575051, 1, 1604575051, 1604575117, '', '', '好像在火星', '42492_1604575051', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 11, '');
INSERT INTO `cmf_live_record` VALUES (138, 42492, 1604575183, 0, 1604575183, 1604575238, '', '', '好像在火星', '42492_1604575183', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 12, '');
INSERT INTO `cmf_live_record` VALUES (139, 42538, 1604575174, 0, 1604575253, 1604575261, '111', '', '好像在火星', '42538_1604575174', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (140, 42538, 1604575277, 0, 1604575324, 1604575557, '1111', '', '好像在火星', '42538_1604575277', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (141, 42538, 1604575557, 0, 1604575632, 1604575645, '1111', '', '好像在火星', '42538_1604575557', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (142, 42538, 1604575645, 0, 1604575662, 1604575672, '111', '', '好像在火星', '42538_1604575645', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (143, 42538, 1604575672, 1, 1604575723, 1604575854, '111', '', '好像在火星', '42538_1604575672', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (144, 42538, 1604575854, 1, 1604575860, 1604575872, '1111', '', '好像在火星', '42538_1604575854', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (145, 42538, 1604575872, 3, 1604575896, 1604576632, '111', '', '好像在火星', '42538_1604575872', '', '', '', 0, '', '30.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (146, 42538, 1604576849, 2, 1604576855, 1604576875, '111', '', '好像在火星', '42538_1604576849', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (147, 42488, 1604576475, 0, 1604576492, 1604577022, 'irfan', '', '好像在火星', '42488_1604576475', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (148, 42538, 1604576879, 1, 1604576926, 1604577416, '111', '', '好像在火星', '42538_1604576879', '', '', '', 0, '', '10', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (149, 42520, 1604577786, 0, 1604577786, 1604577828, '啦啦啦', '', '好像在火星', '42520_1604577786', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 9, '');
INSERT INTO `cmf_live_record` VALUES (150, 42513, 1604577892, 0, 1604577902, 1604577927, '111', '', '好像在火星', '42513_1604577892', '', '', '', 0, '', '0', '2020-11-05', 4, '');
INSERT INTO `cmf_live_record` VALUES (151, 42521, 1604578134, 0, 1604578141, 1604578152, '', '', '好像在火星', '42521_1604578134', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (152, 42521, 1604578151, 0, 1604578155, 1604578163, '', '', '好像在火星', '42521_1604578151', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (153, 42521, 1604578162, 0, 1604578167, 1604578275, '', '', '好像在火星', '42521_1604578162', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (154, 42513, 1604578301, 0, 1604578315, 1604578320, '', '', '好像在火星', '42513_1604578301', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (155, 42514, 1604578211, 0, 1604578211, 1604578517, '', '', '好像在火星', '42514_1604578211', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 11, '');
INSERT INTO `cmf_live_record` VALUES (156, 42496, 1604578649, 0, 1604578649, 1604578659, '哈哈哈', '', '好像在火星', '42496_1604578649', '', '', '', 0, '', '0', '2020-11-05', 15, '');
INSERT INTO `cmf_live_record` VALUES (157, 42521, 1604578275, 0, 1604578278, 1604578674, '', '', '好像在火星', '42521_1604578275', '', '', '', 3, '20', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (158, 42492, 1604578412, 2, 1604578445, 1604578712, '', '', '好像在火星', '42492_1604578412', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (159, 42521, 1604578673, 0, 1604578690, 1604579172, '', '', '好像在火星', '42521_1604578673', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (160, 42514, 1604578745, 0, 1604578745, 1604579194, '', '', '好像在火星', '42514_1604578745', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 12, '');
INSERT INTO `cmf_live_record` VALUES (161, 42521, 1604579197, 0, 1604579222, 1604579254, '', '', '好像在火星', '42521_1604579197', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (162, 42520, 1604578675, 0, 1604578675, 1604579350, '111', '', '好像在火星', '42520_1604578675', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 9, '');
INSERT INTO `cmf_live_record` VALUES (163, 42521, 1604579254, 0, 1604579264, 1604579355, '666', '', '好像在火星', '42521_1604579254', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (164, 42531, 1604579495, 0, 1604579495, 1604579511, '', '', '好像在火星', '42531_1604579495', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 11, '');
INSERT INTO `cmf_live_record` VALUES (165, 42539, 1604558606, 0, 1604558638, 1604579672, '', '', '好像在火星', '42539_1604558606', '', '', '', 0, '', '20.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (166, 42543, 1604579692, 0, 1604579730, 1604579822, '', '', '好像在火星', '42543_1604579692', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (167, 42543, 1604579821, 0, 1604579850, 1604580001, '', '', '好像在火星', '42543_1604579821', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (168, 42492, 1604579038, 1, 1604579072, 1604580342, '', '', '好像在火星', '42492_1604579038', '', '', '', 0, '', '10.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (169, 42492, 1604580342, 0, 1604580348, 1604580362, '', '', '好像在火星', '42492_1604580342', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (170, 42531, 1604580341, 0, 1604580341, 1604580419, '', '', '好像在火星', '42531_1604580341', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 11, '');
INSERT INTO `cmf_live_record` VALUES (171, 42531, 1604580820, 0, 1604580820, 1604580847, '', '', '好像在火星', '42531_1604580820', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 11, '');
INSERT INTO `cmf_live_record` VALUES (172, 42531, 1604580876, 1, 1604580876, 1604580889, '', '', '好像在火星', '42531_1604580876', '', '0.0', '0.0', 0, '0', '0', '2020-11-05', 11, '');
INSERT INTO `cmf_live_record` VALUES (173, 42492, 1604580370, 2, 1604580428, 1604581212, '', '', '好像在火星', '42492_1604580370', '', '', '', 0, '', '30', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (174, 42542, 1604574109, 0, 1604574160, 1604583928, 'www', '', '好像在火星', '42542_1604574109', '', '', '', 0, '', '0', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (175, 42492, 1604581224, 1, 1604581410, 1604584019, '', '', '好像在火星', '42492_1604581224', '', '', '', 0, '', '10.00', '2020-11-05', 0, '');
INSERT INTO `cmf_live_record` VALUES (176, 42547, 1604593528, 1, 1604593570, 1604593702, '', '', '好像在火星', '42547_1604593528', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (177, 42519, 1604625476, 0, 1604625476, 1604625491, '', '', '好像在火星', '42519_1604625476', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 15, '');
INSERT INTO `cmf_live_record` VALUES (178, 42492, 1604626023, 0, 1604626023, 1604626329, '流量开播', '', '好像在火星', '42492_1604626023', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 15, '');
INSERT INTO `cmf_live_record` VALUES (179, 42492, 1604626349, 0, 1604626349, 1604626403, '测试流量开播', '', '好像在火星', '42492_1604626349', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 15, '');
INSERT INTO `cmf_live_record` VALUES (180, 42514, 1604634534, 0, 1604634534, 1604635456, '', '', '好像在火星', '42514_1604634534', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (181, 42523, 1604636836, 0, 1604636836, 1604638492, '', '', '好像在火星', '42523_1604636836', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (182, 1, 1604129295, 1, 1604129295, 1604639315, '', '', '好像在火星', '1_1604129295', '', '', '', 0, '', '2020.00', '2020-10-31', 8, '');
INSERT INTO `cmf_live_record` VALUES (183, 42514, 1604635541, 2, 1604635541, 1604640132, '', '', '好像在火星', '42514_1604635541', '', '0.0', '0.0', 0, '0', '1050.00', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (184, 42514, 1604640286, 0, 1604640286, 1604640418, '', '', '好像在火星', '42514_1604640286', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (185, 42531, 1604640413, 0, 1604640413, 1604640425, '', '', '好像在火星', '42531_1604640413', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (186, 42531, 1604640488, 0, 1604640488, 1604640954, '', '', '好像在火星', '42531_1604640488', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (187, 42523, 1604638930, 0, 1604638930, 1604641082, '', '', '好像在火星', '42523_1604638930', '', '0.0', '0.0', 0, '0', '300.00', '2020-11-06', 15, '');
INSERT INTO `cmf_live_record` VALUES (188, 42514, 1604641426, 0, 1604641426, 1604641698, '', '', '好像在火星', '42514_1604641426', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (189, 42531, 1604641417, 0, 1604641417, 1604641928, '', '', '好像在火星', '42531_1604641417', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (190, 42520, 1604641930, 0, 1604641930, 1604641963, '啦啦啦', '', '好像在火星', '42520_1604641930', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 9, '');
INSERT INTO `cmf_live_record` VALUES (191, 42514, 1604641925, 0, 1604641925, 1604642031, '', '', '好像在火星', '42514_1604641925', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (192, 42531, 1604642003, 2, 1604642003, 1604642485, '', '', '好像在火星', '42531_1604642003', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (193, 42516, 1604642912, 0, 1604642912, 1604642977, '发发发', '', '帕拉纳克市', '42516_1604642912', '', '121.005380', '14.468951', 0, '', '0', '2020-11-06', 9, '');
INSERT INTO `cmf_live_record` VALUES (194, 42531, 1604642972, 0, 1604642972, 1604643011, '', '', '好像在火星', '42531_1604642972', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (195, 42514, 1604642961, 0, 1604642961, 1604643464, '', '', '好像在火星', '42514_1604642961', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (196, 42523, 1604641947, 0, 1604641947, 1604643480, '', '', '好像在火星', '42523_1604641947', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 15, '');
INSERT INTO `cmf_live_record` VALUES (197, 42531, 1604643121, 0, 1604643121, 1604643681, '', '', '好像在火星', '42531_1604643121', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (198, 42515, 1604647847, 0, 1604647886, 1604648287, '', '', '好像在火星', '42515_1604647847', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (199, 42548, 1604649026, 0, 1604649026, 1604649039, '', '', '好像在火星', '42548_1604649026', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (200, 42515, 1604648739, 0, 1604648939, 1604649042, '', '', '好像在火星', '42515_1604648739', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (201, 42548, 1604649102, 0, 1604649102, 1604649114, '', '', '好像在火星', '42548_1604649102', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (202, 42514, 1604649153, 1, 1604649153, 1604649222, '', '', '好像在火星', '42514_1604649153', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (203, 42515, 1604649058, 0, 1604649096, 1604649250, 'TTTTTTASSS', '', '好像在火星', '42515_1604649058', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (204, 42514, 1604649299, 0, 1604649299, 1604649360, '', '', '好像在火星', '42514_1604649299', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (205, 42514, 1604649423, 0, 1604649423, 1604649437, '', '', '好像在火星', '42514_1604649423', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (206, 42523, 1604649441, 0, 1604649441, 1604649452, '', '', '好像在火星', '42523_1604649441', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 12, '');
INSERT INTO `cmf_live_record` VALUES (207, 42547, 1604593711, 0, 1604594766, 1604650749, '', '', '好像在火星', '42547_1604593711', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (208, 42523, 1604650958, 0, 1604650958, 1604650969, '', '', '好像在火星', '42523_1604650958', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 9, '');
INSERT INTO `cmf_live_record` VALUES (209, 42523, 1604651001, 0, 1604651001, 1604651011, '', '', '好像在火星', '42523_1604651001', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 13, '');
INSERT INTO `cmf_live_record` VALUES (210, 42523, 1604651107, 0, 1604651107, 1604651118, '', '', '好像在火星', '42523_1604651107', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 15, '');
INSERT INTO `cmf_live_record` VALUES (211, 42542, 1604651159, 0, 1604651178, 1604651210, '2222', '', '好像在火星', '42542_1604651159', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (212, 42523, 1604651221, 0, 1604651221, 1604651233, '', '', '好像在火星', '42523_1604651221', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 13, '');
INSERT INTO `cmf_live_record` VALUES (213, 42521, 1604651701, 0, 1604651701, 1604651713, '', '', '好像在火星', '42521_1604651701', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (214, 42514, 1604651716, 0, 1604651716, 1604651728, '', '', '好像在火星', '42514_1604651716', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (215, 42545, 1604651977, 0, 1604652002, 1604652010, '', '', '好像在火星', '42545_1604651977', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (216, 42521, 1604652093, 0, 1604652093, 1604652105, '', '', '好像在火星', '42521_1604652093', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (217, 42521, 1604652186, 0, 1604652186, 1604652199, '', '', '好像在火星', '42521_1604652186', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (218, 42514, 1604652325, 0, 1604652325, 1604652338, '', '', '好像在火星', '42514_1604652325', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (219, 42545, 1604652010, 0, 1604652346, 1604652501, '', '', '好像在火星', '42545_1604652010', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (220, 42542, 1604652049, 0, 1604652087, 1604652709, '552', '', '好像在火星', '42542_1604652049', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (221, 42542, 1604652865, 0, 1604652886, 1604652927, 'test', '', '好像在火星', '42542_1604652865', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (222, 42545, 1604652918, 1, 1604652937, 1604653179, '', '', '好像在火星', '42545_1604652918', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (223, 42523, 1604657425, 0, 1604657425, 1604657436, '', '', '好像在火星', '42523_1604657425', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (224, 42523, 1604657483, 0, 1604657483, 1604657983, '', '', '好像在火星', '42523_1604657483', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 15, '');
INSERT INTO `cmf_live_record` VALUES (225, 42545, 1604658453, 0, 1604658559, 1604659667, '', '', '好像在火星', '42545_1604658453', '', '', '', 0, '', '10.00', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (226, 42545, 1604660645, 0, 1604660671, 1604660781, '都是', '', '好像在火星', '42545_1604660645', '', '', '', 0, '', '60.00', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (227, 42533, 1604661151, 0, 1604661184, 1604661208, '69696', '', '好像在火星', '42533_1604661151', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (228, 42499, 1604661407, 0, 1604661407, 1604661419, '', '', '好像在火星', '42499_1604661407', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (229, 42544, 1604660690, 1, 1604660779, 1604661515, '', '', '好像在火星', '42544_1604660690', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (230, 42533, 1604661550, 0, 1604661598, 1604661612, '', '', '好像在火星', '42533_1604661550', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (231, 42533, 1604661685, 5, 1604661716, 1604661740, '', '', '好像在火星', '42533_1604661685', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (232, 42545, 1604661675, 2, 1604661697, 1604661950, '', '', '好像在火星', '42545_1604661675', '', '', '', 0, '', '20.00', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (233, 42545, 1604662106, 0, 1604662133, 1604662189, '来吧！！！！！！！！', '', '好像在火星', '42545_1604662106', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (234, 42514, 1604666443, 0, 1604666443, 1604667774, '', '', '好像在火星', '42514_1604666443', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (235, 42533, 1604661975, 1, 1604662010, 1604669257, '7777777777', '', '好像在火星', '42533_1604661975', '', '', '', 0, '', '50.00', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (236, 42504, 1604670863, 0, 1604670863, 1604672056, '', '', '好像在火星', '42504_1604670863', '', '0.0', '0.0', 0, '0', '0', '2020-11-06', 11, '');
INSERT INTO `cmf_live_record` VALUES (237, 42544, 1604676252, 1, 1604676307, 1604676376, '测试', '', '好像在火星', '42544_1604676252', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (238, 42544, 1604676508, 0, 1604676536, 1604677246, '测试', '', '好像在火星', '42544_1604676508', '', '', '', 0, '', '0', '2020-11-06', 0, '');
INSERT INTO `cmf_live_record` VALUES (239, 42515, 1604719483, 0, 1604719536, 1604720924, '', '', '好像在火星', '42515_1604719483', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (240, 42515, 1604720924, 0, 1604720984, 1604721207, '', '', '好像在火星', '42515_1604720924', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (241, 42542, 1604722857, 0, 1604722878, 1604723056, '34242', '', '好像在火星', '42542_1604722857', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (242, 42542, 1604723056, 0, 1604723075, 1604723099, '２３１', '', '好像在火星', '42542_1604723056', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (243, 42545, 1604728287, 1, 1604728313, 1604728463, '调试在线人数', '', '好像在火星', '42545_1604728287', '', '', '', 0, '', '10.00', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (244, 42545, 1604728957, 1, 1604729045, 1604729207, '测试', '', '好像在火星', '42545_1604728957', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (245, 42552, 1604728279, 0, 1604728279, 1604729456, '', '', '好像在火星', '42552_1604728279', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (246, 42516, 1604727180, 0, 1604727180, 1604729671, '猜猜猜', '', '帕拉纳克市', '42516_1604727180', '', '121.005258', '14.468865', 0, '', '0', '2020-11-07', 9, '');
INSERT INTO `cmf_live_record` VALUES (247, 42514, 1604729450, 0, 1604729450, 1604730477, '', '', '好像在火星', '42514_1604729450', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (248, 42533, 1604729238, 2, 1604729280, 1604730998, 'REGAME', '', '好像在火星', '42533_1604729238', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (249, 42545, 1604731766, 2, 1604731785, 1604732313, '', '', '好像在火星', '42545_1604731766', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (250, 42514, 1604730498, 0, 1604730498, 1604732837, '', '', '好像在火星', '42514_1604730498', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (251, 42514, 1604733523, 1, 1604733523, 1604733732, '', '', '好像在火星', '42514_1604733523', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (252, 42545, 1604735826, 1, 1604735849, 1604736002, 'd', '', '好像在火星', '42545_1604735826', '', '', '', 0, '', '10.00', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (253, 42545, 1604736002, 0, 1604736020, 1604736029, '', '', '好像在火星', '42545_1604736002', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (254, 42545, 1604736029, 0, 1604736048, 1604736062, '', '', '好像在火星', '42545_1604736029', '', '', '', 0, '', '0', '2020-11-07', 0, '');
INSERT INTO `cmf_live_record` VALUES (255, 42531, 1604736377, 0, 1604736377, 1604736551, '', '', '好像在火星', '42531_1604736377', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (256, 42552, 1604736587, 1, 1604736587, 1604736734, '', '', '帕拉纳克市', '42552_1604736587', '', '121.005231', '14.468894', 0, '', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (257, 42503, 1604736573, 0, 1604736573, 1604736739, '', '', '好像在火星', '42503_1604736573', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (258, 42552, 1604736894, 0, 1604736894, 1604736910, '', '', '帕拉纳克市', '42552_1604736894', '', '121.005231', '14.468894', 0, '', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (259, 42552, 1604736929, 0, 1604736929, 1604737118, '', '', '帕拉纳克市', '42552_1604736929', '', '121.005337', '14.468944', 0, '', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (260, 42503, 1604736898, 1, 1604736898, 1604737126, '', '', '好像在火星', '42503_1604736898', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (261, 42552, 1604737147, 0, 1604737147, 1604737163, '', '', '帕拉纳克市', '42552_1604737147', '', '121.005337', '14.468944', 0, '', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (262, 42523, 1604730204, 0, 1604730204, 1604737405, '', '', '好像在火星', '42523_1604730204', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 15, '');
INSERT INTO `cmf_live_record` VALUES (263, 42552, 1604737257, 1, 1604737257, 1604737466, '', '', '帕拉纳克市', '42552_1604737257', '', '121.005352', '14.468931', 0, '', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (264, 42552, 1604737478, 1, 1604737478, 1604737615, '', '', '帕拉纳克市', '42552_1604737478', '', '121.005352', '14.468931', 0, '', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (265, 42552, 1604737631, 1, 1604737631, 1604737663, '', '', '帕拉纳克市', '42552_1604737631', '', '121.005352', '14.468931', 0, '', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (266, 42496, 1604737698, 0, 1604737698, 1604737712, 'rrrr', '', '好像在火星', '42496_1604737698', '', '', '', 0, '', '0', '2020-11-07', 15, '');
INSERT INTO `cmf_live_record` VALUES (267, 42553, 1604738444, 0, 1604738444, 1604738808, '', '', '好像在火星', '42553_1604738444', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (268, 42521, 1604740924, 0, 1604740924, 1604742695, '欧洲杯', '', '好像在火星', '42521_1604740924', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (269, 42541, 1604743573, 0, 1604743573, 1604743590, '', '', '好像在火星', '42541_1604743573', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 16, '');
INSERT INTO `cmf_live_record` VALUES (270, 42501, 1604742686, 0, 1604742686, 1604745983, '欧冠决赛直播', '', '好像在火星', '42501_1604742686', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (271, 42523, 1604745947, 0, 1604745947, 1604746019, '', '', '好像在火星', '42523_1604745947', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 9, '');
INSERT INTO `cmf_live_record` VALUES (272, 42521, 1604745947, 0, 1604745947, 1604746849, '', '', '好像在火星', '42521_1604745947', '', '0.0', '0.0', 0, '0', '30.00', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (273, 42496, 1604747021, 0, 1604747021, 1604747113, '测试', '', '好像在火星', '42496_1604747021', '', '', '', 0, '', '0', '2020-11-07', 9, '');
INSERT INTO `cmf_live_record` VALUES (274, 42496, 1604747199, 0, 1604747199, 1604747225, '测试', '', '好像在火星', '42496_1604747199', '', '', '', 0, '', '0', '2020-11-07', 15, '');
INSERT INTO `cmf_live_record` VALUES (275, 42554, 1604747376, 0, 1604747376, 1604747382, '', '', '好像在火星', '42554_1604747376', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (276, 42554, 1604747467, 0, 1604747467, 1604747486, '', '', '好像在火星', '42554_1604747467', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (277, 42554, 1604747502, 1, 1604747502, 1604747516, '', '', '好像在火星', '42554_1604747502', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (278, 42554, 1604747525, 0, 1604747525, 1604747537, '', '', '好像在火星', '42554_1604747525', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (279, 42554, 1604747613, 1, 1604747613, 1604747970, '', '', '好像在火星', '42554_1604747613', '', '0.0', '0.0', 0, '0', '10.00', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (280, 42545, 1604747989, 0, 1604747989, 1604748362, '来吧', '', '好像在火星', '42545_1604747989', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (281, 42521, 1604748433, 1, 1604748433, 1604748576, '', '', '好像在火星', '42521_1604748433', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 12, '');
INSERT INTO `cmf_live_record` VALUES (282, 42548, 1604748758, 2, 1604748758, 1604749727, '', '', '好像在火星', '42548_1604748758', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (283, 42548, 1604749759, 0, 1604749759, 1604749857, '', '', '好像在火星', '42548_1604749759', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (284, 42548, 1604749925, 1, 1604749925, 1604750110, '', '', '好像在火星', '42548_1604749925', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 15, '');
INSERT INTO `cmf_live_record` VALUES (285, 42548, 1604750269, 1, 1604750269, 1604750470, '', '', '好像在火星', '42548_1604750269', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 15, '');
INSERT INTO `cmf_live_record` VALUES (286, 42524, 1604750507, 1, 1604750507, 1604751030, '', '', '好像在火星', '42524_1604750507', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 16, '');
INSERT INTO `cmf_live_record` VALUES (287, 42523, 1604751258, 1, 1604751258, 1604751474, '', '', '好像在火星', '42523_1604751258', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 15, '');
INSERT INTO `cmf_live_record` VALUES (288, 42524, 1604751491, 1, 1604751491, 1604752142, '', '', '好像在火星', '42524_1604751491', '', '0.0', '0.0', 0, '0', '0', '2020-11-07', 11, '');
INSERT INTO `cmf_live_record` VALUES (289, 42524, 1604809855, 1, 1604809855, 1604810035, '', '', '好像在火星', '42524_1604809855', '', '0.0', '0.0', 0, '0', '0', '2020-11-08', 9, '');
INSERT INTO `cmf_live_record` VALUES (290, 42524, 1604811344, 2, 1604811344, 1604812592, '', '', '好像在火星', '42524_1604811344', '', '0.0', '0.0', 0, '0', '0', '2020-11-08', 9, '');
INSERT INTO `cmf_live_record` VALUES (291, 42521, 1604812580, 1, 1604812580, 1604812738, '', '', '好像在火星', '42521_1604812580', '', '0.0', '0.0', 0, '0', '0', '2020-11-08', 11, '');
INSERT INTO `cmf_live_record` VALUES (292, 42558, 1604812869, 1, 1604812869, 1604812969, '', '', '好像在火星', '42558_1604812869', '', '0.0', '0.0', 0, '0', '0', '2020-11-08', 15, '');
INSERT INTO `cmf_live_record` VALUES (293, 42558, 1604813404, 1, 1604813404, 1604814453, '', '', '好像在火星', '42558_1604813404', '', '0.0', '0.0', 0, '0', '0', '2020-11-08', 11, '');
INSERT INTO `cmf_live_record` VALUES (294, 42521, 1604814474, 0, 1604814474, 1604814870, '', '', '好像在火星', '42521_1604814474', '', '0.0', '0.0', 0, '0', '0', '2020-11-08', 11, '');
INSERT INTO `cmf_live_record` VALUES (295, 42501, 1604835755, 1, 1604835755, 1604835864, '', '', '好像在火星', '42501_1604835755', '', '0.0', '0.0', 0, '0', '0', '2020-11-08', 11, '');
INSERT INTO `cmf_live_record` VALUES (296, 42492, 1604837901, 0, 1604837916, 1604838690, '', '', '好像在火星', '42492_1604837901', '', '', '', 0, '', '0', '2020-11-08', 0, '');
INSERT INTO `cmf_live_record` VALUES (297, 42492, 1604838690, 0, 1604838724, 1604839547, '', '', '好像在火星', '42492_1604838690', '', '', '', 0, '', '10.00', '2020-11-08', 0, '');
INSERT INTO `cmf_live_record` VALUES (298, 42496, 1604840858, 0, 1604840858, 1604840877, '好吃个人', '', '好像在火星', '42496_1604840858', '', '', '', 0, '', '0', '2020-11-08', 15, '');
INSERT INTO `cmf_live_record` VALUES (299, 42496, 1604841235, 0, 1604841235, 1604841262, '干旱哈哈', '', '好像在火星', '42496_1604841235', '', '', '', 0, '', '0', '2020-11-08', 9, '');
INSERT INTO `cmf_live_record` VALUES (300, 42496, 1604841910, 0, 1604841910, 1604842057, '测试', '', '好像在火星', '42496_1604841910', '', '', '', 0, '', '10.00', '2020-11-08', 15, '');
INSERT INTO `cmf_live_record` VALUES (301, 42496, 1604842084, 0, 1604842084, 1604842268, ' ncn', '', '好像在火星', '42496_1604842084', '', '', '', 0, '', '20.00', '2020-11-08', 9, '');
INSERT INTO `cmf_live_record` VALUES (302, 42496, 1604842725, 0, 1604842725, 1604842742, '测试', '', '好像在火星', '42496_1604842725', '', '', '', 0, '', '0', '2020-11-08', 15, '');
INSERT INTO `cmf_live_record` VALUES (303, 42492, 1604839561, 0, 1604839585, 1604843045, '', '', '好像在火星', '42492_1604839561', '', '', '', 0, '', '30', '2020-11-08', 0, '');
INSERT INTO `cmf_live_record` VALUES (304, 42503, 1604893004, 0, 1604893004, 1604893238, '', '', '好像在火星', '42503_1604893004', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (305, 42552, 1604893458, 0, 1604893458, 1604894473, '', '', '好像在火星', '42552_1604893458', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (306, 42516, 1604894820, 0, 1604894820, 1604895065, '', '', '帕拉纳克市', '42516_1604894820', '', '121.005393', '14.469012', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (307, 42516, 1604895088, 0, 1604895088, 1604895099, 'Hogg', '', '帕拉纳克市', '42516_1604895088', '', '121.005393', '14.469012', 0, '', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (308, 42503, 1604894822, 0, 1604894822, 1604895191, '', '', '好像在火星', '42503_1604894822', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (309, 42563, 1604895770, 0, 1604895770, 1604895923, '测试', '', '好像在火星', '42563_1604895770', '', '', '', 0, '', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (310, 42563, 1604896074, 1, 1604896074, 1604896185, '干旱', '', '好像在火星', '42563_1604896074', '', '', '', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (311, 42563, 1604896313, 1, 1604896313, 1604896382, '测试', '', '好像在火星', '42563_1604896313', '', '', '', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (312, 42496, 1604897010, 1, 1604897010, 1604897120, '把', '', '好像在火星', '42496_1604897010', '', '', '', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (313, 42503, 1604898368, 0, 1604898368, 1604898375, '', '', '好像在火星', '42503_1604898368', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (314, 42516, 1604898422, 0, 1604898422, 1604898429, '', '', '帕拉纳克市', '42516_1604898422', '', '121.005255', '14.468882', 0, '', '0', '2020-11-09', 12, '');
INSERT INTO `cmf_live_record` VALUES (315, 42563, 1604898464, 2, 1604898464, 1604898681, '测试', '', '好像在火星', '42563_1604898464', '', '', '', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (316, 42565, 1604899057, 0, 1604899057, 1604899105, '666666', '', '好像在火星', '42565_1604899057', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 12, '');
INSERT INTO `cmf_live_record` VALUES (317, 42513, 1604900473, 0, 1604900473, 1604900501, 'Fog', '', '帕拉纳克市', '42513_1604900473', '', '121.005340', '14.468951', 0, '', '0', '2020-11-09', 12, '');
INSERT INTO `cmf_live_record` VALUES (318, 42563, 1604901012, 0, 1604901012, 1604901018, '有意义', '', '好像在火星', '42563_1604901012', '', '', '', 0, '', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (319, 42513, 1604901151, 0, 1604901151, 1604901161, 'Vgff ', '', '帕拉纳克市', '42513_1604901151', '', '121.005128', '14.468827', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (320, 42563, 1604901308, 0, 1604901308, 1604901467, '法国哥哥', '', '好像在火星', '42563_1604901308', '', '', '', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (321, 42563, 1604901508, 0, 1604901508, 1604901653, '付出才好', '', '好像在火星', '42563_1604901508', '', '', '', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (322, 42513, 1604901671, 1, 1604901671, 1604901687, '', '', '帕拉纳克市', '42513_1604901671', '', '121.005212', '14.468846', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (323, 42513, 1604901705, 1, 1604901705, 1604901720, '', '', '帕拉纳克市', '42513_1604901705', '', '121.005388', '14.469013', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (324, 42563, 1604901734, 0, 1604901734, 1604902163, '哥哥哥哥', '', '好像在火星', '42563_1604901734', '', '', '', 0, '', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (325, 42513, 1604901767, 1, 1604901767, 1604902203, '', '', '帕拉纳克市', '42513_1604901767', '', '121.005393', '14.469013', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (326, 42513, 1604902221, 0, 1604902221, 1604902237, '', '', '帕拉纳克市', '42513_1604902221', '', '121.005393', '14.469013', 0, '', '0', '2020-11-09', 12, '');
INSERT INTO `cmf_live_record` VALUES (327, 42513, 1604903171, 0, 1604903171, 1604903183, ' Cc’d', '', '帕拉纳克市', '42513_1604903171', '', '121.005162', '14.468815', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (328, 42513, 1604903200, 0, 1604903200, 1604903215, 'Cc’d', '', '帕拉纳克市', '42513_1604903200', '', '121.005203', '14.468840', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (329, 42513, 1604903293, 0, 1604903293, 1604903327, '', '', '帕拉纳克市', '42513_1604903293', '', '121.005318', '14.468944', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (330, 42521, 1604901740, 1, 1604901740, 1604903767, '', '', '好像在火星', '42521_1604901740', '', '0.0', '0.0', 0, '0', '20.00', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (331, 42523, 1604903788, 0, 1604903788, 1604903799, '', '', '好像在火星', '42523_1604903788', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 15, '');
INSERT INTO `cmf_live_record` VALUES (332, 42504, 1604903899, 0, 1604903899, 1604903930, '谷歌官方衣服f', '', '好像在火星', '42504_1604903899', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (333, 42565, 1604903876, 0, 1604903876, 1604904097, '54646784564649497994848484894949', '', '好像在火星', '42565_1604903876', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (334, 42516, 1604904153, 0, 1604904153, 1604904185, '啦啦啦', '', '好像在火星', '42516_1604904153', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (335, 42563, 1604904493, 0, 1604904493, 1604904536, '日子', '', '好像在火星', '42563_1604904493', '', '', '', 0, '', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (336, 42565, 1604904554, 0, 1604904554, 1604904655, '', '', '好像在火星', '42565_1604904554', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (337, 42504, 1604904682, 0, 1604904682, 1604904779, '', '', '好像在火星', '42504_1604904682', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (338, 42521, 1604904995, 0, 1604904995, 1604905013, '', '', '好像在火星', '42521_1604904995', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (339, 42545, 1604904227, 0, 1604904245, 1604905162, '音乐直播间', '', '好像在火星', '42545_1604904227', '', '', '', 0, '', '10.00', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (340, 42516, 1604904630, 0, 1604904630, 1604905435, '', '', '好像在火星', '42516_1604904630', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (341, 42516, 1604905545, 0, 1604905545, 1604905597, '', '', '好像在火星', '42516_1604905545', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 12, '');
INSERT INTO `cmf_live_record` VALUES (342, 42516, 1604905607, 0, 1604905607, 1604905654, '', '', '好像在火星', '42516_1604905607', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 15, '');
INSERT INTO `cmf_live_record` VALUES (343, 42518, 1604905743, 1, 1604905743, 1604905816, '', '', '好像在火星', '42518_1604905743', '', '121.005304', '14.468918', 0, '', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (344, 42521, 1604905683, 1, 1604905683, 1604905834, '欧洲杯', '', '好像在火星', '42521_1604905683', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (345, 42521, 1604906082, 1, 1604906082, 1604906225, '', '', '好像在火星', '42521_1604906082', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (346, 42521, 1604906286, 1, 1604906286, 1604906365, '', '', '好像在火星', '42521_1604906286', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (347, 42523, 1604905264, 1, 1604905264, 1604907357, '古琴不染', '', '好像在火星', '42523_1604905264', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (348, 42521, 1604906869, 0, 1604906869, 1604907622, '', '', '好像在火星', '42521_1604906869', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (349, 42521, 1604907728, 0, 1604907728, 1604908220, '', '', '好像在火星', '42521_1604907728', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (350, 42492, 1604902268, 1, 1604902311, 1604908257, '音乐会，大家乐！！！', '', '好像在火星', '42492_1604902268', '', '', '', 0, '', '60.00', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (351, 42545, 1604905175, 1, 1604905198, 1604910150, '音乐会', '', '好像在火星', '42545_1604905175', '', '', '', 0, '', '50.00', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (352, 42545, 1604910724, 1, 1604910739, 1604910923, '', '', '好像在火星', '42545_1604910724', '', '', '', 0, '', '10', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (353, 42545, 1604910929, 1, 1604910947, 1604911165, '', '', '好像在火星', '42545_1604910929', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (354, 42545, 1604911201, 0, 1604911201, 1604911284, '', '', '好像在火星', '42545_1604911201', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (355, 42566, 1604912360, 1, 1604912374, 1604913294, '', '', '好像在火星', '42566_1604912360', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (356, 42566, 1604915814, 0, 1604915829, 1604915837, '', '', '好像在火星', '42566_1604915814', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (357, 42515, 1604906787, 2, 1604906845, 1604916122, 'rgm002的直播', '', '好像在火星', '42515_1604906787', '', '', '', 0, '', '30.00', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (358, 42545, 1604916250, 1, 1604916276, 1604916496, '', '', '好像在火星', '42545_1604916250', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (359, 42545, 1604916516, 2, 1604916543, 1604916734, '', '', '好像在火星', '42545_1604916516', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (360, 42545, 1604916961, 1, 1604916981, 1604917310, '', '', '好像在火星', '42545_1604916961', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (361, 42504, 1604920605, 0, 1604920605, 1604921468, '', '', '好像在火星', '42504_1604920605', '', '0.0', '0.0', 1, '2222', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (362, 42521, 1604921395, 0, 1604921395, 1604921527, '', '', '好像在火星', '42521_1604921395', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (363, 42542, 1604922393, 0, 1604922664, 1604922677, '2222', '', '好像在火星', '42542_1604922393', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (364, 42542, 1604922676, 0, 1604922689, 1604922704, '2321', '', '好像在火星', '42542_1604922676', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (365, 42542, 1604922710, 0, 1604922715, 1604922741, '2222', '', '好像在火星', '42542_1604922710', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (366, 42569, 1604922705, 1, 1604922705, 1604922811, '', '', '好像在火星', '42569_1604922705', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (367, 42516, 1604922607, 0, 1604922607, 1604923075, '得到对方广告费分发的', '', '好像在火星', '42516_1604922607', '', '121.005101', '14.468766', 0, '', '0', '2020-11-09', 15, '');
INSERT INTO `cmf_live_record` VALUES (368, 42513, 1604922990, 0, 1604922990, 1604923077, '啦啦啦', '', '好像在火星', '42513_1604922990', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (369, 42513, 1604923105, 0, 1604923105, 1604923114, '', '', '好像在火星', '42513_1604923105', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (370, 42569, 1604923050, 0, 1604923050, 1604923279, '欧洲杯', '', '好像在火星', '42569_1604923050', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (371, 42567, 1604923200, 0, 1604923398, 1604923406, '', '', '好像在火星', '42567_1604923200', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (372, 42567, 1604923413, 0, 1604923437, 1604923503, '123123', '', '好像在火星', '42567_1604923413', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (373, 42567, 1604923674, 0, 1604923713, 1604923769, '123456', '', '好像在火星', '42567_1604923674', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (374, 42542, 1604923926, 0, 1604923938, 1604924011, 'www', '', '好像在火星', '42542_1604923926', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (375, 42499, 1604924468, 0, 1604924468, 1604924588, '', '', '好像在火星', '42499_1604924468', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 12, '');
INSERT INTO `cmf_live_record` VALUES (376, 42499, 1604924613, 0, 1604924613, 1604924637, '', '', '好像在火星', '42499_1604924613', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (377, 42567, 1604923798, 1, 1604923823, 1604924710, '000000', '', '好像在火星', '42567_1604923798', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (378, 42539, 1604924706, 0, 1604924729, 1604924740, '123456', '', '好像在火星', '42539_1604924706', '', '', '', 1, '123456', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (379, 42567, 1604924730, 1, 1604924770, 1604924849, '晚上好', '', '好像在火星', '42567_1604924730', '', '', '', 0, '', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (380, 42539, 1604924854, 0, 1604924883, 1604925769, '123456', '', '好像在火星', '42539_1604924854', '', '', '', 1, '123456', '10.00', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (381, 42521, 1604925729, 0, 1604925729, 1604925836, '', '', '好像在火星', '42521_1604925729', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (382, 42563, 1604925235, 0, 1604925235, 1604925914, 'fcg', '', '好像在火星', '42563_1604925235', '', '', '', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (383, 42543, 1604925876, 0, 1604925898, 1604925933, '123', '', '好像在火星', '42543_1604925876', '', '', '', 1, '123123', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (384, 42543, 1604925933, 0, 1604925945, 1604925980, '123', '', '好像在火星', '42543_1604925933', '', '', '', 1, '123123', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (385, 42567, 1604924878, 1, 1604924912, 1604926084, '你好', '', '好像在火星', '42567_1604924878', '', '', '', 0, '', '40.00', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (386, 42572, 1604925607, 0, 1604925618, 1604926320, '231', '', '好像在火星', '42572_1604925607', '', '', '', 1, '123', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (387, 42566, 1604926247, 0, 1604926315, 1604926376, '11111111', '', '好像在火星', '42566_1604926247', '', '', '', 1, '111111', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (388, 42516, 1604926584, 1, 1604926584, 1604926606, '', '', '好像在火星', '42516_1604926584', '', '0.0', '0.0', 0, '0', '10.00', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (389, 42513, 1604926535, 0, 1604926535, 1604926636, '', '', '好像在火星', '42513_1604926535', '', '121.005354', '14.468981', 0, '', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (390, 42516, 1604926629, 0, 1604926629, 1604926667, '', '', '好像在火星', '42516_1604926629', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (391, 42566, 1604927671, 0, 1604927698, 1604928013, 'AAAA', '', '好像在火星', '42566_1604927671', '', '', '', 1, '1111', '0', '2020-11-09', 0, '');
INSERT INTO `cmf_live_record` VALUES (392, 42518, 1604929775, 0, 1604929775, 1604929790, '', '', '好像在火星', '42518_1604929775', '', '', '', 0, '', '0', '2020-11-09', 9, '');
INSERT INTO `cmf_live_record` VALUES (393, 42521, 1604929850, 0, 1604929850, 1604930303, '', '', '好像在火星', '42521_1604929850', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (394, 42577, 1604932149, 0, 1604932149, 1604932167, '', '', '好像在火星', '42577_1604932149', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 15, '');
INSERT INTO `cmf_live_record` VALUES (395, 42552, 1604932449, 0, 1604932449, 1604932530, '', '', '好像在火星', '42552_1604932449', '', '', '', 0, '', '0', '2020-11-09', 12, '');
INSERT INTO `cmf_live_record` VALUES (396, 42521, 1604932485, 0, 1604932485, 1604932556, '', '', '好像在火星', '42521_1604932485', '', '0.0', '0.0', 0, '0', '0', '2020-11-09', 11, '');
INSERT INTO `cmf_live_record` VALUES (397, 42488, 1604940567, 0, 1604940567, 1604940616, '', '', '好像在火星', '42488_1604940567', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 15, '');
INSERT INTO `cmf_live_record` VALUES (398, 42578, 1604940731, 1, 1604940731, 1604941063, '', '', '好像在火星', '42578_1604940731', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 15, '');
INSERT INTO `cmf_live_record` VALUES (399, 42580, 1604978970, 0, 1604978970, 1604979594, '', '', '好像在火星', '42580_1604978970', '', '', '', 0, '', '0', '2020-11-10', 11, '');
INSERT INTO `cmf_live_record` VALUES (400, 42521, 1604978910, 0, 1604978910, 1604981121, '', '', '好像在火星', '42521_1604978910', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 12, '');
INSERT INTO `cmf_live_record` VALUES (401, 42542, 1604983414, 0, 1604983427, 1604983559, '23121', '', '好像在火星', '42542_1604983414', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (402, 42504, 1604984509, 2, 1604984509, 1604984770, '', '', '好像在火星', '42504_1604984509', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 11, '');
INSERT INTO `cmf_live_record` VALUES (403, 42541, 1604985922, 0, 1604986103, 1604986167, '1111', '', '好像在火星', '42541_1604985922', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (404, 42541, 1604986167, 1, 1604986173, 1604986190, '1111', '', '好像在火星', '42541_1604986167', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (405, 42574, 1604986262, 0, 1604986262, 1604986274, '', '', '好像在火星', '42574_1604986262', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 9, '');
INSERT INTO `cmf_live_record` VALUES (406, 42541, 1604986198, 2, 1604986220, 1604986417, '1111', '', '好像在火星', '42541_1604986198', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (407, 42523, 1604981924, 0, 1604981924, 1604987003, '古琴', '', '好像在火星', '42523_1604981924', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 9, '');
INSERT INTO `cmf_live_record` VALUES (408, 42548, 1604985782, 1, 1604985791, 1604987272, '1111', '', '好像在火星', '42548_1604985782', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (409, 42504, 1604994117, 0, 1604994117, 1604994121, '', '', '好像在火星', '42504_1604994117', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 12, '');
INSERT INTO `cmf_live_record` VALUES (410, 42488, 1604994180, 1, 1604994243, 1604994498, '11111', '', '好像在火星', '42488_1604994180', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (411, 42538, 1604994450, 3, 1604994488, 1604994636, '111', '', '好像在火星', '42538_1604994450', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (412, 42521, 1604996259, 0, 1604996259, 1604996285, '88888', '', '好像在火星', '42521_1604996259', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 11, '');
INSERT INTO `cmf_live_record` VALUES (413, 42524, 1604999291, 0, 1604999291, 1604999539, '', '', '好像在火星', '42524_1604999291', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 9, '');
INSERT INTO `cmf_live_record` VALUES (414, 42545, 1604998404, 0, 1604998634, 1605000433, '听音乐', '', '好像在火星', '42545_1604998404', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (415, 42545, 1605000450, 0, 1605000465, 1605001652, '', '', '好像在火星', '42545_1605000450', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (416, 42545, 1605001659, 0, 1605001683, 1605002610, '延迟3秒', '', '好像在火星', '42545_1605001659', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (417, 42538, 1604999881, 0, 1604999921, 1605002948, '11111111111', '', '好像在火星', '42538_1604999881', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (418, 42529, 1605003029, 3, 1605003029, 1605003190, '北郊', '', '好像在火星', '42529_1605003029', '', '0.0', '0.0', 0, '0', '40.00', '2020-11-10', 11, '');
INSERT INTO `cmf_live_record` VALUES (419, 42545, 1605002617, 0, 1605002632, 1605003460, '', '', '好像在火星', '42545_1605002617', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (420, 42545, 1605003587, 0, 1605003612, 1605005206, '', '', '好像在火星', '42545_1605003587', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (421, 42524, 1605007601, 1, 1605007601, 1605007655, '', '', '好像在火星', '42524_1605007601', '', '0.0', '0.0', 0, '0', '0', '2020-11-10', 15, '');
INSERT INTO `cmf_live_record` VALUES (422, 42545, 1605007390, 0, 1605007413, 1605008001, '延时测试', '', '好像在火星', '42545_1605007390', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (423, 42545, 1605008545, 0, 1605008570, 1605008641, '降延时测试', '', '好像在火星', '42545_1605008545', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (424, 42566, 1605008293, 2, 1605008844, 1605009257, '测试RGM', '', '好像在火星', '42566_1605008293', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (425, 42566, 1605009476, 0, 1605009533, 1605009539, '清晨的美好', '', '好像在火星', '42566_1605009476', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (426, 42566, 1605009539, 1, 1605009557, 1605009560, '清晨的美好', '', '好像在火星', '42566_1605009539', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (427, 42566, 1605009563, 1, 1605009642, 1605010215, '测试111', '', '好像在火星', '42566_1605009563', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (428, 42544, 1605012177, 0, 1605012210, 1605012331, '测试', '', '好像在火星', '42544_1605012177', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (429, 42544, 1605012410, 1, 1605012444, 1605013216, '测试', '', '好像在火星', '42544_1605012410', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (430, 42542, 1605013211, 0, 1605013225, 1605013244, 'www', '', '好像在火星', '42542_1605013211', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (431, 42583, 1605013566, 0, 1605013604, 1605013615, '测试', '', '好像在火星', '42583_1605013566', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (432, 42583, 1605013620, 2, 1605013641, 1605014968, '测试', '', '好像在火星', '42583_1605013620', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (433, 42542, 1605013244, 1, 1605013344, 1605016487, '', '', '好像在火星', '42542_1605013244', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (434, 42583, 1605014968, 2, 1605015001, 1605022592, '', '', '好像在火星', '42583_1605014968', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (435, 42583, 1605022617, 1, 1605022619, 1605022623, '', '', '好像在火星', '42583_1605022617', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (436, 42545, 1605071907, 0, 1605071991, 1605072074, '', '', '好像在火星', '42545_1605071907', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (437, 42545, 1605072074, 1, 1605072091, 1605072769, '', '', '好像在火星', '42545_1605072074', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (438, 42545, 1605072775, 1, 1605072815, 1605073022, '延迟测试', '', '好像在火星', '42545_1605072775', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (439, 42545, 1605073029, 0, 1605073155, 1605073202, '', '', '好像在火星', '42545_1605073029', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (440, 42521, 1605095413, 0, 1605095436, 1605095518, '测试延迟', '', '好像在火星', '42521_1605095413', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (441, 42521, 1605095519, 1, 1605095883, 1605097852, '测试延迟', '', '好像在火星', '42521_1605095519', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (442, 42492, 1605100134, 0, 1605100163, 1605100175, '', '', '好像在火星', '42492_1605100134', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (443, 42521, 1605100235, 2, 1605100434, 1605100544, '延迟测试', '', '好像在火星', '42521_1605100235', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (444, 42521, 1605100539, 0, 1605100552, 1605100622, '延迟测试', '', '好像在火星', '42521_1605100539', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (445, 42521, 1605100636, 0, 1605100709, 1605101339, '延迟测试', '', '好像在火星', '42521_1605100636', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (446, 42521, 1605101346, 0, 1605101394, 1605101410, '延迟测试', '', '好像在火星', '42521_1605101346', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (447, 42521, 1605101407, 0, 1605101417, 1605101426, '延迟测试', '', '好像在火星', '42521_1605101407', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (448, 42492, 1605101052, 1, 1605101195, 1605101446, '国安VS上港', '', '好像在火星', '42492_1605101052', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (449, 42521, 1605101424, 1, 1605101477, 1605101685, '延迟测试', '', '好像在火星', '42521_1605101424', '', '', '', 0, '', '0', '2020-11-11', 0, '');
INSERT INTO `cmf_live_record` VALUES (450, 42583, 1605022625, 2, 1605022652, 1605110917, '24', '', '好像在火星', '42583_1605022625', '', '', '', 0, '', '0', '2020-11-10', 0, '');
INSERT INTO `cmf_live_record` VALUES (451, 42521, 1605173845, 0, 1605173899, 1605174594, '延迟直播测试', '', '好像在火星', '42521_1605173845', '', '', '', 0, '', '0', '2020-11-12', 0, '');
INSERT INTO `cmf_live_record` VALUES (452, 42521, 1605176312, 0, 1605176346, 1605176350, '111111111', '', '好像在火星', '42521_1605176312', '', '', '', 0, '', '0', '2020-11-12', 0, '');
INSERT INTO `cmf_live_record` VALUES (453, 42521, 1605176350, 0, 1605176358, 1605176360, '', '', '好像在火星', '42521_1605176350', '', '', '', 0, '', '0', '2020-11-12', 0, '');
INSERT INTO `cmf_live_record` VALUES (454, 42521, 1605176360, 0, 1605176365, 1605177312, '11111111', '', '好像在火星', '42521_1605176360', '', '', '', 0, '', '0', '2020-11-12', 0, '');
INSERT INTO `cmf_live_record` VALUES (455, 42515, 1605175264, 0, 1605175298, 1605178008, 'AAAA', '', '好像在火星', '42515_1605175264', '', '', '', 0, '', '0', '2020-11-12', 0, '');
INSERT INTO `cmf_live_record` VALUES (456, 42513, 1605178463, 0, 1605178463, 1605178687, 'rrr', '', '好像在火星', '42513_1605178463', '', '0.0', '0.0', 0, '0', '0', '2020-11-12', 12, '');
INSERT INTO `cmf_live_record` VALUES (457, 42524, 1605241341, 0, 1605241341, 1605246800, '美食', '', '好像在火星', '42524_1605241341', '', '0.0', '0.0', 0, '0', '0', '2020-11-13', 9, '');
INSERT INTO `cmf_live_record` VALUES (458, 42524, 1605246869, 0, 1605246869, 1605247408, '', '', '好像在火星', '42524_1605246869', '', '0.0', '0.0', 0, '0', '0', '2020-11-13', 9, '');
INSERT INTO `cmf_live_record` VALUES (459, 42545, 1605247897, 0, 1605247944, 1605248073, '', '', '好像在火星', '42545_1605247897', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (460, 42545, 1605248080, 0, 1605248115, 1605248790, '', '', '好像在火星', '42545_1605248080', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (461, 42545, 1605250579, 0, 1605250600, 1605250853, '', '', '好像在火星', '42545_1605250579', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (462, 42524, 1605247423, 0, 1605247423, 1605251514, '', '', '好像在火星', '42524_1605247423', '', '0.0', '0.0', 0, '0', '0', '2020-11-13', 11, '');
INSERT INTO `cmf_live_record` VALUES (463, 42542, 1605252550, 0, 1605252597, 1605253082, '2232', '', '好像在火星', '42542_1605252550', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (464, 42524, 1605256947, 0, 1605256947, 1605257064, '', '', '好像在火星', '42524_1605256947', '', '0.0', '0.0', 0, '0', '0', '2020-11-13', 9, '');
INSERT INTO `cmf_live_record` VALUES (465, 42524, 1605257246, 0, 1605257246, 1605257950, '', '', '好像在火星', '42524_1605257246', '', '0.0', '0.0', 0, '0', '0', '2020-11-13', 12, '');
INSERT INTO `cmf_live_record` VALUES (466, 42545, 1605270294, 0, 1605270335, 1605270379, '', '', '好像在火星', '42545_1605270294', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (467, 42545, 1605270445, 0, 1605270488, 1605270695, '', '', '好像在火星', '42545_1605270445', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (468, 42545, 1605270704, 0, 1605270755, 1605270835, '222222222222222', '', '好像在火星', '42545_1605270704', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (469, 42545, 1605270860, 0, 1605270957, 1605271094, 'test-sbi=', '', '好像在火星', '42545_1605270860', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (470, 42545, 1605271100, 0, 1605271135, 1605271177, '', '', '好像在火星', '42545_1605271100', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (471, 42545, 1605271893, 0, 1605271943, 1605272037, 'kaishi', '', '好像在火星', '42545_1605271893', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (472, 42542, 1605272470, 0, 1605272507, 1605273218, 'sss', '', '好像在火星', '42542_1605272470', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (473, 42545, 1605273363, 0, 1605273385, 1605273426, '', '', '好像在火星', '42545_1605273363', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (474, 42545, 1605273539, 0, 1605273556, 1605273591, '', '', '好像在火星', '42545_1605273539', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (475, 42545, 1605273882, 0, 1605273899, 1605273925, '', '', '好像在火星', '42545_1605273882', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (476, 42521, 1605273720, 0, 1605273815, 1605274056, '1111111111', '', '好像在火星', '42521_1605273720', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (477, 42542, 1605275819, 0, 1605275880, 1605275909, '2313', '', '好像在火星', '42542_1605275819', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (478, 42545, 1605326826, 0, 1605326845, 1605326870, '', '', '好像在火星', '42545_1605326826', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (479, 42524, 1605329048, 0, 1605329048, 1605332769, '', '', '好像在火星', '42524_1605329048', '', '0.0', '0.0', 0, '0', '0', '2020-11-14', 15, '');
INSERT INTO `cmf_live_record` VALUES (480, 42545, 1605333877, 0, 1605333903, 1605333954, '', '', '好像在火星', '42545_1605333877', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (481, 42542, 1605333493, 0, 1605333553, 1605334141, '222', '', '好像在火星', '42542_1605333493', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (482, 42545, 1605334322, 0, 1605334358, 1605334402, 'adas', '', '好像在火星', '42545_1605334322', '', '', '', 3, '20', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (483, 42544, 1605337738, 0, 1605337769, 1605337937, '测试', '', '好像在火星', '42544_1605337738', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (484, 42544, 1605337946, 0, 1605337974, 1605338035, '测试', '', '好像在火星', '42544_1605337946', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (485, 42544, 1605338047, 0, 1605338073, 1605338153, '测试', '', '好像在火星', '42544_1605338047', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (486, 42544, 1605338165, 0, 1605338193, 1605338278, '测试', '', '好像在火星', '42544_1605338165', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (487, 42544, 1605338292, 0, 1605338325, 1605338567, '测试', '', '好像在火星', '42544_1605338292', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (488, 42544, 1605338809, 0, 1605338834, 1605338856, '测试', '', '好像在火星', '42544_1605338809', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (489, 42542, 1605344288, 0, 1605344311, 1605344328, 'www', '', '好像在火星', '42542_1605344288', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (490, 42542, 1605344335, 0, 1605344384, 1605347749, 'sdas', '', '好像在火星', '42542_1605344335', '', '', '', 0, '', '0', '2020-11-14', 0, '');
INSERT INTO `cmf_live_record` VALUES (491, 42545, 1605501802, 0, 1605501802, 1605501960, '', '', '好像在火星', '42545_1605501802', '', '0.0', '0.0', 0, '0', '0', '2020-11-16', 12, '');
INSERT INTO `cmf_live_record` VALUES (492, 42496, 1605502347, 0, 1605502347, 1605502616, '测试一下', '', '好像在火星', '42496_1605502347', '', '', '', 0, '', '0', '2020-11-16', 9, '');
INSERT INTO `cmf_live_record` VALUES (493, 42524, 1605501800, 0, 1605501800, 1605502621, '', '', '好像在火星', '42524_1605501800', '', '0.0', '0.0', 0, '0', '0', '2020-11-16', 15, '');
INSERT INTO `cmf_live_record` VALUES (494, 42521, 1605511505, 1, 1605511505, 1605511862, '', '', '好像在火星', '42521_1605511505', '', '0.0', '0.0', 1, '1', '0', '2020-11-16', 11, '');
INSERT INTO `cmf_live_record` VALUES (495, 42496, 1605511465, 0, 1605511465, 1605512698, '测试', '', '好像在火星', '42496_1605511465', '', '', '', 0, '', '0', '2020-11-16', 9, '');
INSERT INTO `cmf_live_record` VALUES (496, 42515, 1605274412, 0, 1605274440, 1605524971, 'aaaaaa', '', '好像在火星', '42515_1605274412', '', '', '', 0, '', '0', '2020-11-13', 0, '');
INSERT INTO `cmf_live_record` VALUES (497, 42545, 1605525050, 1, 1605525050, 1605525681, '', '', '好像在火星', '42545_1605525050', '', '0.0', '0.0', 0, '0', '0', '2020-11-16', 12, '');
INSERT INTO `cmf_live_record` VALUES (498, 42521, 1605526856, 0, 1605526856, 1605526986, '', '', '好像在火星', '42521_1605526856', '', '0.0', '0.0', 0, '0', '0', '2020-11-16', 12, '');
INSERT INTO `cmf_live_record` VALUES (499, 42521, 1605527031, 1, 1605527031, 1605527545, '', '', '好像在火星', '42521_1605527031', '', '0.0', '0.0', 0, '0', '0', '2020-11-16', 15, '');
INSERT INTO `cmf_live_record` VALUES (500, 42521, 1605528000, 1, 1605528000, 1605529020, '', '', '好像在火星', '42521_1605528000', '', '0.0', '0.0', 0, '0', '0', '2020-11-16', 11, '');
INSERT INTO `cmf_live_record` VALUES (501, 42521, 1605531442, 2, 1605531490, 1605531561, '11111111', '', '好像在火星', '42521_1605531442', '', '', '', 0, '', '0', '2020-11-16', 0, '');
INSERT INTO `cmf_live_record` VALUES (502, 42521, 1605590117, 0, 1605590117, 1605590288, '', '', '好像在火星', '42521_1605590117', '', '0.0', '0.0', 0, '0', '0', '2020-11-17', 15, '');
INSERT INTO `cmf_live_record` VALUES (503, 42521, 1605671518, 1, 1605671518, 1605671915, '', '', '好像在火星', '42521_1605671518', '', '0.0', '0.0', 0, '0', '0', '2020-11-18', 9, '');
INSERT INTO `cmf_live_record` VALUES (504, 42499, 1605680459, 0, 1605680480, 1605680493, '111111', '', '好像在火星', '42499_1605680459', '', '', '', 15, '', '0', '2020-11-18', 0, '');
INSERT INTO `cmf_live_record` VALUES (505, 42521, 1605700591, 0, 1605700619, 1605700660, '1111', '', '好像在火星', '42521_1605700591', '', '', '', 9, '', '0', '2020-11-18', 0, '');
INSERT INTO `cmf_live_record` VALUES (506, 42515, 1605700728, 0, 1605700774, 1605700809, 'REGAME', '', '好像在火星', '42515_1605700728', '', '', '', 15, '', '0', '2020-11-18', 0, '');
INSERT INTO `cmf_live_record` VALUES (507, 42516, 1605704050, 5, 1605704050, 1605704072, '厉害', '', '好像在火星', '42516_1605704050', '', '0.0', '0.0', 0, '0', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (508, 42516, 1605704133, 6, 1605704133, 1605704211, '啦啦啦', '', '好像在火星', '42516_1605704133', '', '0.0', '0.0', 1, '111111', '0', '2020-11-18', 9, '');
INSERT INTO `cmf_live_record` VALUES (509, 42516, 1605704230, 6, 1605704230, 1605704282, '啦啦啦', '', '好像在火星', '42516_1605704230', '', '0.0', '0.0', 0, '0', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (510, 42516, 1605704305, 4, 1605704305, 1605704571, '啦啦啦啦', '', '好像在火星', '42516_1605704305', '', '0.0', '0.0', 0, '0', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (511, 42513, 1605704821, 0, 1605704821, 1605704891, '发发发', '', '好像在火星', '42513_1605704821', '', '', '', 1, '111111', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (512, 42513, 1605705003, 0, 1605705003, 1605705075, '好好好', '', '好像在火星', '42513_1605705003', '', '', '', 0, '', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (513, 42516, 1605705158, 0, 1605705158, 1605705249, '娜可露露', '', '好像在火星', '42516_1605705158', '', '0.0', '0.0', 1, '111111', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (514, 42513, 1605705455, 4, 1605705455, 1605705621, '高规格', '', '好像在火星', '42513_1605705455', '', '', '', 1, '111111', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (515, 42516, 1605755189, 0, 1605755189, 1605755310, '卡啊', '', '好像在火星', '42516_1605755189', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (516, 42545, 1605703057, 0, 1605703065, 1605755577, '篮球求', '', '好像在火星', '42545_1605703057', '', '', '', 0, '', '0', '2020-11-18', 9, '');
INSERT INTO `cmf_live_record` VALUES (517, 42545, 1605755563, 0, 1605755612, 1605755737, '篮球111', '', '好像在火星', '42545_1605755563', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (518, 42516, 1605755789, 0, 1605755789, 1605755847, '啦啦啦', '', '好像在火星', '42516_1605755789', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (519, 42521, 1605702886, 0, 1605702917, 1605756887, '1111', '', '好像在火星', '42521_1605702886', '', '', '', 0, '', '0', '2020-11-18', 9, '');
INSERT INTO `cmf_live_record` VALUES (520, 42545, 1605755669, 0, 1605755741, 1605757096, '足球111', '', '好像在火星', '42545_1605755669', '', '', '', 0, '', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (521, 42545, 1605759522, 0, 1605759547, 1605759551, '', '', '好像在火星', '42545_1605759522', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (522, 42545, 1605760322, 0, 1605760377, 1605760742, '', '', '好像在火星', '42545_1605760322', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (523, 42545, 1605762734, 0, 1605762782, 1605763307, '', '', '好像在火星', '42545_1605762734', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (524, 42545, 1605763314, 0, 1605763356, 1605763365, '北京vs大连', '', '好像在火星', '42545_1605763314', '', '', '', 0, '', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (525, 42545, 1605763388, 0, 1605763415, 1605763426, '北京vs大连', '', '好像在火星', '42545_1605763388', '', '', '', 0, '', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (526, 42545, 1605763853, 0, 1605763903, 1605763915, '', '', '好像在火星', '42545_1605763853', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (527, 42545, 1605764678, 0, 1605764699, 1605764751, '', '', '好像在火星', '42545_1605764678', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (528, 42499, 1605765389, 0, 1605765389, 1605765433, '', '', '好像在火星', '42499_1605765389', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (529, 42499, 1605765502, 0, 1605765502, 1605765753, '99999', '', '好像在火星', '42499_1605765502', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (530, 42545, 1605766150, 0, 1605766184, 1605767383, '', '', '好像在火星', '42545_1605766150', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (531, 42545, 1605767410, 0, 1605767439, 1605768666, '北京', '', '好像在火星', '42545_1605767410', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (532, 42545, 1605770705, 0, 1605770744, 1605772971, '北京VS上海', '', '好像在火星', '42545_1605770705', '', '', '', 0, '', '60', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (533, 42545, 1605773758, 1, 1605773791, 1605774045, '', '', '好像在火星', '42545_1605773758', '', '', '', 0, '', '10', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (534, 42542, 1605777142, 0, 1605777227, 1605777368, '德克士VS猛龙', '', '好像在火星', '42542_1605777142', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (535, 42499, 1605781319, 0, 1605781319, 1605781350, '', '', '好像在火星', '42499_1605781319', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (536, 42513, 1605784172, 0, 1605784172, 1605784331, '啦啦啦', '', '好像在火星', '42513_1605784172', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (537, 42499, 1605784243, 0, 1605784243, 1605784637, '6666', '', '好像在火星', '42499_1605784243', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (538, 42499, 1605785123, 0, 1605785123, 1605785280, '88888888', '', '好像在火星', '42499_1605785123', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (539, 42513, 1605785735, 0, 1605785735, 1605785770, '考虑考虑', '', '好像在火星', '42513_1605785735', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (540, 42507, 1605082847, 0, 1605082847, 1605786144, '', '', '好像在火星', '42507_1605082847', '', '', '', 0, '', '10.00', '2020-11-11', 15, '');
INSERT INTO `cmf_live_record` VALUES (541, 42499, 1605786269, 10, 1605786269, 1605786374, '999999999999', '', '好像在火星', '42499_1605786269', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (542, 42499, 1605786402, 10, 1605786402, 1605786641, '555555555', '', '好像在火星', '42499_1605786402', '', '0.0', '0.0', 1, '9999', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (543, 42499, 1605786819, 0, 1605786819, 1605786825, '55555555', '', '好像在火星', '42499_1605786819', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (544, 42499, 1605786847, 0, 1605786847, 1605787268, '66666666', '', '好像在火星', '42499_1605786847', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (545, 42499, 1605788059, 10, 1605788059, 1605788112, '', '', '好像在火星', '42499_1605788059', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (546, 42499, 1605788996, 11, 1605788996, 1605789036, '999999', '', '好像在火星', '42499_1605788996', '', '0.0', '0.0', 1, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (547, 42542, 1605789228, 0, 1605789308, 1605789491, '巴塞卢娜VS奥克斯', '', '好像在火星', '42542_1605789228', '', '', '', 0, '', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (548, 42499, 1605789728, 0, 1605789728, 1605789738, '99999999', '', '好像在火星', '42499_1605789728', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (549, 42499, 1605789821, 11, 1605789821, 1605789911, '', '', '好像在火星', '42499_1605789821', '', '0.0', '0.0', 0, '0', '0', '2020-11-19', 15, '');
INSERT INTO `cmf_live_record` VALUES (550, 42542, 1605789901, 0, 1605789956, 1605790000, '利阿斯VS德克士', '', '好像在火星', '42542_1605789901', '', '', '', 0, '', '0', '2020-11-19', 9, '');
INSERT INTO `cmf_live_record` VALUES (551, 42521, 1605841390, 0, 1605841390, 1605841686, '9999999', '', '好像在火星', '42521_1605841390', '', '0.0', '0.0', 0, '0', '0', '2020-11-20', 15, '');
INSERT INTO `cmf_live_record` VALUES (552, 42499, 1605841719, 25, 1605841719, 1605843937, '999999', '', '好像在火星', '42499_1605841719', '', '0.0', '0.0', 1, '0', '20.00', '2020-11-20', 15, '');
INSERT INTO `cmf_live_record` VALUES (553, 42545, 1605844980, 20, 1605845023, 1605846110, '北京VS上海', '', '好像在火星', '42545_1605844980', '', '', '', 0, '', '10', '2020-11-20', 15, '');
INSERT INTO `cmf_live_record` VALUES (554, 42499, 1605848868, 10, 1605848868, 1605849707, '99999999', '', '好像在火星', '42499_1605848868', '', '0.0', '0.0', 0, '0', '0', '2020-11-20', 15, '');
INSERT INTO `cmf_live_record` VALUES (555, 42499, 1605850880, 11, 1605850880, 1605850911, '66666666', '', '好像在火星', '42499_1605850880', '', '0.0', '0.0', 1, '0', '0', '2020-11-20', 15, '');
INSERT INTO `cmf_live_record` VALUES (556, 42516, 1605851126, 0, 1605851126, 1605851214, '啦啦啦', '', '好像在火星', '42516_1605851126', '', '0.0', '0.0', 0, '0', '0', '2020-11-20', 15, '');
INSERT INTO `cmf_live_record` VALUES (557, 42566, 1605850173, 0, 1605850222, 1605937558, '来呀快活呀', '', '好像在火星', '42566_1605850173', '', '', '', 0, '', '0', '2020-11-20', 15, '');
INSERT INTO `cmf_live_record` VALUES (558, 42515, 1605700786, 0, 1605700837, 1605937561, 'REGAME', '', '好像在火星', '42515_1605700786', '', '', '', 0, '', '0', '2020-11-18', 9, '');
INSERT INTO `cmf_live_record` VALUES (559, 42501, 1605699254, 0, 1605699270, 1605937563, '1111', '', '好像在火星', '42501_1605699254', '', '', '', 0, '', '0', '2020-11-18', 15, '');
INSERT INTO `cmf_live_record` VALUES (560, 42545, 1606709384, 0, 1606709405, 1606710701, '', '', '好像在火星', '42545_1606709384', '', '', '', 0, '', '0', '2020-11-30', 9, '');
INSERT INTO `cmf_live_record` VALUES (561, 42545, 1606913025, 0, 1606913027, 1606971846, '', '', '好像在火星', '42545_1606913025', '', '', '', 0, '', '0', '2020-12-02', 9, '');

-- ----------------------------
-- Table structure for cmf_live_shut
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_shut`;
CREATE TABLE `cmf_live_shut`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `liveuid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '主播ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `showid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '禁言类型，0永久',
  `actionid` int(11) NOT NULL DEFAULT 0 COMMENT '操作者ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_loginbonus
-- ----------------------------
DROP TABLE IF EXISTS `cmf_loginbonus`;
CREATE TABLE `cmf_loginbonus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `day` int(10) NOT NULL DEFAULT 0 COMMENT '登录天数',
  `coin` int(10) NOT NULL DEFAULT 0 COMMENT '登录奖励',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(10) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_loginbonus
-- ----------------------------
INSERT INTO `cmf_loginbonus` VALUES (1, 1, 10, 0, 1604133749);
INSERT INTO `cmf_loginbonus` VALUES (2, 2, 20, 0, 1604320602);
INSERT INTO `cmf_loginbonus` VALUES (3, 3, 30, 0, 0);
INSERT INTO `cmf_loginbonus` VALUES (4, 4, 40, 0, 0);
INSERT INTO `cmf_loginbonus` VALUES (5, 5, 50, 0, 0);
INSERT INTO `cmf_loginbonus` VALUES (6, 6, 60, 0, 0);
INSERT INTO `cmf_loginbonus` VALUES (7, 7, 70, 0, 1543644538);

-- ----------------------------
-- Table structure for cmf_music
-- ----------------------------
DROP TABLE IF EXISTS `cmf_music`;
CREATE TABLE `cmf_music`  (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '音乐名称',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '演唱者',
  `uploader` int(255) NOT NULL DEFAULT 0 COMMENT '上传者ID',
  `upload_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '上传类型  1管理员上传 2 用户上传',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面地址',
  `length` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '音乐长度',
  `file_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件地址',
  `use_nums` int(12) NOT NULL DEFAULT 0 COMMENT '被使用次数',
  `isdel` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否被删除 0否 1是',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `updatetime` int(12) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `classify_id` int(12) NOT NULL DEFAULT 0 COMMENT '音乐分类ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_music
-- ----------------------------
INSERT INTO `cmf_music` VALUES (1, 'Panama', 'Matteo', 1, 1, 'music_thumb_1.jpg', '00:31', 'music_1.mp3', 116, 0, 1528535261, 1604319971, 17);
INSERT INTO `cmf_music` VALUES (3, 'California (On My Mind)', 'Stewart Mac', 1, 1, 'music_thumb_3.jpg', '00:29', 'music_3.mp3', 257, 0, 1529747678, 1604309410, 17);
INSERT INTO `cmf_music` VALUES (4, '最美的期待', '周笔畅', 1, 1, 'music_thumb_4.png', '00:22', 'music_4.mp3', 137, 0, 1530090995, 1590217881, 19);
INSERT INTO `cmf_music` VALUES (5, 'Friendshipss', 'Pascal Letoublon', 1, 1, 'music_thumb_5.jpg', '00:40', 'music_5.mp3', 501, 0, 1530095432, 1604319961, 18);
INSERT INTO `cmf_music` VALUES (9, '夜空的寂静', '赵海洋', 1, 1, 'music_thumb_9.jpg', '00:39', 'music_9.mp3', 124, 0, 1531383448, 1590217645, 16);
INSERT INTO `cmf_music` VALUES (10, '清晨的美好', '张宇桦', 1, 1, 'music_thumb_10.jpg', '00:37', 'music_10.mp3', 235, 0, 1531384116, 1590217636, 18);
INSERT INTO `cmf_music` VALUES (11, 'Because of You', 'Kelly Clarkson', 1, 1, 'music_thumb_11.jpg', '03:40', 'music_11.mp3', 128, 0, 1535004946, 1601257704, 17);
INSERT INTO `cmf_music` VALUES (15, '1323', '213213', 1, 1, 'admin/20201102/355b051ec045ca3003da769a911ee483.jpg', '', 'default/20201103/d47cbf9225089a63acfd550a53c93e4f.mp3', 1, 0, 1604321803, 1604407363, 17);

-- ----------------------------
-- Table structure for cmf_music_classify
-- ----------------------------
DROP TABLE IF EXISTS `cmf_music_classify`;
CREATE TABLE `cmf_music_classify`  (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `list_order` int(12) NOT NULL DEFAULT 9999 COMMENT '排序号',
  `isdel` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `updatetime` int(12) NOT NULL DEFAULT 0 COMMENT '修改时间',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类图标地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_music_classify
-- ----------------------------
INSERT INTO `cmf_music_classify` VALUES (16, '热歌', 2, 0, 1544862438, 0, 'music_class_16.png');
INSERT INTO `cmf_music_classify` VALUES (17, '新歌', 0, 0, 1544862465, 0, 'music_class_17.png');
INSERT INTO `cmf_music_classify` VALUES (18, '经典', 0, 0, 1544862498, 1604407055, 'admin/20201103/0a8c3fd203b59f4a563d12ff76fb253d.png');
INSERT INTO `cmf_music_classify` VALUES (19, '潮流', 1, 1, 1545291271, 0, 'music_class_19.png');
INSERT INTO `cmf_music_classify` VALUES (23, '测试111', 1, 0, 1604307576, 0, 'admin/20201102/57bbd10c048d7393c7cd6d3453715dd4.png');
INSERT INTO `cmf_music_classify` VALUES (24, '369852', 9999, 0, 1604406974, 0, 'admin/20201103/60dc8941d37b18345827dcfc145e46c1.png');

-- ----------------------------
-- Table structure for cmf_music_collection
-- ----------------------------
DROP TABLE IF EXISTS `cmf_music_collection`;
CREATE TABLE `cmf_music_collection`  (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` int(12) NOT NULL DEFAULT 0 COMMENT '用户id',
  `music_id` int(12) NOT NULL DEFAULT 0 COMMENT '音乐id',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `updatetime` int(12) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '音乐收藏状态 1收藏 0 取消收藏',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_music_collection
-- ----------------------------
INSERT INTO `cmf_music_collection` VALUES (1, 42516, 5, 1604493424, 1604493426, 1);
INSERT INTO `cmf_music_collection` VALUES (2, 42516, 11, 1604493439, 1604493441, 0);

-- ----------------------------
-- Table structure for cmf_option
-- ----------------------------
DROP TABLE IF EXISTS `cmf_option`;
CREATE TABLE `cmf_option`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `autoload` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否自动加载;1:自动加载;0:不自动加载',
  `option_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置名',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `option_name`(`option_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '全站配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_option
-- ----------------------------
INSERT INTO `cmf_option` VALUES (1, 1, 'site_info', '{\"maintain_switch\":\"0\",\"maintain_tips\":\"\\u7ef4\\u62a4\\u901a\\u77e5\\uff1a\\u4e3a\\u4e86\\u66f4\\u597d\\u7684\\u4e3a\\u60a8\\u670d\\u52a1\\uff0c\\u672c\\u7ad9\\u6b63\\u5728\\u5347\\u7ea7\\u7ef4\\u62a4\\u4e2d\\uff0c\\u56e0\\u6b64\\u5e26\\u6765\\u4e0d\\u4fbf\\u6df1\\u8868\\u6b49\\u610f\",\"company_name\":\"\\u5c71\\u4e1c\\u7f51\\u7edc\\u79d1\\u6280\\u6709\\u9650\\u516c\\u53f8\",\"company_desc\":\"\\u4e13\\u6ce8\\u4e8e\\u97f3\\u89c6\\u9891\\u76f4\\u64ad\\u7cfb\\u7edf\\u5f00\\u53d1\\uff0c\\u4ee5\\u97f3\\u89c6\\u9891\\u76f4\\u64ad\\u5e73\\u53f0\\u53d1\\u5c55\\u53ca\\u8f6f\\u4ef6\\u4ea7\\u54c1\\u3001\\u89e3\\u51b3\\u65b9\\u6848\\u7684\\u7814\\u53d1\\u548c\\u670d\\u52a1\\u4e3a\\u4e3b\\uff0c\\u4e3a\\u5ba2\\u6237\\u6253\\u9020\\u591a\\u5143\\u5316\\u7684\\u76f4\\u64ad\\u7cfb\\u7edf\\u3002\\u516c\\u53f8\\u79c9\\u627f\\\"\\u4e13\\u6ce8\\u4e13\\u4e1a\\uff0c\\u6c38\\u4e89\\u7b2c\\u4e00\\uff0c\\u7528\\u5fc3\\u670d\\u52a1\\\"\\u7684\\u4f01\\u4e1a\\u7406\\u5ff5\\uff0c\\u81ea\\u4e3b\\u7814\\u53d1\\u4e86\\u4eab\\u8a89\\u884c\\u4e1a\\u7684\\\"\\u77ed\\u89c6\\u9891\\u7cfb\\u7edf\\\"\\uff0c\\u52aa\\u529b\\u4e3a\\u5ba2\\u6237\\u521b\\u5efa\\u66f4\\u9ad8\\u4ef7\\u503c\\u3002\",\"site_name\":\"\\u76f4\\u64ad\",\"site\":\"http:\\/\\/rongyao.com\",\"copyright\":\"\\u7248\\u6743\\u6240\\u6709\\uff1a\\u5c71\\u4e1c\\u7f51\\u7edc\\u79d1\\u6280\\u6709\\u9650\\u516c\\u53f8\",\"name_coin\":\"\\u94bb\\u77f3\",\"name_score\":\"\\u79ef\\u5206\",\"name_votes\":\"\\u6620\\u7968\",\"mobile\":\"\",\"address\":\"\",\"apk_ewm\":\"\",\"ipa_ewm\":\"\",\"wechat_ewm\":\"\",\"sina_icon\":\"\",\"sina_title\":\"\\u65b0\\u6d6a\\u5fae\\u535a\",\"sina_desc\":\"\\u5b98\\u65b9\\u5fae\\u535a\",\"sina_url\":\"\",\"qq_icon\":\"\",\"qq_title\":\"\\u5b98\\u65b9\\u7f51\\u7ad9\",\"qq_desc\":\"\\u5b98\\u65b9\\u7f51\\u7ad9\",\"qq_url\":\"\",\"site_seo_title\":\"\",\"site_seo_keywords\":\"\",\"site_seo_description\":\"\",\"isup\":\"0\",\"apk_ver\":\"1.2\",\"apk_url\":\"https:\\/\\/7niuapk.jiextx.com\\/rongyao-live-v1.2.apk\",\"apk_des\":\"\\u6709\\u65b0\\u7248\\u672c\\uff0c\\u662f\\u5426\\u66f4\\u65b0\",\"ios_shelves\":\"1.0\",\"ipa_type\":\"1\",\"tf_ipa_url\":\"https:\\/\\/testflight.apple.com\\/join\\/fuvPkxAA\",\"c_ipa_url\":\"itms-services:\\/\\/?action=download-manifest&url=https:\\/\\/7niuapk.jiextx.com\\/PhoneLive1.2_20201203.plist\",\"super_ipa_url\":\"https:\\/\\/www.dongbang520.com\\/TeGT.app\",\"ipa_des\":\"\\u6709\\u65b0\\u7248\\u672c\\uff0c\\u662f\\u5426\\u66f4\\u65b0\\r\\n\",\"qr_url\":\"\",\"wx_siteurl\":\"http:\\/\\/x.com\\/wxshare\\/Share\\/show?roomnum=\",\"share_title\":\"\\u5206\\u4eab\\u4e86{username}\\u7684\\u7cbe\\u5f69\\u76f4\\u64ad\\uff01\",\"share_des\":\"\\u6211\\u770b\\u5230\\u4e00\\u4e2a\\u5f88\\u597d\\u770b\\u7684\\u76f4\\u64ad\\uff0c\\u5feb\\u6765\\u8ddf\\u6211\\u4e00\\u8d77\\u56f4\\u89c2\\u5427\",\"app_android\":\"\",\"app_ios\":\"\",\"video_share_title\":\"\\u5206\\u4eab\\u4e86{username}\\u7684\\u7cbe\\u5f69\\u89c6\\u9891\\uff01\",\"video_share_des\":\"\\u6211\\u770b\\u5230\\u4e00\\u4e2a\\u5f88\\u597d\\u770b\\u7684\\u89c6\\u9891\\uff0c\\u5feb\\u6765\\u8ddf\\u6211\\u4e00\\u8d77\\u56f4\\u89c2\\u5427\",\"live_time_coin\":\"20,30,40,50,60\",\"cert_score\":\"100\",\"cert_score_tip\":\"300\",\"hot_score_tip\":\"100\",\"sprout_key\":\"\",\"sprout_key_ios\":\"\",\"skin_whiting\":\"7\",\"skin_smooth\":\"5\",\"skin_tenderness\":\"5\",\"eye_brow\":\"10\",\"big_eye\":\"2\",\"eye_length\":\"3\",\"eye_corner\":\"4\",\"eye_alat\":\"5\",\"face_lift\":\"6\",\"face_shave\":\"7\",\"mouse_lift\":\"8\",\"nose_lift\":\"10\",\"chin_lift\":\"10\",\"forehead_lift\":\"10\",\"lengthen_noseLift\":\"10\",\"payment_des\":\"1. \\u4ed8\\u8d39\\u5185\\u5bb9\\u5fc5\\u987b\\u7b26\\u5408\\u4e2d\\u534e\\u4eba\\u6c11\\u5171\\u548c\\u56fd\\u6cd5\\u5f8b\\u6cd5\\u89c4\\uff0c\\u4e0d\\u5f97\\u6d89\\u53ca\\u56fd\\u5bb6\\u6cd5\\u5f8b\\u6cd5\\u89c4\\u7981\\u6b62\\u7684\\u4e00\\u5207\\u8fdd\\u6cd5\\u5185\\u5bb9\\u3002\\r\\n2.\\u5e73\\u53f0\\u6709\\u6743\\u5229\\u5bf9\\u7528\\u6237\\u4e0a\\u4f20\\u7684\\u4ed8\\u8d39\\u5185\\u5bb9\\u8fdb\\u884c\\u76d1\\u7ba1\\u53ca\\u5ba1\\u6838\\u3002\\r\\n3.\\u5982\\u53d1\\u73b0\\u8fdd\\u53cd\\u56fd\\u5bb6\\u6cd5\\u5f8b\\u7684\\u5185\\u5bb9\\uff0c\\u5e73\\u53f0\\u6709\\u6743\\u5229\\u50cf\\u76f8\\u5173\\u90e8\\u95e8\\u4e3e\\u8bc1\\u4e3e\\u62a5\\u3002\\r\\n4.\\u8bf7\\u4ed4\\u7ec6\\u9605\\u8bfb\",\"payment_time\":\"0\",\"payment_percent\":\"20\",\"login_alert_title\":\"\\u670d\\u52a1\\u534f\\u8bae\\u548c\\u9690\\u79c1\\u653f\\u7b56\",\"login_alert_content\":\"\\u8bf7\\u60a8\\u52a1\\u5fc5\\u4ed4\\u7ec6\\u9605\\u8bfb\\uff0c\\u5145\\u5206\\u7406\\u89e3\\u201c\\u670d\\u52a1\\u534f\\u8bae\\u201d\\u548c\\u201c\\u9690\\u79c1\\u653f\\u7b56\\u201d\\u5404\\u6761\\u6b3e\\uff0c\\u5305\\u62ec\\u4f46\\u4e0d\\u9650\\u4e8e\\u4e3a\\u4e86\\u5411\\u60a8\\u63d0\\u4f9b\\u5373\\u65f6\\u901a\\u8baf\\uff0c\\u5185\\u5bb9\\u5206\\u4eab\\u7b49\\u670d\\u52a1\\uff0c\\u6211\\u4eec\\u9700\\u8981\\u6536\\u96c6\\u60a8\\u8bbe\\u5907\\u4fe1\\u606f\\u548c\\u4e2a\\u4eba\\u4fe1\\u606f\\uff0c\\u60a8\\u53ef\\u4ee5\\u5728\\u8bbe\\u7f6e\\u4e2d\\u67e5\\u770b\\uff0c\\u7ba1\\u7406\\u60a8\\u7684\\u6388\\u6743\\u3002\\u60a8\\u53ef\\u9605\\u8bfb\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\\u548c\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\\u4e86\\u89e3\\u8be6\\u7ec6\\u4fe1\\u606f\\uff0c\\u5982\\u60a8\\u540c\\u610f\\uff0c\\u8bf7\\u70b9\\u51fb\\u540c\\u610f\\u63a5\\u53d7\\u6211\\u4eec\\u7684\\u670d\\u52a1\\u3002\",\"login_clause_title\":\"\\u767b\\u5f55\\u5373\\u4ee3\\u8868\\u540c\\u610f\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\\u548c\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\",\"login_private_title\":\"\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\",\"login_private_url\":\"\\/portal\\/page\\/index?id=3\",\"login_service_title\":\"\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\",\"login_service_url\":\"\\/portal\\/page\\/index?id=4\",\"login_type\":\"\",\"share_type\":\"\",\"live_type\":\"0;\\u666e\\u901a\\u623f\\u95f4,1;\\u5bc6\\u7801\\u623f\\u95f4\"}');
INSERT INTO `cmf_option` VALUES (2, 1, 'cmf_settings', '{\"banned_usernames\":\"\"}');
INSERT INTO `cmf_option` VALUES (3, 1, 'cdn_settings', '{\"cdn_static_root\":\"\"}');
INSERT INTO `cmf_option` VALUES (4, 1, 'admin_settings', '{\"admin_password\":\"\",\"admin_theme\":\"admin_simpleboot3\",\"admin_style\":\"flatadmin\"}');
INSERT INTO `cmf_option` VALUES (5, 1, 'storage', '{\"storages\":{\"Qiniu\":{\"name\":\"\\u4e03\\u725b\\u4e91\\u5b58\\u50a8\",\"driver\":\"\\\\plugins\\\\qiniu\\\\lib\\\\Qiniu\"}},\"type\":\"Qiniu\"}');
INSERT INTO `cmf_option` VALUES (6, 1, 'configpri', '{\"family_switch\":\"0\",\"family_member_divide_switch\":\"0\",\"service_switch\":\"1\",\"service_url\":\"https:\\/\\/chatlink.mstatik.com\\/widget\\/standalone.html?eid=41d250a65354d8d413024012202c83fe&groupid=d4b5f97dd8ac4782c054fc84ed317370\",\"sensitive_words\":\"\\u6bdb\\u6cfd\\u4e1c,\\u4e60\\u8fd1\\u5e73,\\u80e1\\u9526\\u6d9b,\\u6c5f\\u6cfd\\u6c11,\\u6731\\u9555\\u57fa,\\u4e14,weixin,qq,\\u5fae\\u4fe1,QQ,\\\",\'\",\"reg_reward\":\"0\",\"bonus_switch\":\"1\",\"login_wx_pc_appid\":\"\",\"login_wx_pc_appsecret\":\"\",\"login_wx_appid\":\"\",\"login_wx_appsecret\":\"\",\"sendcode_switch\":\"0\",\"typecode_switch\":\"2\",\"aly_keydi\":\"\",\"aly_secret\":\"\",\"aly_signName\":\"\",\"aly_templateCode\":\"\",\"ccp_sid\":\"8a216da8754a45d501756edce3a50ba9\",\"ccp_token\":\"cd3cd9736927483f8596fed1a8b6db81\",\"ccp_appid\":\"8a216da8754a45d501756edce4b30bb0\",\"ccp_tempid\":\"\",\"iplimit_switch\":\"0\",\"iplimit_times\":\"10\",\"auth_islimit\":\"1\",\"cert_score\":\"100\",\"level_islimit\":\"0\",\"level_limit\":\"1\",\"speak_limit\":\"0\",\"barrage_limit\":\"0\",\"barrage_fee\":\"10\",\"userlist_time\":\"10\",\"mic_limit\":\"0\",\"chatserver\":\"https:\\/\\/www.jiextx.com:443\",\"live_sdk\":\"1\",\"cdn_switch\":\"2\",\"push_url\":\"\",\"auth_key_push\":\"\",\"auth_length_push\":\"1800\",\"pull_url\":\"\",\"auth_key_pull\":\"\",\"auth_length_pull\":\"1800\",\"aliy_key_id\":\"\",\"aliy_key_secret\":\"\",\"tx_appid\":\"1304015898\",\"tx_bizid\":\"117756\",\"tx_push_key\":\"dfc80cc003859da0a674b4461fb3c443\",\"tx_acc_key\":\"dfc80cc003859da0a674b4461fb3c443\",\"tx_api_key\":\"dfc80cc003859da0a674b4461fb3c443\",\"tx_play_key\":\"jiextx01\",\"tx_play_time\":\"20\",\"tx_play_key_switch\":\"1\",\"tx_push\":\"push.jiextx.com\",\"tx_pull\":\"play.jiextx.com\",\"txcloud_secret_id\":\"AKIDveLs9udajcK3Zwp5pHVZV1MSM2f5YZgh\",\"txcloud_secret_key\":\"3t4wX6h4Ol2viJgvzqpjUO3So5hGZ5MS\",\"qn_ak\":\"\",\"qn_sk\":\"\",\"qn_hname\":\"\",\"qn_push\":\"\",\"qn_pull\":\"\",\"ws_push\":\"\",\"ws_pull\":\"\",\"ws_apn\":\"\",\"wy_appkey\":\"\",\"wy_appsecret\":\"\",\"ady_push\":\"\",\"ady_pull\":\"\",\"ady_hls_pull\":\"\",\"ady_apn\":\"\",\"cash_rate\":\"100\",\"cash_take\":\"20\",\"cash_min\":\"1\",\"cash_start\":\"1\",\"cash_end\":\"30\",\"cash_max_times\":\"0\",\"letter_switch\":\"1\",\"jpush_sandbox\":\"1\",\"jpush_key\":\"865c5f417bfb86120b331b90\",\"jpush_secret\":\"f0ddf993d88258d3afcb1e45\",\"aliapp_switch\":\"1\",\"aliapp_partner\":\"\",\"aliapp_seller_id\":\"\",\"aliapp_key_android\":\"\",\"aliapp_key_ios\":\"\",\"aliapp_pc\":\"1\",\"aliapp_check\":\"\",\"wx_switch_pc\":\"1\",\"wx_switch\":\"1\",\"wx_appid\":\"\",\"wx_appsecret\":\"\",\"wx_mchid\":\"\",\"wx_key\":\"\",\"shop_aliapp_switch\":\"1\",\"shop_wx_switch\":\"1\",\"shop_balance_switch\":\"1\",\"paidprogram_aliapp_switch\":\"1\",\"paidprogram_wx_switch\":\"1\",\"paidprogram_balance_switch\":\"1\",\"agent_switch\":\"0\",\"distribut1\":\"30\",\"um_apikey\":\"2851614\",\"um_apisecurity\":\"9x1ijrPeIlN\",\"um_appkey_android\":\"5fa3abb745b2b751a924a500\",\"um_appkey_ios\":\"5fa3b7e51c520d3073a1595f\",\"video_audit_switch\":\"0\",\"video_watermark\":\"\",\"shop_system_name\":\"\\u76f4\\u64ad\\u5c0f\\u5e97\",\"shop_bond\":\"10\",\"show_switch\":\"1\",\"shoporder_percent\":\"10\",\"goods_switch\":\"0\",\"shop_certificate_desc\":\"\\u4ee5\\u4e0b\\u8425\\u4e1a\\u6267\\u7167\\u4fe1\\u606f\\u6765\\u6e90\\u4e8e\\u4e70\\u5bb6\\u81ea\\u884c\\u7533\\u62a5\\u6216\\u5de5\\u5546\\u7cfb\\u7edf\\u6570\\u636e\\uff0c\\u5177\\u4f53\\u4ee5\\u5de5\\u5546\\u90e8\\u95e8\\u767b\\u8bb0\\u4e3a\\u51c6\\uff0c\\u7ecf\\u8425\\u8005\\u9700\\u8981\\u786e\\u4fdd\\u4fe1\\u606f\\u771f\\u5b9e\\u6709\\u6548\\uff0c\\u5e73\\u53f0\\u4e5f\\u5c06\\u5b9a\\u671f\\u6838\\u67e5\\u3002\\u5982\\u4e0e\\u5b9e\\u9645\\u4e0d\\u7b26\\uff0c\\u4e3a\\u907f\\u514d\\u8fdd\\u89c4\\uff0c\\u8bf7\\u8054\\u7cfb\\u5f53\\u5730\\u5de5\\u5546\\u90e8\\u95e8\\u6216\\u5e73\\u53f0\\u5ba2\\u670d\\u66f4\\u65b0\\u3002\",\"shop_payment_time\":\"30\",\"shop_shipment_time\":\"2\",\"shop_receive_time\":\"15\",\"shop_refund_time\":\"2\",\"shop_refund_finish_time\":\"2\",\"shop_receive_refund_time\":\"1\",\"shop_settlement_time\":\"1\",\"balance_cash_min\":\"1\",\"balance_cash_start\":\"1\",\"balance_cash_end\":\"30\",\"balance_cash_max_times\":\"0\",\"dynamic_auth\":\"0\",\"dynamic_switch\":\"1\",\"comment_weight\":\"10\",\"like_weight\":\"20\",\"game_banker_limit\":\"100\",\"game_odds\":\"100\",\"game_odds_p\":\"100\",\"game_odds_u\":\"30\",\"game_pump\":\"10\",\"turntable_switch\":\"1\",\"express_type\":\"0\",\"express_id_dev\":\"\",\"express_appkey_dev\":\"\",\"express_id\":\"\",\"express_appkey\":\"\",\"watch_live_term\":\"2\",\"watch_live_coin\":\"10\",\"watch_video_term\":\"2\",\"watch_video_coin\":\"10\",\"open_live_term\":\"1\",\"open_live_coin\":\"30\",\"award_live_term\":\"100\",\"award_live_coin\":\"100\",\"share_live_term\":\"1\",\"share_live_coin\":\"200\",\"game_switch\":\"1,3\"}');
INSERT INTO `cmf_option` VALUES (7, 1, 'jackpot', '{\"switch\":\"1\",\"luck_anchor\":\"60\",\"luck_jackpot\":\"10\"}');
INSERT INTO `cmf_option` VALUES (8, 1, 'guide', '{\"switch\":\"1\",\"type\":\"0\",\"time\":\"3\"}');
INSERT INTO `cmf_option` VALUES (9, 1, 'upload_setting', '{\"max_files\":\"20\",\"chunk_size\":\"512\",\"file_types\":{\"image\":{\"upload_max_filesize\":\"1024\",\"extensions\":\"jpg,jpeg,png,gif,bmp4,svga\"},\"video\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"mp4\"},\"audio\":{\"upload_max_filesize\":\"10240\",\"extensions\":\"mp3\"},\"file\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"txt,pdf,doc,docx,xls,xlsx,ppt,pptx,svga,mp4,apk\"}}}');

-- ----------------------------
-- Table structure for cmf_paidprogram
-- ----------------------------
DROP TABLE IF EXISTS `cmf_paidprogram`;
CREATE TABLE `cmf_paidprogram`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `classid` int(11) NOT NULL DEFAULT 0 COMMENT '分类ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容简介',
  `personal_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '个人介绍',
  `money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '付费内容价格',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '文件类型 0 单视频 1 多视频',
  `videos` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '视频json串',
  `sale_nums` bigint(20) NOT NULL DEFAULT 0 COMMENT '购买数量',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否审核通过 -1 拒绝 0 审核中 1 通过',
  `evaluate_nums` bigint(20) NOT NULL DEFAULT 0 COMMENT '评价总人数',
  `evaluate_total` bigint(20) NOT NULL DEFAULT 0 COMMENT '评价总分',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_uid`(`id`, `uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_paidprogram_apply
-- ----------------------------
DROP TABLE IF EXISTS `cmf_paidprogram_apply`;
CREATE TABLE `cmf_paidprogram_apply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '审核状态 -1 拒绝 0 审核中 1 通过',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  `percent` int(11) NOT NULL DEFAULT 0 COMMENT '抽水比例',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_paidprogram_apply
-- ----------------------------
INSERT INTO `cmf_paidprogram_apply` VALUES (1, 42513, 0, 1604575641, 0, 20);

-- ----------------------------
-- Table structure for cmf_paidprogram_class
-- ----------------------------
DROP TABLE IF EXISTS `cmf_paidprogram_class`;
CREATE TABLE `cmf_paidprogram_class`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `list_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0不显示 1 显示',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_paidprogram_class
-- ----------------------------
INSERT INTO `cmf_paidprogram_class` VALUES (2, 'aaaaaaaaaaaaa99', 10, 1, 1604294598, 1604321712);
INSERT INTO `cmf_paidprogram_class` VALUES (3, 'ccccccccccccca3311', 4, 1, 1604294620, 1604295460);
INSERT INTO `cmf_paidprogram_class` VALUES (4, '6666666哈哈哈', 0, 1, 1604409795, 0);
INSERT INTO `cmf_paidprogram_class` VALUES (5, '009888', 1, 1, 1604409858, 0);

-- ----------------------------
-- Table structure for cmf_paidprogram_comment
-- ----------------------------
DROP TABLE IF EXISTS `cmf_paidprogram_comment`;
CREATE TABLE `cmf_paidprogram_comment`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` bigint(20) NOT NULL DEFAULT 0 COMMENT '项目发布者ID',
  `object_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '付费项目ID',
  `grade` tinyint(1) NOT NULL DEFAULT 0 COMMENT '评价等级',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_paidprogram_order
-- ----------------------------
DROP TABLE IF EXISTS `cmf_paidprogram_order`;
CREATE TABLE `cmf_paidprogram_order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` bigint(20) NOT NULL DEFAULT 0 COMMENT '付费项目发布者ID',
  `object_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '付费项目ID',
  `type` tinyint(1) NOT NULL COMMENT '支付方式 1 支付宝 2 微信 3 余额',
  `status` tinyint(1) NOT NULL COMMENT '订单状态 0 未支付 1 已支付',
  `orderno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单编号',
  `trade_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方订单编号',
  `money` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '金额',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '下单时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  `isdel` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除 0 否 1 是（用于删除付费项目）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid_objectid_status`(`uid`, `object_id`, `status`) USING BTREE,
  INDEX `uid_status`(`uid`, `status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_plugin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_plugin`;
CREATE TABLE `cmf_plugin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '插件类型;1:网站;8:微信',
  `has_admin` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否有后台管理,0:没有;1:有',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:开启;0:禁用',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '插件安装时间',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件标识名,英文字母(惟一)',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件名称',
  `demo_url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '演示地址，带协议',
  `hooks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '实现的钩子;以“,”分隔',
  `author` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件作者',
  `author_url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '作者网站链接',
  `version` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件版本号',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '插件描述',
  `config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '插件配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '插件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_plugin
-- ----------------------------
INSERT INTO `cmf_plugin` VALUES (1, 1, 0, 1, 0, 'Qiniu', '七牛云存储', '', '', 'ThinkCMF', '', '1.0.1', 'ThinkCMF七牛专享优惠码:507670e8', '{\"accessKey\":\"ucSe4-kD4Fq4XY_4Bq2up9X6eZYRMKE-3cuEYVpB\",\"secretKey\":\"lvNY_QtKqHViBVSSZnEU75Dr65Of1GabsAS6E5wq\",\"protocol\":\"https\",\"domain\":\"7niuobs.jiextx.com\",\"bucket\":\"live-7niu-obs\",\"zone\":\"z2\"}');

-- ----------------------------
-- Table structure for cmf_portal_category
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_category`;
CREATE TABLE `cmf_portal_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类父id',
  `post_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类文章数',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布,0:不发布',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类描述',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类层级关系路径',
  `seo_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `seo_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `list_tpl` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类列表模板',
  `one_tpl` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类文章页模板',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '扩展属性',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'portal应用 文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_portal_category
-- ----------------------------
INSERT INTO `cmf_portal_category` VALUES (1, 0, 0, 0, 0, 10000, 'ceshi1', '', '0-1', '', '', '', '', '', '{\"thumbnail\":\"\"}');

-- ----------------------------
-- Table structure for cmf_portal_category_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_category_post`;
CREATE TABLE `cmf_portal_category_post`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章id',
  `category_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类id',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布;0:不发布',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `term_taxonomy_id`(`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'portal应用 分类文章对应表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_portal_category_post
-- ----------------------------
INSERT INTO `cmf_portal_category_post` VALUES (1, 2, 1, 10000, 0);

-- ----------------------------
-- Table structure for cmf_portal_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_post`;
CREATE TABLE `cmf_portal_post`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `post_type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '类型,1:文章;2:页面',
  `post_format` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '内容格式;1:html;2:md',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发表者用户id',
  `post_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:已发布;0:未发布;',
  `comment_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '评论状态;1:允许;0:不允许',
  `is_top` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否置顶;1:置顶;0:不置顶',
  `recommended` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否推荐;1:推荐;0:不推荐',
  `post_hits` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '查看数',
  `post_favorites` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '收藏数',
  `post_like` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞数',
  `comment_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `published_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发布时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  `post_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'post标题',
  `post_keywords` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'seo keywords',
  `post_excerpt` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'post摘要',
  `post_source` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '转载文章的来源',
  `thumbnail` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '缩略图',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '文章内容',
  `post_content_filtered` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '处理过的文章内容',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '扩展属性,如缩略图;格式为json',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '页面类型，0单页面，2关于我们',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_status_date`(`post_type`, `post_status`, `create_time`, `id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'portal应用 文章表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_portal_post
-- ----------------------------
INSERT INTO `cmf_portal_post` VALUES (2, 0, 2, 1, 42494, 1, 1, 0, 0, 0, 0, 0, 0, 1575612912, 1604648704, 1575612840, 0, '社区公约', '', '', '', '', '&lt;p&gt;  社区公约，内容可在管理后台设置。&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 1);
INSERT INTO `cmf_portal_post` VALUES (3, 0, 2, 1, 42494, 1, 1, 0, 0, 0, 0, 0, 0, 1575612937, 1604648024, 1575612900, 0, '隐私政策', '', '', '', '', '\n&lt;p style=&quot;white-space: normal;&quot;&gt;  隐私政策，内容可在管理后台设置。&lt;/p&gt;\n&lt;p&gt;&lt;br&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9999);
INSERT INTO `cmf_portal_post` VALUES (4, 0, 2, 1, 42494, 1, 1, 0, 0, 0, 0, 0, 0, 1575612961, 1604648044, 1575612900, 0, '服务协议', '', '', '', '', '&lt;p&gt;服务协议，内容可在管理后台设置。&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9999);
INSERT INTO `cmf_portal_post` VALUES (5, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575612983, 1578534055, 1575612960, 0, '联系我们', '', '', '', '', '\n&lt;p style=&quot;margin-top: 0px; margin-bottom: 20px; white-space: normal; padding: 0px; font-size: 14px; line-height: 25.2px; text-indent: 28px; color: rgb(85, 85, 85); font-family: arial, &quot;&gt;联系我们，内容可在管理后台设置。&lt;/p&gt;\n&lt;p&gt;&lt;br&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 2);
INSERT INTO `cmf_portal_post` VALUES (6, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613058, 1591087668, 1575613020, 0, '用户充值协议', '', '', '', '', '&lt;p&gt;用户充值协议用户充值协议用户充值协议用户充值协议&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 100);
INSERT INTO `cmf_portal_post` VALUES (10, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613012, 1578534178, 1575612960, 0, '主播协议', '', '', '', '', '&lt;p&gt;   主播协议，内容可在管理后台设置。&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9);
INSERT INTO `cmf_portal_post` VALUES (18, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613036, 1599550227, 1575612960, 0, '签约说明', '', '', '', '', '&lt;p&gt;签约说明&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 99);
INSERT INTO `cmf_portal_post` VALUES (26, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613184, 1575613184, 1575613140, 0, '幸运礼物说明', '', '', '', '', '&lt;p&gt;幸运礼物功能，用户送出“幸运礼物”会有几率获得礼物翻倍中奖和奖池中奖的机会。&lt;/p&gt;', NULL, '{\"thumbnail\":\"\"}', 0, 99);
INSERT INTO `cmf_portal_post` VALUES (31, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1576912662, 1578565627, 1576912620, 0, '直播带货功能', '', '', '', '', '&lt;p&gt;轮播1&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 9999);
INSERT INTO `cmf_portal_post` VALUES (33, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1576913347, 1578565644, 1576913280, 0, '直播系统', '', '', '', '', '&lt;p&gt;轮播2&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 9999);
INSERT INTO `cmf_portal_post` VALUES (34, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1576913387, 1578565656, 1576913340, 0, '超值服务', '', '', '', '', '&lt;p&gt;轮播3&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 9999);
INSERT INTO `cmf_portal_post` VALUES (35, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613926, 1594174677, 1575613860, 0, '转盘规则', '', '', '', '', '&lt;p&gt;转盘规则转盘规则转盘规则&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 9999);
INSERT INTO `cmf_portal_post` VALUES (38, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1576655010, 1578562536, 1576654980, 0, '小店界面联系客服', '', '', '', '', '&lt;p&gt;小店界面联系客服&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 9999);
INSERT INTO `cmf_portal_post` VALUES (39, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1576655030, 1576655030, 1576654980, 0, '道具礼物说明', '', '', '', '', '\n&lt;p style=&quot;white-space: normal;&quot;&gt;道具礼物说明&lt;/p&gt;\n&lt;p style=&quot;white-space: normal;&quot;&gt;道具礼物说明&lt;/p&gt;\n&lt;p style=&quot;white-space: normal;&quot;&gt;道具礼物说明&lt;/p&gt;\n&lt;p&gt;&lt;br&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\"}', 0, 9999);
INSERT INTO `cmf_portal_post` VALUES (40, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1585211267, 1593574434, 1585211220, 0, '付费内容管理规范', '', '', '', '', '&lt;p&gt;付费内容管理规范说明&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 4);
INSERT INTO `cmf_portal_post` VALUES (44, 0, 2, 1, 42494, 1, 1, 0, 0, 0, 0, 0, 0, 1592911539, 1604321262, 1592911500, 0, '注销账号说明', '', '', '', '', '\n&lt;p style=&quot;white-space: normal; line-height: 1.5em;&quot;&gt;&lt;strong&gt;    注销账号是不可恢复的操作，你应自行备份账号相关的信息和数据，操作之前，请确认与账号相关的所有服务均已妥善处理。&lt;/strong&gt;&lt;/p&gt;\n&lt;p style=&quot;white-space: normal; line-height: 1.5em;&quot;&gt;&lt;strong&gt;     注销账号后，你将无法再使用本账号或找回你添加的任何内容信息，包括但不限于：&lt;/strong&gt;&lt;/p&gt;\n&lt;p style=&quot;white-space: normal; line-height: 1.5em;&quot;&gt;（1）你将无法登录、使用本账号，你的朋友将无法通过本账号联系你。&lt;/p&gt;\n&lt;p style=&quot;white-space: normal; line-height: 1.5em;&quot;&gt;（2）你账号的个人资料和历史信息（包含昵称、头像、作品、消息记录、粉丝、关注等）都将无法找回。&lt;/p&gt;\n&lt;p style=&quot;white-space: normal; line-height: 1.5em;&quot;&gt;（3）关注你账号的所有用户将被取消关注，与账号相关的所有功能或服务都将无法继续使用。&lt;/p&gt;\n&lt;p&gt;&lt;br&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9999);
INSERT INTO `cmf_portal_post` VALUES (46, 0, 2, 1, 42494, 1, 1, 0, 0, 0, 0, 0, 0, 1604392155, 1604392155, 1604392140, 0, '222222222', '', '', '', '', '&lt;p&gt;333333333333333333&lt;/p&gt;', NULL, '{\"thumbnail\":\"\"}', 0, 9999);

-- ----------------------------
-- Table structure for cmf_portal_tag
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_tag`;
CREATE TABLE `cmf_portal_tag`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布,0:不发布',
  `recommended` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否推荐;1:推荐;0:不推荐',
  `post_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标签文章数',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标签名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'portal应用 文章标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_portal_tag_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_tag_post`;
CREATE TABLE `cmf_portal_tag_post`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标签 id',
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章 id',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布;0:不发布',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'portal应用 标签文章对应表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_pushrecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_pushrecord`;
CREATE TABLE `cmf_pushrecord`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `touid` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推送对象',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '推送内容',
  `adminid` int(11) NOT NULL COMMENT '管理员ID',
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员账号',
  `ip` bigint(20) NOT NULL DEFAULT 0 COMMENT '管理员IP地址',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '发送时间',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '消息类型 0 后台手动发布的系统消息 1 商品消息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_pushrecord
-- ----------------------------
INSERT INTO `cmf_pushrecord` VALUES (1, '13900000000', '222222222222222222222222', 42494, 'messi', 1892640702, 1604408405, 0);
INSERT INTO `cmf_pushrecord` VALUES (2, '', '888888888888888', 42494, 'messi', 1892640702, 1604408444, 0);
INSERT INTO `cmf_pushrecord` VALUES (3, '13900000000', '777777777777777', 42494, 'messi', 1892640702, 1604408492, 0);
INSERT INTO `cmf_pushrecord` VALUES (4, '42514', '22222222222222222222222222', 42494, 'messi', 1892640702, 1604408558, 0);
INSERT INTO `cmf_pushrecord` VALUES (5, '', '555555555555555555', 42494, 'messi', 1892640702, 1604409182, 0);
INSERT INTO `cmf_pushrecord` VALUES (6, '13312345678', 'www.baidu.com', 42491, 'regame', 1892640744, 1604494056, 0);
INSERT INTO `cmf_pushrecord` VALUES (7, '13500000000', '434322222222222222222', 42494, 'messi', 1892640702, 1604921412, 0);
INSERT INTO `cmf_pushrecord` VALUES (8, '13500000000,13500000008', '777777777777777777777777', 42494, 'messi', 1892640702, 1604921455, 0);
INSERT INTO `cmf_pushrecord` VALUES (9, '', '333333333333333333333', 42494, 'messi', 1892640702, 1604921517, 0);
INSERT INTO `cmf_pushrecord` VALUES (10, '42499', '222222222222222222222222', 42494, 'messi', 1892640702, 1604924628, 0);
INSERT INTO `cmf_pushrecord` VALUES (11, '', '测试', 42494, 'messi', 1892641726, 1605850003, 0);

-- ----------------------------
-- Table structure for cmf_recycle_bin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_recycle_bin`;
CREATE TABLE `cmf_recycle_bin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT 0 COMMENT '删除内容 id',
  `create_time` int(10) UNSIGNED DEFAULT 0 COMMENT '创建时间',
  `table_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '删除内容所在表名',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '删除内容名称',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = ' 回收站' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_recycle_bin
-- ----------------------------
INSERT INTO `cmf_recycle_bin` VALUES (1, 3, 1596874182, 'slide', ' ', 0);
INSERT INTO `cmf_recycle_bin` VALUES (2, 4, 1596878259, 'slide', ' ', 0);

-- ----------------------------
-- Table structure for cmf_red
-- ----------------------------
DROP TABLE IF EXISTS `cmf_red`;
CREATE TABLE `cmf_red`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `showid` int(11) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `liveuid` int(11) NOT NULL DEFAULT 0 COMMENT '主播ID',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '红包类型，0平均，1手气',
  `type_grant` tinyint(1) NOT NULL DEFAULT 0 COMMENT '发放类型，0立即，1延迟',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  `des` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `effecttime` int(11) NOT NULL DEFAULT 0 COMMENT '生效时间',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态，0抢中，1抢完',
  `coin_rob` int(11) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `nums_rob` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `liveuid_showid`(`showid`, `liveuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_red
-- ----------------------------
INSERT INTO `cmf_red` VALUES (1, 1604386353, 42513, 42520, 1, 1, 100, 10, '恭喜发财，大吉大利', 1604386957, 1604386777, 0, 14, 1);
INSERT INTO `cmf_red` VALUES (2, 1604468385, 42516, 42520, 0, 0, 100, 100, '恭喜发财，大吉大利', 1604468657, 1604468657, 0, 1, 1);
INSERT INTO `cmf_red` VALUES (3, 1604468385, 42516, 42520, 0, 0, 100, 1, '恭喜发财，大吉大利', 1604468687, 1604468687, 0, 100, 1);

-- ----------------------------
-- Table structure for cmf_red_record
-- ----------------------------
DROP TABLE IF EXISTS `cmf_red_record`;
CREATE TABLE `cmf_red_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `redid` int(11) NOT NULL DEFAULT 0 COMMENT '红包ID',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '金额',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `redid`(`redid`) USING BTREE COMMENT '红包ID索引'
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_red_record
-- ----------------------------
INSERT INTO `cmf_red_record` VALUES (1, 42520, 1, 14, 1604386958);
INSERT INTO `cmf_red_record` VALUES (2, 42520, 2, 1, 1604468663);
INSERT INTO `cmf_red_record` VALUES (3, 42520, 3, 100, 1604468699);

-- ----------------------------
-- Table structure for cmf_report
-- ----------------------------
DROP TABLE IF EXISTS `cmf_report`;
CREATE TABLE `cmf_report`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '对方ID',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_report
-- ----------------------------
INSERT INTO `cmf_report` VALUES (1, 42492, 42515, '11111111', 0, 1604385877, 0);
INSERT INTO `cmf_report` VALUES (2, 42515, 42492, '角色不对，举报举报', 1, 1604392785, 1604393815);
INSERT INTO `cmf_report` VALUES (3, 42516, 42520, '任性打抱不平，就爱举报 可口可乐了', 0, 1604469288, 0);
INSERT INTO `cmf_report` VALUES (4, 42492, 1, '123', 0, 1604484230, 0);
INSERT INTO `cmf_report` VALUES (5, 42533, 42514, '好看！！！！！', 0, 1604492283, 0);
INSERT INTO `cmf_report` VALUES (6, 42533, 42514, '333333333333', 0, 1604494460, 0);
INSERT INTO `cmf_report` VALUES (7, 42492, 1, 'dsfafaf', 0, 1604496951, 0);
INSERT INTO `cmf_report` VALUES (8, 42521, 42538, '666', 0, 1604577368, 0);
INSERT INTO `cmf_report` VALUES (9, 42545, 42533, '任性打抱不平，就爱举报', 0, 1604729728, 0);
INSERT INTO `cmf_report` VALUES (10, 42545, 42533, '任性打抱不平，就爱举报 测试', 0, 1604729750, 0);
INSERT INTO `cmf_report` VALUES (11, 42584, 42598, '骗取点击', 0, 1605758471, 0);
INSERT INTO `cmf_report` VALUES (12, 42513, 42598, '低俗色情 里还好还好哈', 0, 1605848874, 0);

-- ----------------------------
-- Table structure for cmf_report_classify
-- ----------------------------
DROP TABLE IF EXISTS `cmf_report_classify`;
CREATE TABLE `cmf_report_classify`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `list_order` int(10) NOT NULL DEFAULT 9999 COMMENT '排序',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '举报类型名称',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_report_classify
-- ----------------------------
INSERT INTO `cmf_report_classify` VALUES (1, 0, '骗取点击', 1544855181);
INSERT INTO `cmf_report_classify` VALUES (2, 0, '低俗色情', 1544855189);
INSERT INTO `cmf_report_classify` VALUES (3, 0, '侮辱谩骂', 1544855198);
INSERT INTO `cmf_report_classify` VALUES (4, 0, '盗用他人作品', 1544855213);
INSERT INTO `cmf_report_classify` VALUES (5, 0, '引人不适', 1544855224);
INSERT INTO `cmf_report_classify` VALUES (6, 0, '任性打抱不平，就爱举报', 1544855259);
INSERT INTO `cmf_report_classify` VALUES (7, 0, '其他', 1544855273);
INSERT INTO `cmf_report_classify` VALUES (8, 9999, '垃圾广告XXX', 1599709932);
INSERT INTO `cmf_report_classify` VALUES (10, 1, '傻逼', 1604304505);

-- ----------------------------
-- Table structure for cmf_role
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role`;
CREATE TABLE `cmf_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父角色ID',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态;0:禁用;1:正常',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `list_order` float NOT NULL DEFAULT 0 COMMENT '排序',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_role
-- ----------------------------
INSERT INTO `cmf_role` VALUES (1, 0, 1, 1329633709, 1329633709, 0, '超级管理员', '拥有网站最高管理员权限！');
INSERT INTO `cmf_role` VALUES (7, 0, 1, 0, 0, 0, '运营', 'rgm测试运营好');
INSERT INTO `cmf_role` VALUES (8, 0, 1, 0, 0, 0, '测试', '测试');

-- ----------------------------
-- Table structure for cmf_role_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role_user`;
CREATE TABLE `cmf_role_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色 id',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色对应表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_role_user
-- ----------------------------
INSERT INTO `cmf_role_user` VALUES (1, 7, 42491);
INSERT INTO `cmf_role_user` VALUES (2, 1, 42491);
INSERT INTO `cmf_role_user` VALUES (3, 1, 42494);
INSERT INTO `cmf_role_user` VALUES (4, 1, 42512);
INSERT INTO `cmf_role_user` VALUES (5, 7, 42535);
INSERT INTO `cmf_role_user` VALUES (6, 8, 42573);

-- ----------------------------
-- Table structure for cmf_route
-- ----------------------------
DROP TABLE IF EXISTS `cmf_route`;
CREATE TABLE `cmf_route`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路由id',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '状态;1:启用,0:不启用',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'URL规则类型;1:用户自定义;2:别名添加',
  `full_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '完整url',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '实际显示的url',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'url路由表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_seller_goods_class
-- ----------------------------
DROP TABLE IF EXISTS `cmf_seller_goods_class`;
CREATE TABLE `cmf_seller_goods_class`  (
  `uid` bigint(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `goods_classid` int(11) NOT NULL DEFAULT 0 COMMENT '商品一级分类id',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否显示 0 否 1 是'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_sendcode
-- ----------------------------
DROP TABLE IF EXISTS `cmf_sendcode`;
CREATE TABLE `cmf_sendcode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '消息类型，1表示短信验证码，2表示邮箱验证码',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接收账号',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息内容',
  `addtime` int(11) NOT NULL COMMENT '提交时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_address
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_address`;
CREATE TABLE `cmf_shop_address`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '国家',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '市',
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '详细地址',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `country_code` int(11) NOT NULL DEFAULT 86 COMMENT '国家代号',
  `is_default` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为默认地址 0 否 1 是',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_shop_address
-- ----------------------------
INSERT INTO `cmf_shop_address` VALUES (1, 42514, '李白', '中国', '北京市', '北京市', '东城区', '北京市朝阳区群众演员', '13599998888', 86, 0, 1604380131, 0);
INSERT INTO `cmf_shop_address` VALUES (2, 42514, '李四', '中国', '天津市', '天津市', '和平区', '天津市武清区', '13155556669', 86, 1, 1604380186, 1604380200);

-- ----------------------------
-- Table structure for cmf_shop_apply
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_apply`;
CREATE TABLE `cmf_shop_apply`  (
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面',
  `des` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '简介',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '联系人姓名',
  `cardno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '身份证号码',
  `contact` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '联系人',
  `country_code` int(11) NOT NULL DEFAULT 86 COMMENT '国家代号',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '市',
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '地区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '详细地址',
  `service_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客服电话',
  `receiver` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退货收货人',
  `receiver_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退货人联系电话',
  `receiver_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退货人省份',
  `receiver_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退货人市',
  `receiver_area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '退货人地区',
  `receiver_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '退货人详细地址',
  `license` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '许可证',
  `certificate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '营业执照',
  `other` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '其他证件',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '申请时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态，0审核中1通过2拒绝',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原因',
  `order_percent` int(11) NOT NULL DEFAULT 0 COMMENT '订单分成比例',
  `sale_nums` bigint(11) NOT NULL DEFAULT 0 COMMENT '店铺总销量',
  `quality_points` float(11, 1) NOT NULL DEFAULT 0.0 COMMENT '店铺商品质量(商品描述)平均分',
  `service_points` float(11, 1) NOT NULL DEFAULT 0.0 COMMENT '店铺服务态度平均分',
  `express_points` float(11, 1) NOT NULL DEFAULT 0.0 COMMENT '物流服务平均分',
  `shipment_overdue_num` int(11) NOT NULL DEFAULT 0 COMMENT '店铺逾期发货次数',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_bond
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_bond`;
CREATE TABLE `cmf_shop_bond`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `bond` int(11) NOT NULL DEFAULT 0 COMMENT '保证金',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态，0已退回1已支付,-1已扣除',
  `addtime` bigint(20) NOT NULL DEFAULT 0 COMMENT '支付时间',
  `uptime` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_express
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_express`;
CREATE TABLE `cmf_shop_express`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `express_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递公司电话',
  `express_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递公司客服电话',
  `express_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递公司图标',
  `express_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示 0 否 1 是',
  `express_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递公司对应三方平台的编码',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '编辑时间',
  `list_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_shop_express
-- ----------------------------
INSERT INTO `cmf_shop_express` VALUES (1, '顺丰速运', '95338', 'express_1.png', 1, 'SF', 1583898216, 1604387266, 1);
INSERT INTO `cmf_shop_express` VALUES (2, '韵达速递', '95546', 'express_2.png', 1, 'YD', 1583905367, 0, 1);
INSERT INTO `cmf_shop_express` VALUES (3, '中通快递', '95311', 'express_3.png', 1, 'ZTO', 1583905579, 0, 3);
INSERT INTO `cmf_shop_express` VALUES (4, '圆通速递', '95554', 'express_4.png', 1, 'YTO', 1583905611, 1586230191, 1);
INSERT INTO `cmf_shop_express` VALUES (5, '申通快递', '95543', 'express_5.png', 1, 'STO', 1583905650, 0, 1);
INSERT INTO `cmf_shop_express` VALUES (6, '中国邮政', '11183', 'express_6.png', 1, 'YZPY', 1583905722, 0, 6);
INSERT INTO `cmf_shop_express` VALUES (7, '百世快递', '95320', 'express_7.png', 1, 'HTKY', 1583905749, 0, 7);
INSERT INTO `cmf_shop_express` VALUES (8, '宅急送', '400-6789-000', 'express_8.png', 1, 'ZJS', 1583905771, 0, 8);

-- ----------------------------
-- Table structure for cmf_shop_goods
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_goods`;
CREATE TABLE `cmf_shop_goods`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `one_classid` int(11) NOT NULL DEFAULT 0 COMMENT '商品一级分类',
  `two_classid` int(11) NOT NULL DEFAULT 0 COMMENT '商品二级分类',
  `three_classid` int(11) NOT NULL DEFAULT 0 COMMENT '商品三级分类',
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商品视频地址',
  `video_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商品视频封面',
  `thumbs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '封面',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品文字内容',
  `pictures` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品内容图集',
  `specs` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品规格',
  `postage` int(11) NOT NULL DEFAULT 0 COMMENT '邮费',
  `addtime` bigint(20) NOT NULL DEFAULT 0 COMMENT '时间',
  `uptime` bigint(20) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `hits` int(11) NOT NULL DEFAULT 0 COMMENT '点击数',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态，0审核中-1商家下架1通过-2管理员下架 2拒绝',
  `isrecom` tinyint(1) NOT NULL DEFAULT 0 COMMENT '推荐，0否1是',
  `sale_nums` int(11) NOT NULL DEFAULT 0 COMMENT '总销量',
  `refuse_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商品拒绝原因',
  `issale` tinyint(1) NOT NULL DEFAULT 0 COMMENT '商品是否在直播间销售 0 否 1 是',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '商品类型 0 站内商品 1 站外商品',
  `original_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '站外商品原价',
  `present_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '站外商品现价',
  `goods_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '站外商品简介',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '站外商品链接',
  `live_isshow` tinyint(1) NOT NULL DEFAULT 0 COMMENT '直播间是否展示商品简介 0 否 1 是 默认0',
  `low_price` decimal(20, 2) NOT NULL COMMENT '站外商品最低价',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid_status`(`uid`, `status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_goods_class
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_goods_class`;
CREATE TABLE `cmf_shop_goods_class`  (
  `gc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品分类ID',
  `gc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商品分类名称',
  `gc_parentid` int(11) NOT NULL DEFAULT 0 COMMENT '上级分类ID',
  `gc_one_id` int(11) NOT NULL COMMENT '所属一级分类ID',
  `gc_sort` int(11) NOT NULL DEFAULT 0 COMMENT '商品分类排序号',
  `gc_isshow` tinyint(1) NOT NULL COMMENT '是否展示 0 否 1 是',
  `gc_addtime` int(11) NOT NULL DEFAULT 0 COMMENT '商品分类添加时间',
  `gc_edittime` int(11) NOT NULL DEFAULT 0 COMMENT '商品分类修改时间',
  `gc_grade` tinyint(1) NOT NULL DEFAULT 0 COMMENT '商品分类等级',
  `gc_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品分类图标',
  PRIMARY KEY (`gc_id`) USING BTREE,
  INDEX `list1`(`gc_parentid`, `gc_isshow`) USING BTREE,
  INDEX `gc_parentid`(`gc_parentid`) USING BTREE,
  INDEX `gc_grade`(`gc_grade`) USING BTREE,
  INDEX `list2`(`gc_one_id`, `gc_grade`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 217 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_shop_goods_class
-- ----------------------------
INSERT INTO `cmf_shop_goods_class` VALUES (1, '手机/数码/电脑办公', 0, 0, 1, 1, 1581417338, 1601012108, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (2, '手机', 1, 1, 12234, 1, 1581418030, 0, 2, 'shop_two_class_1.png');
INSERT INTO `cmf_shop_goods_class` VALUES (3, '华为', 2, 1, 1, 1, 1581419247, 1604388119, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (4, '苹果', 2, 1, 2423, 1, 1581419261, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (5, '小米', 2, 1, 3, 1, 1581419272, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (6, 'OPPO', 2, 1, 4, 1, 1581419284, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (7, 'vivo', 2, 1, 5, 1, 1581419312, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (8, '数码', 1, 1, 223423, 1, 1581420086, 1581581595, 2, 'shop_two_class_2.png');
INSERT INTO `cmf_shop_goods_class` VALUES (9, '佳能', 8, 1, 1234, 1, 1581420123, 1581581595, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (11, '索尼', 8, 1, 3234, 1, 1581420243, 1581581595, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (12, '三星', 2, 1, 6, 1, 1581559545, 1581580638, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (13, '电脑办公', 1, 1, 1, 1, 1581559571, 1599217485, 2, 'shop_two_class_3.png');
INSERT INTO `cmf_shop_goods_class` VALUES (14, '华硕', 13, 1, 1, 1, 1581559693, 1599217485, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (15, '戴尔', 13, 1, 1, 1, 1581559874, 1599217485, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (16, '惠普', 13, 1, 1, 1, 1581559886, 1599217485, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (17, '宏碁', 13, 1, 1, 1, 1581559897, 1599217485, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (18, '联想', 13, 1, 1, 1, 1581559911, 1599217485, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (19, '家具/家饰/家纺', 0, 0, 2, 1, 1582271415, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (20, '家具', 19, 19, 1, 1, 1582271459, 0, 2, 'shop_two_class_4.png');
INSERT INTO `cmf_shop_goods_class` VALUES (21, '布艺软饰', 19, 19, 2, 1, 1582271471, 0, 2, 'shop_two_class_5.png');
INSERT INTO `cmf_shop_goods_class` VALUES (22, '床上用品', 19, 19, 3, 1, 1582271491, 0, 2, 'shop_two_class_6.png');
INSERT INTO `cmf_shop_goods_class` VALUES (23, '沙发', 20, 19, 1, 1, 1582271560, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (24, '床', 20, 19, 2, 1, 1582271574, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (25, '电视柜', 20, 19, 3, 1, 1582271588, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (26, '鞋柜', 20, 19, 4, 1, 1582271607, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (27, '窗帘', 21, 19, 1, 1, 1582272244, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (28, '地毯', 21, 19, 2, 1, 1582272254, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (29, '桌布', 21, 19, 3, 1, 1582272265, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (30, '沙发垫', 21, 19, 4, 1, 1582272281, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (31, '四件套', 22, 19, 1, 1, 1582272322, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (32, '空调被', 22, 19, 2, 1, 1582272331, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (33, '夏凉被', 22, 19, 3, 1, 1582272341, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (34, '枕头', 22, 19, 4, 1, 1582272378, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (35, '竹席', 22, 19, 5, 1, 1582272404, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (36, '美食/生鲜/零食', 0, 0, 3, 1, 1582272626, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (37, '美食', 36, 36, 1, 1, 1582272696, 0, 2, 'shop_two_class_7.png');
INSERT INTO `cmf_shop_goods_class` VALUES (38, '生鲜', 36, 36, 2, 1, 1582272705, 0, 2, 'shop_two_class_8.png');
INSERT INTO `cmf_shop_goods_class` VALUES (39, '零食', 36, 36, 3, 1, 1582272715, 0, 2, 'shop_two_class_9.png');
INSERT INTO `cmf_shop_goods_class` VALUES (40, '牛奶', 37, 36, 1, 1, 1582272837, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (41, '红茶', 37, 36, 2, 1, 1582272847, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (42, '绿茶', 37, 36, 2, 1, 1582272857, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (43, '黑茶', 37, 36, 3, 1, 1582272868, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (44, '荔枝', 38, 36, 1, 1, 1582272950, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (45, '芒果', 38, 36, 2, 1, 1582272959, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (46, '樱桃', 38, 36, 3, 1, 1582272968, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (47, '小龙虾', 38, 36, 4, 1, 1582272994, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (48, '三文鱼', 38, 36, 5, 1, 1582273003, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (49, '零食大礼包', 39, 36, 1, 1, 1582273055, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (50, '面包', 39, 36, 2, 1, 1582273064, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (51, '巧克力', 39, 36, 3, 1, 1582273093, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (52, '鱼干', 39, 36, 4, 1, 1582273115, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (53, '女鞋/箱包/钟表/珠宝', 0, 0, 4, 1, 1582772109, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (54, '精品女鞋', 53, 53, 1, 1, 1582772122, 1582772266, 2, 'shop_two_class_10.png');
INSERT INTO `cmf_shop_goods_class` VALUES (55, '潮流女包', 53, 53, 2, 1, 1582772155, 0, 2, 'shop_two_class_11.png');
INSERT INTO `cmf_shop_goods_class` VALUES (56, '精品男包', 53, 53, 3, 1, 1582772177, 0, 2, 'shop_two_class_12.png');
INSERT INTO `cmf_shop_goods_class` VALUES (57, '功能箱包', 53, 53, 4, 1, 1582772204, 0, 2, 'shop_two_class_13.png');
INSERT INTO `cmf_shop_goods_class` VALUES (58, '钟表', 53, 53, 5, 1, 1582772232, 0, 2, 'shop_two_class_14.png');
INSERT INTO `cmf_shop_goods_class` VALUES (59, '珠宝首饰', 53, 53, 6, 1, 1582772248, 0, 2, 'shop_two_class_15.png');
INSERT INTO `cmf_shop_goods_class` VALUES (60, '马丁靴', 54, 53, 1, 1, 1582772311, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (61, '高跟鞋', 54, 53, 2, 1, 1582772323, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (62, '帆布鞋', 54, 53, 3, 1, 1582772346, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (63, '松糕鞋', 54, 53, 4, 1, 1582772416, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (64, '真皮包', 55, 53, 1, 1, 1582772438, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (65, '单肩包', 55, 53, 2, 1, 1582772449, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (66, '斜挎包', 55, 53, 3, 1, 1582772460, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (67, '钱包', 55, 53, 4, 1, 1582772479, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (68, '手拿包', 55, 53, 5, 1, 1582772488, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (69, '钥匙包', 55, 53, 6, 1, 1582772505, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (70, '男士钱包', 56, 53, 1, 1, 1582772539, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (71, '双肩包', 56, 53, 2, 1, 1582772568, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (72, '单肩/斜挎包', 56, 53, 3, 1, 1582772590, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (73, '商务公文包', 56, 53, 4, 1, 1582772614, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (74, '拉杆箱', 57, 53, 1, 1, 1582772654, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (75, '旅行包', 57, 53, 2, 1, 1582772664, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (76, '电脑包', 57, 53, 3, 1, 1582772674, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (77, '登山包', 57, 53, 4, 1, 1582772699, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (78, '休闲运动包', 57, 53, 5, 1, 1582772722, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (79, '天梭', 58, 53, 1, 1, 1582772745, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (80, '浪琴', 58, 53, 2, 1, 1582772760, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (81, '欧米茄', 58, 53, 3, 1, 1582772770, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (82, '卡西欧', 58, 53, 4, 1, 1582772790, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (83, '天王', 58, 53, 5, 1, 1582772810, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (84, '闹钟', 58, 53, 6, 1, 1582772828, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (85, '挂钟', 58, 53, 7, 1, 1582772838, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (86, '座钟', 58, 53, 8, 1, 1582772852, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (87, '钟表配件', 58, 53, 9, 1, 1582772870, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (88, '黄金', 59, 53, 1, 1, 1582772908, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (89, '钻石', 59, 53, 2, 1, 1582772917, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (90, '翡翠玉石', 59, 53, 3, 1, 1582772928, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (91, '水晶玛瑙', 59, 53, 4, 1, 1582772950, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (92, '手串/把件', 59, 53, 5, 1, 1582772978, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (93, '银饰', 59, 53, 6, 1, 1582773002, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (94, '珍珠', 59, 53, 7, 1, 1582773012, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (95, '汽车/汽车用品', 0, 0, 5, 1, 1582773070, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (96, '汽车装饰', 95, 95, 1, 1, 1582773104, 0, 2, 'shop_two_class_16.png');
INSERT INTO `cmf_shop_goods_class` VALUES (97, '车载电器', 95, 95, 2, 1, 1582773118, 0, 2, 'shop_two_class_17.png');
INSERT INTO `cmf_shop_goods_class` VALUES (98, '汽车美容', 95, 95, 3, 1, 1582773147, 0, 2, 'shop_two_class_18.png');
INSERT INTO `cmf_shop_goods_class` VALUES (99, '随车用品', 95, 95, 4, 1, 1582773181, 0, 2, 'shop_two_class_19.png');
INSERT INTO `cmf_shop_goods_class` VALUES (100, '坐垫套装', 96, 95, 1, 1, 1582773212, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (101, '脚垫', 96, 95, 2, 1, 1582773223, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (102, '方向盘套', 96, 95, 3, 1, 1582773246, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (103, '装饰灯', 96, 95, 4, 1, 1582773279, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (104, '车衣', 96, 95, 5, 1, 1582773301, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (105, '雨刮器', 96, 95, 6, 1, 1582773313, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (106, '雨眉', 96, 95, 7, 1, 1582773323, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (107, '行车记录仪', 97, 95, 1, 1, 1582773354, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (108, '车载充电器', 97, 95, 2, 1, 1582773367, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (109, '倒车雷达', 97, 95, 3, 1, 1582773398, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (110, '车载吸尘器', 97, 95, 4, 1, 1582773429, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (111, '应急电源', 97, 95, 5, 1, 1582773454, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (112, '车载电器配件', 97, 95, 6, 1, 1582773472, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (113, '洗车机', 98, 95, 1, 1, 1582773497, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (114, '洗车水枪', 98, 95, 2, 1, 1582773508, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (115, '玻璃水', 98, 95, 3, 1, 1582773519, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (116, '车蜡', 98, 95, 4, 1, 1582773539, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (117, '汽车贴膜', 98, 95, 5, 1, 1582773549, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (118, '底盘装甲', 98, 95, 5, 1, 1582773569, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (119, '补漆笔', 98, 95, 6, 1, 1582773587, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (120, '汽车美容配件', 98, 95, 7, 1, 1582773611, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (121, '灭火器', 99, 95, 1, 1, 1582773638, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (122, '保温杯', 99, 95, 2, 1, 1582773647, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (123, '充气泵', 99, 95, 3, 1, 1582773673, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (124, '车载床', 99, 95, 4, 1, 1582773682, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (125, '储物箱', 99, 95, 5, 1, 1582773706, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (126, '母婴/玩具', 0, 0, 6, 1, 1582773775, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (127, '奶粉', 126, 126, 1, 1, 1582773803, 0, 2, 'shop_two_class_20.png');
INSERT INTO `cmf_shop_goods_class` VALUES (128, '营养辅食', 126, 126, 2, 1, 1582773816, 0, 2, 'shop_two_class_21.png');
INSERT INTO `cmf_shop_goods_class` VALUES (129, '尿不湿', 126, 126, 3, 1, 1582773833, 0, 2, 'shop_two_class_22.png');
INSERT INTO `cmf_shop_goods_class` VALUES (130, '喂养用品', 126, 126, 4, 1, 1582773846, 0, 2, 'shop_two_class_23.png');
INSERT INTO `cmf_shop_goods_class` VALUES (131, '母婴洗护用品', 126, 126, 5, 1, 1582773874, 0, 2, 'shop_two_class_24.png');
INSERT INTO `cmf_shop_goods_class` VALUES (132, '寝居服饰', 126, 126, 5, 1, 1582773907, 0, 2, 'shop_two_class_25.png');
INSERT INTO `cmf_shop_goods_class` VALUES (133, '妈妈专区', 126, 126, 6, 1, 1582773924, 0, 2, 'shop_two_class_26.png');
INSERT INTO `cmf_shop_goods_class` VALUES (134, '童车童床', 126, 126, 7, 1, 1582773943, 0, 2, 'shop_two_class_27.png');
INSERT INTO `cmf_shop_goods_class` VALUES (135, '玩具', 126, 126, 8, 1, 1582773954, 0, 2, 'shop_two_class_28.png');
INSERT INTO `cmf_shop_goods_class` VALUES (136, '1段奶粉', 127, 126, 1, 1, 1582773979, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (137, '2段奶粉', 127, 126, 2, 1, 1582773991, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (138, '3段奶粉', 127, 126, 3, 1, 1582774002, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (139, '4段奶粉', 127, 126, 4, 1, 1582774017, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (140, '特殊配方奶粉', 127, 126, 5, 1, 1582774052, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (141, '米粉/菜粉', 128, 126, 1, 1, 1582774085, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (142, '面条/粥', 128, 126, 2, 1, 1582774099, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (143, '果泥/果汁', 128, 126, 3, 1, 1582774138, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (144, '宝宝零食', 128, 126, 4, 1, 1582774157, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (145, 'NB', 129, 126, 1, 1, 1582774204, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (146, 'S', 129, 126, 2, 1, 1582774213, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (147, 'M', 129, 126, 3, 1, 1582774227, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (148, 'L', 129, 126, 4, 1, 1582774246, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (149, 'XL', 129, 126, 5, 1, 1582774263, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (150, '拉拉裤', 129, 126, 6, 1, 1582774276, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (151, '奶瓶奶嘴', 130, 126, 1, 1, 1582774305, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (152, '吸奶器', 130, 126, 2, 1, 1582774316, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (153, '辅食料理机', 130, 126, 3, 1, 1582774332, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (154, '儿童餐具', 130, 126, 4, 1, 1582774350, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (155, '水壶/水杯', 130, 126, 6, 1, 1582774368, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (156, '牙胶安抚', 130, 126, 7, 1, 1582774396, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (157, '宝宝护肤', 131, 126, 1, 1, 1582774430, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (158, '日常护理', 131, 126, 2, 1, 1582774444, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (159, '洗发沐浴', 131, 126, 3, 1, 1582774459, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (160, '驱蚊防晒', 131, 126, 4, 1, 1582774475, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (161, '理发器', 131, 126, 5, 1, 1582774489, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (162, '洗澡用具', 131, 126, 6, 1, 1582774506, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (163, '婴童睡袋/抱被', 132, 126, 1, 1, 1582774553, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (164, '婴童隔尿垫/巾', 132, 126, 2, 1, 1582774570, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (165, '婴童浴巾/浴衣', 132, 126, 3, 1, 1582774584, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (166, '婴童毛巾/口水巾', 132, 126, 4, 1, 1582774597, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (167, '婴童布尿裤/尿布', 132, 126, 5, 1, 1582774613, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (168, '婴儿内衣', 132, 126, 6, 1, 1582774644, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (169, '爬行垫', 132, 126, 7, 1, 1582774660, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (170, '孕妈装', 133, 126, 1, 1, 1582774710, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (171, '孕妇护肤', 133, 126, 2, 1, 1582774727, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (172, '孕妇内衣', 133, 126, 3, 1, 1582774764, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (173, '防溢乳垫', 133, 126, 4, 1, 1582774788, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (174, '婴儿推车', 134, 126, 1, 1, 1582774839, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (175, '婴儿床', 134, 126, 2, 1, 1582774850, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (176, '餐椅', 134, 126, 3, 1, 1582774871, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (177, '学步车', 134, 126, 4, 1, 1582774882, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (178, '积木', 135, 126, 1, 1, 1582774927, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (179, '芭比娃娃', 135, 126, 2, 1, 1582774937, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (180, '毛绒玩具', 135, 126, 3, 1, 1582774967, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (181, '益智玩具', 135, 126, 4, 1, 1582774984, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (182, '服装/男装/女装', 0, 0, 7, 1, 1585703754, 1585703923, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (183, '女装', 182, 182, 1, 1, 1585703810, 1585703948, 2, 'shop_two_class_29.png');
INSERT INTO `cmf_shop_goods_class` VALUES (184, '卫衣', 183, 182, 1, 1, 1585703834, 1585703967, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (185, '休闲裤', 183, 182, 2, 1, 1585703850, 1585703997, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (186, '男装', 182, 182, 2, 1, 1585704024, 0, 2, 'shop_two_class_30.png');
INSERT INTO `cmf_shop_goods_class` VALUES (187, '运动服', 186, 182, 1, 1, 1585704052, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (188, '西装', 186, 182, 2, 1, 1585704064, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (189, '衬衫', 186, 182, 3, 1, 1585704100, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (190, '连衣裙', 183, 182, 3, 1, 1585704113, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (191, 'T恤', 183, 182, 4, 1, 1585704128, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (192, '时尚套装', 183, 182, 5, 1, 1585704146, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (193, '医药', 0, 0, 8, 1, 1585705240, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (194, '眼药水', 193, 193, 1, 1, 1585705254, 0, 2, 'shop_two_class_31.png');
INSERT INTO `cmf_shop_goods_class` VALUES (195, '口罩', 193, 193, 2, 1, 1585705266, 0, 2, 'shop_two_class_32.png');
INSERT INTO `cmf_shop_goods_class` VALUES (196, 'KN95', 195, 193, 1, 1, 1585709911, 1585721825, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (197, '普通一次医用口罩', 195, 193, 2, 1, 1585709936, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (198, '抗疲劳', 194, 193, 1, 1, 1585709951, 1585721805, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (199, '防近视', 194, 193, 2, 1, 1585709974, 0, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (200, '游戏 / 动漫 / 影视', 0, 0, 9, 1, 1585901648, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (201, '游戏', 200, 200, 1, 1, 1585901690, 1601012289, 2, 'shop_two_class_33.png');
INSERT INTO `cmf_shop_goods_class` VALUES (202, '动漫周边', 200, 200, 2, 1, 1585901704, 0, 2, 'shop_two_class_34.png');
INSERT INTO `cmf_shop_goods_class` VALUES (203, '热门影视周边', 200, 200, 3, 1, 1585901720, 0, 2, 'shop_two_class_35.png');
INSERT INTO `cmf_shop_goods_class` VALUES (204, 'DNF', 201, 200, 1, 1, 1585901741, 1601012289, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (205, '梦幻西游', 201, 200, 2, 1, 1585901748, 1601012289, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (206, ' 魔兽', 201, 200, 3, 1, 1585901759, 1601012289, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (207, 'LOL', 201, 200, 4, 1, 1585901770, 1601012289, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (208, '坦克世界', 201, 200, 5, 1, 1585901783, 1601012289, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (209, '剑网3', 201, 200, 6, 1, 1585901797, 1601012289, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (211, 'DOTA2', 201, 200, 7, 1, 1585901826, 1601012289, 3, '');
INSERT INTO `cmf_shop_goods_class` VALUES (212, '笔记本', 0, 0, 1, 1, 1599286060, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (213, '好好', 0, 0, 7, 1, 1599286121, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (214, '活牛羊', 0, 0, 9999, 1, 1599704752, 0, 1, '');
INSERT INTO `cmf_shop_goods_class` VALUES (215, '活牛', 214, 214, 9999, 1, 1599704971, 0, 2, 'admin/20200910/b36901be194ed5f7c7005f37ab57310b.jpg');
INSERT INTO `cmf_shop_goods_class` VALUES (216, '西门塔尔', 215, 214, 999999, 1, 1599704990, 0, 3, '');

-- ----------------------------
-- Table structure for cmf_shop_order
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_order`;
CREATE TABLE `cmf_shop_order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '购买者ID',
  `shop_uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '卖家用户ID',
  `goodsid` bigint(20) NOT NULL DEFAULT 0 COMMENT '商品id',
  `goods_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商品名称',
  `spec_id` int(11) NOT NULL DEFAULT 0 COMMENT '商品规格ID',
  `spec_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规格名称',
  `spec_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规格封面',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '购买数量',
  `price` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '商品单价',
  `total` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '商品总价（包含邮费）',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '购买者姓名',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '购买者联系电话',
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '国家',
  `country_code` int(11) NOT NULL DEFAULT 0 COMMENT '国家代号',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '购买者省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '购买者市',
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '购买者地区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '购买者详细地址',
  `postage` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '邮费',
  `orderno` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单编号',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '订单类型 1 支付宝 2 微信 3 余额',
  `trade_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方订单号',
  `status` tinyint(2) NOT NULL DEFAULT 0 COMMENT '订单状态  -1 已关闭  0 待付款 1 待发货 2 待收货 3 待评价 4 已评价 5 退款',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '订单添加时间',
  `cancel_time` int(11) NOT NULL DEFAULT 0 COMMENT '订单取消时间',
  `paytime` int(11) NOT NULL DEFAULT 0 COMMENT '订单付款时间',
  `shipment_time` int(11) NOT NULL DEFAULT 0 COMMENT '订单发货时间',
  `receive_time` int(11) NOT NULL DEFAULT 0 COMMENT '订单收货时间',
  `evaluate_time` int(11) NOT NULL DEFAULT 0 COMMENT '订单评价时间',
  `settlement_time` int(11) NOT NULL DEFAULT 0 COMMENT '订单结算时间（款项打给卖家）',
  `is_append_evaluate` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否可追加评价',
  `order_percent` int(11) NOT NULL DEFAULT 0 COMMENT '订单抽成比例',
  `refund_starttime` int(11) NOT NULL DEFAULT 0 COMMENT '订单发起退款时间',
  `refund_endtime` int(11) NOT NULL DEFAULT 0 COMMENT '订单退款处理结束时间',
  `refund_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '退款处理结果 -2取消申请 -1 失败 0 处理中 1 成功 ',
  `refund_shop_result` tinyint(1) NOT NULL DEFAULT 0 COMMENT '退款时卖家处理结果 0 未处理 -1 拒绝 1 同意',
  `express_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '物流公司名称',
  `express_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '物流公司电话',
  `express_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '物流公司图标',
  `express_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递公司对应三方平台的编码',
  `express_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '物流单号',
  `isdel` tinyint(1) NOT NULL DEFAULT 0 COMMENT '订单是否删除 0 否 -1 买家删除 -2 卖家删除 1 买家卖家都删除',
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '买家留言内容',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_uid`(`id`, `uid`) USING BTREE,
  INDEX `shopuid_status`(`shop_uid`, `status`) USING BTREE,
  INDEX `shopuid_status_refundstatus`(`shop_uid`, `status`, `refund_status`) USING BTREE,
  INDEX `id_shopuid`(`id`, `shop_uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_order_comments
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_order_comments`;
CREATE TABLE `cmf_shop_order_comments`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `orderid` bigint(20) NOT NULL DEFAULT 0 COMMENT '商品订单ID',
  `goodsid` bigint(20) NOT NULL COMMENT '商品ID',
  `shop_uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '店铺用户id',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文字内容',
  `thumbs` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '评论图片列表',
  `video_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '视频封面',
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '视频地址',
  `is_anonym` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否匿名 0否 1是',
  `quality_points` tinyint(1) NOT NULL DEFAULT 0 COMMENT '商品描述评分',
  `service_points` tinyint(1) NOT NULL DEFAULT 0 COMMENT '服务态度评分',
  `express_points` tinyint(1) NOT NULL DEFAULT 0 COMMENT '物流速度评分',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `is_append` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是追评0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goodsid_isappend`(`goodsid`, `is_append`) USING BTREE,
  INDEX `uid_orderid`(`uid`, `orderid`, `is_append`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_order_message
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_order_message`;
CREATE TABLE `cmf_shop_order_message`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '消息内容',
  `orderid` bigint(20) NOT NULL DEFAULT 0,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '接受消息用户ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '用户身份 0买家 1卖家',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_order_refund
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_order_refund`;
CREATE TABLE `cmf_shop_order_refund`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '买家id',
  `orderid` bigint(20) NOT NULL DEFAULT 0 COMMENT '订单ID',
  `goodsid` bigint(20) NOT NULL DEFAULT 0 COMMENT '商品ID',
  `shop_uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '商家ID',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款原因',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款说明',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款图片（废弃）',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '退款类型 0 仅退款 1退货退款',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '申请时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  `shop_process_time` int(11) NOT NULL DEFAULT 0 COMMENT '店铺处理时间',
  `shop_result` tinyint(1) NOT NULL DEFAULT 0 COMMENT '店铺处理结果 -1 拒绝 0 处理中 1 同意',
  `shop_process_num` tinyint(1) NOT NULL DEFAULT 0 COMMENT '店铺驳回次数',
  `platform_process_time` int(11) NOT NULL DEFAULT 0 COMMENT '平台处理时间',
  `platform_result` tinyint(1) NOT NULL DEFAULT 0 COMMENT '平台处理结果 -1 拒绝 0 处理中 1 同意',
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '平台处理账号',
  `ip` bigint(20) NOT NULL DEFAULT 0 COMMENT '平台账号ip',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '退款处理状态 0 处理中 -1 买家已取消 1 已完成 ',
  `is_platform_interpose` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否平台介入 0 否 1 是',
  `system_process_time` int(11) NOT NULL DEFAULT 0 COMMENT '系统自动处理时间',
  `platform_interpose_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '申请平台介入的理由',
  `platform_interpose_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '申请平台介入的详细原因',
  `platform_interpose_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '申请平台介入的图片举证',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid_orderid`(`uid`, `orderid`) USING BTREE,
  INDEX `orderid_shopuid`(`orderid`, `shop_uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_order_refund_list
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_order_refund_list`;
CREATE TABLE `cmf_shop_order_refund_list`  (
  `orderid` bigint(11) NOT NULL DEFAULT 0 COMMENT '订单ID',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '处理方 1 买家 2 卖家 3 平台 4 系统',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '处理时间',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '处理说明',
  `handle_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '处理备注说明',
  `refuse_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '卖家拒绝理由'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_platform_reason
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_platform_reason`;
CREATE TABLE `cmf_shop_platform_reason`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原因名称',
  `list_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0不显示 1 显示',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_shop_platform_reason
-- ----------------------------
INSERT INTO `cmf_shop_platform_reason` VALUES (1, '卖家未履行约定', 1, 1, 1584774096, 1589785484);
INSERT INTO `cmf_shop_platform_reason` VALUES (2, '商品质量存在问题', 2, 1, 1584774114, 0);
INSERT INTO `cmf_shop_platform_reason` VALUES (3, '卖家态度蛮横无理', 3, 1, 1584774131, 0);
INSERT INTO `cmf_shop_platform_reason` VALUES (4, '其它', 4, 1, 1589785536, 0);

-- ----------------------------
-- Table structure for cmf_shop_points
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_points`;
CREATE TABLE `cmf_shop_points`  (
  `shop_uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '店铺用户ID',
  `evaluate_total` bigint(20) NOT NULL DEFAULT 0 COMMENT '评价总数',
  `quality_points_total` int(11) NOT NULL DEFAULT 0 COMMENT '店铺商品质量(商品描述)总分',
  `service_points_total` int(11) NOT NULL DEFAULT 0 COMMENT '店铺服务态度总分',
  `express_points_total` int(11) NOT NULL DEFAULT 0 COMMENT '物流服务总分'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_shop_refund_reason
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_refund_reason`;
CREATE TABLE `cmf_shop_refund_reason`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原因名称',
  `list_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0不显示 1 显示',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_shop_refund_reason
-- ----------------------------
INSERT INTO `cmf_shop_refund_reason` VALUES (1, '拍错/拍多/不想要了', 1, 1, 1584430567, 1584432392);
INSERT INTO `cmf_shop_refund_reason` VALUES (2, '卖家未按约定时间发货', 2, 1, 1584430600, 0);
INSERT INTO `cmf_shop_refund_reason` VALUES (3, '其他', 4, 1, 1584431428, 0);
INSERT INTO `cmf_shop_refund_reason` VALUES (4, '存在质量问题', 3, 1, 1586829690, 0);
INSERT INTO `cmf_shop_refund_reason` VALUES (5, '7天无理由退款', 0, 1, 1586829705, 0);

-- ----------------------------
-- Table structure for cmf_shop_refuse_reason
-- ----------------------------
DROP TABLE IF EXISTS `cmf_shop_refuse_reason`;
CREATE TABLE `cmf_shop_refuse_reason`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原因名称',
  `list_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0不显示 1 显示',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `edittime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_shop_refuse_reason
-- ----------------------------
INSERT INTO `cmf_shop_refuse_reason` VALUES (1, '买家未举证/举证无效', 1, 1, 1584698435, 1584699310);
INSERT INTO `cmf_shop_refuse_reason` VALUES (2, '收到退货后再退款', 2, 1, 1584698538, 0);
INSERT INTO `cmf_shop_refuse_reason` VALUES (3, '已发货,请买家承担运费', 3, 1, 1584698558, 0);
INSERT INTO `cmf_shop_refuse_reason` VALUES (4, '商品损坏', 0, 1, 1586829756, 0);

-- ----------------------------
-- Table structure for cmf_slide
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide`;
CREATE TABLE `cmf_slide`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:显示,0不显示',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片分类',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '幻灯片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_slide
-- ----------------------------
INSERT INTO `cmf_slide` VALUES (1, 1, 0, 'PC首页轮播', '不可删除');
INSERT INTO `cmf_slide` VALUES (2, 1, 0, 'APP首页轮播', '不可删除');
INSERT INTO `cmf_slide` VALUES (3, 1, 1596874182, ' ', '');
INSERT INTO `cmf_slide` VALUES (4, 1, 1596878259, ' ', '');
INSERT INTO `cmf_slide` VALUES (5, 1, 0, 'APP商城轮播', '不可删除');

-- ----------------------------
-- Table structure for cmf_slide_item
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide_item`;
CREATE TABLE `cmf_slide_item`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slide_id` int(11) NOT NULL DEFAULT 0 COMMENT '幻灯片id',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:显示;0:隐藏',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片图片',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片链接',
  `target` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接打开方式',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片描述',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '幻灯片内容',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '扩展信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `slide_id`(`slide_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '幻灯片子项表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_slide_item
-- ----------------------------
INSERT INTO `cmf_slide_item` VALUES (6, 2, 1, 10000, '1', 'admin/20201109/5b85250262fbc4f8c9c0740606ac5db7.png', '', '', '', '', NULL);
INSERT INTO `cmf_slide_item` VALUES (7, 2, 1, 10000, '2', 'admin/20201109/9ec597f61c1376477f488a19539494fe.png', '', '', '', '', NULL);
INSERT INTO `cmf_slide_item` VALUES (8, 2, 1, 10000, '3', 'admin/20201109/825abc0efac507988c5709c5567727b8.png', '', '', '', '', NULL);
INSERT INTO `cmf_slide_item` VALUES (9, 1, 1, 10000, '4', 'admin/20201109/4d73087899b40a53c3d6db143f78a691.png', '', '', '', '', NULL);
INSERT INTO `cmf_slide_item` VALUES (10, 1, 1, 10000, '5', 'admin/20201109/5304d1363812646564925ed7e49afcfb.png', '', '', '', '', NULL);
INSERT INTO `cmf_slide_item` VALUES (11, 1, 1, 10000, '6', 'admin/20201109/fc6e09aff6ad0912733f87c17027bbdf.png', '', '', '', '', NULL);

-- ----------------------------
-- Table structure for cmf_theme
-- ----------------------------
DROP TABLE IF EXISTS `cmf_theme`;
CREATE TABLE `cmf_theme`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '安装时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后升级时间',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模板状态,1:正在使用;0:未使用',
  `is_compiled` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为已编译模板',
  `theme` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题目录名，用于主题的维一标识',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题名称',
  `version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题版本号',
  `demo_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '演示地址，带协议',
  `thumbnail` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '缩略图',
  `author` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题作者',
  `author_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '作者网站链接',
  `lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '支持语言',
  `keywords` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题关键字',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_theme
-- ----------------------------
INSERT INTO `cmf_theme` VALUES (1, 0, 0, 0, 0, 'default', 'default', '1.0.0', 'http://demo.thinkcmf.com', '', 'ThinkCMF', 'http://www.thinkcmf.com', 'zh-cn', 'ThinkCMF默认模板', 'ThinkCMF默认模板');

-- ----------------------------
-- Table structure for cmf_theme_file
-- ----------------------------
DROP TABLE IF EXISTS `cmf_theme_file`;
CREATE TABLE `cmf_theme_file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_public` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否公共的模板文件',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `theme` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板名称',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板文件名',
  `action` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作',
  `file` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板文件，相对于模板根目录，如Portal/index.html',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板文件描述',
  `more` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '模板更多配置,用户自己后台设置的',
  `config_more` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '模板更多配置,来源模板的配置文件',
  `draft_more` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '模板更多配置,用户临时保存的配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_turntable
-- ----------------------------
DROP TABLE IF EXISTS `cmf_turntable`;
CREATE TABLE `cmf_turntable`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型，0无奖1钻石2礼物',
  `type_val` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '类型值',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片',
  `rate` decimal(10, 3) NOT NULL DEFAULT 0.000 COMMENT '中奖率',
  `uptime` bigint(20) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_turntable
-- ----------------------------
INSERT INTO `cmf_turntable` VALUES (1, 1, '201', '', 12.230, 1604320180);
INSERT INTO `cmf_turntable` VALUES (2, 2, '5', '', 10.000, 1600165609);
INSERT INTO `cmf_turntable` VALUES (3, 1, '5000', '', 10.000, 1600165620);
INSERT INTO `cmf_turntable` VALUES (4, 2, '28', '', 10.000, 1600165627);
INSERT INTO `cmf_turntable` VALUES (5, 2, '74', '', 10.000, 1600166128);
INSERT INTO `cmf_turntable` VALUES (6, 3, '水晶鞋', '', 10.000, 1600165643);
INSERT INTO `cmf_turntable` VALUES (7, 0, '0', '', 0.000, 1594864893);
INSERT INTO `cmf_turntable` VALUES (8, 2, '19', '', 10.000, 1600165656);

-- ----------------------------
-- Table structure for cmf_turntable_con
-- ----------------------------
DROP TABLE IF EXISTS `cmf_turntable_con`;
CREATE TABLE `cmf_turntable_con`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `times` int(11) NOT NULL DEFAULT 0 COMMENT '次数',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '价格',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_turntable_con
-- ----------------------------
INSERT INTO `cmf_turntable_con` VALUES (1, 4, 108, -98);
INSERT INTO `cmf_turntable_con` VALUES (2, 10, 102, 99);
INSERT INTO `cmf_turntable_con` VALUES (3, 100, 1000, 9999);

-- ----------------------------
-- Table structure for cmf_turntable_log
-- ----------------------------
DROP TABLE IF EXISTS `cmf_turntable_log`;
CREATE TABLE `cmf_turntable_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `liveuid` bigint(20) NOT NULL DEFAULT 0 COMMENT '主播ID',
  `showid` bigint(20) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '价格',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  `addtime` bigint(20) NOT NULL DEFAULT 0 COMMENT '时间',
  `iswin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否中奖',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_turntable_log
-- ----------------------------
INSERT INTO `cmf_turntable_log` VALUES (1, 42516, 42520, 1604468385, 108, 4, 1604468789, 1);
INSERT INTO `cmf_turntable_log` VALUES (2, 42516, 42520, 1604468385, 108, 4, 1604468806, 1);
INSERT INTO `cmf_turntable_log` VALUES (3, 42516, 42520, 1604468385, 108, 4, 1604468814, 1);
INSERT INTO `cmf_turntable_log` VALUES (4, 42516, 42520, 1604468385, 108, 4, 1604468829, 1);
INSERT INTO `cmf_turntable_log` VALUES (5, 42516, 42520, 1604468385, 108, 4, 1604468912, 1);
INSERT INTO `cmf_turntable_log` VALUES (6, 42516, 42520, 1604468385, 108, 4, 1604468923, 1);
INSERT INTO `cmf_turntable_log` VALUES (7, 42516, 42520, 1604468385, 108, 4, 1604468942, 1);
INSERT INTO `cmf_turntable_log` VALUES (8, 42488, 1, 1604129295, 108, 4, 1604512125, 1);

-- ----------------------------
-- Table structure for cmf_turntable_win
-- ----------------------------
DROP TABLE IF EXISTS `cmf_turntable_win`;
CREATE TABLE `cmf_turntable_win`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `logid` bigint(20) NOT NULL DEFAULT 0 COMMENT '转盘记录ID',
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型，0无奖1钻石2礼物',
  `type_val` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '类型值',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  `addtime` bigint(20) NOT NULL DEFAULT 0 COMMENT '时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '处理状态，0未处理1已处理',
  `uptime` bigint(20) NOT NULL DEFAULT 0 COMMENT '处理时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_turntable_win
-- ----------------------------
INSERT INTO `cmf_turntable_win` VALUES (1, 1, 42516, 2, '74', '', 1, 1604468789, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (2, 1, 42516, 1, '201', '', 1, 1604468789, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (3, 1, 42516, 1, '5000', '', 1, 1604468789, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (4, 2, 42516, 1, '5000', '', 2, 1604468806, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (5, 2, 42516, 2, '19', '', 1, 1604468806, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (6, 3, 42516, 1, '201', '', 1, 1604468814, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (7, 3, 42516, 2, '19', '', 1, 1604468814, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (8, 3, 42516, 2, '5', '', 1, 1604468814, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (9, 4, 42516, 2, '28', '', 1, 1604468829, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (10, 4, 42516, 1, '201', '', 2, 1604468829, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (11, 4, 42516, 3, '水晶鞋', '', 1, 1604468829, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (12, 5, 42516, 2, '19', '', 2, 1604468912, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (13, 5, 42516, 2, '28', '', 1, 1604468912, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (14, 6, 42516, 2, '74', '', 1, 1604468923, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (15, 6, 42516, 2, '19', '', 1, 1604468923, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (16, 7, 42516, 1, '201', '', 1, 1604468942, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (17, 7, 42516, 1, '5000', '', 2, 1604468942, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (18, 8, 42488, 2, '19', '', 1, 1604512125, 0, 0);
INSERT INTO `cmf_turntable_win` VALUES (19, 8, 42488, 2, '74', '', 1, 1604512125, 0, 0);

-- ----------------------------
-- Table structure for cmf_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user`;
CREATE TABLE `cmf_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '用户类型;1:admin;2:会员',
  `sex` tinyint(2) NOT NULL DEFAULT 0 COMMENT '性别;0:保密,1:男,2:女',
  `birthday` int(11) NOT NULL DEFAULT 0 COMMENT '生日',
  `last_login_time` int(11) NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `score` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户积分',
  `coin` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '金币',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `user_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '用户状态;0:禁用,1:正常,2:未验证',
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pass` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录密码;cmf_password加密',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户登录邮箱',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户个人网址',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `avatar_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '小头像',
  `signature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '个性签名',
  `last_login_ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `user_activation_key` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '激活码',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '中国手机不带国家代码，国际手机号格式为：国家代码-手机号',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '扩展属性',
  `consumption` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '消费总额',
  `votes` decimal(20, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '映票余额',
  `votestotal` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '映票总额',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '城市',
  `isrecommend` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 未推荐 1 推荐',
  `openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方标识',
  `login_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'phone' COMMENT '注册方式',
  `iszombie` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启僵尸粉',
  `isrecord` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开起回放',
  `iszombiep` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否僵尸粉',
  `issuper` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否超管',
  `ishot` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否热门显示',
  `goodnum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '当前装备靓号',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'pc' COMMENT '注册来源',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所在地',
  `end_bantime` bigint(20) NOT NULL DEFAULT 0 COMMENT '禁用到期时间',
  `balance` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '用户商城人民币账户金额',
  `balance_total` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '用户商城累计收入人民币',
  `balance_consumption` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '用户商城累计消费人民币',
  `recommend_time` int(1) NOT NULL DEFAULT 0 COMMENT '推荐时间',
  `cert_score` int(11) DEFAULT 0 COMMENT '主播推荐值',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_login`(`user_login`) USING BTREE,
  INDEX `user_nicename`(`user_nicename`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42621 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user
-- ----------------------------
INSERT INTO `cmf_user` VALUES (1, 1, 0, -28800, 1612243677, 0, 0, 1571800517, 1, 'admin', '###66bc82ebde1af7c7a129adf2a2eb4f57', 'admin', 'admin@163.com', '', '', '', '', '127.0.0.1', '', '', NULL, 0, 2030.00, 2034, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42486, 2, 0, 0, 1604057517, 0, 30, 1604057517, 1, '13800000002', '###a4210ade7a5c99c2b5b6902231406cbf', '手机用户0002', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13800000002', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42487, 2, 0, 0, 1604470831, 0, 150, 1604058211, 1, '13632537616', '###cb7c5a8e8e84ad985795f93a501163c4', '手机用户7616', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13632537616', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42488, 2, 0, 0, 1605804457, 0, 18446744073709551526, 1604059740, 1, '13366669999', '###6c038ff1d5df721a56d42bdbf033baec', 'irfan', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '18.157.131.237', '', '13366669999', NULL, 109, 0.00, 0, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 1, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1604060277, 100);
INSERT INTO `cmf_user` VALUES (42489, 2, 1, 0, 1604289821, 0, 0, 1604164493, 1, '15888888888', '###d524b857fe478a4532e62bdc0f6bdbdb', '15888888888', '', '', 'user/20201101/2bedfcea6338a36bb436c0efdf8014c3.jpg', 'user/20201101/69d064ec72cc964790a914bdff40cb67.jpg', '15888888888', '180.191.158.49', '', '15888888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42490, 2, 1, 0, 1604302140, 0, 0, 1604164950, 1, '15999999999', '###d524b857fe478a4532e62bdc0f6bdbdb', '15999999999', '', '', 'user/20201101/c3cc807c20a28edbcfed1e731c2e2166.jpg', 'user/20201101/f0b3cbe5041661e7312f83ef0bcadddb.jpg', '15999999999', '122.10.38.132', '', '15999999999', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42491, 1, 0, -28800, 1605850517, 0, 0, 0, 1, 'regame', '###1dec74e2a99cd4e2e253975538219738', '管理员regame', 'weeds.test@gmail.com', '', '', '', '', '112.207.100.99', '', '', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42492, 2, 1, 1567353600, 1605684242, 0, 990, 1604292637, 1, '13312345678', '###6c038ff1d5df721a56d42bdbf033baec', 'rgm001', '', '', 'home/20201102/d6acc10ab4aad07fd8dcac5a0b05b88f.jpg?imageView2/2/w/600/h/600', 'user/20201106/37cf7b231c9a69c2b7cd4e60406f37c8.jpg', '你二大爷', '122.10.38.132', '', '13312345678', NULL, 12020, 11480.00, 11515, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 1, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1604571865, 100);
INSERT INTO `cmf_user` VALUES (42493, 2, 0, 0, 1604293909, 0, 0, 1604293883, 1, '13899999999', '###f3d0df7cb0654f368c65cacdad0b0199', 'WEB用户9999', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13899999999', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 1605024000, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42494, 1, 0, 0, 1605848898, 0, 0, 0, 1, 'messi', '###f3d0df7cb0654f368c65cacdad0b0199', '', 'messiyong@gmail.com', '', '', '', '', '112.207.107.190', '', '', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42496, 2, 1, 1288713600, 1605775099, 0, 210, 1604298526, 1, '13811111112', '###a4210ade7a5c99c2b5b6902231406cbf', '彭彭～彭于晏', '', '', 'http://7niuobs.jiextx.com/20201104194808_e353b048c1661b8e03e7b3cf4a63c292?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201104194808_e353b048c1661b8e03e7b3cf4a63c292?imageView2/2/w/200/h/200', '我是彭于晏 你是谁', '13.57.45.205', '', '13811111112', NULL, 0, 30.00, 30, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '上海市上海市徐汇区', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42497, 2, 0, 0, 0, 0, 0, 1604298948, 1, '13600000001', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0001', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13600000001', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42498, 2, 0, 0, 0, 0, 0, 1604298971, 1, '13600000002', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0002', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13600000002', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42499, 2, 0, 0, 1605848839, 0, 10, 1604300358, 1, '13500000001', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0001', '', '', 'https://7niuobs.jiextx.com/20201119135748_128b8b755772b27f900b78649b5f9373?imageView2/2/w/600/h/600', 'https://7niuobs.jiextx.com/20201119135748_128b8b755772b27f900b78649b5f9373?imageView2/2/w/200/h/200', '这家伙很懒，什么都没留下', '18.166.25.109', '', '13500000001', NULL, 0, 40.00, 40, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42500, 2, 0, 0, 0, 0, 0, 1604300456, 1, '13600000003', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0003', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13600000003', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 1604246400, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42501, 2, 0, 0, 1605849136, 0, 0, 1604301535, 1, '13500000003', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0003', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.107.190', '', '13500000003', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42502, 2, 0, 946656000, 1604392199, 0, 0, 1604301585, 1, '13854575884', '###86f77245b6db18127de555124019e110', 'PAGANI', '', '', 'home/20201102/07c2efc2a9a470add114ae355c5aa7f4.jpg?imageView2/2/w/600/h/600', 'home/20201102/07c2efc2a9a470add114ae355c5aa7f4.jpg?imageView2/2/w/200/h/200', '游山恋', '112.207.98.233', '', '13854575884', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42503, 2, 0, 0, 1605843637, 0, 0, 1604301754, 1, '13500000004', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0004', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.107.190', '', '13500000004', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42504, 2, 0, 0, 1605177675, 0, 0, 1604301807, 1, '13500000005', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0005', '', '', 'http://7niuobs.jiextx.com/20201110150446_fdf3897ae4c9d75e7cc8f3c5bc2d1ab5?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201110150446_fdf3897ae4c9d75e7cc8f3c5bc2d1ab5?imageView2/2/w/200/h/200', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000005', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42505, 2, 0, 0, 1605774395, 0, 10, 1604302012, 1, '13500000006', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0006', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.107.190', '', '13500000006', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 1604419200, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42506, 2, 0, 0, 1604897769, 0, 0, 1604304703, 1, '15777777777', '###23f339a6edb8bf76c62b6b58ec6ab408', 'WEB用户7777', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '15777777777', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42507, 2, 0, 0, 1604304880, 0, 0, 1604304880, 1, '18888888888', '###6fc6d778cf2ccf39d64c8bcff6eec636', 'WEB用户8888', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '18888888888', NULL, 0, 10.00, 10, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42508, 2, 0, 0, 1604304942, 0, 0, 1604304942, 1, '17121570695', '###6fc6d778cf2ccf39d64c8bcff6eec636', 'WEB用户0695', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '17121570695', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42509, 2, 0, 0, 1604482107, 0, 0, 1604305189, 1, '13812345678', '###6fc6d778cf2ccf39d64c8bcff6eec636', 'WEB用户5678', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13812345678', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42510, 2, 0, 0, 1604305583, 0, 0, 1604305583, 1, '18812345678', '###6fc6d778cf2ccf39d64c8bcff6eec636', 'WEB用户5678', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '18812345678', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42511, 2, 0, 0, 1604485993, 0, 0, 1604305824, 1, '13212345678', '###6fc6d778cf2ccf39d64c8bcff6eec636', 'WEB用户5678', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13212345678', NULL, 0, 1.00, 1, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42512, 1, 0, 0, 1605789690, 0, 0, 0, 1, 'kane', '###1dec74e2a99cd4e2e253975538219738', '', 'kane@gmail.com', '', '', '', '', '112.207.107.190', '', '', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42513, 2, 1, 1604332800, 1605851078, 0, 220, 1604318667, 1, '18888888881', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'a111111', '', '', 'user/20201102/169a88fc1616b7b445729a971500224a.jpg', 'user/20201102/7d171baffc84a935e126a63271c3f672.jpg', '22222', '3.122.140.118', '', '18888888881', NULL, 12810, 5800.80, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 1, 0, 1, '88888', 'pc', '上海市上海市黄浦区', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42514, 2, 2, 1603641600, 1604734272, 0, 149, 1604319003, 1, '13900000000', '###cea4745a900c9de8632a25c2b89c1051', '足球0000', '', '', 'http://7niuobs.jiextx.com/20201106194149_a933b2c8ba2790f9f2be24ba8fb3cd59?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201106194149_a933b2c8ba2790f9f2be24ba8fb3cd59?imageView2/2/w/200/h/200', '这家伙很懒，什么都', '112.207.103.190', '', '13900000000', NULL, 11, 13505.20, 18943, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '上海市上海市黄浦区', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42515, 2, 2, 1604851200, 1605701108, 0, 88834, 1604319507, 1, '13012340001', '###6c038ff1d5df721a56d42bdbf033baec', 'rgm002', '', '', 'home/20201109/7700befcc0cbd46e859ab0b68c2452bc.jpg?imageView2/2/w/600/h/600', 'home/20201109/7700befcc0cbd46e859ab0b68c2452bc.jpg?imageView2/2/w/200/h/200', '大波波美女要直播。嘻嘻嘻', '112.207.107.232', '', '13012340001', NULL, 11176, 907.00, 907, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1604719941, 100);
INSERT INTO `cmf_user` VALUES (42516, 2, 2, 0, 1605849652, 0, 16349, 1604374115, 1, '18888888882', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'a22222', '', '', 'http://7niuobs.jiextx.com/20201104194506_78f99e6cc06dbdf2491beaefe61d349c?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201104194506_78f99e6cc06dbdf2491beaefe61d349c?imageView2/2/w/200/h/200', '333', '13.57.45.205', '', '18888888882', NULL, 10666, 2560.00, 3010, '', '好像在火星', 0, '', 'phone', 1, 0, 1, 1, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42517, 2, 0, 0, 1604394642, 0, 60, 1604377603, 1, '13632537617', '###cb7c5a8e8e84ad985795f93a501163c4', '手机用户7617', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13632537617', NULL, 0, 5.00, 5, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42518, 2, 1, 0, 1605704621, 0, 0, 1604380202, 1, '18888888883', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'a333333', '', '', 'user/20201103/9f177cab4d1c5b3d4fa7ecccb88dea54.jpg', 'user/20201103/9825d9e42bfff6c36a6d6a57bc1c6728.jpg', '', '13.57.45.205', '', '18888888883', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42519, 2, 1, 0, 1604392402, 0, 100, 1604382735, 1, '18811111111', '###ea1e6729b1bcd5575033baf0e8cd127b', '呼呼', '', '', '/default.jpg', '/default_thumb.jpg', '', '119.183.140.166', '', '18811111111', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42520, 2, 1, 0, 1604727796, 0, 2725, 1604383450, 1, '18888888884', '###a490cfab34568c4bf5fa6a2f5a072e7f', '会员一推会员四', '', '', '/default.jpg', '/default_thumb.jpg', '', '112.207.103.190', '', '18888888884', NULL, 400, 1770.00, 2330, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42521, 2, 0, 1604851200, 1605841365, 0, 100, 1604391139, 1, '13500000000', '###f3d0df7cb0654f368c65cacdad0b0199', '1350000', '', '', 'home/20201118/a93448a0eb632824dd79d03c53277d9d.png?imageView2/2/w/600/h/600', 'home/20201118/a93448a0eb632824dd79d03c53277d9d.png?imageView2/2/w/200/h/200', '欧洲杯', '18.166.25.109', '', '13500000000', NULL, 10, 80.00, 80, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '北京市北京市东城区', 0, 0.00, 0.00, 0.00, 0, 1200);
INSERT INTO `cmf_user` VALUES (42522, 2, 0, 0, 1604392603, 0, 10, 1604392603, 1, '17694288417', '###ee07437a3d0888f9806016f87bbf6e86', '手机用户8417', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '119.181.183.92', '', '17694288417', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42523, 2, 0, 0, 1605845708, 0, 70, 1604397848, 1, '13632537620', '###cb7c5a8e8e84ad985795f93a501163c4', '手机用户7620', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '18.157.131.237', '', '13632537620', NULL, 0, 700.00, 701, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42524, 2, 0, 0, 1605846832, 0, 95, 1604401457, 1, '13632537621', '###cb7c5a8e8e84ad985795f93a501163c4', '手机用户7621', '', '', 'http://7niuobs.jiextx.com/20201103205803_249fd02410787147a950d912013abac5?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201103205803_249fd02410787147a950d912013abac5?imageView2/2/w/200/h/200', '这家伙很懒，什么都没留下', '18.162.90.124', '', '13632537621', NULL, 5, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42525, 2, 0, 0, 1604407110, 0, 0, 1604407110, 1, '15862678984', '###d0ed4f5f1574c1f708cbca4aaf83dd7d', '手机用户8984', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '15862678984', NULL, 30, 0.00, 0, '', '马卡蒂', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42526, 2, 0, 0, 1604471470, 0, 0, 1604471470, 1, '18231231231', '###805061f6d9219e4273e55c90f2da4f0f', 'WEB用户1231', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.206.124.141', '', '18231231231', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42527, 2, 0, 0, 1604478480, 0, 10, 1604478480, 1, '15842316987', '###cc35a234990642bed5f4ecdc92122976', '手机用户6987', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '15842316987', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42528, 2, 2, 0, 1604582133, 0, 60, 1604478643, 1, '15832336161', '###49d247c1361d71e421b46066d90bf37e', '么么哒', '', '', 'http://7niuobs.jiextx.com/20201104164407_be7065eac6ac2a6aa5e73aa4ea143d0c?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201104164407_be7065eac6ac2a6aa5e73aa4ea143d0c?imageView2/2/w/200/h/200', '么么哒', '180.191.159.193', '', '15832336161', NULL, 0, 0.00, 0, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 1604487582, 100);
INSERT INTO `cmf_user` VALUES (42529, 2, 0, 0, 1605002958, 0, 90, 1604478647, 1, '15928718292', '###e28fb3b5b376b00e23c4e0dad953aa93', '手机用户8292', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '15928718292', NULL, 10, 40.00, 70, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42530, 2, 0, 0, 1604479300, 0, 10, 1604479300, 1, '15572573698', '###4a0155d6f008f9629fe8ab90ab1b3843', '手机用户3698', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '15572573698', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42531, 2, 0, 0, 1604736364, 0, 68, 1604490743, 1, '13800000000', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0000', '', '', 'http://7niuobs.jiextx.com/20201106131741_cdaeb0b757bee3dd64e8bbb7da11aa89?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201106131741_cdaeb0b757bee3dd64e8bbb7da11aa89?imageView2/2/w/200/h/200', '66666', '112.207.103.190', '', '13800000000', NULL, 2, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42532, 2, 0, 0, 1604490750, 0, 0, 1604490749, 1, '13111111112', '###a4210ade7a5c99c2b5b6902231406cbf', '手机用户1112', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13111111112', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42533, 2, 0, 0, 1604905709, 0, 9999999999998000, 1604490926, 1, '13312345679', '###6c038ff1d5df721a56d42bdbf033baec', 'rgm003', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.232', '', '13312345679', NULL, 2010, 50.00, 50, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1604729379, 100);
INSERT INTO `cmf_user` VALUES (42534, 2, 0, 0, 1604491024, 0, 0, 1604491024, 1, '13311112222', '###6c038ff1d5df721a56d42bdbf033baec', '手机用户2222', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.232', '', '13311112222', NULL, 10, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42535, 1, 0, 0, 1605776477, 0, 0, 0, 1, 'irfan', '###6c038ff1d5df721a56d42bdbf033baec', '', '6561212@qq.com', '', '', '', '', '112.207.107.232', '', '', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42536, 2, 0, 0, 1604492999, 0, 0, 1604492999, 1, '13822222223', '###a4210ade7a5c99c2b5b6902231406cbf', '手机用户2223', '', '', 'http://7niuobs.jiextx.com/20201105123829_d04777ca338911af902d4d6e4434fe2b?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201105123829_d04777ca338911af902d4d6e4434fe2b?imageView2/2/w/200/h/200', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13822222223', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42537, 2, 0, 0, 1604493111, 0, 0, 1604493111, 1, '17231322323', '###805061f6d9219e4273e55c90f2da4f0f', 'WEB用户2323', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.206.124.141', '', '17231322323', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42538, 2, 2, 0, 1605101284, 0, 30, 1604494643, 1, '16888888888', '###fedf8dab5b4af437d5f704122f3f5a80', '仙女', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.188', '', '16888888888', NULL, 0, 40.00, 40, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 1, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1604498358, 100);
INSERT INTO `cmf_user` VALUES (42539, 2, 1, 0, 1604992315, 0, 30, 1604495844, 1, '17777777777', '###6c038ff1d5df721a56d42bdbf033baec', 'eren123', '', '', 'user/20201104/ab704550e4199fb47dc2aacc11a96e81.png', 'user/20201104/93ffc6f1920dadc7aea2e53309b581f9.png', '', '112.198.68.136', '', '17777777777', NULL, 0, 30.00, 30, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42541, 2, 0, 0, 1605430998, 0, 100, 1604502583, 1, '15188888888', '###6c038ff1d5df721a56d42bdbf033baec', '手机用户8888', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '110.54.148.163', '', '15188888888', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42542, 2, 0, 0, 1605800228, 0, 57409, 1604555656, 1, '18350281111', '###805061f6d9219e4273e55c90f2da4f0f', 'WEB用户1111', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.209.136.150', '', '18350281111', NULL, 2601, 10.00, 10, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 1, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 600);
INSERT INTO `cmf_user` VALUES (42543, 2, 1, 0, 1605770179, 0, 9960, 1604558880, 1, '17777777776', '###6c038ff1d5df721a56d42bdbf033baec', 'eren122', '', '', 'home/20201110/1ee51eb4d0357bbdd7dd753450466ac7.png?imageView2/2/w/600/h/600', 'home/20201110/1ee51eb4d0357bbdd7dd753450466ac7.png?imageView2/2/w/200/h/200', '', '112.207.107.81', '', '17777777776', NULL, 40, 10000.00, 100000, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42544, 2, 0, 0, 1605338975, 0, 0, 1604577728, 1, '13911111111', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'WEB用户1111', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '117.9.144.123', '', '13911111111', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42545, 2, 0, 0, 1606796424, 0, 10, 1604579676, 1, '13399999999', '###6fc6d778cf2ccf39d64c8bcff6eec636', '听音乐', '', '', 'home/20201109/5b0f07cbe8f63b13b7efa0bacefcb5aa.jpg?imageView2/2/w/600/h/600', 'home/20201109/5b0f07cbe8f63b13b7efa0bacefcb5aa.jpg?imageView2/2/w/200/h/200', 'good音乐直播间', '127.0.0.1', '', '13399999999', NULL, 0, 310.00, 310, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1604729086, 501);
INSERT INTO `cmf_user` VALUES (42547, 2, 0, 0, 1604593521, 0, 0, 1604593521, 1, '15288888888', '###6c038ff1d5df721a56d42bdbf033baec', 'WEB用户8888', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.137', '', '15288888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42548, 2, 1, 0, 1605844358, 0, 10, 1604644502, 1, '13500000002', '###f3d0df7cb0654f368c65cacdad0b0199', 'fcb0002', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '18.166.25.109', '', '13500000002', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42549, 2, 0, 0, 1604662238, 0, 0, 1604662238, 1, '13132213111', '###805061f6d9219e4273e55c90f2da4f0f', 'WEB用户3111', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.206.124.141', '', '13132213111', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 0, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42550, 2, 1, 0, 0, 0, 0, 1604663653, 1, '18888888885', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'a55555', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '18888888885', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42551, 2, 0, 0, 1604731240, 0, 10, 1604666709, 0, '13500000007', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0007', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000007', NULL, 0, 0.00, 0, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 1604764799, 0.00, 0.00, 0.00, 1604719886, 100);
INSERT INTO `cmf_user` VALUES (42552, 2, 0, 1586188800, 1605844139, 0, 0, 1604727347, 1, '13500000008', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0008', '', '', 'http://7niuobs.jiextx.com/20201107160005_fd1888a3fa228c81a9155ede73bc7bc5?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201107160005_fd1888a3fa228c81a9155ede73bc7bc5?imageView2/2/w/200/h/200', '广告费', '112.207.107.190', '', '13500000008', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '北京市北京市东城区', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42553, 2, 0, 0, 1604738222, 0, 0, 1604738222, 1, '13500000009', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0009', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000009', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42554, 2, 0, 0, 1604748141, 0, 0, 1604747211, 1, '13500000010', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0010', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000010', NULL, 0, 10.00, 10, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42555, 2, 0, 0, 1604749659, 0, 0, 1604749659, 1, '13612354550', '###a4210ade7a5c99c2b5b6902231406cbf', '手机用户4550', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13612354550', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42556, 2, 0, 0, 1604749984, 0, 0, 1604749984, 1, '13356855888', '###a4210ade7a5c99c2b5b6902231406cbf', '手机用户5888', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13356855888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42557, 2, 0, 0, 1604752116, 0, 10, 1604752115, 1, '13545765584', '###82aba1cec1f9f44636775a8d3749ee85', '手机用户5584', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13545765584', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42558, 2, 0, 0, 1604813361, 0, 0, 1604812839, 1, '13632537622', '###cb7c5a8e8e84ad985795f93a501163c4', '手机用户7622', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13632537622', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42559, 2, 0, 0, 1604813395, 0, 0, 1604813206, 1, '13632537623', '###cb7c5a8e8e84ad985795f93a501163c4', '手机用户7623', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13632537623', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42560, 2, 0, 0, 1604831374, 0, 0, 1604831374, 1, '12563429853', '###671c4b6fd2efd640eb6ee04d935615e9', '手机用户9853', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '12563429853', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42561, 2, 0, 0, 1604832198, 0, 0, 1604832198, 1, '13695374626', '###671c4b6fd2efd640eb6ee04d935615e9', '手机用户4626', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13695374626', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42562, 2, 0, 0, 1604835771, 0, 0, 1604835771, 1, '13695367562', '###671c4b6fd2efd640eb6ee04d935615e9', '手机用户7562', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13695367562', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42563, 2, 0, 0, 1605528687, 0, 0, 1604843570, 1, '13711111112', '###a4210ade7a5c99c2b5b6902231406cbf', '手机用户1112', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13711111112', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 1111);
INSERT INTO `cmf_user` VALUES (42564, 2, 0, 0, 1604895803, 0, 0, 1604895803, 1, '13655555555', '###82aba1cec1f9f44636775a8d3749ee85', '手机用户5555', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13655555555', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42565, 2, 0, 0, 1604898995, 0, 0, 1604898995, 1, '13500000011', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0011', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000011', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42566, 2, 1, 0, 1605849246, 0, 0, 1604906938, 1, '13012340002', '###6c038ff1d5df721a56d42bdbf033baec', 'rgm002观众', '', '', 'home/20201109/edbbf8b07004c9499d51c681f9275e5a.jpg?imageView2/2/w/600/h/600', 'home/20201109/edbbf8b07004c9499d51c681f9275e5a.jpg?imageView2/2/w/200/h/200', '这家伙很懒，什么都没留下', '112.207.100.99', '', '13012340002', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42567, 2, 1, 0, 1604925903, 0, 0, 1604910070, 1, '13012340003', '###6c038ff1d5df721a56d42bdbf033baec', '观众0003', '', '', 'home/20201109/d14f2749c9bf42ed4d8fe666d483d483.jpg?imageView2/2/w/600/h/600', 'home/20201109/d14f2749c9bf42ed4d8fe666d483d483.jpg?imageView2/2/w/200/h/200', 'nanannnananananan', '112.198.70.75', '', '13012340003', NULL, 0, 40.00, 40, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42568, 2, 0, 0, 1604916106, 0, 0, 1604916106, 1, '15208425874', '###6c038ff1d5df721a56d42bdbf033baec', '手机用户5874', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '130.105.185.246', '', '15208425874', NULL, 0, 0.00, 0, '', '马卡蒂', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42569, 2, 0, 0, 1604922504, 0, 0, 1604922504, 1, '13500000012', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0012', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000012', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42570, 2, 1, 0, 1604923496, 0, 0, 1604923496, 1, '13012340004', '###6c038ff1d5df721a56d42bdbf033baec', 'rgm0004', '', '', '/default.jpg', '/default_thumb.jpg', '这家啊的发伙很懒，什么都没留下', '112.207.103.232', '', '13012340004', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42571, 2, 0, 0, 1604923989, 0, 0, 1604923989, 1, '13632537660', '###cb7c5a8e8e84ad985795f93a501163c4', '手机用户7660', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.132', '', '13632537660', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42572, 2, 0, 0, 1604925128, 0, 0, 1604925128, 1, '18350281234', '###805061f6d9219e4273e55c90f2da4f0f', 'WEB用户1234', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.206.124.141', '', '18350281234', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42573, 1, 0, 0, 1604925212, 0, 0, 0, 1, 'kanetest', '###1dec74e2a99cd4e2e253975538219738', '', 'kanetest@gmail.com', '', '', '', '', '112.207.103.190', '', '', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42574, 2, 1, 0, 1604986246, 0, 0, 1604925305, 1, '18888888886', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'a111111', '', '', '/default.jpg', '/default_thumb.jpg', '', '112.207.103.190', '', '18888888886', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42575, 2, 0, 0, 1605015479, 0, 0, 1604925344, 1, '18888888887', '###a490cfab34568c4bf5fa6a2f5a072e7f', '手机用户8887', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '18888888887', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42576, 2, 0, 0, 1604982656, 0, 0, 1604927950, 1, '13500000013', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0013', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000013', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42577, 2, 0, 0, 1604931496, 0, 0, 1604931495, 1, '15388888888', '###6c038ff1d5df721a56d42bdbf033baec', '555888', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '110.54.178.110', '', '15388888888', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42578, 2, 0, 0, 1604940679, 0, 0, 1604940678, 1, '13311111111', '###6c038ff1d5df721a56d42bdbf033baec', '手机用户1111', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.232', '', '13311111111', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42579, 2, 0, 0, 1604941019, 0, 0, 1604941017, 1, '13322222222', '###6c038ff1d5df721a56d42bdbf033baec', '手机用户2222', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.232', '', '13322222222', NULL, 0, 0.00, 0, '', '马卡蒂', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42580, 2, 0, 0, 1604977846, 0, 0, 1604977846, 1, '13500000014', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0014', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.103.190', '', '13500000014', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42581, 2, 0, 0, 1604994923, 0, 0, 1604994923, 1, '13366666666', '###6c038ff1d5df721a56d42bdbf033baec', 'WEB用户6666', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.188', '', '13366666666', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42582, 2, 0, 0, 1604997648, 0, 0, 1604997646, 1, '12356247853', '###4a0155d6f008f9629fe8ab90ab1b3843', '手机用户7853', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.188', '', '12356247853', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42583, 2, 1, 0, 1605022612, 0, 0, 1604999081, 1, '15588888888', '###6c038ff1d5df721a56d42bdbf033baec', '24h直播间测试', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '122.10.38.137', '', '15588888888', NULL, 0, 10.00, 10, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1605092788, 100);
INSERT INTO `cmf_user` VALUES (42584, 2, 0, 0, 1605005334, 0, 0, 1605005334, 1, '18563985698', '###6746eac090953164c3e74f48819b536e', '手机用户5698', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '130.105.185.246', '', '18563985698', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42585, 2, 0, 0, 1605704365, 0, 0, 1605017523, 1, '18888888889', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'WEB用户8889', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.107.190', '', '18888888889', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 0, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42586, 2, 1, 0, 1605179595, 0, 0, 1605179592, 1, '16534087769', '###6c038ff1d5df721a56d42bdbf033baec', 'UISTARK', '', '', 'http://7niuobs.jiextx.com/20201112191731_7e0808204fcc56313bd0d89204b0d689?imageView2/2/w/600/h/600', 'http://7niuobs.jiextx.com/20201112191731_7e0808204fcc56313bd0d89204b0d689?imageView2/2/w/200/h/200', '这家伙很懒，什么都没留下', '180.190.118.120', '', '16534087769', NULL, 0, 0.00, 0, '', '好像在火星', 1, '', 'phone', 1, 0, 0, 0, 1, '0', 'ios', '', 0, 0.00, 0.00, 0.00, 1605338929, 100);
INSERT INTO `cmf_user` VALUES (42588, 2, 1, 0, 1605845506, 0, 60, 1605351851, 1, '18615678958', '###6746eac090953164c3e74f48819b536e', '手机用户8958', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '18.210.182.60', '', '18615678958', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42589, 2, 0, 0, 1605364283, 0, 10, 1605364282, 1, '18611111111', '###6c038ff1d5df721a56d42bdbf033baec', '手机用户1111', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '110.54.134.142', '', '18611111111', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42590, 2, 2, 0, 1605430127, 0, 0, 1605429686, 1, '13911888888', '###38790da65d2e6d0635e4b81eaa1d5d3a', '蹦出新滋味', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13911888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42591, 2, 2, 0, 1605432826, 0, 0, 1605430523, 1, '13912888888', '###c659798d7d36f1da26d6089eeaee3dd1', '淘气宝贝珍妮', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13912888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42592, 2, 2, 0, 1605432910, 0, 0, 1605430553, 1, '13913888888', '###ee068c2e914b054c6cc70b050b88166a', '安安', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13913888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42593, 2, 2, 0, 1605432961, 0, 0, 1605430574, 1, '13914888888', '###1c8781891485255b4142f9ea9e82ca67', '初初', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13914888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42594, 2, 2, 0, 1605433031, 0, 0, 1605430619, 1, '13915888888', '###a006fcd7018c2e8f503f07c6eb81daa9', '回放1', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13915888888', NULL, 0, 0.00, 0, '', '', 1, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1605534530, 100);
INSERT INTO `cmf_user` VALUES (42595, 2, 2, 0, 1605433092, 0, 0, 1605430646, 1, '13916888888', '###10cfcb8b5fc3a7a304aa45468be7580a', '回放2', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13916888888', NULL, 0, 0.00, 0, '', '', 1, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1605534540, 100);
INSERT INTO `cmf_user` VALUES (42596, 2, 2, 0, 1605433207, 0, 0, 1605430699, 1, '13917888888', '###51c44af032190d66794d0f331a6b841b', '空空如也', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13917888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42597, 2, 2, 0, 1605433260, 0, 0, 1605430721, 1, '13918888888', '###ac09e4a5629aaa386be234efe482d184', 'Dolly', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13918888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42598, 2, 2, 0, 1605433319, 0, 0, 1605430745, 1, '13919888888', '###cd61fd6356dd343e2691505b85e39034', '紫琪', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13919888888', NULL, 0, 0.00, 0, '', '', 1, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 1605534379, 500);
INSERT INTO `cmf_user` VALUES (42599, 2, 1, 0, 1605433811, 0, 0, 1605430783, 1, '13920888888', '###6019cff0b1a85e199c506c508c48d8b6', '足球解说', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13920888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42600, 2, 2, 0, 1605433957, 0, 0, 1605430812, 1, '13921888888', '###314d64af3cccddd3ce85a376410a9f98', '小篮来了', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13921888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42601, 2, 2, 0, 1605434031, 0, 0, 1605430837, 1, '13922888888', '###8f1bc9bb8e0984d28c90a73ef878b5c0', '可可', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13922888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42602, 2, 2, 0, 1605434146, 0, 0, 1605430866, 1, '13923888888', '###e565a135828c48b59c31d50d756f01bf', '空空如也', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13923888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42603, 2, 2, 0, 1605434209, 0, 0, 1605430887, 1, '13924888888', '###4b04cd05b9dd6da177014df530c69ed6', '云儿', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13924888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42604, 2, 2, 0, 1605434252, 0, 0, 1605430935, 1, '13925888888', '###67b4086226a1f141fd0d30088a789316', 'Nia', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13925888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42605, 2, 2, 0, 1605434488, 0, 0, 1605430954, 1, '13926888888', '###cb23c866b991bee0d0553fcfa253e9d1', '潇潇', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13926888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42606, 2, 2, 0, 1605434547, 0, 0, 1605430972, 1, '13927888888', '###9485ac141e1d22d1580981d3b3a6912c', '空空如也', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13927888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42607, 2, 2, 0, 1605434608, 0, 0, 1605431046, 1, '13928888888', '###e789f30a5e957cd816cb03531d55d168', '火星妹子小猩猩', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13928888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42608, 2, 2, 0, 1605434689, 0, 0, 1605431063, 1, '13929888888', '###a1d2fe8a893041891d50f56206b5da94', '豆芽', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13929888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42609, 2, 2, 0, 1605434744, 0, 0, 1605431093, 1, '13930888888', '###71ae894ee7fb23cb158bc8cad1eb782b', '蒂儿', '', '', '/default.jpg', '/default_thumb.jpg', '', '122.10.38.157', '', '13930888888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42610, 2, 0, 0, 1605496233, 0, 0, 1605496233, 1, '13790414404', '###6e9518477c5e27e0d312f5fbe15e6526', 'WEB用户4404', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.102.63', '', '13790414404', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42611, 2, 0, 0, 1605525296, 0, 0, 1605525296, 1, '18888888810', '###a490cfab34568c4bf5fa6a2f5a072e7f', 'WEB用户8810', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.107.190', '', '18888888810', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 100);
INSERT INTO `cmf_user` VALUES (42612, 2, 0, 0, 1605596401, 0, 0, 1605596400, 1, '13888880000', '###f3d0df7cb0654f368c65cacdad0b0199', '？', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.207.107.190', '', '13888880000', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42613, 2, 0, 0, 1605779662, 0, 0, 1605779662, 1, '13900000001', '###f3d0df7cb0654f368c65cacdad0b0199', '手机用户0001', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '54.205.203.210', '', '13900000001', NULL, 0, 0.00, 0, '', '好像在火星', 0, '', 'phone', 1, 0, 1, 0, 1, '0', 'android', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42614, 2, 1, 0, 0, 0, 0, 1606374039, 1, '13399999998', '###6fc6d778cf2ccf39d64c8bcff6eec636', '222', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '13399999998', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42615, 2, 1, 0, 0, 0, 0, 1606374228, 1, '13399999997', '###6fc6d778cf2ccf39d64c8bcff6eec636', '888', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '13399999997', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42616, 2, 1, 0, 0, 0, 0, 1606374398, 1, '13399999996', '###6fc6d778cf2ccf39d64c8bcff6eec636', '66666', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '13399999996', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42617, 2, 1, 0, 0, 0, 0, 1606374968, 1, '13399999911', '###6fc6d778cf2ccf39d64c8bcff6eec636', '11111', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '13399999911', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42618, 2, 1, 0, 0, 0, 0, 1606375494, 1, '13399911111', '###6fc6d778cf2ccf39d64c8bcff6eec636', '111232232', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '13399911111', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42619, 2, 1, 0, 0, 0, 0, 1606376756, 1, '13399999888', '###6fc6d778cf2ccf39d64c8bcff6eec636', '12123', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '13399999888', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 0);
INSERT INTO `cmf_user` VALUES (42620, 2, 1, 0, 0, 0, 0, 1611830031, 1, '13399999111', '###6fc6d778cf2ccf39d64c8bcff6eec636', '1111111', '', '', '/default.jpg', '/default_thumb.jpg', '', '', '', '13399999111', NULL, 0, 0.00, 0, '', '', 0, '', 'phone', 0, 0, 0, 0, 1, '0', 'pc', '', 0, 0.00, 0.00, 0.00, 0, 0);

-- ----------------------------
-- Table structure for cmf_user_attention
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_attention`;
CREATE TABLE `cmf_user_attention`  (
  `uid` int(12) NOT NULL COMMENT '用户ID',
  `touid` int(12) NOT NULL COMMENT '关注人ID',
  INDEX `uid_touid_index`(`uid`, `touid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_attention
-- ----------------------------
INSERT INTO `cmf_user_attention` VALUES (42488, 1);
INSERT INTO `cmf_user_attention` VALUES (42488, 42507);
INSERT INTO `cmf_user_attention` VALUES (42492, 1);
INSERT INTO `cmf_user_attention` VALUES (42492, 42521);
INSERT INTO `cmf_user_attention` VALUES (42492, 42523);
INSERT INTO `cmf_user_attention` VALUES (42492, 42525);
INSERT INTO `cmf_user_attention` VALUES (42492, 42529);
INSERT INTO `cmf_user_attention` VALUES (42492, 42533);
INSERT INTO `cmf_user_attention` VALUES (42492, 42539);
INSERT INTO `cmf_user_attention` VALUES (42492, 42545);
INSERT INTO `cmf_user_attention` VALUES (42492, 42563);
INSERT INTO `cmf_user_attention` VALUES (42496, 42515);
INSERT INTO `cmf_user_attention` VALUES (42496, 42533);
INSERT INTO `cmf_user_attention` VALUES (42496, 42563);
INSERT INTO `cmf_user_attention` VALUES (42496, 42594);
INSERT INTO `cmf_user_attention` VALUES (42496, 42598);
INSERT INTO `cmf_user_attention` VALUES (42499, 42513);
INSERT INTO `cmf_user_attention` VALUES (42499, 42521);
INSERT INTO `cmf_user_attention` VALUES (42501, 1);
INSERT INTO `cmf_user_attention` VALUES (42501, 42521);
INSERT INTO `cmf_user_attention` VALUES (42501, 42566);
INSERT INTO `cmf_user_attention` VALUES (42503, 42488);
INSERT INTO `cmf_user_attention` VALUES (42503, 42492);
INSERT INTO `cmf_user_attention` VALUES (42503, 42515);
INSERT INTO `cmf_user_attention` VALUES (42503, 42528);
INSERT INTO `cmf_user_attention` VALUES (42503, 42531);
INSERT INTO `cmf_user_attention` VALUES (42503, 42533);
INSERT INTO `cmf_user_attention` VALUES (42503, 42538);
INSERT INTO `cmf_user_attention` VALUES (42503, 42545);
INSERT INTO `cmf_user_attention` VALUES (42503, 42551);
INSERT INTO `cmf_user_attention` VALUES (42503, 42552);
INSERT INTO `cmf_user_attention` VALUES (42504, 42488);
INSERT INTO `cmf_user_attention` VALUES (42504, 42492);
INSERT INTO `cmf_user_attention` VALUES (42504, 42514);
INSERT INTO `cmf_user_attention` VALUES (42504, 42528);
INSERT INTO `cmf_user_attention` VALUES (42504, 42531);
INSERT INTO `cmf_user_attention` VALUES (42504, 42538);
INSERT INTO `cmf_user_attention` VALUES (42504, 42548);
INSERT INTO `cmf_user_attention` VALUES (42505, 42488);
INSERT INTO `cmf_user_attention` VALUES (42505, 42492);
INSERT INTO `cmf_user_attention` VALUES (42505, 42528);
INSERT INTO `cmf_user_attention` VALUES (42505, 42538);
INSERT INTO `cmf_user_attention` VALUES (42513, 42499);
INSERT INTO `cmf_user_attention` VALUES (42513, 42506);
INSERT INTO `cmf_user_attention` VALUES (42513, 42521);
INSERT INTO `cmf_user_attention` VALUES (42513, 42538);
INSERT INTO `cmf_user_attention` VALUES (42513, 42586);
INSERT INTO `cmf_user_attention` VALUES (42513, 42598);
INSERT INTO `cmf_user_attention` VALUES (42514, 1);
INSERT INTO `cmf_user_attention` VALUES (42514, 42492);
INSERT INTO `cmf_user_attention` VALUES (42514, 42498);
INSERT INTO `cmf_user_attention` VALUES (42514, 42499);
INSERT INTO `cmf_user_attention` VALUES (42514, 42500);
INSERT INTO `cmf_user_attention` VALUES (42514, 42501);
INSERT INTO `cmf_user_attention` VALUES (42514, 42503);
INSERT INTO `cmf_user_attention` VALUES (42514, 42511);
INSERT INTO `cmf_user_attention` VALUES (42514, 42513);
INSERT INTO `cmf_user_attention` VALUES (42514, 42552);
INSERT INTO `cmf_user_attention` VALUES (42515, 42492);
INSERT INTO `cmf_user_attention` VALUES (42516, 42492);
INSERT INTO `cmf_user_attention` VALUES (42516, 42499);
INSERT INTO `cmf_user_attention` VALUES (42516, 42514);
INSERT INTO `cmf_user_attention` VALUES (42516, 42520);
INSERT INTO `cmf_user_attention` VALUES (42516, 42552);
INSERT INTO `cmf_user_attention` VALUES (42516, 42598);
INSERT INTO `cmf_user_attention` VALUES (42518, 42488);
INSERT INTO `cmf_user_attention` VALUES (42518, 42492);
INSERT INTO `cmf_user_attention` VALUES (42521, 42514);
INSERT INTO `cmf_user_attention` VALUES (42521, 42538);
INSERT INTO `cmf_user_attention` VALUES (42521, 42554);
INSERT INTO `cmf_user_attention` VALUES (42521, 42565);
INSERT INTO `cmf_user_attention` VALUES (42523, 42517);
INSERT INTO `cmf_user_attention` VALUES (42523, 42529);
INSERT INTO `cmf_user_attention` VALUES (42523, 42548);
INSERT INTO `cmf_user_attention` VALUES (42523, 42595);
INSERT INTO `cmf_user_attention` VALUES (42524, 42517);
INSERT INTO `cmf_user_attention` VALUES (42524, 42523);
INSERT INTO `cmf_user_attention` VALUES (42524, 42594);
INSERT INTO `cmf_user_attention` VALUES (42524, 42595);
INSERT INTO `cmf_user_attention` VALUES (42524, 42598);
INSERT INTO `cmf_user_attention` VALUES (42525, 42488);
INSERT INTO `cmf_user_attention` VALUES (42525, 42492);
INSERT INTO `cmf_user_attention` VALUES (42527, 42488);
INSERT INTO `cmf_user_attention` VALUES (42527, 42492);
INSERT INTO `cmf_user_attention` VALUES (42528, 42488);
INSERT INTO `cmf_user_attention` VALUES (42528, 42492);
INSERT INTO `cmf_user_attention` VALUES (42529, 42488);
INSERT INTO `cmf_user_attention` VALUES (42529, 42492);
INSERT INTO `cmf_user_attention` VALUES (42530, 42488);
INSERT INTO `cmf_user_attention` VALUES (42530, 42492);
INSERT INTO `cmf_user_attention` VALUES (42530, 42514);
INSERT INTO `cmf_user_attention` VALUES (42531, 42488);
INSERT INTO `cmf_user_attention` VALUES (42531, 42492);
INSERT INTO `cmf_user_attention` VALUES (42531, 42514);
INSERT INTO `cmf_user_attention` VALUES (42531, 42528);
INSERT INTO `cmf_user_attention` VALUES (42531, 42552);
INSERT INTO `cmf_user_attention` VALUES (42533, 42514);
INSERT INTO `cmf_user_attention` VALUES (42534, 42488);
INSERT INTO `cmf_user_attention` VALUES (42534, 42492);
INSERT INTO `cmf_user_attention` VALUES (42534, 42528);
INSERT INTO `cmf_user_attention` VALUES (42537, 1);
INSERT INTO `cmf_user_attention` VALUES (42539, 42488);
INSERT INTO `cmf_user_attention` VALUES (42539, 42492);
INSERT INTO `cmf_user_attention` VALUES (42539, 42496);
INSERT INTO `cmf_user_attention` VALUES (42539, 42521);
INSERT INTO `cmf_user_attention` VALUES (42539, 42528);
INSERT INTO `cmf_user_attention` VALUES (42539, 42583);
INSERT INTO `cmf_user_attention` VALUES (42539, 2147483647);
INSERT INTO `cmf_user_attention` VALUES (42541, 42538);
INSERT INTO `cmf_user_attention` VALUES (42542, 1);
INSERT INTO `cmf_user_attention` VALUES (42542, 42492);
INSERT INTO `cmf_user_attention` VALUES (42542, 42507);
INSERT INTO `cmf_user_attention` VALUES (42542, 42523);
INSERT INTO `cmf_user_attention` VALUES (42542, 42533);
INSERT INTO `cmf_user_attention` VALUES (42542, 42538);
INSERT INTO `cmf_user_attention` VALUES (42542, 42539);
INSERT INTO `cmf_user_attention` VALUES (42542, 42545);
INSERT INTO `cmf_user_attention` VALUES (42542, 42566);
INSERT INTO `cmf_user_attention` VALUES (42542, 42567);
INSERT INTO `cmf_user_attention` VALUES (42542, 42583);
INSERT INTO `cmf_user_attention` VALUES (42542, 42598);
INSERT INTO `cmf_user_attention` VALUES (42543, 42545);
INSERT INTO `cmf_user_attention` VALUES (42545, 42496);
INSERT INTO `cmf_user_attention` VALUES (42545, 42514);
INSERT INTO `cmf_user_attention` VALUES (42545, 42515);
INSERT INTO `cmf_user_attention` VALUES (42545, 42516);
INSERT INTO `cmf_user_attention` VALUES (42545, 42518);
INSERT INTO `cmf_user_attention` VALUES (42545, 42521);
INSERT INTO `cmf_user_attention` VALUES (42545, 42523);
INSERT INTO `cmf_user_attention` VALUES (42545, 42533);
INSERT INTO `cmf_user_attention` VALUES (42545, 42554);
INSERT INTO `cmf_user_attention` VALUES (42548, 42488);
INSERT INTO `cmf_user_attention` VALUES (42548, 42492);
INSERT INTO `cmf_user_attention` VALUES (42548, 42521);
INSERT INTO `cmf_user_attention` VALUES (42548, 42528);
INSERT INTO `cmf_user_attention` VALUES (42548, 42538);
INSERT INTO `cmf_user_attention` VALUES (42548, 42547);
INSERT INTO `cmf_user_attention` VALUES (42551, 42514);
INSERT INTO `cmf_user_attention` VALUES (42552, 42488);
INSERT INTO `cmf_user_attention` VALUES (42552, 42492);
INSERT INTO `cmf_user_attention` VALUES (42552, 42499);
INSERT INTO `cmf_user_attention` VALUES (42552, 42515);
INSERT INTO `cmf_user_attention` VALUES (42552, 42528);
INSERT INTO `cmf_user_attention` VALUES (42552, 42531);
INSERT INTO `cmf_user_attention` VALUES (42552, 42533);
INSERT INTO `cmf_user_attention` VALUES (42552, 42538);
INSERT INTO `cmf_user_attention` VALUES (42552, 42551);
INSERT INTO `cmf_user_attention` VALUES (42553, 42488);
INSERT INTO `cmf_user_attention` VALUES (42553, 42492);
INSERT INTO `cmf_user_attention` VALUES (42553, 42515);
INSERT INTO `cmf_user_attention` VALUES (42553, 42528);
INSERT INTO `cmf_user_attention` VALUES (42553, 42533);
INSERT INTO `cmf_user_attention` VALUES (42553, 42538);
INSERT INTO `cmf_user_attention` VALUES (42553, 42545);
INSERT INTO `cmf_user_attention` VALUES (42553, 42551);
INSERT INTO `cmf_user_attention` VALUES (42554, 42545);
INSERT INTO `cmf_user_attention` VALUES (42558, 42488);
INSERT INTO `cmf_user_attention` VALUES (42558, 42492);
INSERT INTO `cmf_user_attention` VALUES (42558, 42515);
INSERT INTO `cmf_user_attention` VALUES (42558, 42528);
INSERT INTO `cmf_user_attention` VALUES (42558, 42533);
INSERT INTO `cmf_user_attention` VALUES (42558, 42538);
INSERT INTO `cmf_user_attention` VALUES (42558, 42545);
INSERT INTO `cmf_user_attention` VALUES (42558, 42551);
INSERT INTO `cmf_user_attention` VALUES (42559, 42488);
INSERT INTO `cmf_user_attention` VALUES (42559, 42492);
INSERT INTO `cmf_user_attention` VALUES (42559, 42515);
INSERT INTO `cmf_user_attention` VALUES (42559, 42528);
INSERT INTO `cmf_user_attention` VALUES (42559, 42533);
INSERT INTO `cmf_user_attention` VALUES (42559, 42538);
INSERT INTO `cmf_user_attention` VALUES (42559, 42545);
INSERT INTO `cmf_user_attention` VALUES (42559, 42551);
INSERT INTO `cmf_user_attention` VALUES (42560, 42488);
INSERT INTO `cmf_user_attention` VALUES (42560, 42492);
INSERT INTO `cmf_user_attention` VALUES (42560, 42515);
INSERT INTO `cmf_user_attention` VALUES (42560, 42528);
INSERT INTO `cmf_user_attention` VALUES (42560, 42533);
INSERT INTO `cmf_user_attention` VALUES (42560, 42538);
INSERT INTO `cmf_user_attention` VALUES (42560, 42545);
INSERT INTO `cmf_user_attention` VALUES (42560, 42551);
INSERT INTO `cmf_user_attention` VALUES (42561, 42488);
INSERT INTO `cmf_user_attention` VALUES (42561, 42492);
INSERT INTO `cmf_user_attention` VALUES (42561, 42515);
INSERT INTO `cmf_user_attention` VALUES (42561, 42528);
INSERT INTO `cmf_user_attention` VALUES (42561, 42533);
INSERT INTO `cmf_user_attention` VALUES (42561, 42538);
INSERT INTO `cmf_user_attention` VALUES (42561, 42545);
INSERT INTO `cmf_user_attention` VALUES (42561, 42551);
INSERT INTO `cmf_user_attention` VALUES (42562, 42488);
INSERT INTO `cmf_user_attention` VALUES (42562, 42492);
INSERT INTO `cmf_user_attention` VALUES (42562, 42515);
INSERT INTO `cmf_user_attention` VALUES (42562, 42528);
INSERT INTO `cmf_user_attention` VALUES (42562, 42533);
INSERT INTO `cmf_user_attention` VALUES (42562, 42538);
INSERT INTO `cmf_user_attention` VALUES (42562, 42545);
INSERT INTO `cmf_user_attention` VALUES (42562, 42551);
INSERT INTO `cmf_user_attention` VALUES (42563, 42488);
INSERT INTO `cmf_user_attention` VALUES (42563, 42492);
INSERT INTO `cmf_user_attention` VALUES (42563, 42496);
INSERT INTO `cmf_user_attention` VALUES (42563, 42515);
INSERT INTO `cmf_user_attention` VALUES (42563, 42528);
INSERT INTO `cmf_user_attention` VALUES (42563, 42533);
INSERT INTO `cmf_user_attention` VALUES (42563, 42538);
INSERT INTO `cmf_user_attention` VALUES (42563, 42545);
INSERT INTO `cmf_user_attention` VALUES (42563, 42551);
INSERT INTO `cmf_user_attention` VALUES (42565, 42488);
INSERT INTO `cmf_user_attention` VALUES (42565, 42492);
INSERT INTO `cmf_user_attention` VALUES (42565, 42515);
INSERT INTO `cmf_user_attention` VALUES (42565, 42528);
INSERT INTO `cmf_user_attention` VALUES (42565, 42533);
INSERT INTO `cmf_user_attention` VALUES (42565, 42538);
INSERT INTO `cmf_user_attention` VALUES (42565, 42545);
INSERT INTO `cmf_user_attention` VALUES (42565, 42551);
INSERT INTO `cmf_user_attention` VALUES (42566, 42515);
INSERT INTO `cmf_user_attention` VALUES (42567, 42515);
INSERT INTO `cmf_user_attention` VALUES (42568, 42488);
INSERT INTO `cmf_user_attention` VALUES (42568, 42492);
INSERT INTO `cmf_user_attention` VALUES (42568, 42515);
INSERT INTO `cmf_user_attention` VALUES (42568, 42528);
INSERT INTO `cmf_user_attention` VALUES (42568, 42533);
INSERT INTO `cmf_user_attention` VALUES (42568, 42538);
INSERT INTO `cmf_user_attention` VALUES (42568, 42545);
INSERT INTO `cmf_user_attention` VALUES (42568, 42551);
INSERT INTO `cmf_user_attention` VALUES (42569, 42488);
INSERT INTO `cmf_user_attention` VALUES (42569, 42492);
INSERT INTO `cmf_user_attention` VALUES (42569, 42515);
INSERT INTO `cmf_user_attention` VALUES (42569, 42528);
INSERT INTO `cmf_user_attention` VALUES (42569, 42533);
INSERT INTO `cmf_user_attention` VALUES (42569, 42538);
INSERT INTO `cmf_user_attention` VALUES (42569, 42545);
INSERT INTO `cmf_user_attention` VALUES (42569, 42551);
INSERT INTO `cmf_user_attention` VALUES (42570, 42567);
INSERT INTO `cmf_user_attention` VALUES (42571, 42488);
INSERT INTO `cmf_user_attention` VALUES (42571, 42492);
INSERT INTO `cmf_user_attention` VALUES (42571, 42515);
INSERT INTO `cmf_user_attention` VALUES (42571, 42528);
INSERT INTO `cmf_user_attention` VALUES (42571, 42533);
INSERT INTO `cmf_user_attention` VALUES (42571, 42538);
INSERT INTO `cmf_user_attention` VALUES (42571, 42545);
INSERT INTO `cmf_user_attention` VALUES (42571, 42551);
INSERT INTO `cmf_user_attention` VALUES (42572, 42563);
INSERT INTO `cmf_user_attention` VALUES (42575, 42488);
INSERT INTO `cmf_user_attention` VALUES (42575, 42492);
INSERT INTO `cmf_user_attention` VALUES (42575, 42515);
INSERT INTO `cmf_user_attention` VALUES (42575, 42528);
INSERT INTO `cmf_user_attention` VALUES (42575, 42533);
INSERT INTO `cmf_user_attention` VALUES (42575, 42538);
INSERT INTO `cmf_user_attention` VALUES (42575, 42545);
INSERT INTO `cmf_user_attention` VALUES (42575, 42551);
INSERT INTO `cmf_user_attention` VALUES (42580, 42488);
INSERT INTO `cmf_user_attention` VALUES (42580, 42492);
INSERT INTO `cmf_user_attention` VALUES (42580, 42515);
INSERT INTO `cmf_user_attention` VALUES (42580, 42528);
INSERT INTO `cmf_user_attention` VALUES (42580, 42533);
INSERT INTO `cmf_user_attention` VALUES (42580, 42538);
INSERT INTO `cmf_user_attention` VALUES (42580, 42545);
INSERT INTO `cmf_user_attention` VALUES (42580, 42551);
INSERT INTO `cmf_user_attention` VALUES (42582, 42488);
INSERT INTO `cmf_user_attention` VALUES (42582, 42492);
INSERT INTO `cmf_user_attention` VALUES (42582, 42515);
INSERT INTO `cmf_user_attention` VALUES (42582, 42528);
INSERT INTO `cmf_user_attention` VALUES (42582, 42533);
INSERT INTO `cmf_user_attention` VALUES (42582, 42538);
INSERT INTO `cmf_user_attention` VALUES (42582, 42545);
INSERT INTO `cmf_user_attention` VALUES (42582, 42551);
INSERT INTO `cmf_user_attention` VALUES (42584, 42488);
INSERT INTO `cmf_user_attention` VALUES (42584, 42492);
INSERT INTO `cmf_user_attention` VALUES (42584, 42515);
INSERT INTO `cmf_user_attention` VALUES (42584, 42528);
INSERT INTO `cmf_user_attention` VALUES (42584, 42533);
INSERT INTO `cmf_user_attention` VALUES (42584, 42538);
INSERT INTO `cmf_user_attention` VALUES (42584, 42545);
INSERT INTO `cmf_user_attention` VALUES (42584, 42551);
INSERT INTO `cmf_user_attention` VALUES (42584, 42598);
INSERT INTO `cmf_user_attention` VALUES (42586, 42488);
INSERT INTO `cmf_user_attention` VALUES (42586, 42492);
INSERT INTO `cmf_user_attention` VALUES (42586, 42515);
INSERT INTO `cmf_user_attention` VALUES (42586, 42528);
INSERT INTO `cmf_user_attention` VALUES (42586, 42533);
INSERT INTO `cmf_user_attention` VALUES (42586, 42538);
INSERT INTO `cmf_user_attention` VALUES (42586, 42545);
INSERT INTO `cmf_user_attention` VALUES (42586, 42551);
INSERT INTO `cmf_user_attention` VALUES (42588, 42513);
INSERT INTO `cmf_user_attention` VALUES (42588, 42514);
INSERT INTO `cmf_user_attention` VALUES (42588, 42516);
INSERT INTO `cmf_user_attention` VALUES (42588, 42538);
INSERT INTO `cmf_user_attention` VALUES (42588, 42554);
INSERT INTO `cmf_user_attention` VALUES (42588, 42565);
INSERT INTO `cmf_user_attention` VALUES (42588, 42594);
INSERT INTO `cmf_user_attention` VALUES (42588, 42595);
INSERT INTO `cmf_user_attention` VALUES (42588, 42598);
INSERT INTO `cmf_user_attention` VALUES (42589, 42488);
INSERT INTO `cmf_user_attention` VALUES (42589, 42492);
INSERT INTO `cmf_user_attention` VALUES (42589, 42515);
INSERT INTO `cmf_user_attention` VALUES (42589, 42528);
INSERT INTO `cmf_user_attention` VALUES (42589, 42533);
INSERT INTO `cmf_user_attention` VALUES (42589, 42538);
INSERT INTO `cmf_user_attention` VALUES (42589, 42545);
INSERT INTO `cmf_user_attention` VALUES (42589, 42551);
INSERT INTO `cmf_user_attention` VALUES (42589, 42586);

-- ----------------------------
-- Table structure for cmf_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_auth`;
CREATE TABLE `cmf_user_auth`  (
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `real_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `cer_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '身份证号',
  `front_view` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '正面',
  `back_view` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '反面',
  `handset_view` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手持',
  `reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '审核说明',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '提交时间',
  `uptime` int(12) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0 处理中 1 成功 2 失败',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_auth
-- ----------------------------
INSERT INTO `cmf_user_auth` VALUES (42492, '测试', '13312345678', '510000199910101010', 'http://7niuobs.jiextx.com/default/20201102/bdbf3a7720758f04b0c566e02c763d62.png', 'http://7niuobs.jiextx.com/default/20201102/c39bb4203173ed0826431ac6c58d7049.png', 'http://7niuobs.jiextx.com/default/20201102/60b37add502745526d697aa7cec98951.png', '5555555555', 1604295292, 1604303439, 1);
INSERT INTO `cmf_user_auth` VALUES (42496, '彭于晏', '13811111112', '220104199711220871', 'auth_42496_11604410534859.jpg', 'auth_42496_21604410541333.jpg', 'auth_42496_31604410545787.jpg', '', 1604410671, 1604752119, 1);
INSERT INTO `cmf_user_auth` VALUES (42499, '李四', '13500000001', '431153200012126457', 'auth_42499_11605526245649.jpg', 'auth_42499_21605526256521.jpg', 'auth_42499_31605526264494.jpg', '', 1605526268, 1605676306, 1);
INSERT INTO `cmf_user_auth` VALUES (42501, '张三', '13500000003', '13040419910101424x', 'https://7niuobs.jiextx.com/default/20201118/6a4563f7b6eaf9ebb44879d23441fe9c.png', 'https://7niuobs.jiextx.com/default/20201118/aeba0bdbde73ac6a5c64efa7e1840f0a.png', 'https://7niuobs.jiextx.com/default/20201118/d6fae20668655fdb24f37a9f31c86a14.png', '000', 1605698780, 1605698828, 1);
INSERT INTO `cmf_user_auth` VALUES (42505, '李四', '13500000000', '123231190012012345', 'auth_42505_11605021532235.jpg', 'auth_42505_21605021520747.jpg', 'auth_42505_31605021562533.jpg', NULL, 1605021648, 0, 0);
INSERT INTO `cmf_user_auth` VALUES (42511, 'dsfa', '139898989898', '612588199902154487', 'http://7niuobs.jiextx.com/default/20201103/344808095e389174be9fa7860eb1da7f.jpg', 'http://7niuobs.jiextx.com/default/20201103/4d0af380e8545b977cf51a4bcc430f60.jpeg', 'http://7niuobs.jiextx.com/default/20201103/1472f5b3ef6d76523a8e716a76358a53.jpg', '', 1604401714, 1604752112, 1);
INSERT INTO `cmf_user_auth` VALUES (42513, '会员一', '18888888881', '511601198112122343', 'auth_42513_11604388589063.jpg', 'auth_42513_21604388634392.jpg', 'auth_42513_31604388674756.jpg', '', 1604388725, 1604388736, 1);
INSERT INTO `cmf_user_auth` VALUES (42515, '狗哥', '13012340001', '510700202010101010', 'http://7niuobs.jiextx.com/default/20201111/419488956a2f3f0c1b9b1a1a09ddedcf.jpg', 'http://7niuobs.jiextx.com/default/20201111/7a55c7d7ecb5996057e1d16db86058e2.jpg', 'http://7niuobs.jiextx.com/default/20201111/296e166912ad91c1c659148fdbbe1db7.jpg', '', 1605095406, 1605095605, 1);
INSERT INTO `cmf_user_auth` VALUES (42516, '会员二', '18888888882', '511602199011161722', 'auth_42516_11604490567496.jpg', 'auth_42516_21604490608761.jpg', 'auth_42516_31604490650019.jpg', '', 1604490749, 1604490768, 1);
INSERT INTO `cmf_user_auth` VALUES (42520, '会员四', '188888888884', '511603198012124545', 'auth_42520_11604387998903.jpg', 'auth_42520_21604388016554.jpg', 'auth_42520_31604388026885.jpg', 'dasdsad', 1604388065, 1604388230, 1);
INSERT INTO `cmf_user_auth` VALUES (42521, '李四', '13500000000', '231167190012087654', 'http://7niuobs.jiextx.com/default/20201111/c055652ce832dca04635d69146c4d147.png', 'http://7niuobs.jiextx.com/default/20201111/7dc598a98ac3c97430ed4c40d6660d57.png', 'http://7niuobs.jiextx.com/default/20201111/0934e8f47e3976c161fff2cacad3701f.png', '', 1605095349, 1605095408, 1);
INSERT INTO `cmf_user_auth` VALUES (42524, '老王', '13632537621', '430963199206087260', 'auth_42524_11605181473217.jpg', 'auth_42524_21605181477797.jpg', 'auth_42524_31605181481251.jpg', '', 1605181515, 1605241289, 1);
INSERT INTO `cmf_user_auth` VALUES (42542, '技术测试', '18350281111', '622621198008136123', 'http://7niuobs.jiextx.com/default/20201113/2e8f78d4d571732ed2ac2239d38f3dce.jpg', 'http://7niuobs.jiextx.com/default/20201113/027060bd276e85c0cd29a168e8a1d34c.jpg', 'http://7niuobs.jiextx.com/default/20201113/cceddcc1d7fd5f51ef8118d1a344e93b.jpg', '', 1605249534, 1605249560, 1);
INSERT INTO `cmf_user_auth` VALUES (42544, '吴万涛', '13911111111', '429004198604070033', 'http://7niuobs.jiextx.com/default/20201114/efcaad4c27f31051b7427d5bfac35cd9.jpg', 'http://7niuobs.jiextx.com/default/20201114/5b142b83dad352d02d188bf223c0f96c.jpg', 'http://7niuobs.jiextx.com/default/20201114/20c705502d389462330bb9cde1e532fb.png', '', 1605337588, 1605337632, 1);
INSERT INTO `cmf_user_auth` VALUES (42545, 'abc', '13788888888', '521255199909182258', 'http://7niuobs.jiextx.com/default/20201113/6a3972daa6442d3c45c46dff1c6161cd.png', 'http://7niuobs.jiextx.com/default/20201113/e9ba58066ea42d2ccceacebc02581170.jpeg', 'http://7niuobs.jiextx.com/default/20201113/26712ddc59e50739f353cf0ed6857666.jpg', '', 1605247867, 1605247889, 1);
INSERT INTO `cmf_user_auth` VALUES (42563, '测试', '13124739362', '51100219990608491x', 'auth_42563_11605533578175.jpg', 'auth_42563_21605533583783.jpg', 'auth_42563_31605533588510.jpg', '6666666666666', 1605533638, 1605679823, 1);
INSERT INTO `cmf_user_auth` VALUES (42566, '测试测试', '13012340002', '510700200010101010', 'https://7niuobs.jiextx.com/default/20201119/d629b52ae712b0c013deba6de6f0fc7c.jpg', 'https://7niuobs.jiextx.com/default/20201119/dbba798581de29e33b0dcaf587ac1631.jpg', 'https://7niuobs.jiextx.com/default/20201119/64d199296114ad54db79a600cec211b4.jpg', '', 1605785059, 1605785596, 1);
INSERT INTO `cmf_user_auth` VALUES (42590, '笨笨', '13911888888', '130423199401011584', 'http://7niuobs.jiextx.com/default/20201115/03d2f0d997e29eac517ac1e7f6d89167.jpg', 'http://7niuobs.jiextx.com/default/20201115/366ec37688d66da629d31d810e06ad29.jpg', 'http://7niuobs.jiextx.com/default/20201115/9c0ca5e27a50062eab7c4f4b54c239bb.jpg', '', 1605430299, 1605434843, 1);
INSERT INTO `cmf_user_auth` VALUES (42591, '珍妮', '13912888888', '130404199101016608', 'http://7niuobs.jiextx.com/default/20201115/c47359f5b577e0dc442102f3febdefd3.jpg', 'http://7niuobs.jiextx.com/default/20201115/2475225de77f223b6778aa2e441b1ed4.jpg', 'http://7niuobs.jiextx.com/default/20201115/41efd1adc10a29359244ec6528e71f51.jpg', '', 1605432884, 1605434851, 1);
INSERT INTO `cmf_user_auth` VALUES (42592, '安安', '13913888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/5cbe5b09975e65e17f4de433a0cfd544.jpg', 'http://7niuobs.jiextx.com/default/20201115/0c706befe01b01bd6f370be69dccbfe7.jpg', 'http://7niuobs.jiextx.com/default/20201115/eecf5a2b00452c1cb85cd240145c1d83.jpg', '', 1605432948, 1605434859, 1);
INSERT INTO `cmf_user_auth` VALUES (42593, '初初', '13914888888', '130404199101011006', 'http://7niuobs.jiextx.com/default/20201115/d4bfd87e54c3666dd64685b0d367e6d6.jpg', 'http://7niuobs.jiextx.com/default/20201115/43223bb5fdbf59a441f79f0c91aeb68e.jpg', 'http://7niuobs.jiextx.com/default/20201115/803ea54f74b59ab6e0e105731a07877f.jpg', '', 1605433010, 1605434866, 1);
INSERT INTO `cmf_user_auth` VALUES (42594, '王富贵', '13915888888', '130404199101011946', 'http://7niuobs.jiextx.com/default/20201115/cc0178a64a199beeb6e9e575bf092e5e.jpg', 'http://7niuobs.jiextx.com/default/20201115/940ff1f1629fe53efbe7b478d59adb02.jpg', 'http://7niuobs.jiextx.com/default/20201115/1a53a75513ff812535711b1edf4f0e67.jpg', '', 1605433077, 1605434874, 1);
INSERT INTO `cmf_user_auth` VALUES (42595, '龙傲天', '13916888888', '130404199101016608', 'http://7niuobs.jiextx.com/default/20201115/a778a33945fed5b095a85efe9040a8cd.jpg', 'http://7niuobs.jiextx.com/default/20201115/89d9c96285038729a1d90e049975d972.jpg', 'http://7niuobs.jiextx.com/default/20201115/eeab1557b66656b0f7dbf3ce69038e49.jpg', '', 1605433134, 1605434880, 1);
INSERT INTO `cmf_user_auth` VALUES (42596, '汪峰', '13917888888', '130404199101016608', 'http://7niuobs.jiextx.com/default/20201115/8b695224e599ec33d9f14f8289883464.jpg', 'http://7niuobs.jiextx.com/default/20201115/4e94feaf708062c1f4efe64805b55729.jpg', 'http://7niuobs.jiextx.com/default/20201115/874b0644f38681aa00cb9f86b00f5ad9.jpg', '', 1605433242, 1605434888, 1);
INSERT INTO `cmf_user_auth` VALUES (42597, '多多', '13918888888', '130404199101016608', 'http://7niuobs.jiextx.com/default/20201115/84fa17b001ba5187fe8724b93eebbdff.jpg', 'http://7niuobs.jiextx.com/default/20201115/98a8c55930c4c877ff0e5fc33ad0ae59.jpg', 'http://7niuobs.jiextx.com/default/20201115/897c727cd9a43b5d2211aec4c04ca210.jpg', '', 1605433298, 1605434894, 1);
INSERT INTO `cmf_user_auth` VALUES (42598, '薇薇', '13919888888', '130404199101016608', 'http://7niuobs.jiextx.com/default/20201115/76d4502008d189787c049e021be21191.jpg', 'http://7niuobs.jiextx.com/default/20201115/729239f39472a29ca2ed80f1f6e5f5ac.jpg', 'http://7niuobs.jiextx.com/default/20201115/012cf1a4df377800f149bb5e81a3b76b.jpg', '', 1605433377, 1605434904, 1);
INSERT INTO `cmf_user_auth` VALUES (42599, '李解说', '13920888888', '130404199101016608', 'http://7niuobs.jiextx.com/default/20201115/900a31d57c13bc211340fecdcd34c85f.jpg', 'http://7niuobs.jiextx.com/default/20201115/2bc6ac0b5bcd51331d5e3038dc8d3e32.jpg', 'http://7niuobs.jiextx.com/default/20201115/b0be00435cbb2fa6e29eb16e73f62e7b.jpg', '', 1605433929, 1605434912, 1);
INSERT INTO `cmf_user_auth` VALUES (42600, '蓝甜', '13921888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/f5a548caae1ae599f707cb53550fb509.jpg', 'http://7niuobs.jiextx.com/default/20201115/a0c3ac05f0a68f2ff90d534dfb06a4ea.jpg', 'http://7niuobs.jiextx.com/default/20201115/d04013162cf3529da2bc4f014689df10.jpg', '', 1605434018, 1605434920, 1);
INSERT INTO `cmf_user_auth` VALUES (42601, '可可', '13922888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/6355b9f366d818cd9f36ce467956dfa4.jpg', 'http://7niuobs.jiextx.com/default/20201115/ec3d9a3552973693d9b35a65b3d5ca10.jpg', 'http://7niuobs.jiextx.com/default/20201115/cdcba8aa3adb7fec45913965f5300926.jpg', '', 1605434128, 1605434928, 1);
INSERT INTO `cmf_user_auth` VALUES (42602, '谢霆锋', '13923888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/09e52d4e33e8d2871095d920c68746d6.jpg', 'http://7niuobs.jiextx.com/default/20201115/1d09b9b21a93e2984a72ff02f17b97c7.jpg', 'http://7niuobs.jiextx.com/default/20201115/04c792d0f759f1b2e1ac638e2ca82efc.jpg', '', 1605434185, 1605434997, 1);
INSERT INTO `cmf_user_auth` VALUES (42603, '云儿', '13924888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/77c88efc2a18b50bad5c393534f8ceb5.jpg', 'http://7niuobs.jiextx.com/default/20201115/ef11eb47367bce084f33846e0b3f10d9.jpg', 'http://7niuobs.jiextx.com/default/20201115/e6b28ae4b7e82a25ea62162d71966a1d.jpg', '', 1605434238, 1605435005, 1);
INSERT INTO `cmf_user_auth` VALUES (42604, '念念', '13925888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/b26884b561d60001c768c862e1976f99.jpg', 'http://7niuobs.jiextx.com/default/20201115/79f194c8c3a9cb4e827261572e3f5b20.jpg', 'http://7niuobs.jiextx.com/default/20201115/c695c7229178ca649bd181f7d12d6eab.jpg', '', 1605434469, 1605435091, 1);
INSERT INTO `cmf_user_auth` VALUES (42605, '潇潇', '13926888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/fd80d8f5eb3e81f161093c09ae812af4.jpg', 'http://7niuobs.jiextx.com/default/20201115/45ae2877b0a1bf0ebcb21d5e96b742d0.jpg', 'http://7niuobs.jiextx.com/default/20201115/b455240b5a606363302dc338636e5b4c.jpg', '', 1605434532, 1605435098, 1);
INSERT INTO `cmf_user_auth` VALUES (42606, '溥仪', '13927888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/5c4772e05eb370803a0db3b212f081b3.jpg', 'http://7niuobs.jiextx.com/default/20201115/b24eb16a2586f958e4f72ccaa9950271.jpg', 'http://7niuobs.jiextx.com/default/20201115/8ca13e2e7fe2d4147005bb0e06f06d78.jpg', '', 1605434589, 1605435106, 1);
INSERT INTO `cmf_user_auth` VALUES (42607, '霍馨', '13928888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/40e48e00f8c835fbd9dfbf1f8146c129.jpg', 'http://7niuobs.jiextx.com/default/20201115/c766814d7c834aabd2d8bdf182037f64.jpg', 'http://7niuobs.jiextx.com/default/20201115/01261ac279f028c5355ea2f8e2b2b63e.jpg', '', 1605434674, 1605435113, 1);
INSERT INTO `cmf_user_auth` VALUES (42608, '豆芽', '13929888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/549abdcb99363bd9310bf20818129a45.jpg', 'http://7niuobs.jiextx.com/default/20201115/58cd4fb6b6c03105a8083d3247fad6ea.jpg', 'http://7niuobs.jiextx.com/default/20201115/e9bc3c16199b68006b1181136e4e4fbe.jpg', '', 1605434722, 1605435120, 1);
INSERT INTO `cmf_user_auth` VALUES (42609, '翟迩', '13930888888', '13040419910101424x', 'http://7niuobs.jiextx.com/default/20201115/5c0133640ac985f8e8fe0d3a2a3c0306.jpg', 'http://7niuobs.jiextx.com/default/20201115/6b3300a78b8f61d94fd56b08d6686bdd.jpg', 'http://7niuobs.jiextx.com/default/20201115/ea03c1aaf7fb64b20be42f582e613561.jpg', '', 1605434815, 1605532511, 2);
INSERT INTO `cmf_user_auth` VALUES (42614, '', '13399999998', '', '', '', '', '后台创建主播', 1606374039, 0, 1);
INSERT INTO `cmf_user_auth` VALUES (42615, '', '13399999997', '', '', '', '', '后台创建主播', 1606374228, 1606374228, 1);
INSERT INTO `cmf_user_auth` VALUES (42616, '', '13399999996', '', '', '', '', '后台创建主播', 1606374398, 1606374398, 1);
INSERT INTO `cmf_user_auth` VALUES (42617, '', '13399999911', '', '', '', '', '后台创建主播', 1606374968, 1606374968, 1);
INSERT INTO `cmf_user_auth` VALUES (42618, '', '13399911111', '', '', '', '', '后台创建主播', 1606375494, 1606375494, 1);
INSERT INTO `cmf_user_auth` VALUES (42619, '', '13399999888', '', '', '', '', '后台创建主播', 1606376756, 1606376756, 1);

-- ----------------------------
-- Table structure for cmf_user_balance_cashrecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_balance_cashrecord`;
CREATE TABLE `cmf_user_balance_cashrecord`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `money` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '提现金额',
  `orderno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方订单号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态，0审核中，1审核通过，2审核拒绝',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '申请时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '账号类型 1 支付宝 2 微信 3 银行卡',
  `account_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '银行名称',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '帐号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_balance_record
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_balance_record`;
CREATE TABLE `cmf_user_balance_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL COMMENT '用户id',
  `touid` bigint(20) NOT NULL COMMENT '对方用户id',
  `balance` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '操作的余额数',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支类型,0支出1收入',
  `action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支行为 1 买家使用余额付款 2 系统自动结算货款给卖家  3 卖家超时未发货,退款给买家 4 买家发起退款，卖家超时未处理,系统自动退款 5买家发起退款，卖家同意 6 买家发起退款，平台介入后同意 7 用户使用余额购买付费项目  8 付费项目收入',
  `orderid` bigint(20) NOT NULL DEFAULT 0 COMMENT '对应的订单ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_banrecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_banrecord`;
CREATE TABLE `cmf_user_banrecord`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ban_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '被禁用原因',
  `ban_long` int(10) DEFAULT 0 COMMENT '用户禁用时长：单位：分钟',
  `uid` int(10) DEFAULT 0 COMMENT '禁用 用户ID',
  `addtime` int(10) DEFAULT 0 COMMENT '提交时间',
  `end_time` int(10) DEFAULT 0 COMMENT '禁用到期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_banrecord
-- ----------------------------
INSERT INTO `cmf_user_banrecord` VALUES (1, 'dsf', 1606233600, 42492, 1604295428, 0);
INSERT INTO `cmf_user_banrecord` VALUES (2, '测试1112', 1605110400, 42487, 1604293814, 0);
INSERT INTO `cmf_user_banrecord` VALUES (3, 'sddas', 1605024000, 42493, 1604298507, 0);
INSERT INTO `cmf_user_banrecord` VALUES (4, '123456', 1604246400, 42500, 1604300746, 0);
INSERT INTO `cmf_user_banrecord` VALUES (5, '111', 1604419200, 42505, 1604303370, 0);
INSERT INTO `cmf_user_banrecord` VALUES (6, '111111111111', 1605542399, 42504, 1605528952, 0);
INSERT INTO `cmf_user_banrecord` VALUES (7, 'dsd', 1604764799, 42551, 1604732416, 0);
INSERT INTO `cmf_user_banrecord` VALUES (8, '3232', 1606579199, 42488, 1605284559, 0);
INSERT INTO `cmf_user_banrecord` VALUES (9, '3232', 1606579199, 42488, 1605284559, 0);
INSERT INTO `cmf_user_banrecord` VALUES (10, '1', 1605542399, 42521, 1605528979, 0);
INSERT INTO `cmf_user_banrecord` VALUES (11, '11', 1605801599, 42499, 1605787047, 0);

-- ----------------------------
-- Table structure for cmf_user_black
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_black`;
CREATE TABLE `cmf_user_black`  (
  `uid` int(12) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(12) NOT NULL DEFAULT 0 COMMENT '被拉黑人ID',
  INDEX `uid_touid_index`(`uid`, `touid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_black
-- ----------------------------
INSERT INTO `cmf_user_black` VALUES (42515, 42503);
INSERT INTO `cmf_user_black` VALUES (42545, 42566);
INSERT INTO `cmf_user_black` VALUES (42567, 42545);

-- ----------------------------
-- Table structure for cmf_user_coinrecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_coinrecord`;
CREATE TABLE `cmf_user_coinrecord`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支类型,0支出1收入',
  `action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支行为，1赠送礼物,2弹幕,3登录奖励,4购买VIP,5购买坐骑,6房间扣费,7计时扣费,8发送红包,9抢红包,10开通守护,11注册奖励,12礼物中奖,13奖池中奖,14缴纳保证金,15退还保证金,16转盘游戏,17转盘中奖,18购买靓号,19游戏下注,20游戏退还,21每日任务奖励',
  `uid` int(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(20) NOT NULL DEFAULT 0 COMMENT '对方ID',
  `giftid` int(20) NOT NULL DEFAULT 0 COMMENT '行为对应ID',
  `giftcount` int(20) NOT NULL DEFAULT 0 COMMENT '数量',
  `totalcoin` int(20) NOT NULL DEFAULT 0 COMMENT '总价',
  `showid` int(12) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mark` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识，1表示热门礼物，2表示守护礼物',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `action_uid_addtime`(`action`, `uid`, `addtime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 383 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_coinrecord
-- ----------------------------
INSERT INTO `cmf_user_coinrecord` VALUES (1, 1, 3, 42486, 42486, 1, 0, 10, 0, 1604057524, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (2, 1, 3, 42487, 42487, 1, 0, 10, 0, 1604058226, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (3, 1, 3, 42487, 42487, 2, 0, 20, 0, 1604118292, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (4, 1, 3, 42486, 42486, 2, 0, 20, 0, 1604124930, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (5, 1, 3, 42487, 42487, 3, 0, 30, 0, 1604290029, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (6, 1, 3, 42513, 42513, 1, 0, 10, 0, 1604318733, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (7, 1, 3, 42514, 42514, 1, 0, 10, 0, 1604320525, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (8, 0, 4, 42513, 42513, 1, 1, 100, 0, 1604320906, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (9, 0, 4, 42513, 42513, 1, 1, 100, 0, 1604321062, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (10, 0, 5, 42513, 42513, 2, 1, 200, 0, 1604321502, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (11, 1, 3, 42496, 42496, 1, 0, 10, 0, 1604322527, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (12, 0, 18, 42513, 42513, 2, 1, 800, 0, 1604322538, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (13, 1, 3, 42513, 42513, 2, 0, 20, 0, 1604372950, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (14, 1, 3, 42514, 42514, 2, 0, 20, 0, 1604373455, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (15, 0, 1, 42514, 1, 62, 1, 1, 1604129295, 1604374726, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (16, 1, 3, 42487, 42487, 4, 0, 40, 0, 1604376334, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (17, 1, 3, 42517, 42517, 1, 0, 10, 0, 1604377613, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (18, 1, 3, 42496, 42496, 2, 0, 20, 0, 1604381975, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (19, 0, 2, 42515, 42515, 0, 1, 10, 1604382660, 1604382814, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (20, 0, 2, 42515, 42515, 0, 1, 10, 1604382660, 1604382835, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (21, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383948, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (22, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383949, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (23, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383950, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (24, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383951, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (25, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383952, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (26, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383952, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (27, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383952, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (28, 0, 1, 42513, 42514, 3, 1, 1000, 1604383859, 1604383952, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (29, 0, 8, 42513, 42513, 1, 1, 100, 1604386353, 1604386777, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (30, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386917, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (31, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386919, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (32, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386919, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (33, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386919, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (34, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386920, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (35, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386920, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (36, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386921, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (37, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386921, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (38, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386921, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (39, 0, 1, 42513, 42520, 62, 1, 1, 1604386353, 1604386922, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (40, 1, 9, 42520, 42520, 1, 1, 14, 1604386353, 1604386958, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (41, 0, 1, 42513, 42520, 8, 1, 100, 1604386353, 1604387013, 1);
INSERT INTO `cmf_user_coinrecord` VALUES (42, 0, 1, 42513, 42520, 8, 1, 100, 1604386353, 1604387015, 1);
INSERT INTO `cmf_user_coinrecord` VALUES (43, 0, 1, 42513, 42520, 8, 1, 100, 1604386353, 1604387022, 1);
INSERT INTO `cmf_user_coinrecord` VALUES (44, 0, 1, 42513, 42520, 8, 1, 100, 1604386353, 1604387022, 1);
INSERT INTO `cmf_user_coinrecord` VALUES (45, 0, 1, 42513, 42520, 8, 1, 100, 1604386353, 1604387023, 1);
INSERT INTO `cmf_user_coinrecord` VALUES (46, 1, 3, 42520, 42520, 1, 0, 10, 0, 1604387235, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (47, 1, 21, 42514, 42514, 0, 0, 10, 0, 1604387543, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (48, 1, 3, 42521, 42521, 1, 0, 10, 0, 1604391157, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (49, 0, 2, 42515, 42492, 0, 1, 10, 1604390390, 1604392128, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (50, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604392141, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (51, 1, 3, 42519, 42519, 1, 0, 10, 0, 1604392405, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (52, 1, 3, 42522, 42522, 1, 0, 10, 0, 1604392611, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (53, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604392958, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (54, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393030, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (55, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393033, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (56, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393034, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (57, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393035, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (58, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393035, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (59, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393035, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (60, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393035, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (61, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393035, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (62, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393036, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (63, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393036, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (64, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393036, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (65, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393036, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (66, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393037, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (67, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393037, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (68, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393037, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (69, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393037, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (70, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393037, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (71, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393037, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (72, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393037, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (73, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393038, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (74, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393038, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (75, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393038, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (76, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393038, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (77, 0, 1, 42515, 42492, 62, 1, 1, 1604390782, 1604393039, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (78, 0, 1, 42515, 42492, 7, 1, 300, 1604390782, 1604393059, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (79, 0, 1, 42515, 42492, 7, 1, 300, 1604390782, 1604393066, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (80, 0, 1, 42515, 42492, 5, 1, 500, 1604390782, 1604393074, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (81, 0, 1, 42515, 42492, 4, 1, 10000, 1604390782, 1604393430, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (82, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393708, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (83, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393712, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (84, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393714, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (85, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393715, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (86, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393715, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (87, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393715, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (88, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393716, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (89, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393716, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (90, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393716, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (91, 0, 1, 42521, 42514, 62, 1, 1, 1604392678, 1604393716, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (92, 1, 21, 42521, 42521, 0, 0, 10, 0, 1604393758, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (93, 1, 3, 42523, 42523, 1, 0, 10, 0, 1604397856, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (94, 1, 21, 42523, 42523, 0, 0, 10, 0, 1604400661, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (95, 1, 3, 42524, 42524, 1, 0, 10, 0, 1604401464, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (96, 0, 1, 42513, 42516, 3, 1, 1000, 1604403971, 1604404057, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (97, 0, 1, 42513, 42516, 3, 1, 1000, 1604403971, 1604404059, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (98, 0, 1, 42513, 42516, 3, 1, 1000, 1604403971, 1604404060, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (99, 1, 3, 42525, 42525, 1, 0, 10, 0, 1604407119, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (100, 1, 3, 42519, 42519, 2, 0, 20, 0, 1604450375, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (101, 1, 3, 42524, 42524, 2, 0, 20, 0, 1604463522, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (102, 1, 3, 42517, 42517, 2, 0, 20, 0, 1604465158, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (103, 1, 3, 42496, 42496, 3, 0, 30, 0, 1604467792, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (104, 0, 4, 42520, 42520, 1, 1, 100, 0, 1604468121, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (105, 0, 5, 42520, 42520, 5, 1, 300, 0, 1604468141, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (106, 0, 1, 42516, 42520, 5, 1, 500, 1604468385, 1604468529, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (107, 0, 8, 42516, 42516, 2, 1, 100, 1604468385, 1604468657, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (108, 1, 9, 42520, 42520, 2, 1, 1, 1604468385, 1604468663, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (109, 0, 8, 42516, 42516, 3, 1, 100, 1604468385, 1604468687, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (110, 1, 9, 42520, 42520, 3, 1, 100, 1604468385, 1604468699, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (111, 1, 21, 42516, 42516, 0, 0, 10, 0, 1604468760, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (112, 0, 16, 42516, 42516, 0, 4, 108, 0, 1604468789, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (113, 1, 17, 42516, 42516, 0, 1, 201, 1604468385, 1604468789, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (114, 1, 17, 42516, 42516, 0, 1, 5000, 1604468385, 1604468789, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (115, 0, 16, 42516, 42516, 0, 4, 108, 0, 1604468806, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (116, 1, 17, 42516, 42516, 0, 1, 10000, 1604468385, 1604468806, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (117, 0, 16, 42516, 42516, 0, 4, 108, 0, 1604468814, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (118, 1, 17, 42516, 42516, 0, 1, 201, 1604468385, 1604468814, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (119, 0, 16, 42516, 42516, 0, 4, 108, 0, 1604468829, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (120, 1, 17, 42516, 42516, 0, 1, 402, 1604468385, 1604468829, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (121, 0, 16, 42516, 42516, 0, 4, 108, 0, 1604468912, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (122, 0, 16, 42516, 42516, 0, 4, 108, 0, 1604468923, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (123, 0, 16, 42516, 42516, 0, 4, 108, 0, 1604468942, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (124, 1, 17, 42516, 42516, 0, 1, 201, 1604468385, 1604468942, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (125, 1, 17, 42516, 42516, 0, 1, 10000, 1604468385, 1604468942, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (126, 0, 10, 42516, 42520, 1, 1, 300, 1604468385, 1604468998, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (127, 0, 10, 42516, 42520, 2, 1, 1000, 1604468385, 1604469030, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (128, 0, 2, 42516, 42520, 0, 1, 10, 1604468385, 1604469371, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (129, 0, 2, 42516, 42520, 0, 1, 10, 1604468385, 1604469399, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (130, 1, 3, 42487, 42487, 5, 0, 50, 0, 1604470839, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (131, 1, 3, 42525, 42525, 2, 0, 20, 0, 1604477555, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (132, 1, 3, 42527, 42527, 1, 0, 10, 0, 1604478497, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (133, 1, 3, 42528, 42528, 1, 0, 10, 0, 1604478651, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (134, 1, 3, 42529, 42529, 1, 0, 10, 0, 1604478655, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (135, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478866, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (136, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478867, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (137, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478868, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (138, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478868, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (139, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478868, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (140, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478868, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (141, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478868, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (142, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478868, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (143, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478869, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (144, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478869, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (145, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478869, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (146, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478869, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (147, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478869, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (148, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478870, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (149, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478870, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (150, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478870, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (151, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478870, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (152, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478870, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (153, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478871, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (154, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478872, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (155, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478872, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (156, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478872, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (157, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478872, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (158, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478872, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (159, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478873, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (160, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478873, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (161, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478873, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (162, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478873, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (163, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478873, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (164, 0, 1, 42525, 42529, 62, 1, 1, 1604478681, 1604478874, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (165, 0, 1, 42524, 42517, 12, 1, 5, 1604480600, 1604480644, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (166, 0, 1, 42529, 42511, 21, 1, 1, 1604486095, 1604486260, 1);
INSERT INTO `cmf_user_coinrecord` VALUES (167, 1, 3, 42514, 42514, 3, 0, 30, 0, 1604489126, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (168, 1, 3, 42521, 42521, 2, 0, 20, 0, 1604489159, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (169, 0, 1, 42492, 42515, 22, 1, 2, 1604489098, 1604490310, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (170, 0, 1, 42492, 42515, 19, 1, 10, 1604489098, 1604490316, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (171, 0, 1, 42492, 42515, 19, 1, 10, 1604489098, 1604490323, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (172, 1, 3, 42531, 42531, 1, 0, 10, 0, 1604490764, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (173, 1, 3, 42534, 42534, 1, 0, 10, 0, 1604491462, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (174, 1, 21, 42531, 42531, 0, 0, 10, 0, 1604491579, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (175, 0, 1, 42531, 42514, 62, 1, 1, 1604491293, 1604491590, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (176, 0, 1, 42516, 42514, 5, 1, 500, 1604491293, 1604491758, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (177, 0, 1, 42516, 42514, 5, 1, 500, 1604491293, 1604491760, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (178, 0, 1, 42516, 42514, 13, 10, 3000, 1604491293, 1604491787, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (179, 0, 1, 42516, 42514, 13, 10, 3000, 1604491293, 1604491793, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (180, 0, 1, 42516, 42514, 28, 10, 280, 1604491293, 1604491810, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (181, 0, 1, 42516, 42514, 28, 10, 280, 1604491293, 1604491817, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (182, 0, 1, 42531, 42514, 62, 1, 1, 1604491293, 1604491837, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (183, 0, 1, 42533, 42514, 3, 1, 1000, 1604491293, 1604491878, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (184, 0, 10, 42516, 42514, 1, 1, 300, 1604491293, 1604491955, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (185, 0, 19, 42516, 42516, 2, 1, 10, 0, 1604492050, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (186, 0, 19, 42534, 42534, 2, 1, 10, 0, 1604492052, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (187, 0, 19, 42516, 42516, 2, 1, 10, 0, 1604492059, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (188, 0, 19, 42516, 42516, 2, 1, 10, 0, 1604492065, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (189, 0, 2, 42533, 42514, 0, 1, 10, 1604491293, 1604492185, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (190, 0, 2, 42514, 42514, 0, 1, 10, 1604491293, 1604492211, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (191, 0, 1, 42533, 42514, 3, 1, 1000, 0, 1604492310, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (192, 0, 2, 42492, 1, 0, 1, 10, 1604129295, 1604492775, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (193, 0, 2, 42492, 42515, 0, 1, 10, 1604489018, 1604492953, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (194, 0, 1, 42492, 42515, 12, 1, 5, 1604489098, 1604492963, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (195, 0, 1, 42492, 42515, 13, 1, 300, 1604489098, 1604492966, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (196, 0, 1, 42492, 42515, 9, 1, 500, 1604489098, 1604492969, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (197, 1, 3, 42538, 42538, 1, 0, 10, 0, 1604494716, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (198, 1, 3, 42539, 42539, 1, 0, 10, 0, 1604495954, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (199, 1, 3, 42530, 42530, 1, 0, 10, 0, 1604496404, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (200, 1, 3, 42541, 42541, 1, 0, 10, 0, 1604502600, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (201, 0, 1, 42488, 1, 62, 1, 1, 1604129295, 1604509049, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (202, 0, 16, 42488, 42488, 0, 4, 108, 0, 1604512125, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (203, 1, 3, 42519, 42519, 3, 0, 30, 0, 1604537155, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (204, 1, 3, 42528, 42528, 2, 0, 20, 0, 1604549087, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (205, 1, 3, 42524, 42524, 3, 0, 30, 0, 1604550863, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (206, 0, 2, 42492, 42492, 0, 1, 10, 1604551622, 1604551710, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (207, 0, 2, 42492, 42492, 0, 1, 10, 1604551622, 1604551731, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (208, 0, 2, 42492, 42492, 0, 1, 10, 1604551622, 1604551840, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (209, 1, 3, 42517, 42517, 3, 0, 30, 0, 1604552362, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (210, 1, 3, 42523, 42523, 2, 0, 20, 0, 1604554954, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (211, 1, 3, 42514, 42514, 4, 0, 40, 0, 1604555659, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (212, 1, 3, 42541, 42541, 2, 0, 20, 0, 1604560763, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (213, 0, 2, 42492, 42492, 0, 1, 10, 1604561271, 1604561365, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (214, 0, 2, 42515, 42492, 0, 1, 10, 1604561271, 1604561624, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (215, 0, 2, 42515, 42492, 0, 1, 10, 1604561271, 1604561636, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (216, 0, 1, 42492, 1, 62, 1, 1, 1604129295, 1604561887, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (217, 0, 1, 42492, 1, 62, 1, 1, 1604129295, 1604561896, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (218, 1, 3, 42521, 42521, 3, 0, 30, 0, 1604561952, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (219, 0, 2, 42492, 42492, 0, 1, 10, 1604562092, 1604562138, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (220, 0, 2, 42492, 42492, 0, 1, 10, 1604562092, 1604562152, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (221, 0, 2, 42543, 42539, 0, 1, 10, 1604558606, 1604562626, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (222, 0, 2, 42543, 42492, 0, 1, 10, 1604562092, 1604562660, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (223, 0, 2, 42543, 42492, 0, 1, 10, 1604562092, 1604562678, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (224, 0, 2, 42543, 42492, 0, 1, 10, 1604562092, 1604562717, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (225, 1, 3, 42496, 42496, 4, 0, 40, 0, 1604563581, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (226, 0, 2, 42492, 42515, 0, 1, 10, 1604562328, 1604563911, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (227, 0, 1, 42492, 1, 3, 1, 1000, 1604129295, 1604564905, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (228, 0, 2, 42492, 42515, 0, 1, 10, 1604562328, 1604565942, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (229, 0, 2, 42543, 42539, 0, 1, 10, 1604558606, 1604566108, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (230, 0, 2, 42492, 42492, 0, 1, 10, 1604566111, 1604566142, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (231, 0, 2, 42492, 42492, 0, 1, 10, 1604566111, 1604566350, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (232, 0, 2, 42492, 42492, 0, 1, 10, 1604569827, 1604569878, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (233, 1, 3, 42492, 42492, 1, 0, 10, 0, 1604571744, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (234, 0, 2, 42492, 42492, 0, 1, 10, 1604571917, 1604572139, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (235, 0, 2, 42492, 42492, 0, 1, 10, 1604571917, 1604572155, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (236, 0, 2, 42492, 42492, 0, 1, 10, 1604571917, 1604572227, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (237, 0, 2, 42492, 42492, 0, 1, 10, 1604571917, 1604572245, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (238, 0, 2, 42492, 42492, 0, 1, 10, 1604571917, 1604572263, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (239, 0, 2, 42492, 42492, 0, 1, 10, 1604571917, 1604572275, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (240, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572275, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (241, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572278, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (242, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572278, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (243, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572279, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (244, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572279, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (245, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572279, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (246, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572279, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (247, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572280, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (248, 0, 1, 42529, 42492, 62, 1, 1, 1604571917, 1604572280, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (249, 0, 2, 42515, 42542, 0, 1, 10, 1604573031, 1604573187, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (250, 0, 2, 42492, 42492, 0, 1, 10, 1604574022, 1604574089, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (251, 0, 2, 42515, 1, 0, 1, 10, 1604129295, 1604574264, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (252, 1, 3, 42529, 42529, 2, 0, 20, 0, 1604575058, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (253, 0, 2, 42513, 42538, 0, 1, 10, 1604575872, 1604576084, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (254, 0, 2, 42513, 42538, 0, 1, 10, 1604575872, 1604576085, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (255, 0, 2, 42538, 42538, 0, 1, 10, 1604575872, 1604576327, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (256, 1, 3, 42538, 42538, 2, 0, 20, 0, 1604576705, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (257, 0, 2, 42521, 42538, 0, 1, 10, 1604576879, 1604577402, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (258, 1, 3, 42515, 42515, 1, 0, 10, 0, 1604577796, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (259, 0, 2, 42492, 42492, 0, 1, 10, 1604578157, 1604578295, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (260, 1, 3, 42531, 42531, 2, 0, 20, 0, 1604578318, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (261, 0, 2, 42521, 42492, 0, 1, 10, 1604579038, 1604579383, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (262, 0, 2, 42545, 42492, 0, 1, 10, 1604580370, 1604580882, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (263, 0, 2, 42545, 42492, 0, 1, 10, 1604580370, 1604580903, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (264, 0, 2, 42545, 42492, 0, 1, 10, 1604580370, 1604580913, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (265, 1, 3, 42533, 42533, 1, 0, 10, 0, 1604581828, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (266, 0, 2, 42545, 42492, 0, 1, 10, 1604581224, 1604583156, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (267, 1, 3, 42488, 42488, 2, 0, 20, 0, 1604592146, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (268, 1, 3, 42541, 42541, 3, 0, 30, 0, 1604593638, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (269, 1, 3, 42519, 42519, 4, 0, 40, 0, 1604625458, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (270, 1, 3, 42514, 42514, 5, 0, 50, 0, 1604633149, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (271, 1, 3, 42531, 42531, 3, 0, 30, 0, 1604635439, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (272, 0, 1, 42542, 42514, 3, 1, 1000, 1604635541, 1604635803, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (273, 0, 1, 42542, 1, 3, 1, 1000, 1604129295, 1604635865, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (274, 1, 3, 42523, 42523, 3, 0, 30, 0, 1604636822, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (275, 1, 3, 42524, 42524, 4, 0, 40, 0, 1604637229, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (276, 0, 2, 42545, 42514, 0, 1, 10, 1604635541, 1604639517, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (277, 0, 2, 42545, 42514, 0, 1, 10, 1604635541, 1604639641, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (278, 0, 2, 42545, 42514, 0, 1, 10, 1604635541, 1604639672, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (279, 0, 1, 42542, 42523, 13, 1, 300, 1604638930, 1604639748, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (280, 0, 1, 42542, 42514, 62, 1, 1, 1604635541, 1604639975, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (281, 0, 2, 42545, 42514, 0, 1, 10, 1604635541, 1604639994, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (282, 0, 2, 42545, 42514, 0, 1, 10, 1604635541, 1604639998, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (283, 1, 3, 42529, 42529, 3, 0, 30, 0, 1604644053, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (284, 0, 1, 42542, 42523, 7, 1, 300, 1604643841, 1604644436, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (285, 1, 3, 42548, 42548, 1, 0, 10, 0, 1604644511, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (286, 0, 1, 42492, 42523, 8, 1, 100, 1604643841, 1604644714, 1);
INSERT INTO `cmf_user_coinrecord` VALUES (287, 0, 1, 42492, 42523, 62, 1, 1, 1604643841, 1604644795, 3);
INSERT INTO `cmf_user_coinrecord` VALUES (288, 1, 3, 42521, 42521, 4, 0, 40, 0, 1604650555, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (289, 0, 2, 42545, 42545, 0, 1, 10, 1604658453, 1604658576, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (290, 1, 3, 42499, 42499, 1, 0, 10, 0, 1604660649, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (291, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660729, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (292, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660737, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (293, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660743, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (294, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660754, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (295, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660763, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (296, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660768, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (297, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660799, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (298, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604660958, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (299, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604661020, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (300, 0, 2, 42492, 42545, 0, 1, 10, 1604660645, 1604661073, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (301, 1, 3, 42496, 42496, 5, 0, 50, 0, 1604661603, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (302, 0, 2, 42545, 42545, 0, 1, 10, 1604661675, 1604661757, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (303, 0, 2, 42545, 42545, 0, 1, 10, 1604661675, 1604661850, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (304, 0, 2, 42545, 42533, 0, 1, 10, 1604661975, 1604662239, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (305, 0, 2, 42545, 42533, 0, 1, 10, 1604661975, 1604662256, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (306, 0, 2, 42549, 42533, 0, 1, 10, 1604661975, 1604662447, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (307, 0, 2, 42545, 42533, 0, 1, 10, 1604661975, 1604663275, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (308, 0, 2, 42545, 42533, 0, 1, 10, 1604661975, 1604664038, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (309, 1, 3, 42551, 42551, 1, 0, 10, 0, 1604666718, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (310, 1, 3, 42505, 42505, 1, 0, 10, 0, 1604667057, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (311, 0, 2, 42545, 42545, 0, 1, 10, 1604728287, 1604728362, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (312, 1, 3, 42545, 42545, 1, 0, 10, 0, 1604734482, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (313, 0, 2, 42545, 42545, 0, 1, 10, 1604735826, 1604735867, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (314, 1, 3, 42541, 42541, 4, 0, 40, 0, 1604743225, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (315, 0, 2, 42545, 42521, 0, 1, 10, 1604745947, 1604746093, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (316, 0, 2, 42545, 42521, 0, 1, 10, 1604745947, 1604746101, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (317, 0, 2, 42545, 42521, 0, 1, 10, 1604745947, 1604746265, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (318, 0, 2, 42545, 42554, 0, 1, 10, 1604747613, 1604747736, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (319, 1, 3, 42496, 42496, 6, 0, 60, 0, 1604749541, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (320, 1, 3, 42557, 42557, 1, 0, 10, 0, 1604752121, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (321, 1, 3, 42542, 42542, 1, 0, 10, 0, 1604772889, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (322, 0, 2, 42545, 1, 0, 1, 10, 1604828362, 1604833024, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (323, 0, 2, 42492, 42492, 0, 1, 10, 1604838690, 1604838865, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (324, 0, 2, 42492, 42492, 0, 1, 10, 1604839561, 1604840576, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (325, 0, 2, 42492, 42492, 0, 1, 10, 1604839561, 1604840587, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (326, 0, 2, 42539, 42496, 0, 1, 10, 1604841910, 1604841957, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (327, 0, 2, 42539, 42496, 0, 1, 10, 1604842084, 1604842131, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (328, 0, 2, 42539, 42496, 0, 1, 10, 1604842084, 1604842137, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (329, 0, 2, 42492, 42492, 0, 1, 10, 1604839561, 1604842469, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (330, 0, 2, 42492, 42521, 0, 1, 10, 1604901740, 1604901939, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (331, 0, 2, 42492, 42492, 0, 1, 10, 1604902268, 1604902356, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (332, 0, 2, 42539, 42521, 0, 1, 10, 1604901740, 1604902604, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (333, 0, 2, 42492, 42492, 0, 1, 10, 1604902268, 1604902941, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (334, 0, 2, 42492, 42492, 0, 1, 10, 1604902268, 1604903237, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (335, 0, 2, 42492, 42492, 0, 1, 10, 1604902268, 1604903314, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (336, 0, 2, 42545, 42545, 0, 1, 10, 1604904227, 1604904984, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (337, 0, 2, 42545, 42545, 0, 1, 10, 1604905175, 1604905340, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (338, 0, 2, 42545, 42545, 0, 1, 10, 1604905175, 1604905531, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (339, 0, 2, 42545, 42545, 0, 1, 10, 1604905175, 1604905695, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (340, 0, 2, 42496, 42545, 0, 1, 10, 1604905175, 1604905935, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (341, 0, 2, 42496, 42545, 0, 1, 10, 1604905175, 1604905939, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (342, 0, 2, 42496, 42521, 0, 1, 10, 1604906286, 1604906394, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (343, 0, 2, 42496, 42521, 0, 1, 10, 1604906286, 1604906414, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (344, 0, 2, 42496, 42492, 0, 1, 10, 1604902268, 1604906738, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (345, 0, 2, 42496, 42492, 0, 1, 10, 1604902268, 1604906742, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (346, 0, 2, 42566, 42515, 0, 1, 10, 1604906787, 1604907589, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (347, 0, 2, 42545, 42515, 0, 1, 10, 1604906787, 1604910455, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (348, 0, 2, 42545, 42545, 0, 1, 10, 1604910724, 1604910884, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (349, 0, 2, 42566, 42515, 0, 1, 10, 1604906787, 1604912085, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (350, 0, 2, 42566, 42567, 0, 1, 10, 1604924878, 1604925098, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (351, 0, 2, 42539, 42539, 0, 1, 10, 1604924854, 1604925430, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (352, 0, 2, 42539, 42567, 0, 1, 10, 1604924878, 1604925803, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (353, 0, 2, 42539, 42567, 0, 1, 10, 1604924878, 1604925909, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (354, 0, 2, 42539, 42567, 0, 1, 10, 1604924878, 1604926004, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (355, 0, 2, 42539, 42516, 0, 1, 10, 1604926584, 1604926604, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (356, 1, 3, 42539, 42539, 2, 0, 20, 0, 1604930870, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (357, 1, 3, 42529, 42529, 4, 0, 40, 0, 1605002959, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (358, 0, 2, 42563, 42529, 0, 1, 10, 1605003029, 1605003167, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (359, 0, 2, 42563, 42529, 0, 1, 10, 1605003029, 1605003178, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (360, 0, 2, 42539, 42529, 0, 1, 10, 1605003029, 1605003183, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (361, 0, 2, 42563, 42529, 0, 1, 10, 1605003029, 1605003187, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (362, 0, 2, 42538, 42583, 0, 1, 10, 1604999446, 1605003714, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (363, 1, 3, 42588, 42588, 1, 0, 10, 0, 1605351883, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (364, 1, 3, 42589, 42589, 1, 0, 10, 0, 1605364295, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (365, 1, 3, 42588, 42588, 2, 0, 20, 0, 1605500395, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (366, 0, 2, 42584, 42507, 0, 1, 10, 1605082847, 1605533353, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (367, 1, 3, 42588, 42588, 3, 0, 30, 0, 1605542604, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (368, 1, 3, 42528, 42528, 3, 0, 30, 0, 1605597404, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (369, 0, 2, 42545, 42545, 0, 1, 10, 1605770705, 1605772533, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (370, 0, 2, 42543, 42545, 0, 1, 10, 1605770705, 1605772617, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (371, 0, 2, 42543, 42545, 0, 1, 10, 1605770705, 1605772637, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (372, 0, 2, 42543, 42545, 0, 1, 10, 1605770705, 1605772651, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (373, 0, 2, 42545, 42545, 0, 1, 10, 1605770705, 1605772759, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (374, 0, 2, 42545, 42545, 0, 1, 10, 1605770705, 1605772771, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (375, 0, 2, 42545, 42545, 0, 1, 10, 1605772981, 1605773063, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (376, 0, 2, 42545, 42545, 0, 1, 10, 1605773758, 1605773885, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (377, 0, 2, 42545, 42521, 0, 1, 10, 1605790698, 1605793738, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (378, 0, 2, 42503, 42499, 0, 1, 10, 1605841719, 1605843678, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (379, 0, 2, 42545, 42499, 0, 1, 10, 1605841719, 1605843904, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (380, 0, 2, 42545, 42499, 0, 1, 10, 1605843952, 1605844241, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (381, 0, 2, 42545, 42499, 0, 1, 10, 1605844435, 1605844616, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (382, 0, 2, 42545, 42545, 0, 1, 10, 1605844980, 1605845087, 0);

-- ----------------------------
-- Table structure for cmf_user_daily_tasks
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_daily_tasks`;
CREATE TABLE `cmf_user_daily_tasks`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL DEFAULT 0 COMMENT '用户uid',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '任务类型 1观看直播, 2观看视频, 3直播奖励, 4打赏奖励, 5分享奖励',
  `target` int(11) NOT NULL DEFAULT 0 COMMENT '目标',
  `schedule` int(11) NOT NULL DEFAULT 0 COMMENT '当前进度',
  `reward` int(5) NOT NULL DEFAULT 0 COMMENT '奖励钻石数量',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '生成时间',
  `uptime` int(12) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `state` tinyint(1) DEFAULT 0 COMMENT '状态 0未达成  1可领取  2已领取',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `uid`(`uid`, `type`) USING BTREE,
  INDEX `uid_2`(`uid`) USING BTREE,
  INDEX `uid_3`(`uid`, `type`, `addtime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 153 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '每日任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_daily_tasks
-- ----------------------------
INSERT INTO `cmf_user_daily_tasks` VALUES (1, 42486, 1, 2, 2, 10, 1603987200, 1604062083, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (2, 42487, 2, 2, 2, 10, 1604246400, 1604308929, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (3, 42487, 1, 2, 0, 10, 1604246400, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (4, 42487, 3, 1, 0, 30, 1604246400, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (5, 42487, 4, 100, 0, 100, 1604246400, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (6, 42487, 5, 1, 0, 200, 1604246400, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (7, 42496, 1, 2, 2, 10, 1604419200, 1604490172, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (8, 42514, 2, 2, 0, 10, 1604505600, 1604577959, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (9, 42513, 1, 2, 0, 10, 1604505600, 1604575771, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (10, 42514, 4, 100, 0, 100, 1604505600, 1604577959, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (11, 42514, 1, 2, 0, 10, 1604505600, 1604577959, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (12, 42514, 3, 1, 1, 30, 1604505600, 1604578397, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (13, 42514, 5, 1, 0, 200, 1604505600, 1604577959, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (14, 42513, 1, 2, 2, 10, 1604332800, 1604385900, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (15, 42496, 1, 2, 2, 10, 1604332800, 1604387144, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (16, 42496, 2, 2, 0, 10, 1604419200, 1604486093, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (17, 42513, 5, 1, 0, 200, 1604505600, 1604575771, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (18, 42513, 4, 100, 0, 100, 1604505600, 1604575771, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (19, 42520, 1, 2, 0, 10, 1604505600, 1604577115, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (20, 42520, 2, 2, 0, 10, 1604505600, 1604577115, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (21, 42520, 3, 1, 0, 30, 1604505600, 1604577115, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (22, 42520, 4, 100, 0, 100, 1604505600, 1604577115, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (23, 42520, 5, 1, 0, 200, 1604505600, 1604577115, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (24, 42519, 2, 2, 1, 10, 1604332800, 1604392481, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (25, 42521, 4, 100, 10, 100, 1604332800, 1604393716, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (26, 42521, 1, 2, 2, 10, 1604332800, 1604393758, 2);
INSERT INTO `cmf_user_daily_tasks` VALUES (27, 42521, 2, 2, 0, 10, 1604332800, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (28, 42521, 3, 1, 0, 30, 1604332800, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (29, 42521, 5, 1, 0, 200, 1604332800, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (30, 42523, 1, 2, 0, 10, 1604592000, 1604639222, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (31, 42523, 2, 2, 0, 10, 1604592000, 1604639222, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (32, 42523, 3, 1, 1, 30, 1604592000, 1604640013, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (33, 42523, 4, 100, 0, 100, 1604592000, 1604639222, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (34, 42523, 5, 1, 0, 200, 1604592000, 1604639222, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (35, 42524, 1, 2, 0, 10, 1604592000, 1604637850, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (36, 42524, 2, 2, 0, 10, 1604592000, 1604637850, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (37, 42524, 3, 1, 0, 30, 1604592000, 1604637850, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (38, 42524, 4, 100, 0, 100, 1604592000, 1604637850, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (39, 42524, 5, 1, 0, 200, 1604592000, 1604637850, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (40, 42525, 1, 2, 1, 10, 1604332800, 1604407213, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (41, 42525, 2, 2, 2, 10, 1604332800, 1604408023, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (42, 42519, 2, 2, 1, 10, 1604419200, 1604450426, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (43, 42516, 4, 100, 100, 100, 1604419200, 1604468529, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (44, 42516, 1, 2, 2, 10, 1604419200, 1604468760, 2);
INSERT INTO `cmf_user_daily_tasks` VALUES (45, 42516, 2, 2, 2, 10, 1604419200, 1604493307, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (46, 42516, 3, 1, 0, 30, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (47, 42516, 5, 1, 0, 200, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (48, 42496, 1, 2, 1, 10, 1604419200, 1604468763, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (49, 42487, 2, 2, 2, 10, 1604419200, 1604470978, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (50, 42528, 2, 2, 2, 10, 1604419200, 1604478690, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (51, 42525, 4, 100, 30, 100, 1604419200, 1604478874, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (52, 42496, 2, 2, 2, 10, 1604419200, 1604478884, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (53, 42525, 1, 2, 2, 10, 1604419200, 1604478895, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (54, 42496, 3, 1, 0, 30, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (55, 42496, 4, 100, 0, 100, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (56, 42496, 5, 1, 0, 200, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (57, 42518, 1, 2, 0, 10, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (58, 42518, 2, 2, 0, 10, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (59, 42518, 3, 1, 0, 30, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (60, 42518, 4, 100, 0, 100, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (61, 42518, 5, 1, 0, 200, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (62, 42534, 2, 2, 1, 10, 1604419200, 1604491505, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (63, 42531, 1, 2, 0, 10, 1604592000, 1604641841, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (64, 42531, 2, 2, 0, 10, 1604592000, 1604641841, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (65, 42531, 3, 1, 1, 30, 1604592000, 1604641929, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (66, 42531, 4, 100, 0, 100, 1604592000, 1604641841, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (67, 42531, 5, 1, 0, 200, 1604592000, 1604641841, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (68, 42534, 1, 2, 2, 10, 1604419200, 1604505352, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (69, 42534, 3, 1, 0, 30, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (70, 42534, 4, 100, 0, 100, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (71, 42534, 5, 1, 0, 200, 1604419200, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (72, 42517, 2, 2, 2, 10, 1604419200, 1604494049, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (73, 42488, 1, 2, 0, 10, 1604592000, 1604597721, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (74, 42488, 2, 2, 0, 10, 1604592000, 1604597721, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (75, 42488, 3, 1, 0, 30, 1604592000, 1604597721, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (76, 42488, 4, 100, 0, 100, 1604592000, 1604597721, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (77, 42488, 5, 1, 0, 200, 1604592000, 1604597721, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (78, 42530, 1, 2, 0, 10, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (79, 42530, 2, 2, 0, 10, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (80, 42530, 3, 1, 0, 30, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (81, 42530, 4, 100, 0, 100, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (82, 42530, 5, 1, 0, 200, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (83, 42519, 2, 2, 1, 10, 1604505600, 1604542712, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (84, 42536, 2, 2, 1, 10, 1604505600, 1604551230, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (85, 42496, 2, 2, 2, 10, 1604505600, 1604566081, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (86, 42496, 1, 2, 2, 10, 1604505600, 1604566907, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (87, 42513, 2, 2, 0, 10, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (88, 42513, 3, 1, 0, 30, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (89, 42492, 1, 2, 0, 10, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (90, 42492, 2, 2, 0, 10, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (91, 42492, 3, 1, 0, 30, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (92, 42492, 4, 100, 0, 100, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (93, 42492, 5, 1, 0, 200, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (94, 42516, 1, 2, 2, 10, 1604505600, 1604576555, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (95, 42520, 1, 2, 1, 10, 1604505600, 1604577115, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (96, 42541, 1, 2, 0, 10, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (97, 42541, 2, 2, 0, 10, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (98, 42541, 3, 1, 0, 30, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (99, 42541, 4, 100, 0, 100, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (100, 42541, 5, 1, 0, 200, 1604505600, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (101, 42524, 1, 2, 1, 10, 1604592000, 1604637850, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (102, 42516, 1, 2, 2, 10, 1604592000, 1604641592, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (103, 42496, 1, 2, 2, 10, 1604592000, 1604659665, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (104, 42499, 2, 2, 2, 10, 1604592000, 1604661721, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (105, 42516, 1, 2, 2, 10, 1604678400, 1604729813, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (106, 42545, 1, 2, 2, 10, 1604678400, 1604735080, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (107, 42545, 2, 2, 2, 10, 1604678400, 1604734907, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (108, 42552, 1, 2, 2, 10, 1604678400, 1604736477, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (109, 42496, 1, 2, 2, 10, 1604678400, 1604737097, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (110, 42496, 2, 2, 1, 10, 1604678400, 1604749558, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (111, 42496, 1, 2, 2, 10, 1604764800, 1604840501, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (112, 42516, 1, 2, 2, 10, 1604851200, 1604894158, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (113, 42513, 1, 2, 2, 10, 1604851200, 1604901370, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (114, 42563, 1, 2, 2, 10, 1604851200, 1604903084, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (115, 42518, 1, 2, 2, 10, 1604851200, 1604904743, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (116, 42496, 1, 2, 2, 10, 1604851200, 1604905995, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (117, 42539, 2, 2, 1, 10, 1604851200, 1604906193, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (118, 42568, 1, 2, 2, 10, 1604851200, 1604926785, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (119, 42543, 1, 2, 1, 10, 1604851200, 1604937228, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (120, 42578, 2, 2, 1, 10, 1604937600, 1604941377, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (121, 42563, 1, 2, 2, 10, 1604937600, 1605002987, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (122, 42584, 1, 2, 2, 10, 1604937600, 1605005473, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (123, 42579, 1, 2, 1, 10, 1605024000, 1605086972, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (124, 42563, 1, 2, 2, 10, 1605024000, 1605091031, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (125, 42488, 1, 2, 2, 10, 1605024000, 1605094915, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (126, 42496, 1, 2, 2, 10, 1605024000, 1605107385, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (127, 42516, 1, 2, 2, 10, 1605110400, 1605177790, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (128, 42518, 1, 2, 1, 10, 1605110400, 1605178677, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (129, 42586, 1, 2, 1, 10, 1605110400, 1605179694, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (130, 42584, 1, 2, 1, 10, 1605283200, 1605324348, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (131, 42588, 2, 2, 2, 10, 1605542400, 1605582725, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (132, 42548, 1, 2, 2, 10, 1605456000, 1605502362, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (133, 42496, 1, 2, 2, 10, 1605456000, 1605503359, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (134, 42588, 2, 2, 2, 10, 1605456000, 1605506757, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (135, 42518, 1, 2, 2, 10, 1605456000, 1605509848, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (136, 42563, 1, 2, 2, 10, 1605456000, 1605512733, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (137, 42582, 2, 2, 1, 10, 1605456000, 1605512707, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (138, 42584, 1, 2, 2, 10, 1605456000, 1605517622, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (139, 42588, 1, 2, 1, 10, 1605542400, 1605581432, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (140, 42588, 3, 1, 0, 30, 1605542400, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (141, 42588, 4, 100, 0, 100, 1605542400, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (142, 42588, 5, 1, 0, 200, 1605542400, 0, 0);
INSERT INTO `cmf_user_daily_tasks` VALUES (143, 42584, 1, 2, 2, 10, 1605542400, 1605587527, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (144, 42496, 1, 2, 2, 10, 1605542400, 1605612464, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (145, 42568, 1, 2, 2, 10, 1605628800, 1605672269, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (146, 42496, 1, 2, 2, 10, 1605628800, 1605678429, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (147, 0, 1, 2, 2, 10, 1605628800, 1605703452, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (148, 42513, 1, 2, 2, 10, 1605628800, 1605704176, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (149, 42513, 1, 2, 2, 10, 1605715200, 1605754560, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (150, 0, 1, 2, 2, 10, 1605715200, 1605754914, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (151, 42568, 1, 2, 2, 10, 1605715200, 1605764039, 1);
INSERT INTO `cmf_user_daily_tasks` VALUES (152, 42586, 1, 2, 2, 10, 1605715200, 1605765142, 1);

-- ----------------------------
-- Table structure for cmf_user_goods_visit
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_goods_visit`;
CREATE TABLE `cmf_user_goods_visit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` bigint(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `goodsid` int(11) NOT NULL DEFAULT 0 COMMENT '商品id',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `time_format` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_pushid
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_pushid`;
CREATE TABLE `cmf_user_pushid`  (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '用户ID',
  `pushid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户对应极光registration_id',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_pushid
-- ----------------------------
INSERT INTO `cmf_user_pushid` VALUES (0, '170976fa8a3e8dc6585');
INSERT INTO `cmf_user_pushid` VALUES (42487, '170976fa8a3a4ef0158');
INSERT INTO `cmf_user_pushid` VALUES (42488, '140fe1da9e170c2e972');
INSERT INTO `cmf_user_pushid` VALUES (42496, '121c83f7609b663c785');
INSERT INTO `cmf_user_pushid` VALUES (42499, '190e35f7e0ff158eb4c');
INSERT INTO `cmf_user_pushid` VALUES (42513, '191e35f7e0fb7839d7a');
INSERT INTO `cmf_user_pushid` VALUES (42514, '');
INSERT INTO `cmf_user_pushid` VALUES (42516, '');
INSERT INTO `cmf_user_pushid` VALUES (42517, '190e35f7e0fd3d69818');
INSERT INTO `cmf_user_pushid` VALUES (42519, '18071adc03b5b1ee795');
INSERT INTO `cmf_user_pushid` VALUES (42520, '100d855909ff893a718');
INSERT INTO `cmf_user_pushid` VALUES (42521, '');
INSERT INTO `cmf_user_pushid` VALUES (42522, '18071adc03b7184c791');
INSERT INTO `cmf_user_pushid` VALUES (42523, '190e35f7e0fd96d9cd0');
INSERT INTO `cmf_user_pushid` VALUES (42524, '170976fa8a3e8dc6585');
INSERT INTO `cmf_user_pushid` VALUES (42527, '18071adc03b3dfaba9d');
INSERT INTO `cmf_user_pushid` VALUES (42528, '1104a897921793a8735');
INSERT INTO `cmf_user_pushid` VALUES (42529, '1104a8979211fe76b45');
INSERT INTO `cmf_user_pushid` VALUES (42530, '1104a8979213376155d');
INSERT INTO `cmf_user_pushid` VALUES (42531, '1104a8979211fdf514c');
INSERT INTO `cmf_user_pushid` VALUES (42534, '');
INSERT INTO `cmf_user_pushid` VALUES (42539, '170976fa8a3978560db');
INSERT INTO `cmf_user_pushid` VALUES (42541, '160a3797c8bff0ddd87');
INSERT INTO `cmf_user_pushid` VALUES (42544, '18071adc03b36ed0aac');
INSERT INTO `cmf_user_pushid` VALUES (42548, '');
INSERT INTO `cmf_user_pushid` VALUES (42551, '190e35f7e0ff158eb4c');
INSERT INTO `cmf_user_pushid` VALUES (42553, '');
INSERT INTO `cmf_user_pushid` VALUES (42554, '');
INSERT INTO `cmf_user_pushid` VALUES (42557, '170976fa8a3a4ef0158');
INSERT INTO `cmf_user_pushid` VALUES (42558, '');
INSERT INTO `cmf_user_pushid` VALUES (42559, '');
INSERT INTO `cmf_user_pushid` VALUES (42560, '1507bfd3f779502e6b0');
INSERT INTO `cmf_user_pushid` VALUES (42562, '1507bfd3f779502e6b0');
INSERT INTO `cmf_user_pushid` VALUES (42563, '191e35f7e0fcfa3ce2d');
INSERT INTO `cmf_user_pushid` VALUES (42564, '170976fa8a3c7fcdbef');
INSERT INTO `cmf_user_pushid` VALUES (42565, '190e35f7e0ff158eb4c');
INSERT INTO `cmf_user_pushid` VALUES (42567, '');
INSERT INTO `cmf_user_pushid` VALUES (42568, '1114a8979210c4aca57');
INSERT INTO `cmf_user_pushid` VALUES (42569, '');
INSERT INTO `cmf_user_pushid` VALUES (42571, '');
INSERT INTO `cmf_user_pushid` VALUES (42574, '100d855909ff893a718');
INSERT INTO `cmf_user_pushid` VALUES (42575, '');
INSERT INTO `cmf_user_pushid` VALUES (42576, '1104a8979211fdf514c');
INSERT INTO `cmf_user_pushid` VALUES (42577, '');
INSERT INTO `cmf_user_pushid` VALUES (42578, '140fe1da9e170c2e972');
INSERT INTO `cmf_user_pushid` VALUES (42580, '191e35f7e0fb7839d7a');
INSERT INTO `cmf_user_pushid` VALUES (42582, '1104a8979213376155d');
INSERT INTO `cmf_user_pushid` VALUES (42584, '121c83f7609ed4060c1');
INSERT INTO `cmf_user_pushid` VALUES (42586, '191e35f7e0fce181576');
INSERT INTO `cmf_user_pushid` VALUES (42588, '1a0018970a1f1a27dda');
INSERT INTO `cmf_user_pushid` VALUES (42589, '1a0018970a1e9affcd4');
INSERT INTO `cmf_user_pushid` VALUES (42612, '');
INSERT INTO `cmf_user_pushid` VALUES (42613, '190e35f7e0ff158eb4c');

-- ----------------------------
-- Table structure for cmf_user_scorerecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_scorerecord`;
CREATE TABLE `cmf_user_scorerecord`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支类型,0支出1收入',
  `action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支行为，4购买VIP,5购买坐骑,18购买靓号,21游戏获胜',
  `uid` int(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(20) NOT NULL DEFAULT 0 COMMENT '对方ID',
  `giftid` int(20) NOT NULL DEFAULT 0 COMMENT '行为对应ID',
  `giftcount` int(20) NOT NULL DEFAULT 0 COMMENT '数量',
  `totalcoin` int(20) NOT NULL DEFAULT 0 COMMENT '总价',
  `showid` int(12) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `game_action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '游戏类型',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `action_uid_addtime`(`action`, `uid`, `addtime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_scorerecord
-- ----------------------------
INSERT INTO `cmf_user_scorerecord` VALUES (1, 1, 21, 42516, 42516, 2, 1, 0, 0, 1604492073, 3);
INSERT INTO `cmf_user_scorerecord` VALUES (2, 1, 21, 42534, 42534, 2, 1, 0, 0, 1604492073, 3);

-- ----------------------------
-- Table structure for cmf_user_sign
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_sign`;
CREATE TABLE `cmf_user_sign`  (
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `bonus_day` int(11) NOT NULL DEFAULT 0 COMMENT '登录天数',
  `bonus_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `count_day` int(11) NOT NULL DEFAULT 0 COMMENT '连续登陆天数',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_sign
-- ----------------------------
INSERT INTO `cmf_user_sign` VALUES (42486, 2, 1604160000, 2);
INSERT INTO `cmf_user_sign` VALUES (42487, 5, 1604505600, 3);
INSERT INTO `cmf_user_sign` VALUES (42488, 2, 1604678400, 2);
INSERT INTO `cmf_user_sign` VALUES (42492, 1, 1604592000, 1);
INSERT INTO `cmf_user_sign` VALUES (42496, 6, 1604764800, 6);
INSERT INTO `cmf_user_sign` VALUES (42499, 1, 1604678400, 1);
INSERT INTO `cmf_user_sign` VALUES (42505, 1, 1604678400, 1);
INSERT INTO `cmf_user_sign` VALUES (42513, 2, 1604419200, 2);
INSERT INTO `cmf_user_sign` VALUES (42514, 5, 1604678400, 5);
INSERT INTO `cmf_user_sign` VALUES (42515, 1, 1604592000, 1);
INSERT INTO `cmf_user_sign` VALUES (42517, 3, 1604592000, 3);
INSERT INTO `cmf_user_sign` VALUES (42519, 4, 1604678400, 4);
INSERT INTO `cmf_user_sign` VALUES (42520, 1, 1604419200, 1);
INSERT INTO `cmf_user_sign` VALUES (42521, 4, 1604678400, 4);
INSERT INTO `cmf_user_sign` VALUES (42522, 1, 1604419200, 1);
INSERT INTO `cmf_user_sign` VALUES (42523, 3, 1604678400, 2);
INSERT INTO `cmf_user_sign` VALUES (42524, 4, 1604678400, 4);
INSERT INTO `cmf_user_sign` VALUES (42525, 2, 1604505600, 2);
INSERT INTO `cmf_user_sign` VALUES (42527, 1, 1604505600, 1);
INSERT INTO `cmf_user_sign` VALUES (42528, 3, 1605628800, 1);
INSERT INTO `cmf_user_sign` VALUES (42529, 4, 1605024000, 1);
INSERT INTO `cmf_user_sign` VALUES (42530, 1, 1604505600, 1);
INSERT INTO `cmf_user_sign` VALUES (42531, 3, 1604678400, 3);
INSERT INTO `cmf_user_sign` VALUES (42533, 1, 1604592000, 1);
INSERT INTO `cmf_user_sign` VALUES (42534, 1, 1604505600, 1);
INSERT INTO `cmf_user_sign` VALUES (42538, 2, 1604592000, 2);
INSERT INTO `cmf_user_sign` VALUES (42539, 2, 1604937600, 1);
INSERT INTO `cmf_user_sign` VALUES (42541, 4, 1604764800, 4);
INSERT INTO `cmf_user_sign` VALUES (42542, 1, 1604851200, 1);
INSERT INTO `cmf_user_sign` VALUES (42545, 1, 1604764800, 1);
INSERT INTO `cmf_user_sign` VALUES (42548, 1, 1604678400, 1);
INSERT INTO `cmf_user_sign` VALUES (42551, 1, 1604678400, 1);
INSERT INTO `cmf_user_sign` VALUES (42557, 1, 1604764800, 1);
INSERT INTO `cmf_user_sign` VALUES (42588, 3, 1605628800, 2);
INSERT INTO `cmf_user_sign` VALUES (42589, 1, 1605369600, 1);

-- ----------------------------
-- Table structure for cmf_user_super
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_super`;
CREATE TABLE `cmf_user_super`  (
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_super
-- ----------------------------
INSERT INTO `cmf_user_super` VALUES (42488, 1604060628);
INSERT INTO `cmf_user_super` VALUES (42492, 1604585421);
INSERT INTO `cmf_user_super` VALUES (42516, 1604936759);
INSERT INTO `cmf_user_super` VALUES (42538, 1604498327);
INSERT INTO `cmf_user_super` VALUES (42542, 1604635384);

-- ----------------------------
-- Table structure for cmf_user_token
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_token`;
CREATE TABLE `cmf_user_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户id',
  `expire_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT ' 过期时间',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'token',
  `device_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备类型;mobile,android,iphone,ipad,web,pc,mac,wxapp',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 149 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户客户端登录 token 表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_token
-- ----------------------------
INSERT INTO `cmf_user_token` VALUES (1, 42486, 1629977517, 1604057517, 'c5a9069d5affef7bb336837b22efb58b', '');
INSERT INTO `cmf_user_token` VALUES (2, 42487, 1630390831, 1604470831, '2f67e93968b8ac30f1edf29caf65d677', '');
INSERT INTO `cmf_user_token` VALUES (3, 1, 1619610636, 1604058636, 'd5a64f09a6d6f78a51116d81ed085c54123292ce5dfb445463ca1c9b9983cef7', 'web');
INSERT INTO `cmf_user_token` VALUES (4, 42488, 1631724457, 1605804457, '298a7b9353909c9d160a7bb89f2d7770', '');
INSERT INTO `cmf_user_token` VALUES (5, 42490, 1630222140, 1604302140, '02136cde662abb9010a265bdf72fbe1d', '');
INSERT INTO `cmf_user_token` VALUES (6, 42489, 1630209821, 1604289821, 'a8fac8910168752fdf62c72ce88c68b5', '');
INSERT INTO `cmf_user_token` VALUES (7, 42491, 1619844353, 1604292353, '6c2906ca686c8cdd279a87e43445c445a07d401fcf532723fea2f115d1d35809', 'web');
INSERT INTO `cmf_user_token` VALUES (8, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (9, 42493, 1630213909, 1604293909, '11c3d2a3d76bcf9b1b86fdba0d3c3790', '');
INSERT INTO `cmf_user_token` VALUES (10, 42494, 1619846304, 1604294304, '03d9c0e2d26b376b816db7bb5388c7895acc0eff59dae4ccc63cf5fbc4cba80c', 'web');
INSERT INTO `cmf_user_token` VALUES (11, 42496, 1631695099, 1605775099, 'f46b012e01f2da1155ae1cbea0276371', '');
INSERT INTO `cmf_user_token` VALUES (12, 42502, 1630312199, 1604392199, '6ac747db5cddfeda198c12ca5786e809', '');
INSERT INTO `cmf_user_token` VALUES (13, 42509, 1630402107, 1604482107, '41f4ff2b71675a04f055e84b643dff98', '');
INSERT INTO `cmf_user_token` VALUES (14, 42511, 1630405993, 1604485993, '53b8f2996664a3776fbc1aab09f3e258', '');
INSERT INTO `cmf_user_token` VALUES (15, 42512, 1619867462, 1604315462, 'f6798de6dbd48d71452c2d128502cf1e985e6a225a53e12d6ea2c6e933f266b3', 'web');
INSERT INTO `cmf_user_token` VALUES (16, 42513, 1631771078, 1605851078, '739678af097dd88933d024c4e3e1613b', '');
INSERT INTO `cmf_user_token` VALUES (17, 42514, 1630654272, 1604734272, 'eae8c5679ce6353047b8cb30722fc979', '');
INSERT INTO `cmf_user_token` VALUES (18, 42515, 1631621108, 1605701108, '40b55e718baaa66ccea96f6d9cf33f57', '');
INSERT INTO `cmf_user_token` VALUES (19, 42516, 1631769652, 1605849652, '1334772a0a2a7951e2c0c4dc13c64342', '');
INSERT INTO `cmf_user_token` VALUES (20, 42517, 1630314642, 1604394642, '1eaf489ce14b4775aaf37b451dffea01', '');
INSERT INTO `cmf_user_token` VALUES (21, 42518, 1631624621, 1605704621, 'cba3e4fcc87bbfc5d160b5c297976b36', '');
INSERT INTO `cmf_user_token` VALUES (22, 42519, 1630312402, 1604392402, '85cf3e6cec42e5d8d563860dcba49d65', '');
INSERT INTO `cmf_user_token` VALUES (23, 42520, 1630647796, 1604727796, 'dbbf6cf22e401ea65ad89205f3a29b2d', '');
INSERT INTO `cmf_user_token` VALUES (24, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (25, 42521, 1631761365, 1605841365, 'e8395abb78dd0dfc50c97fcf7b97c3aa', '');
INSERT INTO `cmf_user_token` VALUES (26, 42522, 1630312603, 1604392603, 'e0be154352b2c96b609567fe0327e71f', '');
INSERT INTO `cmf_user_token` VALUES (27, 42523, 1631765708, 1605845708, 'd7800c21513783e4b4f7d887280d9463', '');
INSERT INTO `cmf_user_token` VALUES (28, 42524, 1631766832, 1605846832, 'dec14e5b16a2ade9c2095ea9ed30de12', '');
INSERT INTO `cmf_user_token` VALUES (29, 42525, 1630327110, 1604407110, 'c1f7b88683fad64b3c12638b17ca0c7a', '');
INSERT INTO `cmf_user_token` VALUES (30, 42526, 1630391470, 1604471470, 'b05f40caa8ca7edf7ad78fa978bc397e', '');
INSERT INTO `cmf_user_token` VALUES (31, 42527, 1630398480, 1604478480, '0d983926c64e37774341245a14730035', '');
INSERT INTO `cmf_user_token` VALUES (32, 42528, 1630502133, 1604582133, 'a0894b07741c8bc0f947ba182279305d', '');
INSERT INTO `cmf_user_token` VALUES (33, 42529, 1630922958, 1605002958, 'c22bce7c2883336f8094c1a4a3d46454', '');
INSERT INTO `cmf_user_token` VALUES (34, 42530, 1630399300, 1604479300, '5a452cd8302175c40a386f79303d3a8b', '');
INSERT INTO `cmf_user_token` VALUES (35, 42531, 1630656364, 1604736364, 'b2b8220808dcbeeab67274addb93c9a0', '');
INSERT INTO `cmf_user_token` VALUES (36, 42532, 1630410750, 1604490750, '5e90be066dab6abe4053dc8aa64eedcb', '');
INSERT INTO `cmf_user_token` VALUES (37, 42533, 1630825709, 1604905709, 'ee47b3f8812067ae8755851c2908bf4a', '');
INSERT INTO `cmf_user_token` VALUES (38, 42534, 1630411024, 1604491024, 'f7da196c3a8ebc615ec111bf0a26d354', '');
INSERT INTO `cmf_user_token` VALUES (39, 42535, 1620043280, 1604491280, 'c83de77dc67ba0a15a36553aa13f35c0ae79b5c51c2e4d9362e573b8a691b6c8', 'web');
INSERT INTO `cmf_user_token` VALUES (40, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (41, 42536, 1630412999, 1604492999, '1a088bf820dc387f7f4eea173278e51f', '');
INSERT INTO `cmf_user_token` VALUES (42, 42537, 1630413111, 1604493111, '932ba0f3cbddbd6e655f7c4019fb2377', '');
INSERT INTO `cmf_user_token` VALUES (43, 42538, 1631021284, 1605101284, '51df0d2bf78b7782375e64f199f543d4', '');
INSERT INTO `cmf_user_token` VALUES (44, 42539, 1630912315, 1604992315, '981002ad866a17838382445ae7b487f1', '');
INSERT INTO `cmf_user_token` VALUES (45, 42540, 1630422484, 1604502484, 'c11f7998e0b82cd25353648fb605a71b', '');
INSERT INTO `cmf_user_token` VALUES (46, 42540, 1630422484, 1604502484, 'c11f7998e0b82cd25353648fb605a71b', '');
INSERT INTO `cmf_user_token` VALUES (47, 42540, 1630422484, 1604502484, 'c11f7998e0b82cd25353648fb605a71b', '');
INSERT INTO `cmf_user_token` VALUES (48, 42541, 1631350998, 1605430998, '84e3c009b2eec1b8e7fd49ab9574519c', '');
INSERT INTO `cmf_user_token` VALUES (49, 42488, 1631724457, 1605804457, '298a7b9353909c9d160a7bb89f2d7770', '');
INSERT INTO `cmf_user_token` VALUES (50, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (51, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (52, 42542, 1631720228, 1605800228, 'f70dbc72d0c6ef3792947acc063f1e3e', '');
INSERT INTO `cmf_user_token` VALUES (53, 42543, 1631690179, 1605770179, 'a5c981206cabad6d6cf47913d5128dfa', '');
INSERT INTO `cmf_user_token` VALUES (54, 42541, 1631350998, 1605430998, '84e3c009b2eec1b8e7fd49ab9574519c', '');
INSERT INTO `cmf_user_token` VALUES (55, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (56, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (57, 42544, 1631258975, 1605338975, '23a484e50980176d043142ee09712afd', '');
INSERT INTO `cmf_user_token` VALUES (58, 42545, 1632716424, 1606796424, '5f304681e9e3586bf0f8daf979ecce93', '');
INSERT INTO `cmf_user_token` VALUES (59, 42528, 1630502133, 1604582133, 'a0894b07741c8bc0f947ba182279305d', '');
INSERT INTO `cmf_user_token` VALUES (60, 42546, 1630513444, 1604593444, '915ef80fdc61fda31a83feb17365e076', '');
INSERT INTO `cmf_user_token` VALUES (61, 42547, 1630513521, 1604593521, 'b0aa2a7ce714ed8b71b2c9fd2bab585c', '');
INSERT INTO `cmf_user_token` VALUES (62, 42548, 1631764358, 1605844358, 'fc43eb2e0428cd4b58725eef813df25b', '');
INSERT INTO `cmf_user_token` VALUES (63, 42545, 1632716424, 1606796424, '5f304681e9e3586bf0f8daf979ecce93', '');
INSERT INTO `cmf_user_token` VALUES (64, 42542, 1631720228, 1605800228, 'f70dbc72d0c6ef3792947acc063f1e3e', '');
INSERT INTO `cmf_user_token` VALUES (65, 42499, 1631768839, 1605848839, '46aa2e67f6c98196757dbd2eca6b5186', '');
INSERT INTO `cmf_user_token` VALUES (66, 42549, 1630582238, 1604662238, '5204adc6569d719e7869b01760a1190c', '');
INSERT INTO `cmf_user_token` VALUES (67, 42551, 1630651240, 1604731240, 'a6da4a5e39a0ef0726a76642bff6cabf', '');
INSERT INTO `cmf_user_token` VALUES (68, 42505, 1631694395, 1605774395, 'bc501a314a69f70d4a46c9cb066d9171', '');
INSERT INTO `cmf_user_token` VALUES (69, 42504, 1631097675, 1605177675, 'de874fdf51a436574ad01c86001995ab', '');
INSERT INTO `cmf_user_token` VALUES (70, 42552, 1631764139, 1605844139, '23a4eb8acda9ec11e5811785e3727cd3', '');
INSERT INTO `cmf_user_token` VALUES (71, 42542, 1631720228, 1605800228, 'f70dbc72d0c6ef3792947acc063f1e3e', '');
INSERT INTO `cmf_user_token` VALUES (72, 42503, 1631763637, 1605843637, 'ab2cd7290f10ae13569ba10d634b3e59', '');
INSERT INTO `cmf_user_token` VALUES (73, 42531, 1630656364, 1604736364, 'b2b8220808dcbeeab67274addb93c9a0', '');
INSERT INTO `cmf_user_token` VALUES (74, 42553, 1630658222, 1604738222, 'd5df919a2ccc13f1806a58ff80e70fe7', '');
INSERT INTO `cmf_user_token` VALUES (75, 42501, 1631769136, 1605849136, 'b12eaa531872c8b41c8f60acc5b3c9e2', '');
INSERT INTO `cmf_user_token` VALUES (76, 42501, 1631769136, 1605849136, 'b12eaa531872c8b41c8f60acc5b3c9e2', '');
INSERT INTO `cmf_user_token` VALUES (77, 42554, 1630668141, 1604748141, '01de0357a0b67f7bd0d02decd7377a0c', '');
INSERT INTO `cmf_user_token` VALUES (78, 42555, 1630669659, 1604749659, '5afd0a8ba0ac6249f8651faaaa8d462b', '');
INSERT INTO `cmf_user_token` VALUES (79, 42556, 1630669984, 1604749984, '5e9fe2ee4067998f0e86d84b383fce10', '');
INSERT INTO `cmf_user_token` VALUES (80, 42557, 1630672116, 1604752116, '6d5e8e9624ae9bdac6190b5eca377505', '');
INSERT INTO `cmf_user_token` VALUES (81, 42558, 1630733361, 1604813361, 'b6a6f7631c50e4cea93ab0aea1d3f19e', '');
INSERT INTO `cmf_user_token` VALUES (82, 42559, 1630733395, 1604813395, '84afc3aa705e44fb9cb9072df3b67d9d', '');
INSERT INTO `cmf_user_token` VALUES (83, 42560, 1630751374, 1604831374, '41db167365599bbb2fd8811a44b4f3f1', '');
INSERT INTO `cmf_user_token` VALUES (84, 42561, 1630752198, 1604832198, '362ac6034af16449828c538238c7ead0', '');
INSERT INTO `cmf_user_token` VALUES (85, 42562, 1630755771, 1604835771, '3f8a3ce47a8f95f188b9643c5648cba2', '');
INSERT INTO `cmf_user_token` VALUES (86, 42563, 1631448687, 1605528687, '07b6fe5b11dcac13aeb15df9385a2649', '');
INSERT INTO `cmf_user_token` VALUES (87, 42564, 1630815803, 1604895803, 'b0ed8a34466a60af12e0d53d7f777e76', '');
INSERT INTO `cmf_user_token` VALUES (88, 42506, 1630817769, 1604897769, '8666bc6e928f5e2620c1b4a52185611f', '');
INSERT INTO `cmf_user_token` VALUES (89, 42565, 1630818995, 1604898995, 'c886cc5ca5eda16b0f5fd9713c3eb59d', '');
INSERT INTO `cmf_user_token` VALUES (90, 42566, 1631769246, 1605849246, 'fe53fee71f81ad639c4705909da5acd8', '');
INSERT INTO `cmf_user_token` VALUES (91, 42567, 1630845903, 1604925903, '077c3f195a0d92b1a0345247a7caefa5', '');
INSERT INTO `cmf_user_token` VALUES (92, 42568, 1630836106, 1604916106, 'a62cdba81c261b0a520702d13922f11c', '');
INSERT INTO `cmf_user_token` VALUES (93, 42569, 1630842504, 1604922504, '650f5ed06bcb03558dfebd521ef1b30b', '');
INSERT INTO `cmf_user_token` VALUES (94, 42521, 1631761365, 1605841365, 'e8395abb78dd0dfc50c97fcf7b97c3aa', '');
INSERT INTO `cmf_user_token` VALUES (95, 42570, 1630843496, 1604923496, '69ce37cd14ee83c5eedaccbad0eb8b71', '');
INSERT INTO `cmf_user_token` VALUES (96, 42571, 1630843989, 1604923989, '69b88b19be43c746bdb42114576a62dd', '');
INSERT INTO `cmf_user_token` VALUES (97, 42572, 1630845128, 1604925128, 'ddd2f1ba28bde1512fc2ccc5dbafbf9c', '');
INSERT INTO `cmf_user_token` VALUES (98, 42573, 1620477212, 1604925212, '3d9247f28d76d1d069f98546c127957dbf44295bed063a7b7b01502a353abc64', 'web');
INSERT INTO `cmf_user_token` VALUES (99, 42575, 1630935479, 1605015479, 'e2f77c73050e17de49960cfcf4095a0b', '');
INSERT INTO `cmf_user_token` VALUES (100, 42504, 1631097675, 1605177675, 'de874fdf51a436574ad01c86001995ab', '');
INSERT INTO `cmf_user_token` VALUES (101, 42576, 1630902656, 1604982656, '0684d2da1d4accc890755935394eea44', '');
INSERT INTO `cmf_user_token` VALUES (102, 42577, 1630851496, 1604931496, 'bda9ceb9795d2444200b42d06ccbad48', '');
INSERT INTO `cmf_user_token` VALUES (103, 42488, 1631724457, 1605804457, '298a7b9353909c9d160a7bb89f2d7770', '');
INSERT INTO `cmf_user_token` VALUES (104, 42578, 1630860679, 1604940679, '2e48fcbf5a367db0864603de0ab7cae3', '');
INSERT INTO `cmf_user_token` VALUES (105, 42579, 1630861019, 1604941019, '37f7dbde007a56fa9bebc57aaf6b3b2d', '');
INSERT INTO `cmf_user_token` VALUES (106, 42580, 1630897846, 1604977846, 'fb3bee2700afa691f30d40daf4648327', '');
INSERT INTO `cmf_user_token` VALUES (107, 42574, 1630906246, 1604986246, '6dffd372c07d3c6081b60c325be0f2e6', '');
INSERT INTO `cmf_user_token` VALUES (108, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (109, 42581, 1630914923, 1604994923, '9337b7558562ceb325ffd950e90722dd', '');
INSERT INTO `cmf_user_token` VALUES (110, 42582, 1630917648, 1604997648, 'dc646904cb3c46ed7d4795c0306c797a', '');
INSERT INTO `cmf_user_token` VALUES (111, 42583, 1630942612, 1605022612, 'ddbf019aeeeab200a5c1145b12ec3ad3', '');
INSERT INTO `cmf_user_token` VALUES (112, 42584, 1630925334, 1605005334, 'c6db47391fa1e8265be8fb8635836331', '');
INSERT INTO `cmf_user_token` VALUES (113, 42585, 1631624365, 1605704365, 'e255b4fb45873c53e2b6d766e7e09d41', '');
INSERT INTO `cmf_user_token` VALUES (114, 42515, 1631621108, 1605701108, '40b55e718baaa66ccea96f6d9cf33f57', '');
INSERT INTO `cmf_user_token` VALUES (115, 42586, 1631099595, 1605179595, 'e3434e02ea2f521ebe75619f025286fe', '');
INSERT INTO `cmf_user_token` VALUES (116, 42521, 1631761365, 1605841365, 'e8395abb78dd0dfc50c97fcf7b97c3aa', '');
INSERT INTO `cmf_user_token` VALUES (117, 42588, 1631765506, 1605845506, '69cd9c6164a9716b2ac22a41d5d82acf', '');
INSERT INTO `cmf_user_token` VALUES (118, 42589, 1631284283, 1605364283, 'c5132c2ba905a8c53c16610e0d67dffe', '');
INSERT INTO `cmf_user_token` VALUES (119, 42492, 1631604242, 1605684242, '6b00043f7af6e7a6eb6f1bb156cf16c7', '');
INSERT INTO `cmf_user_token` VALUES (120, 42590, 1631350127, 1605430127, 'f3bca3ab925b69d388c909ce367ce092', '');
INSERT INTO `cmf_user_token` VALUES (121, 42591, 1631352826, 1605432826, '4c64062b83a7b75bebbe24964254d0a7', '');
INSERT INTO `cmf_user_token` VALUES (122, 42592, 1631352910, 1605432910, '21f6d2c5e5bcb3557b24bd8bbd91d997', '');
INSERT INTO `cmf_user_token` VALUES (123, 42593, 1631352961, 1605432961, 'a5920825996ff0e8011b7ed2bbe2daa8', '');
INSERT INTO `cmf_user_token` VALUES (124, 42594, 1631353031, 1605433031, 'a5419184018bfc74c7f06aa2871ac194', '');
INSERT INTO `cmf_user_token` VALUES (125, 42595, 1631353092, 1605433092, '4e16c8937ddc23766ef3da1acf5d0726', '');
INSERT INTO `cmf_user_token` VALUES (126, 42596, 1631353207, 1605433207, 'a03041ef58becd439e7252e8e7cb0a9f', '');
INSERT INTO `cmf_user_token` VALUES (127, 42597, 1631353260, 1605433260, '9d8fa9ec89eaf1b128dc20b6433619ff', '');
INSERT INTO `cmf_user_token` VALUES (128, 42598, 1631353319, 1605433319, 'a1bfb88491e59a66785ff53d43e9144f', '');
INSERT INTO `cmf_user_token` VALUES (129, 42599, 1631353811, 1605433811, '0477ddb5756fe3dbb5e997b55c547b3d', '');
INSERT INTO `cmf_user_token` VALUES (130, 42600, 1631353957, 1605433957, '9fcf59d69c8b2ada580d57dbafc8c275', '');
INSERT INTO `cmf_user_token` VALUES (131, 42601, 1631354031, 1605434031, '867a723c29d3c41a309ba90157edc3c3', '');
INSERT INTO `cmf_user_token` VALUES (132, 42602, 1631354146, 1605434146, '701283991897e8b12d81dd2aae14f288', '');
INSERT INTO `cmf_user_token` VALUES (133, 42603, 1631354209, 1605434209, '8615f4d55c92c913be41cf742fc3e7aa', '');
INSERT INTO `cmf_user_token` VALUES (134, 42604, 1631354252, 1605434252, 'ecd553ac79fbc685cdece10d3ecb5ed4', '');
INSERT INTO `cmf_user_token` VALUES (135, 42605, 1631354488, 1605434488, '4ca2e655e38ad4c2b9a0bc120607b129', '');
INSERT INTO `cmf_user_token` VALUES (136, 42606, 1631354547, 1605434547, '91ebf0554babfcd032a5c7d614f1a800', '');
INSERT INTO `cmf_user_token` VALUES (137, 42607, 1631354608, 1605434608, '63e3eb4f667805b1a0456f356d013c00', '');
INSERT INTO `cmf_user_token` VALUES (138, 42608, 1631354689, 1605434689, '6c3c4f2e0e34f440655a2123c522ef2a', '');
INSERT INTO `cmf_user_token` VALUES (139, 42609, 1631354744, 1605434744, '7aeef73adcb241c8b174ed111acd65f6', '');
INSERT INTO `cmf_user_token` VALUES (140, 42610, 1631416233, 1605496233, 'b45abd822ed7afde547992cf85cd7334', '');
INSERT INTO `cmf_user_token` VALUES (141, 42611, 1631445296, 1605525296, 'd18b511b9ee8d42e91dd259cc410d0e7', '');
INSERT INTO `cmf_user_token` VALUES (142, 42612, 1631516401, 1605596401, '453a0c8ae42608ddb087831fd1578204', '');
INSERT INTO `cmf_user_token` VALUES (143, 42524, 1631766832, 1605846832, 'dec14e5b16a2ade9c2095ea9ed30de12', '');
INSERT INTO `cmf_user_token` VALUES (144, 42613, 1631699662, 1605779662, '771f5f9ed5c3f055feab6618ef5f564c', '');
INSERT INTO `cmf_user_token` VALUES (145, 42499, 1631768839, 1605848839, '46aa2e67f6c98196757dbd2eca6b5186', '');
INSERT INTO `cmf_user_token` VALUES (146, 42521, 1631761365, 1605841365, 'e8395abb78dd0dfc50c97fcf7b97c3aa', '');
INSERT INTO `cmf_user_token` VALUES (147, 42499, 1631768839, 1605848839, '46aa2e67f6c98196757dbd2eca6b5186', '');
INSERT INTO `cmf_user_token` VALUES (148, 42548, 1631764358, 1605844358, 'fc43eb2e0428cd4b58725eef813df25b', '');

-- ----------------------------
-- Table structure for cmf_user_voterecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_voterecord`;
CREATE TABLE `cmf_user_voterecord`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支类型,0支出，1收入',
  `action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支行为,1收礼物2弹幕3分销收益4家族长收益6房间收费7计时收费10守护',
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `fromid` bigint(20) NOT NULL DEFAULT 0 COMMENT '来源用户ID',
  `actionid` bigint(20) NOT NULL DEFAULT 0 COMMENT '行为对应ID',
  `nums` bigint(20) NOT NULL DEFAULT 0 COMMENT '数量',
  `total` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '总价',
  `showid` bigint(20) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `votes` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '收益映票',
  `addtime` bigint(20) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `action_uid_addtime`(`action`, `uid`, `addtime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 196 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_voterecord
-- ----------------------------
INSERT INTO `cmf_user_voterecord` VALUES (1, 1, 2, 42515, 42515, 0, 1, 10.00, 1604382660, 10.00, 1604382814);
INSERT INTO `cmf_user_voterecord` VALUES (2, 1, 2, 42515, 42515, 0, 1, 10.00, 1604382660, 10.00, 1604382835);
INSERT INTO `cmf_user_voterecord` VALUES (3, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383948);
INSERT INTO `cmf_user_voterecord` VALUES (4, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383948);
INSERT INTO `cmf_user_voterecord` VALUES (5, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383949);
INSERT INTO `cmf_user_voterecord` VALUES (6, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383949);
INSERT INTO `cmf_user_voterecord` VALUES (7, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383950);
INSERT INTO `cmf_user_voterecord` VALUES (8, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383950);
INSERT INTO `cmf_user_voterecord` VALUES (9, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383951);
INSERT INTO `cmf_user_voterecord` VALUES (10, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383951);
INSERT INTO `cmf_user_voterecord` VALUES (11, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (12, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (13, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (14, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (15, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (16, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (17, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (18, 1, 1, 42514, 42513, 3, 1, 1000.00, 1604383859, 700.00, 1604383952);
INSERT INTO `cmf_user_voterecord` VALUES (19, 1, 1, 42520, 42513, 8, 1, 100.00, 1604386353, 100.00, 1604387013);
INSERT INTO `cmf_user_voterecord` VALUES (20, 1, 1, 42520, 42513, 8, 1, 100.00, 1604386353, 100.00, 1604387015);
INSERT INTO `cmf_user_voterecord` VALUES (21, 1, 1, 42520, 42513, 8, 1, 100.00, 1604386353, 100.00, 1604387022);
INSERT INTO `cmf_user_voterecord` VALUES (22, 1, 1, 42520, 42513, 8, 1, 100.00, 1604386353, 100.00, 1604387022);
INSERT INTO `cmf_user_voterecord` VALUES (23, 1, 1, 42520, 42513, 8, 1, 100.00, 1604386353, 100.00, 1604387023);
INSERT INTO `cmf_user_voterecord` VALUES (24, 1, 2, 42492, 42515, 0, 1, 10.00, 1604390390, 10.00, 1604392128);
INSERT INTO `cmf_user_voterecord` VALUES (25, 1, 1, 42492, 42515, 7, 1, 300.00, 1604390782, 300.00, 1604393059);
INSERT INTO `cmf_user_voterecord` VALUES (26, 1, 1, 42492, 42515, 7, 1, 300.00, 1604390782, 300.00, 1604393066);
INSERT INTO `cmf_user_voterecord` VALUES (27, 1, 1, 42492, 42515, 5, 1, 500.00, 1604390782, 500.00, 1604393074);
INSERT INTO `cmf_user_voterecord` VALUES (28, 1, 1, 42492, 42515, 4, 1, 10000.00, 1604390782, 10000.00, 1604393430);
INSERT INTO `cmf_user_voterecord` VALUES (29, 1, 4, 42513, 42516, 0, 0, 150.00, 0, 150.00, 1604404057);
INSERT INTO `cmf_user_voterecord` VALUES (30, 1, 1, 42516, 42513, 3, 1, 1000.00, 1604403971, 850.00, 1604404057);
INSERT INTO `cmf_user_voterecord` VALUES (31, 1, 4, 42513, 42516, 0, 0, 150.00, 0, 150.00, 1604404059);
INSERT INTO `cmf_user_voterecord` VALUES (32, 1, 1, 42516, 42513, 3, 1, 1000.00, 1604403971, 850.00, 1604404059);
INSERT INTO `cmf_user_voterecord` VALUES (33, 1, 4, 42513, 42516, 0, 0, 150.00, 0, 150.00, 1604404060);
INSERT INTO `cmf_user_voterecord` VALUES (34, 1, 1, 42516, 42513, 3, 1, 1000.00, 1604403971, 850.00, 1604404060);
INSERT INTO `cmf_user_voterecord` VALUES (35, 1, 4, 42513, 42520, 0, 0, 150.00, 0, 150.00, 1604468529);
INSERT INTO `cmf_user_voterecord` VALUES (36, 1, 1, 42520, 42516, 5, 1, 500.00, 1604468385, 350.00, 1604468529);
INSERT INTO `cmf_user_voterecord` VALUES (37, 1, 10, 42520, 42516, 1, 1, 300.00, 1604468385, 300.00, 1604468998);
INSERT INTO `cmf_user_voterecord` VALUES (38, 1, 10, 42520, 42516, 2, 1, 1000.00, 1604468385, 1000.00, 1604469030);
INSERT INTO `cmf_user_voterecord` VALUES (39, 1, 2, 42520, 42516, 0, 1, 10.00, 1604468385, 10.00, 1604469371);
INSERT INTO `cmf_user_voterecord` VALUES (40, 1, 2, 42520, 42516, 0, 1, 10.00, 1604468385, 10.00, 1604469399);
INSERT INTO `cmf_user_voterecord` VALUES (41, 1, 1, 42517, 42524, 12, 1, 5.00, 1604480600, 5.00, 1604480644);
INSERT INTO `cmf_user_voterecord` VALUES (42, 1, 1, 42511, 42529, 21, 1, 1.00, 1604486095, 1.00, 1604486260);
INSERT INTO `cmf_user_voterecord` VALUES (43, 1, 1, 42515, 42492, 22, 1, 2.00, 1604489098, 2.00, 1604490310);
INSERT INTO `cmf_user_voterecord` VALUES (44, 1, 1, 42515, 42492, 19, 1, 10.00, 1604489098, 10.00, 1604490316);
INSERT INTO `cmf_user_voterecord` VALUES (45, 1, 1, 42515, 42492, 19, 1, 10.00, 1604489098, 10.00, 1604490323);
INSERT INTO `cmf_user_voterecord` VALUES (46, 1, 4, 42513, 42514, 0, 0, 150.00, 0, 150.00, 1604491758);
INSERT INTO `cmf_user_voterecord` VALUES (47, 1, 1, 42514, 42516, 5, 1, 500.00, 1604491293, 350.00, 1604491758);
INSERT INTO `cmf_user_voterecord` VALUES (48, 1, 4, 42513, 42514, 0, 0, 150.00, 0, 150.00, 1604491760);
INSERT INTO `cmf_user_voterecord` VALUES (49, 1, 1, 42514, 42516, 5, 1, 500.00, 1604491293, 350.00, 1604491760);
INSERT INTO `cmf_user_voterecord` VALUES (50, 1, 4, 42513, 42514, 0, 0, 900.00, 0, 900.00, 1604491787);
INSERT INTO `cmf_user_voterecord` VALUES (51, 1, 1, 42514, 42516, 13, 10, 3000.00, 1604491293, 2100.00, 1604491787);
INSERT INTO `cmf_user_voterecord` VALUES (52, 1, 4, 42513, 42514, 0, 0, 900.00, 0, 900.00, 1604491793);
INSERT INTO `cmf_user_voterecord` VALUES (53, 1, 1, 42514, 42516, 13, 10, 3000.00, 1604491293, 2100.00, 1604491793);
INSERT INTO `cmf_user_voterecord` VALUES (54, 1, 4, 42513, 42514, 0, 0, 50.40, 0, 50.40, 1604491810);
INSERT INTO `cmf_user_voterecord` VALUES (55, 1, 1, 42514, 42516, 28, 10, 280.00, 1604491293, 117.60, 1604491810);
INSERT INTO `cmf_user_voterecord` VALUES (56, 1, 4, 42513, 42514, 0, 0, 50.40, 0, 50.40, 1604491817);
INSERT INTO `cmf_user_voterecord` VALUES (57, 1, 1, 42514, 42516, 28, 10, 280.00, 1604491293, 117.60, 1604491817);
INSERT INTO `cmf_user_voterecord` VALUES (58, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604491878);
INSERT INTO `cmf_user_voterecord` VALUES (59, 1, 1, 42514, 42533, 3, 1, 1000.00, 1604491293, 700.00, 1604491878);
INSERT INTO `cmf_user_voterecord` VALUES (60, 1, 10, 42514, 42516, 1, 1, 300.00, 1604491293, 300.00, 1604491955);
INSERT INTO `cmf_user_voterecord` VALUES (61, 1, 2, 42514, 42533, 0, 1, 10.00, 1604491293, 10.00, 1604492185);
INSERT INTO `cmf_user_voterecord` VALUES (62, 1, 2, 42514, 42514, 0, 1, 10.00, 1604491293, 10.00, 1604492211);
INSERT INTO `cmf_user_voterecord` VALUES (63, 1, 4, 42513, 42514, 0, 0, 300.00, 0, 300.00, 1604492310);
INSERT INTO `cmf_user_voterecord` VALUES (64, 1, 1, 42514, 42533, 3, 1, 1000.00, 0, 700.00, 1604492310);
INSERT INTO `cmf_user_voterecord` VALUES (65, 1, 2, 1, 42492, 0, 1, 10.00, 1604129295, 10.00, 1604492775);
INSERT INTO `cmf_user_voterecord` VALUES (66, 1, 2, 42515, 42492, 0, 1, 10.00, 1604489018, 10.00, 1604492953);
INSERT INTO `cmf_user_voterecord` VALUES (67, 1, 1, 42515, 42492, 12, 1, 5.00, 1604489098, 5.00, 1604492963);
INSERT INTO `cmf_user_voterecord` VALUES (68, 1, 1, 42515, 42492, 13, 1, 300.00, 1604489098, 300.00, 1604492966);
INSERT INTO `cmf_user_voterecord` VALUES (69, 1, 1, 42515, 42492, 9, 1, 500.00, 1604489098, 500.00, 1604492969);
INSERT INTO `cmf_user_voterecord` VALUES (70, 1, 2, 42492, 42492, 0, 1, 10.00, 1604551622, 10.00, 1604551710);
INSERT INTO `cmf_user_voterecord` VALUES (71, 1, 2, 42492, 42492, 0, 1, 10.00, 1604551622, 10.00, 1604551731);
INSERT INTO `cmf_user_voterecord` VALUES (72, 1, 2, 42492, 42492, 0, 1, 10.00, 1604551622, 10.00, 1604551840);
INSERT INTO `cmf_user_voterecord` VALUES (73, 1, 2, 42492, 42492, 0, 1, 10.00, 1604561271, 10.00, 1604561365);
INSERT INTO `cmf_user_voterecord` VALUES (74, 1, 2, 42492, 42515, 0, 1, 10.00, 1604561271, 10.00, 1604561624);
INSERT INTO `cmf_user_voterecord` VALUES (75, 1, 2, 42492, 42515, 0, 1, 10.00, 1604561271, 10.00, 1604561636);
INSERT INTO `cmf_user_voterecord` VALUES (76, 1, 2, 42492, 42492, 0, 1, 10.00, 1604562092, 10.00, 1604562138);
INSERT INTO `cmf_user_voterecord` VALUES (77, 1, 2, 42492, 42492, 0, 1, 10.00, 1604562092, 10.00, 1604562152);
INSERT INTO `cmf_user_voterecord` VALUES (78, 1, 2, 42539, 42543, 0, 1, 10.00, 1604558606, 10.00, 1604562626);
INSERT INTO `cmf_user_voterecord` VALUES (79, 1, 2, 42492, 42543, 0, 1, 10.00, 1604562092, 10.00, 1604562660);
INSERT INTO `cmf_user_voterecord` VALUES (80, 1, 2, 42492, 42543, 0, 1, 10.00, 1604562092, 10.00, 1604562678);
INSERT INTO `cmf_user_voterecord` VALUES (81, 1, 2, 42492, 42543, 0, 1, 10.00, 1604562092, 10.00, 1604562717);
INSERT INTO `cmf_user_voterecord` VALUES (82, 1, 2, 42515, 42492, 0, 1, 10.00, 1604562328, 10.00, 1604563911);
INSERT INTO `cmf_user_voterecord` VALUES (83, 1, 1, 1, 42492, 3, 1, 1000.00, 1604129295, 1000.00, 1604564905);
INSERT INTO `cmf_user_voterecord` VALUES (84, 1, 2, 42515, 42492, 0, 1, 10.00, 1604562328, 10.00, 1604565942);
INSERT INTO `cmf_user_voterecord` VALUES (85, 1, 2, 42539, 42543, 0, 1, 10.00, 1604558606, 10.00, 1604566108);
INSERT INTO `cmf_user_voterecord` VALUES (86, 1, 2, 42492, 42492, 0, 1, 10.00, 1604566111, 10.00, 1604566142);
INSERT INTO `cmf_user_voterecord` VALUES (87, 1, 2, 42492, 42492, 0, 1, 10.00, 1604566111, 10.00, 1604566350);
INSERT INTO `cmf_user_voterecord` VALUES (88, 1, 2, 42492, 42492, 0, 1, 10.00, 1604569827, 10.00, 1604569878);
INSERT INTO `cmf_user_voterecord` VALUES (89, 1, 2, 42492, 42492, 0, 1, 10.00, 1604571917, 10.00, 1604572139);
INSERT INTO `cmf_user_voterecord` VALUES (90, 1, 2, 42492, 42492, 0, 1, 10.00, 1604571917, 10.00, 1604572155);
INSERT INTO `cmf_user_voterecord` VALUES (91, 1, 2, 42492, 42492, 0, 1, 10.00, 1604571917, 10.00, 1604572227);
INSERT INTO `cmf_user_voterecord` VALUES (92, 1, 2, 42492, 42492, 0, 1, 10.00, 1604571917, 10.00, 1604572245);
INSERT INTO `cmf_user_voterecord` VALUES (93, 1, 2, 42492, 42492, 0, 1, 10.00, 1604571917, 10.00, 1604572263);
INSERT INTO `cmf_user_voterecord` VALUES (94, 1, 2, 42492, 42492, 0, 1, 10.00, 1604571917, 10.00, 1604572275);
INSERT INTO `cmf_user_voterecord` VALUES (95, 1, 2, 42542, 42515, 0, 1, 10.00, 1604573031, 10.00, 1604573187);
INSERT INTO `cmf_user_voterecord` VALUES (96, 1, 2, 42492, 42492, 0, 1, 10.00, 1604574022, 10.00, 1604574089);
INSERT INTO `cmf_user_voterecord` VALUES (97, 1, 2, 1, 42515, 0, 1, 10.00, 1604129295, 10.00, 1604574264);
INSERT INTO `cmf_user_voterecord` VALUES (98, 1, 2, 42538, 42513, 0, 1, 10.00, 1604575872, 10.00, 1604576084);
INSERT INTO `cmf_user_voterecord` VALUES (99, 1, 2, 42538, 42513, 0, 1, 10.00, 1604575872, 10.00, 1604576085);
INSERT INTO `cmf_user_voterecord` VALUES (100, 1, 2, 42538, 42538, 0, 1, 10.00, 1604575872, 10.00, 1604576327);
INSERT INTO `cmf_user_voterecord` VALUES (101, 1, 2, 42538, 42521, 0, 1, 10.00, 1604576879, 10.00, 1604577402);
INSERT INTO `cmf_user_voterecord` VALUES (102, 1, 2, 42492, 42492, 0, 1, 10.00, 1604578157, 10.00, 1604578295);
INSERT INTO `cmf_user_voterecord` VALUES (103, 1, 2, 42492, 42521, 0, 1, 10.00, 1604579038, 10.00, 1604579383);
INSERT INTO `cmf_user_voterecord` VALUES (104, 1, 2, 42492, 42545, 0, 1, 10.00, 1604580370, 10.00, 1604580882);
INSERT INTO `cmf_user_voterecord` VALUES (105, 1, 2, 42492, 42545, 0, 1, 10.00, 1604580370, 10.00, 1604580903);
INSERT INTO `cmf_user_voterecord` VALUES (106, 1, 2, 42492, 42545, 0, 1, 10.00, 1604580370, 10.00, 1604580913);
INSERT INTO `cmf_user_voterecord` VALUES (107, 1, 2, 42492, 42545, 0, 1, 10.00, 1604581224, 10.00, 1604583156);
INSERT INTO `cmf_user_voterecord` VALUES (108, 1, 1, 42514, 42542, 3, 1, 1000.00, 1604635541, 1000.00, 1604635803);
INSERT INTO `cmf_user_voterecord` VALUES (109, 1, 1, 1, 42542, 3, 1, 1000.00, 1604129295, 1000.00, 1604635865);
INSERT INTO `cmf_user_voterecord` VALUES (110, 1, 2, 42514, 42545, 0, 1, 10.00, 1604635541, 10.00, 1604639517);
INSERT INTO `cmf_user_voterecord` VALUES (111, 1, 2, 42514, 42545, 0, 1, 10.00, 1604635541, 10.00, 1604639641);
INSERT INTO `cmf_user_voterecord` VALUES (112, 1, 2, 42514, 42545, 0, 1, 10.00, 1604635541, 10.00, 1604639672);
INSERT INTO `cmf_user_voterecord` VALUES (113, 1, 1, 42523, 42542, 13, 1, 300.00, 1604638930, 300.00, 1604639748);
INSERT INTO `cmf_user_voterecord` VALUES (114, 1, 2, 42514, 42545, 0, 1, 10.00, 1604635541, 10.00, 1604639994);
INSERT INTO `cmf_user_voterecord` VALUES (115, 1, 2, 42514, 42545, 0, 1, 10.00, 1604635541, 10.00, 1604639998);
INSERT INTO `cmf_user_voterecord` VALUES (116, 1, 1, 42523, 42542, 7, 1, 300.00, 1604643841, 300.00, 1604644436);
INSERT INTO `cmf_user_voterecord` VALUES (117, 1, 1, 42523, 42492, 8, 1, 100.00, 1604643841, 100.00, 1604644714);
INSERT INTO `cmf_user_voterecord` VALUES (118, 1, 2, 42545, 42545, 0, 1, 10.00, 1604658453, 10.00, 1604658576);
INSERT INTO `cmf_user_voterecord` VALUES (119, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660729);
INSERT INTO `cmf_user_voterecord` VALUES (120, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660737);
INSERT INTO `cmf_user_voterecord` VALUES (121, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660743);
INSERT INTO `cmf_user_voterecord` VALUES (122, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660754);
INSERT INTO `cmf_user_voterecord` VALUES (123, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660763);
INSERT INTO `cmf_user_voterecord` VALUES (124, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660768);
INSERT INTO `cmf_user_voterecord` VALUES (125, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660799);
INSERT INTO `cmf_user_voterecord` VALUES (126, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604660958);
INSERT INTO `cmf_user_voterecord` VALUES (127, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604661020);
INSERT INTO `cmf_user_voterecord` VALUES (128, 1, 2, 42545, 42492, 0, 1, 10.00, 1604660645, 10.00, 1604661073);
INSERT INTO `cmf_user_voterecord` VALUES (129, 1, 2, 42545, 42545, 0, 1, 10.00, 1604661675, 10.00, 1604661757);
INSERT INTO `cmf_user_voterecord` VALUES (130, 1, 2, 42545, 42545, 0, 1, 10.00, 1604661675, 10.00, 1604661850);
INSERT INTO `cmf_user_voterecord` VALUES (131, 1, 2, 42533, 42545, 0, 1, 10.00, 1604661975, 10.00, 1604662239);
INSERT INTO `cmf_user_voterecord` VALUES (132, 1, 2, 42533, 42545, 0, 1, 10.00, 1604661975, 10.00, 1604662256);
INSERT INTO `cmf_user_voterecord` VALUES (133, 1, 2, 42533, 42549, 0, 1, 10.00, 1604661975, 10.00, 1604662447);
INSERT INTO `cmf_user_voterecord` VALUES (134, 1, 2, 42533, 42545, 0, 1, 10.00, 1604661975, 10.00, 1604663275);
INSERT INTO `cmf_user_voterecord` VALUES (135, 1, 2, 42533, 42545, 0, 1, 10.00, 1604661975, 10.00, 1604664038);
INSERT INTO `cmf_user_voterecord` VALUES (136, 1, 2, 42545, 42545, 0, 1, 10.00, 1604728287, 10.00, 1604728362);
INSERT INTO `cmf_user_voterecord` VALUES (137, 1, 2, 42545, 42545, 0, 1, 10.00, 1604735826, 10.00, 1604735867);
INSERT INTO `cmf_user_voterecord` VALUES (138, 1, 2, 42521, 42545, 0, 1, 10.00, 1604745947, 10.00, 1604746093);
INSERT INTO `cmf_user_voterecord` VALUES (139, 1, 2, 42521, 42545, 0, 1, 10.00, 1604745947, 10.00, 1604746101);
INSERT INTO `cmf_user_voterecord` VALUES (140, 1, 2, 42521, 42545, 0, 1, 10.00, 1604745947, 10.00, 1604746265);
INSERT INTO `cmf_user_voterecord` VALUES (141, 1, 2, 42554, 42545, 0, 1, 10.00, 1604747613, 10.00, 1604747736);
INSERT INTO `cmf_user_voterecord` VALUES (142, 1, 2, 1, 42545, 0, 1, 10.00, 1604828362, 10.00, 1604833024);
INSERT INTO `cmf_user_voterecord` VALUES (143, 1, 2, 42492, 42492, 0, 1, 10.00, 1604838690, 10.00, 1604838865);
INSERT INTO `cmf_user_voterecord` VALUES (144, 1, 2, 42492, 42492, 0, 1, 10.00, 1604839561, 10.00, 1604840576);
INSERT INTO `cmf_user_voterecord` VALUES (145, 1, 2, 42492, 42492, 0, 1, 10.00, 1604839561, 10.00, 1604840587);
INSERT INTO `cmf_user_voterecord` VALUES (146, 1, 2, 42496, 42539, 0, 1, 10.00, 1604841910, 10.00, 1604841957);
INSERT INTO `cmf_user_voterecord` VALUES (147, 1, 2, 42496, 42539, 0, 1, 10.00, 1604842084, 10.00, 1604842131);
INSERT INTO `cmf_user_voterecord` VALUES (148, 1, 2, 42496, 42539, 0, 1, 10.00, 1604842084, 10.00, 1604842137);
INSERT INTO `cmf_user_voterecord` VALUES (149, 1, 2, 42492, 42492, 0, 1, 10.00, 1604839561, 10.00, 1604842469);
INSERT INTO `cmf_user_voterecord` VALUES (150, 1, 2, 42521, 42492, 0, 1, 10.00, 1604901740, 10.00, 1604901939);
INSERT INTO `cmf_user_voterecord` VALUES (151, 1, 2, 42492, 42492, 0, 1, 10.00, 1604902268, 10.00, 1604902356);
INSERT INTO `cmf_user_voterecord` VALUES (152, 1, 2, 42521, 42539, 0, 1, 10.00, 1604901740, 10.00, 1604902604);
INSERT INTO `cmf_user_voterecord` VALUES (153, 1, 2, 42492, 42492, 0, 1, 10.00, 1604902268, 10.00, 1604902941);
INSERT INTO `cmf_user_voterecord` VALUES (154, 1, 2, 42492, 42492, 0, 1, 10.00, 1604902268, 10.00, 1604903237);
INSERT INTO `cmf_user_voterecord` VALUES (155, 1, 2, 42492, 42492, 0, 1, 10.00, 1604902268, 10.00, 1604903315);
INSERT INTO `cmf_user_voterecord` VALUES (156, 1, 2, 42545, 42545, 0, 1, 10.00, 1604904227, 10.00, 1604904984);
INSERT INTO `cmf_user_voterecord` VALUES (157, 1, 2, 42545, 42545, 0, 1, 10.00, 1604905175, 10.00, 1604905340);
INSERT INTO `cmf_user_voterecord` VALUES (158, 1, 2, 42545, 42545, 0, 1, 10.00, 1604905175, 10.00, 1604905531);
INSERT INTO `cmf_user_voterecord` VALUES (159, 1, 2, 42545, 42545, 0, 1, 10.00, 1604905175, 10.00, 1604905695);
INSERT INTO `cmf_user_voterecord` VALUES (160, 1, 2, 42545, 42496, 0, 1, 10.00, 1604905175, 10.00, 1604905935);
INSERT INTO `cmf_user_voterecord` VALUES (161, 1, 2, 42545, 42496, 0, 1, 10.00, 1604905175, 10.00, 1604905939);
INSERT INTO `cmf_user_voterecord` VALUES (162, 1, 2, 42521, 42496, 0, 1, 10.00, 1604906286, 10.00, 1604906394);
INSERT INTO `cmf_user_voterecord` VALUES (163, 1, 2, 42521, 42496, 0, 1, 10.00, 1604906286, 10.00, 1604906414);
INSERT INTO `cmf_user_voterecord` VALUES (164, 1, 2, 42492, 42496, 0, 1, 10.00, 1604902268, 10.00, 1604906738);
INSERT INTO `cmf_user_voterecord` VALUES (165, 1, 2, 42492, 42496, 0, 1, 10.00, 1604902268, 10.00, 1604906742);
INSERT INTO `cmf_user_voterecord` VALUES (166, 1, 2, 42515, 42566, 0, 1, 10.00, 1604906787, 10.00, 1604907589);
INSERT INTO `cmf_user_voterecord` VALUES (167, 1, 2, 42515, 42545, 0, 1, 10.00, 1604906787, 10.00, 1604910455);
INSERT INTO `cmf_user_voterecord` VALUES (168, 1, 2, 42545, 42545, 0, 1, 10.00, 1604910724, 10.00, 1604910884);
INSERT INTO `cmf_user_voterecord` VALUES (169, 1, 2, 42515, 42566, 0, 1, 10.00, 1604906787, 10.00, 1604912085);
INSERT INTO `cmf_user_voterecord` VALUES (170, 1, 2, 42567, 42566, 0, 1, 10.00, 1604924878, 10.00, 1604925098);
INSERT INTO `cmf_user_voterecord` VALUES (171, 1, 2, 42539, 42539, 0, 1, 10.00, 1604924854, 10.00, 1604925430);
INSERT INTO `cmf_user_voterecord` VALUES (172, 1, 2, 42567, 42539, 0, 1, 10.00, 1604924878, 10.00, 1604925803);
INSERT INTO `cmf_user_voterecord` VALUES (173, 1, 2, 42567, 42539, 0, 1, 10.00, 1604924878, 10.00, 1604925909);
INSERT INTO `cmf_user_voterecord` VALUES (174, 1, 2, 42567, 42539, 0, 1, 10.00, 1604924878, 10.00, 1604926004);
INSERT INTO `cmf_user_voterecord` VALUES (175, 1, 2, 42516, 42539, 0, 1, 10.00, 1604926584, 10.00, 1604926604);
INSERT INTO `cmf_user_voterecord` VALUES (176, 1, 2, 42529, 42563, 0, 1, 10.00, 1605003029, 10.00, 1605003167);
INSERT INTO `cmf_user_voterecord` VALUES (177, 1, 2, 42529, 42563, 0, 1, 10.00, 1605003029, 10.00, 1605003178);
INSERT INTO `cmf_user_voterecord` VALUES (178, 1, 2, 42529, 42539, 0, 1, 10.00, 1605003029, 10.00, 1605003183);
INSERT INTO `cmf_user_voterecord` VALUES (179, 1, 2, 42529, 42563, 0, 1, 10.00, 1605003029, 10.00, 1605003187);
INSERT INTO `cmf_user_voterecord` VALUES (180, 1, 2, 42583, 42538, 0, 1, 10.00, 1604999446, 10.00, 1605003714);
INSERT INTO `cmf_user_voterecord` VALUES (181, 1, 2, 42507, 42584, 0, 1, 10.00, 1605082847, 10.00, 1605533353);
INSERT INTO `cmf_user_voterecord` VALUES (182, 1, 2, 42545, 42545, 0, 1, 10.00, 1605770705, 10.00, 1605772533);
INSERT INTO `cmf_user_voterecord` VALUES (183, 1, 2, 42545, 42543, 0, 1, 10.00, 1605770705, 10.00, 1605772617);
INSERT INTO `cmf_user_voterecord` VALUES (184, 1, 2, 42545, 42543, 0, 1, 10.00, 1605770705, 10.00, 1605772637);
INSERT INTO `cmf_user_voterecord` VALUES (185, 1, 2, 42545, 42543, 0, 1, 10.00, 1605770705, 10.00, 1605772651);
INSERT INTO `cmf_user_voterecord` VALUES (186, 1, 2, 42545, 42545, 0, 1, 10.00, 1605770705, 10.00, 1605772759);
INSERT INTO `cmf_user_voterecord` VALUES (187, 1, 2, 42545, 42545, 0, 1, 10.00, 1605770705, 10.00, 1605772771);
INSERT INTO `cmf_user_voterecord` VALUES (188, 1, 2, 42545, 42545, 0, 1, 10.00, 1605772981, 10.00, 1605773063);
INSERT INTO `cmf_user_voterecord` VALUES (189, 1, 2, 42545, 42545, 0, 1, 10.00, 1605773758, 10.00, 1605773885);
INSERT INTO `cmf_user_voterecord` VALUES (190, 1, 2, 42521, 42545, 0, 1, 10.00, 1605790698, 10.00, 1605793738);
INSERT INTO `cmf_user_voterecord` VALUES (191, 1, 2, 42499, 42503, 0, 1, 10.00, 1605841719, 10.00, 1605843678);
INSERT INTO `cmf_user_voterecord` VALUES (192, 1, 2, 42499, 42545, 0, 1, 10.00, 1605841719, 10.00, 1605843904);
INSERT INTO `cmf_user_voterecord` VALUES (193, 1, 2, 42499, 42545, 0, 1, 10.00, 1605843952, 10.00, 1605844241);
INSERT INTO `cmf_user_voterecord` VALUES (194, 1, 2, 42499, 42545, 0, 1, 10.00, 1605844435, 10.00, 1605844616);
INSERT INTO `cmf_user_voterecord` VALUES (195, 1, 2, 42545, 42545, 0, 1, 10.00, 1605844980, 10.00, 1605845087);

-- ----------------------------
-- Table structure for cmf_user_zombie
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_zombie`;
CREATE TABLE `cmf_user_zombie`  (
  `uid` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_verification_code
-- ----------------------------
DROP TABLE IF EXISTS `cmf_verification_code`;
CREATE TABLE `cmf_verification_code`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '表id',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '当天已经发送成功的次数',
  `send_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后发送成功时间',
  `expire_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证码过期时间',
  `code` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后发送成功的验证码',
  `account` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号或者邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '手机邮箱数字验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_video
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video`;
CREATE TABLE `cmf_video`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面图片',
  `thumb_s` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面小图',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '视频地址',
  `href_w` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '水印视频',
  `likes` int(11) NOT NULL DEFAULT 0 COMMENT '点赞数',
  `views` int(11) NOT NULL DEFAULT 1 COMMENT '浏览数（涉及到推荐排序机制，所以默认为1）',
  `comments` int(11) NOT NULL DEFAULT 0 COMMENT '评论数',
  `steps` int(11) NOT NULL DEFAULT 0 COMMENT '踩总数',
  `shares` int(11) NOT NULL DEFAULT 0 COMMENT '分享数量',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '发布时间',
  `lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '维度',
  `lng` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '经度',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '城市',
  `isdel` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1删除（下架）0不下架',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '视频状态 0未审核 1通过 2拒绝',
  `music_id` int(12) NOT NULL DEFAULT 0 COMMENT '背景音乐ID',
  `xiajia_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '下架原因',
  `nopass_time` int(12) NOT NULL DEFAULT 0 COMMENT '审核不通过时间（第一次审核不通过时更改此值，用于判断是否发送极光IM）',
  `watch_ok` int(12) NOT NULL DEFAULT 1 COMMENT '视频完整看完次数(涉及到推荐排序机制，所以默认为1)',
  `is_ad` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为广告视频 0 否 1 是',
  `ad_endtime` int(12) NOT NULL DEFAULT 0 COMMENT '广告显示到期时间',
  `ad_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '广告外链',
  `orderno` int(12) NOT NULL DEFAULT 0 COMMENT '权重值，数字越大越靠前',
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '视频绑定类型 0 未绑定 1 商品  2 付费内容',
  `goodsid` bigint(20) NOT NULL DEFAULT 0 COMMENT '商品ID',
  `classid` int(11) NOT NULL DEFAULT 0 COMMENT '分类ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_video
-- ----------------------------
INSERT INTO `cmf_video` VALUES (1, 42489, '旅游', 'admin/20201101/952eabb364f2fa9412c538378c236776.jpg', '', 'admin/20201101/2f7f893d464e4db47bd0a287ea555056.mp4', 'admin/20201101/d4b5a8d051891670df0d641b99fb61ec.mp4', 1, 26, 1, 0, 0, 1604166671, '', '', '', 0, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 2);
INSERT INTO `cmf_video` VALUES (2, 42492, '测试111', 'admin/20201102/5bd0334689372ebcd35b9cf49b38bd03.png', '', 'admin/20201102/b020fbdcfa678f2ae03ad094d9759406.mp4', 'admin/20201102/a3225e5479a850af115717631e36985d.mp4', 0, 31, 2, 0, 0, 1604310224, '', '', '', 0, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 8);
INSERT INTO `cmf_video` VALUES (3, 42492, '测试112', 'admin/20201102/7205a7b7a17148751d1c902d8ca96f48.png', '', 'admin/20201102/78657a9bba1dc8e699a79c2b28388e07.mp4', 'admin/20201102/db2d611521497b1608fdb0e883eab18d.mp4', 0, 1, 0, 0, 0, 1604310559, '', '', '', 1, 1, 0, '123456', 0, 1, 0, 0, '', 0, 0, 0, 8);
INSERT INTO `cmf_video` VALUES (4, 42519, '', 'android_42519_20201103_163355_7074909_c.jpg', 'android_42519_20201103_163355_7074909_c.jpg?imageView2/2/w/200/h/200', 'android_42519_20201103_163355_7074909.mp4', 'android_42519_20201103_163355_7074909.mp4', 0, 1, 0, 0, 0, 1604392449, '0.0', '0.0', '', 1, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 8);
INSERT INTO `cmf_video` VALUES (5, 42519, '', 'android_42519_20201103_163502_5396579_c.jpg', 'android_42519_20201103_163502_5396579_c.jpg?imageView2/2/w/200/h/200', 'android_42519_20201103_163502_5396579.mp4', 'android_42519_20201103_163502_5396579.mp4', 0, 2, 0, 0, 0, 1604392526, '0.0', '0.0', '', 1, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 8);
INSERT INTO `cmf_video` VALUES (6, 42487, '', 'android_42487_20201104_142134_7813130_c.jpg', 'android_42487_20201104_142134_7813130_c.jpg?imageView2/2/w/200/h/200', 'android_42487_20201104_142134_7813130.mp4', 'android_42487_20201104_142134_7813130.mp4', 0, 13, 0, 0, 0, 1604470916, '0.0', '0.0', '', 0, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 8);
INSERT INTO `cmf_video` VALUES (7, 42516, '', 'android_42516_20201104_203347_4494822_c.jpg', 'android_42516_20201104_203347_4494822_c.jpg?imageView2/2/w/200/h/200', 'android_42516_20201104_203347_4494822.mp4', 'android_42516_20201104_203347_4494822.mp4', 0, 4, 0, 0, 0, 1604493275, '0.0', '0.0', '', 0, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 6);
INSERT INTO `cmf_video` VALUES (8, 42519, '测试', 'admin/20201105/873cd19470348f6c13b42cafb5312122.png', '', 'admin/20201105/cb669fea2243bfaab1864bd4ed6b091d.mp4', 'admin/20201105/c24444fd84d332fda563d7ce1d63c1b6.mp4', 0, 4, 0, 0, 0, 1604542677, '', '', '', 0, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 8);
INSERT INTO `cmf_video` VALUES (9, 42536, '', 'http://7niuobs.jiextx.com//image_42536_IOS_20201105123908.png', 'http://7niuobs.jiextx.com//image_42536_IOS_20201105123908.png?imageView2/2/w/200/h/200', 'http://7niuobs.jiextx.com//video_42536_IOS_20201105123912.mp4', 'http://7niuobs.jiextx.com//video_42536_IOS_20201105123952.mp4', 0, 2, 0, 0, 0, 1604551211, '', '', '', 0, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 8);
INSERT INTO `cmf_video` VALUES (10, 42496, '好看的小哥哥', 'http://7niuobs.jiextx.com//image_42496_IOS_20201105124258.png', 'http://7niuobs.jiextx.com//image_42496_IOS_20201105124258.png?imageView2/2/w/200/h/200', 'http://7niuobs.jiextx.com//video_42496_IOS_20201105124300.mp4', 'http://7niuobs.jiextx.com//video_42496_IOS_20201105124930.mp4', 0, 3, 1, 0, 0, 1604552053, '', '', '好像在火星', 0, 1, 0, '', 0, 1, 0, 0, '', 0, 0, 0, 2);

-- ----------------------------
-- Table structure for cmf_video_black
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_black`;
CREATE TABLE `cmf_video_black`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `videoid` int(10) NOT NULL DEFAULT 0 COMMENT '视频ID',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_video_class
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_class`;
CREATE TABLE `cmf_video_class`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_video_class
-- ----------------------------
INSERT INTO `cmf_video_class` VALUES (1, '美食', 1);
INSERT INTO `cmf_video_class` VALUES (2, '旅行', 2);
INSERT INTO `cmf_video_class` VALUES (3, '摄影', 3);
INSERT INTO `cmf_video_class` VALUES (4, '穿搭', 4);
INSERT INTO `cmf_video_class` VALUES (5, '美妆', 5);
INSERT INTO `cmf_video_class` VALUES (6, '宠物', 6);
INSERT INTO `cmf_video_class` VALUES (7, '搞笑', 7);
INSERT INTO `cmf_video_class` VALUES (8, '测试视频', 0);

-- ----------------------------
-- Table structure for cmf_video_comments
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_comments`;
CREATE TABLE `cmf_video_comments`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '评论用户ID',
  `touid` int(10) NOT NULL DEFAULT 0 COMMENT '被评论的用户ID',
  `videoid` int(10) NOT NULL DEFAULT 0 COMMENT '视频ID',
  `commentid` int(10) NOT NULL DEFAULT 0 COMMENT '所属评论ID',
  `parentid` int(10) NOT NULL DEFAULT 0 COMMENT '上级评论ID',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '评论内容',
  `likes` int(11) NOT NULL DEFAULT 0 COMMENT '点赞数',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '时间',
  `at_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '评论时被@用户的信息（json串）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_video_comments
-- ----------------------------
INSERT INTO `cmf_video_comments` VALUES (1, 42514, 42492, 2, 0, 0, '6666666', 0, 1604322021, '');
INSERT INTO `cmf_video_comments` VALUES (2, 42514, 42492, 2, 0, 0, 'pppp', 0, 1604322045, '');
INSERT INTO `cmf_video_comments` VALUES (3, 42514, 42489, 1, 0, 0, '哈哈哈哈哈', 0, 1604380391, '');
INSERT INTO `cmf_video_comments` VALUES (4, 42496, 42496, 10, 0, 0, '好看', 0, 1604575552, '');

-- ----------------------------
-- Table structure for cmf_video_comments_like
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_comments_like`;
CREATE TABLE `cmf_video_comments_like`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `commentid` int(10) NOT NULL DEFAULT 0 COMMENT '评论ID',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '时间',
  `touid` int(12) NOT NULL DEFAULT 0 COMMENT '被喜欢的评论者id',
  `videoid` int(12) NOT NULL DEFAULT 0 COMMENT '评论所属视频id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_video_like
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_like`;
CREATE TABLE `cmf_video_like`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `videoid` int(10) NOT NULL DEFAULT 0 COMMENT '视频ID',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '视频是否被删除或被拒绝 0被删除或被拒绝 1 正常',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_video_like
-- ----------------------------
INSERT INTO `cmf_video_like` VALUES (2, 42514, 1, 1604380385, 1);

-- ----------------------------
-- Table structure for cmf_video_report
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_report`;
CREATE TABLE `cmf_video_report`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '被举报用户ID',
  `videoid` int(11) NOT NULL DEFAULT 0 COMMENT '视频ID',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0处理中 1已处理  2审核失败',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '提交时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_video_report_classify
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_report_classify`;
CREATE TABLE `cmf_video_report_classify`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `list_order` int(10) NOT NULL DEFAULT 9999 COMMENT '排序',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '举报类型名称',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_video_report_classify
-- ----------------------------
INSERT INTO `cmf_video_report_classify` VALUES (1, 0, '骗取点击', 1544855181);
INSERT INTO `cmf_video_report_classify` VALUES (2, 0, '低俗色情', 1544855189);
INSERT INTO `cmf_video_report_classify` VALUES (3, 0, '侮辱谩骂', 1544855198);
INSERT INTO `cmf_video_report_classify` VALUES (4, 0, '盗用他人作品', 1544855213);
INSERT INTO `cmf_video_report_classify` VALUES (5, 0, '引人不适', 1544855224);
INSERT INTO `cmf_video_report_classify` VALUES (6, 0, '任性打抱不平，就爱举报', 1544855259);
INSERT INTO `cmf_video_report_classify` VALUES (7, 0, '其他', 1544855273);
INSERT INTO `cmf_video_report_classify` VALUES (8, 1, '来举报他们', 1604310650);

-- ----------------------------
-- Table structure for cmf_video_step
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_step`;
CREATE TABLE `cmf_video_step`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `videoid` int(10) NOT NULL DEFAULT 0 COMMENT '视频ID',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_video_view
-- ----------------------------
DROP TABLE IF EXISTS `cmf_video_view`;
CREATE TABLE `cmf_video_view`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `videoid` int(10) NOT NULL DEFAULT 0 COMMENT '视频ID',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_vip
-- ----------------------------
DROP TABLE IF EXISTS `cmf_vip`;
CREATE TABLE `cmf_vip`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '价格',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `length` int(11) NOT NULL DEFAULT 1 COMMENT '时长（月）',
  `score` int(11) NOT NULL DEFAULT 0 COMMENT '积分价格',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_vip
-- ----------------------------
INSERT INTO `cmf_vip` VALUES (1, 100, 'VIP1', 1, 100, 3, 1499925149);
INSERT INTO `cmf_vip` VALUES (2, 300, 'vip3', 3, 300, 2, 1499925155);
INSERT INTO `cmf_vip` VALUES (3, 600, 'vip6', 6, 600, 1, 1499925166);

-- ----------------------------
-- Table structure for cmf_vip_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_vip_user`;
CREATE TABLE `cmf_vip_user`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `addtime` int(10) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `endtime` int(10) NOT NULL DEFAULT 0 COMMENT '到期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_vip_user
-- ----------------------------
INSERT INTO `cmf_vip_user` VALUES (2, 42513, 1604321062, 1606913062);
INSERT INTO `cmf_vip_user` VALUES (3, 42516, 1604382552, 1604419200);
INSERT INTO `cmf_vip_user` VALUES (4, 42520, 1604468121, 1607060121);

-- ----------------------------
-- Function structure for getDistance
-- ----------------------------
DROP FUNCTION IF EXISTS `getDistance`;
delimiter ;;
CREATE DEFINER=`livenew`@`%` FUNCTION `getDistance`(lat1 FLOAT, lon1 FLOAT, lat2 FLOAT, lon2 FLOAT) RETURNS float
    DETERMINISTIC
BEGIN
    RETURN ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN((lat1 * PI() / 180 - lat2 * PI() / 180) / 2), 2)
           + COS(lat1 * PI() / 180) * COS(lat2 * PI() / 180)
           * POW(SIN(( lon1 * PI() / 180 - lon2 * PI() / 180 ) / 2),2))),2);
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
