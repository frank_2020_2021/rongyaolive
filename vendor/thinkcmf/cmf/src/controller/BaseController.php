<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +---------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace cmf\controller;

use think\Container;
use think\Controller;
use think\Db;
use think\facade\View;
use think\facade\Config;

class BaseController extends Controller
{
    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->app     = Container::get('app');
        $this->request = $this->app['request'];
        if (!cmf_is_installed() && $this->request->module() != 'install') {
            return $this->redirect(cmf_get_root() . '/?s=install');
        }

        $this->_initializeView();
        $this->view = View::init(Config::get('template.'));

        // 控制器初始化
        $this->initialize();

        // 前置操作方法 即将废弃
        foreach ((array)$this->beforeActionList as $method => $options) {
            is_numeric($method) ?
                $this->beforeAction($options) :
                $this->beforeAction($method, $options);
        }

    }


    // 初始化视图配置
    protected function _initializeView()
    {
    }

    /**
     *  排序 排序字段为list_orders数组 POST 排序字段为：list_order
     */
    protected function listOrders($model)
    {
        $modelName = '';
        if (is_object($model)) {
            $modelName = $model->getName();
        } else {
            $modelName = $model;
        }

        $pk  = Db::name($modelName)->getPk(); //获取主键名称
        $ids = $this->request->post("list_orders/a");

        if (!empty($ids)) {
            foreach ($ids as $key => $r) {
                $data['list_order'] = $r;
                Db::name($modelName)->where($pk, $key)->update($data);
            }
        }

        return true;
    }
	 //返回数据处理
    public static function successResponse($data = "")
    {
		if ($data === "") $data = ['status'=>'success'];
		self::send_response(['status'=>200,'data'=>$data], true);
    }

    public static function failResponse($code, $data = "", $errorMsg = "")
    {
		if ($errorMsg == "")
		{
			//$errorMsg = ErrorMsg::getMsg($code);//自定义错误码及消息内容
		}
		
		$_data = ['status'=>$code,'errorMsg'=>$errorMsg];
		
		if ($data != "")
		{
			$_data['data'] = $data;
		}
		
		self::send_response($_data);
    }
    
        public static function tipsResponse($msg, $code = 0)
    {
		self::send_response(['status'=>$code,'errorMsg'=>$msg]);
    }
	
	private static function send_response($response, $cache = false)
	{
	    
	   // $userinfo=getcaches("token_".$uid);
	    
		//if(!$userinfo || $userinfo['token']!=$token || $userinfo['expire_time']<time()){

			//$response['token'] = $_SESSION['token'];
			//$response['errorMsg'] .= "(token无效)";		
	//	}
	
		$response['cache'] = false;
		//
		$json = self::safe_json_encode($response);

         $json = json_encode($response);
		if ($cache)
		{
			unset($response['token']);
			$response['cache'] = true;			
		}
		
		$json = str_replace(":null", ":\"\"", $json);
	
		die($json);
	}
	
	private static function safe_json_encode($value, $options = 0, $depth = 512, $utfErrorFlag = false){
	   
		$encoded = json_encode($value, $options, $depth);
		switch (json_last_error())
		{
			case JSON_ERROR_NONE:
				return $encoded;
			case JSON_ERROR_DEPTH:
				self::tipsResponse('Maximum stack depth exceeded');
			case JSON_ERROR_STATE_MISMATCH:
				self::tipsResponse('Underflow or the modes mismatch'); 
			case JSON_ERROR_CTRL_CHAR:
				self::tipsResponse('Unexpected control character found');
			case JSON_ERROR_SYNTAX:
				self::tipsResponse('Syntax error, malformed JSON'); 
			case JSON_ERROR_UTF8:
				$clean = self::utf8ize($value);
				if ($utfErrorFlag) {
					self::tipsResponse('UTF8 encoding error'); 
				}
				return self::safe_json_encode($clean, $options, $depth, true);
			default:
				return 'Unknown error'; 
		}
   }

	public static function utf8ize($mixed)
	{
		if (is_array($mixed)) 
		{
			foreach ($mixed as $key => $value) 
			{
				$mixed[$key] = self::utf8ize($value);
			}
		} 
		else if (is_string ($mixed)) 
		{
			//return utf8_encode($mixed);
             $current_encode = mb_detect_encoding($mixed, array("ASCII","GB2312","GBK",'BIG5','UTF-8')); 
             return mb_convert_encoding($mixed, 'UTF-8', $current_encode);
			//return iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($mixed));
		}
		return $mixed;
	}

}